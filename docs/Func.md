# Func

In `mongo-formula`, each func is corresponding to one or more mongoDB aggregation pipeline operators. To learn more about the mongoDB operators, you can refer to the [MongoDB Aggregation Pipeline Operators Reference](https://docs.mongodb.com/manual/reference/operator/aggregation/).

## Accumulator Funcs

### MongoDB Operators

| funcKey    | mongoDB operator | signature(s)                       | Remarks        |
| ---------- | ---------------- | ---------------------------------- | -------------- |
| AVG        | `$avg`           | `(unknown[]) => number`            |                |
|            |                  | `(unknown) => accumulator(number)` | in GROUP Stage |
| FIRST      | `$first`         | `<T>(T[]) => T`                    |                |
|            |                  | `<T>(T) => accumulator(T)`         | in GROUP Stage |
| LAST       | `$last`          | `<T>(T[]) => T`                    |                |
|            |                  | `<T>(T) => accumulator(T)`         | in GROUP Stage |
| MAX        | `$max`           | `<T>(T[]) => T`                    |                |
|            |                  | `<T>(T) => accumulator(T)`         | in GROUP Stage |
| MIN        | `$min`           | `<T>(T[]) => T`                    |                |
|            |                  | `<T>(T) => accumulator(T)`         | in GROUP Stage |
| STDDEVPOP  | `$stdDevPop`     | `(unknown[]) => number`            |                |
|            |                  | `(unknown) => accumulator(number)` | in GROUP Stage |
| STDDEVSANP | `$stdDevSamp`    | `(unknown[]) => number`            |                |
|            |                  | `(unknown) => accumulator(number)` | in GROUP Stage |
| SUM        | `$sum`           | `(unknown[]) => number`            |                |
|            |                  | `(unknown) => accumulator(number)` | in GROUP Stage |

## Arithmetic Funcs

### MongoDB Operators

| funcKey  | mongoDB operator | signature(s)                    |
| -------- | ---------------- | ------------------------------- |
| ABS      | `$abs`           | `(number) => number`            |
| ADD      | `$add`           | `(number, ...number) => number` |
|          |                  | `(date, ...number) => number`   |
| CEIL     | `$ceil`          | `(number) => number`            |
| DIVIDE   | `$divide`        | `(number, number) => number`    |
| EXP      | `$exp`           | `(number) => number`            |
| FLOOR    | `$floor`         | `(number) => number`            |
| LN       | `$ln`            | `(number) => number`            |
| LOG      | `$log`           | `(number, number) => number`    |
| LOG10    | `$log10`         | `(number) => number`            |
| MOD      | `$mod`           | `(number, number) => number`    |
| MULTIPLY | `multiply`       | `(number, ...number) => number` |
| POW      | `$pow`           | `(number, number) => number`    |
| ROUND    | `$round`         | `(number, number?) => number`   |
| SQRT     | `$sqrt`          | `(number) => number`            |
| SUBTRACT | `$subtract`      | `(number, number) => number`    |
|          |                  | `(date, number) => date`        |
|          |                  | `(date, date) => number`        |
| TRUNC    | `$trunc`         | `(number) => number`            |

### Extra Operators

| funcKey      | description                                 | signature(s)                  |
| ------------ | ------------------------------------------- | ----------------------------- |
| MROUND       | round arg0 to nearest to arg1.              | `(number, number) => number`  |
| MROUNDDOWN   | round arg0 down to nearest to arg1.         | `(number, number) => number`  |
| MROUNDUP     | round arg0 up to nearest to arg1.           | `(number, number) => number`  |
| NEG          | negation of arg.                            | `(number) => number`          |
| QUOTIENT     | quotient of division of arg0 by arg1.       | `(number, number) => number`  |
| ROUNDDOWN    | round arg0 down to arg1 dp.                 | `(number, number?) => number` |
| ROUNDUP      | round arg0 up to arg1 dp.                   | `(number, number?) => number` |
| SAFEDIVIDE   | DIVIDE and return null when arg1 is zero.   | `(number, number) => number`  |
| SAFEQUOTIENT | QUOTIENT and return null when arg1 is zero. | `(number, number) => number`  |
| SIGN         | sign of arg                                 | `(number) => number`          |

## Array Funcs

### MongoDB Operators

| funcKey      | mongoDB operator | signature(s)                                       |
| ------------ | ---------------- | -------------------------------------------------- |
| ARRAYELEMAT  | `$arrayElemAt`   | `<T>(T[], number) => T`                            |
| CONCATARRAYS | `$concatArrays`  | `<T0, ...Tn>(T0[], ...Tn[]) => Union<T0, ...Tn>[]` |
| FILTER       | `$filter`        | `<T>(T[], (T) => boolean) => T[]`                  |
| IN           | `$in`            | `(unknown, unknown[]) => boolean`                  |
| INDEXOFARRAY | `$indexOfArray`  | `(unknown[], unknown, number?, number?) => number` |
| ISARRAY      | `$isArray`       | `(unknown) => boolean`                             |
| MAP          | `$map`           | `<T, U>(T[], (T) => U) => U[]`                     |
| RANGE        | `$range`         | `(number, number, number?) => number[]`            |
| REDUCE       | `$reduce`        | `<T, U>(T[], U, (U, T) => U) => U`                 |
| REVERSEARRAY | `$reverseArray`  | `<T>(T[]) => T[]`                                  |
| SIZE         | `$size`          | `(unknown[]) => number`                            |
| SLICE        | `$slice`         | `<T>(T[], number, number?) => T[]`                 |
| ZIP          | `$zip`           | `<T>(T[], ...T[]) => T[][]`                        |

### Extra Operators

| funcKey   | description                              | signature(s)                       |
| --------- | ---------------------------------------- | ---------------------------------- |
| SAFESLICE | extend SLICE to support negative indices | `<T>(T[], number, number?) => T[]` |

## Boolean Funcs

### MongoDB Operators

| funcKey | mongoDB operator | signature(s)                       |
| ------- | ---------------- | ---------------------------------- |
| AND     | `$and`           | `(unknown, ...unknown) => boolean` |
| NOT     | `$not`           | `(unknown) => boolean`             |
| OR      | `$or`            | `(unknown, ...unknown) => boolean` |

## Comparison Funcs

### MongoBD Operators

| funcKey | mongoDB operator | signature(s)                    |
| ------- | ---------------- | ------------------------------- |
| CMP     | `$cmp`           | `(unknown, unknown) => number`  |
| EQ      | `$eq`            | `(unknown, unknown) => boolean` |
| GT      | `$gt`            | `(unknown, unknown) => boolean` |
| GTE     | `$gte`           | `(unknown, unknown) => boolean` |
| LT      | `$lt`            | `(unknown, unknown) => boolean` |
| LTE     | `$lte`           | `(unknown, unknown) => boolean` |
| NE      | `$ne`            | `(unknown, unknown) => boolean` |

## Conditional Funcs

### MongoDB Operators

| funcKey | mongoDB operator | signature(s)                           |
| ------- | ---------------- | -------------------------------------- |
| COND    | `$cond`          | `<T, U>(unknown, T, U) => Union<T, U>` |
| IFNULL  | `$ifNull`        | `<T, U>(T, U) => Union<T, U>`          |

## Date Funcs

### MongoDB Operators

| funcKey          | mongoDB operator  | signature(s)                         |
| ---------------- | ----------------- | ------------------------------------ |
| DATEFROMISOPARTS | `$dateFromParts`  | `(isoDateParts, string?) => date`    |
| DATEFROMPARTS    | `$dateFromParts`  | `(dateParts, string?) => date`       |
| DATEFROMSTRING   | `$dateFromString` | `(string, string?, string?) => date` |
| DATETOISOPARTS   | `$dateToParts`    | `(date, string?) => isoDateParts`    |
| DATETOPARTS      | `$dateToParts`    | `(date, string?) => dateParts`       |
| DATETOSTRING     | `$dateToString`   | `(date, string?, string?) => string` |
| DAYOFMONTH       | `$dayOfMonth`     | `(date, string?) => number`          |
| DAYOFWEEK        | `$dayOfWeek`      | `(date, string?) => number`          |
| DAYOFYEAR        | `$dayOfYear`      | `(date, string?) => number`          |
| HOUR             | `$hour`           | `(date, string?) => number`          |
| ISODAYOFWEEK     | `$isoDayOfweek`   | `(date, string?) => number`          |
| ISOWEEK          | `$isoWeek`        | `(date, string?) => number`          |
| ISOWEEKYEAR      | `$isoWeekYear`    | `(date, string?) => number`          |
| MILLISECOND      | `$millisecond`    | `(date, string?) => number`          |
| MINUTE           | `$minute`         | `(date, string?) => number`          |
| MONTH            | `$month`          | `(date, string?) => number`          |
| SECOND           | `$second`         | `(date, string?) => number`          |
| WEEK             | `$week`           | `(date, string?) => number`          |
| YEAR             | `$year`           | `(date, string?) => number`          |

### Extra Operators

| funcKey          | description                                           | signature(s)                |
| ---------------- | ----------------------------------------------------- | --------------------------- |
| MILLISECONDOFDAY | the time escaped in millisecond from the start of day | `(date, string?) => number` |

## Literal Funcs

### MongoDB Operators

| funcKey | mongoDB operator | signature(s)                |
| ------- | ---------------- | --------------------------- |
| LITERAL | `$literal`       | `(unknown) => InferFromVal` |

## Object Funcs

### MongoDB Operators

| funcKey      | mongoDB operator | signature(s)                                                       |
| ------------ | ---------------- | ------------------------------------------------------------------ |
| MERGEOBJECTS | `$mergeObjects`  | `<T0 extends {}, ...Tn extends {}>(T0, ...Tn) => Merge<T0, ...Tn>` |

## Overload Funcs

### Extra Operators

| funcKey    | description  | signature(s)                          |
| ---------- | ------------ | ------------------------------------- |
| SMARTADD   | ADD          | `(number, number) => number`          |
|            | ADD          | `(date, number) => date`              |
|            | CONCAT       | `(string, string) => string`          |
| SMARTINDEX | STRELEMATCP  | `(string, number) => string`          |
|            | ARRAYELEMAT  | `<T>(T[], number) => T`               |
| SMARTLEN   | ABS          | `(number) => number`                  |
|            | STRLENCP     | `(string) => number`                  |
|            | SIZE         | `(unknown[]) => number`               |
| SMARTSLICE | SAFESUBSTRCP | `(string, number, number?) => string` |
|            | SAFESLICE    | `<T>(T[], number, number?) => T[]`    |

## Set Funcs

### MongoDB Operators

| funcKey         | mongoDB operator   | signature(s)                                           |
| --------------- | ------------------ | ------------------------------------------------------ |
| ALLELEMENTSTRUE | `$allElementsTrue` | `(unknown[]) => boolean`                               |
| ANYELEMENTTRUE  | `$anyElementTrue`  | `(unknown[]) => boolean`                               |
| SETDIFFERENCE   | `$setDifference`   | `<T>(T[], unknown[]) => T[]`                           |
| SETEQUALS       | `$setEquals`       | `(unknown[], unknown[]) => boolean`                    |
| SETINTERSECTION | `$setIntersection` | `<T0, ...Tn>(T0[], ...Tn[]) => Intersect<T0, ...Tn>[]` |
| SETISSUBSET     | `$setIsSubset`     | `(unknown[], unknown[]) => boolean`                    |
| SETUNION        | `$setUnion`        | `<T0, ...Tn>(T0[], ...Tn[]) => Union<T0, ...Tn>[]`     |

### Extra Operators

| funcKey       | description                | signature(s)                        |
| ------------- | -------------------------- | ----------------------------------- |
| SETISSUPERSET | SETISSUBSET(arg1, arg0)    | `(unknown[], unknown[]) => boolean` |
| SETNOTEQUALS  | NOT(SETEQUALS(arg0, arg1)) | `(unknown[], unknown[]) => boolean` |

## String Funcs

### MongoDB Operators

| funcKey      | mongoDB operator | signature(s)                                   |
| ------------ | ---------------- | ---------------------------------------------- |
| CONCAT       | `$concat`        | `(string, ...string) => string`                |
| INDEXOFBYTES | `$indexOfBytes`  | `(string, string, number?, number?) => number` |
| INDEXOFCP    | `$indexOfCP`     | `(string, string, number?, number?) => number` |
| LTRIM        | `$ltrim`         | `(string, string?) => string`                  |
| RTRIM        | `$rtrim`         | `(string, string?) => string`                  |
| SPLIT        | `$split`         | `(string, string) => string[]`                 |
| STRCASECMP   | `$strcasecmp`    | `(string, string) => number`                   |
| STRLENBYTES  | `$strLenBytes`   | `(string) => number`                           |
| STRLENCP     | `$strLenCP`      | `(string) => number`                           |
| SUBSTRBYTES  | `$substrBytes`   | `(string, number, number) => string`           |
| SUBSTRCP     | `$substrCP`      | `(string, number, number) => string`           |
| TOLOWER      | `$toLower`       | `(string) => string`                           |
| TOUPPER      | `$toUpper`       | `(string) => string`                           |
| TRIM         | `$trim`          | `(string, string?) => string`                  |

### Extra Operators

| funcKey         | description                                                 | signature(s)                          |
| --------------- | ----------------------------------------------------------- | ------------------------------------- |
| SAFESUBSTRBYTES | extend SUBSTRBYTES to support optional and negative indices | `(string, number, number?) => string` |
| SAFESUBSTRCP    | extend SUBSTRCP to support optional and negative indices    | `(string, number, number?) => string` |
| STRELEMATBYTES  | use SUBSTRBYTES to slice one character                      | `(string, number) => string`          |
| STRELEMATCP     | use SUBSTRCP to slice one character                         | `(string, number) => string`          |

## Type Funcs

### MongoDB Operators

| funcKey   | mongoDB operator | signature(s)                        |
| --------- | ---------------- | ----------------------------------- |
| CONVERT   | `$convert`       | `(unknown, string) => InferFromVal` |
| TOBOOL    | `$toBool`        | `(unknown) => boolean`              |
| TODATE    | `$toDate`        | `(unknown) => date`                 |
| TODECIMAL | `$toDecimal`     | `(unknown) => number`               |
| TODOUBLE  | `$toDouble`      | `(unknown) => number`               |
| TOINT     | `$toInt`         | `(unknown) => number`               |
| TOLONG    | `$toLong`        | `(unknown) => number`               |
| TOSTRING  | `$toString`      | `(unknown) => string`               |
| TYPE      | `$type`          | `(unknown) => string`               |
