# Stage

In `mongo-formula`, each stage is corresponding to one or more mongoDB aggregation stage. To learn more about the mongoDB stages, you can refer to the [MongoDB Aggregation Pipeline Stages Reference](https://docs.mongodb.com/manual/reference/operator/aggregation-pipeline/).

## MongoDB Stages

| stageKey    | mongoDB stage  | signature(s)                                              |
| ----------- | -------------- | --------------------------------------------------------- |
| GROUP       | `$group`       | `({ _id: unknown, + accumulator(unknown) }L) => pipeline` |
| LIMIT       | `$limit`       | `(numberL) => pipeline`                                   |
| MATCH       | `$match`       | `(boolean) => pipeline`                                   |
| REPLACEROOT | `$replaceRoot` | `({}) => pipeline`                                        |
| SAMPLE      | `$sample`      | `(numberL) => pipeline`                                   |
| SET         | `$set`         | `({}L) => pipeline`                                       |
| SKIP        | `$skip`        | `(numberL) => pipeline`                                   |
| SORT        | `$sort`        | `({ + numberL }L) => pipeline`                            |
| UNSET       | `$unset`       | `(stringL, ...stringL) => pipeline`                       |
| UNWIND      | `$unwind`      | `(stringL) => pipeline`                                   |
