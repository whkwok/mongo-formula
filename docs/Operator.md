# Operator

In `mongo-formula`, each operator is corresponding to a func. To learn more about the funcs, you can refer to the [Func Reference](./Func.md).

For precedence, greater means preceder.

## Unary Operator

| symbol     | funcKey  | precedence | precedence type |
| ---------- | -------- | ---------- | --------------- |
| `-`        | NEG      | 7          | left            |
| `!`        | NOT      | 13         | left            |
| `\|expr\|` | SMARTLEN |            |                 |

## Binary Operator

| symbol   | funcKey         | precedence | precedence type |
| -------- | --------------- | ---------- | --------------- |
| `\|\|`   | OR              | 1          | left            |
| `&&`     | AND             | 2          | left            |
| `==`     | EQ              | 3          | left            |
| `!=`     | NE              | 3          | left            |
| `>`      | GT              | 4          | left            |
| `>=`     | GTE             | 4          | left            |
| `<`      | LT              | 4          | left            |
| `<=`     | LTE             | 4          | left            |
| `in`     | IN              | 4          | left            |
| `+`      | SMARTADD        | 5          | left            |
| `-`      | SUBSTRACT       | 5          | left            |
| `*`      | MULTIPLY        | 6          | left            |
| `/`      | DIVIDE          | 6          | left            |
| `%`      | MOD             | 6          | left            |
| `**`     | POW             | 8          | right           |
| `{==}`   | SETEQUALS       | 9          | nonassoc        |
| `{!=}`   | SETNOTEQUALS    | 9          | nonassoc        |
| `{>=}`   | SETISSUPERSET   | 9          | nonassoc        |
| `{<=}`   | SETISSUBSET     | 9          | nonassoc        |
| `{\|\|}` | SETUNION        | 10         | nonassoc        |
| `{&&}`   | SETINTERSECTION | 11         | nonassoc        |
| `{&!}`   | SETDIFFERENCE   | 11         | nonassoc        |
| `??`     | IFNULL          | 12         | left            |

### Ternary Operator

| symbol               | funcKey | precedence | type  |
| -------------------- | ------- | ---------- | ----- |
| `expr ? expr : expr` | COND    | 0          | right |
