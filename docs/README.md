# `mongo-formula` Documentation

# Syntax

This section of documentation explains syntax of `mongo-formula`.

## Token

Tokenization is the first step of parsing. Whitespaces between tokens are ignored. If multiple rules are matched, the longest (includes trailing pattern) token rule is chosen. The details can be found in [here](../src/lexer/mongoFormulaLexer.ts).

### NUM

`NUM` is the token for numeric text. Decimal fractions, exponential notation and percentage are supported. The pattern used is `/[0-9]+(?:\.[0-9]+)?(?:e[-+]?[0-9]+)?%?/`.

### BOOL

`BOOL` is the token for boolean text. Boolean text is case sensitive. The pattern used is `/true|false`.

### NULL

`NULL` is the token for null text. Null text is case sensitive. The pattern used is `/null/`.

### STR

`STR_S`, `STR_C` and `STR_E` are tokens for string text. `STR_S` and `STR_E` indicate the start and the end of a string. They can be either `'` or `"`. `STR_C` is the string content which can be any text. To include `'` in single quotes or `"` in double quotes, you can use escape charater `\`.

### KEY

`KEY` is the token for word text. Word text should be not quoted by `'` or `"`. The pattern used is `/w+/`.

### SYMBOL

`SYMBOL` is the class of tokens that has no semantic value. They are used for syntax rules only. Their token is the same as matching text (except `CB_S`). The pattern used is `/{(?:&[&!]|\|\||[!<>=]=)}|\.\.\.|let|[!<>=]=|\*\*|\?\?|&&|\|\||=>|>>|as|in|[+\-*/(){}[\].,%;:!?|<>=]/`.

`CB_S` is a special `SYMBOL` token that is used in callback syntax. It indicates the start of a callback. The pattern used is `/\(/` and the trailing pattern used is `/[^()]*\)\s*=>/`.

### EOF

`EOF` is the end of file token. It appears at the end of input text.

## Typ

`Typ` is the object description of type of output. Typs include `AnyTyp`, `NeverTyp`, `UnknownTyp`, `BooleanTyp`, `DateTyp`, `NumberTyp`, `StringTyp`, `ArrayTyp`, `ObjectTyp`, `AccumulatorTyp`, `CallbackTyp`, `PipelineTyp`. `TypStr` is the string representation of `Typ`.

Many concepts of `Typ`, such as type composition and type assignability, are borrowed from Typescript. A concept, literalness, is specific to mongo-formula. If a `typ` is `literal`, the corresponding output does not involve any mongodb operators or stages.

```ts
interface AnyTyp {
	readonly type: 'any';
	readonly literal?: undefined;
}

interface NeverTyp {
	readonly type: 'never';
	readonly literal?: undefined;
}

interface UnknownTyp {
	readonly type: 'unknown';
	readonly literal?: undefined;
}

type BooleanTyp = NonLiteralBooleanTyp | LiteralBooleanTyp;

interface NonLiteralBooleanTyp {
	readonly type: 'boolean';
	readonly literal?: undefined;
}

interface LiteralBooleanTyp {
	readonly type: 'boolean';
	readonly literal: true;
}

type DateTyp = NonLiteralDateTyp | LiteralDateTyp;

interface NonLiteralDateTyp {
	readonly type: 'date';
	readonly literal?: undefined;
}

interface LiteralDateTyp {
	readonly type: 'date';
	readonly literal: true;
}

type NumberTyp = NonLiteralNumberTyp | LiteralNumberTyp;

interface NonLiteralNumberTyp {
	readonly type: 'number';
	readonly literal?: undefined;
}

interface LiteralNumberTyp {
	readonly type: 'number';
	readonly literal: true;
}

type StringTyp = NonLiteralStringTyp | LiteralStringTyp;

interface NonLiteralStringTyp {
	readonly type: 'string';
	readonly literal?: undefined;
}

interface LiteralStringTyp {
	readonly type: 'string';
	readonly literal: true;
}

type ArrayTyp = NonLiteralArrayTyp | LiteralArrayTyp;

interface NonLiteralArrayTyp {
	readonly type: 'array';
	readonly items: NonLiteralTyp;
	readonly literal?: undefined;
}

interface LiteralArrayTyp {
	readonly type: 'array';
	readonly items: Typ[];
	readonly literal: true;
}

type ObjectTyp = NonLiteralObjectTyp | LiteralObjectTyp;

interface NonLiteralObjectTyp {
	readonly type: 'object';
	readonly properties: Dictionary<NonLiteralTyp>;
	readonly additionalProperties?: NonLiteralTyp;
	readonly optional?: string[];
	readonly literal?: undefined;
}

interface LiteralObjectTyp {
	readonly type: 'object';
	readonly properties: Dictionary<Typ>;
	readonly additionalProperties?: Typ;
	readonly optional?: string[];
	readonly literal: true;
}

interface AccumulatorTyp {
	readonly type: 'accumulator';
	readonly in: NonLiteralTyp;
	readonly literal?: undefined;
}

interface CallbackTyp {
	readonly type: 'callback';
	readonly args: Typ[];
	readonly in: Typ;
	readonly literal?: undefined;
}

interface PipelineTyp {
	readonly type: 'pipeline';
	readonly literal?: undefined;
}

type Typ =
	| AnyTyp
	| NeverTyp
	| UnknownTyp
	| BooleanTyp
	| DateTyp
	| NumberTyp
	| StringTyp
	| ArrayTyp
	| ObjectTyp
	| AccumulatorTyp
	| CallbackTyp
	| PipelineTyp;
```

## Val

`Val` of output is either an aggregation expression or an aggreation pipeline.

## Dep

`Dep` of output stores variables involved in the formula.

```ts
type LiteralObjectDep = Dictionary<Dep>; // { [key: string]: Dep }

type LiteralArrayDep = Dep[];

type Dep = string[] | LiteralArrayDep | LiteralObjectDep;
```

## Grammar

The grammar is written is [jison](https://zaa.ch/jison/). The details can be found in [here](../src/parser/Parser.jison). In this section of documentation, various syntax rules are elaborated by providing examples.

### Schema

The following schema is used in the examples of this section.

| variable key | variable typStr             |
| ------------ | --------------------------- |
| `n`          | `number`                    |
| `b`          | `boolean`                   |
| `s`          | `string`                    |
| `a`          | `number[]`                  |
| `o`          | `{ p: string, q: boolean }` |

### Include Number, Boolean and Null

To include number, boolean and null in a formula, you can simply use the corresponding texts. Note that the typ of number and the typ of boolean are literal. Typ of null is `NeverTyp` as it is assignable to all typ.

| input   | output typStr | output val | output dep |
| ------- | ------------- | ---------- | ---------- |
| `1.2`   | `numberL`     | `1.2`      | `[]`       |
| `true`  | `booleanL`    | `true`     | `[]`       |
| `false` | `booleanL`    | `false`    | `[]`       |
| `null`  | `never`       | `null`     | `[]`       |

### Construct String

To contruct string, you can use `''` or `""`. To include single (double) quote in single (double) qutoes, escape character `\` is required.

| input     | output typStr | output val | output dep |
| --------- | ------------- | ---------- | ---------- |
| `'abc'`   | `stringL`     | `'abc'`    | `[]`       |
| `"abc"`   | `stringL`     | `'abc'`    | `[]`       |
| `"abc\""` | `stringL`     | `'abc"'`   | `[]`       |
| `'abc"'`  | `stringL`     | `'abc"'`   | `[]`       |

### Refer Variable

To refer a variable in schema, you can use the corresponding key text. VarKeys are case sensitive. One can refer variables undefined in schema. In such case, the typ of variables will be `undefinedVarTyp` in `ParseOptions`. The default `undefinedVarTyp` is `UnknownTyp`.

| input | output typStr       | output val | output dep |
| ----- | ------------------- | ---------- | ---------- |
| `n`   | `number`            | `$n`       | `['n']`    |
| `b`   | `boolean`           | `$b`       | `['b']`    |
| `s`   | `string`            | `$s`       | `['s']`    |
| `u`   | `unknown` (default) | `$u`       | `['u']`    |

### Construct Array

To contruct an array, you can use `[]`. Spread operator is supported.

| input             | output typStr                | output val                             | output dep        |
| ----------------- | ---------------------------- | -------------------------------------- | ----------------- |
| `[]`              | `[]`                         | `[]`                                   | `[]`              |
| `[n, 1, 2]`       | `[number, numberL, numberL]` | `['$n', 1, 2]`                         | `[['n'], [], []]` |
| `[1, 2, 3, ...a]` | `number[]`                   | `{ $concatArrays: [[1, 2, 3], '$a'] }` | `['a']`           |

### Construct Object

To contruct an object, you can use `{}`. Spread operator and shorthand property name are supported.

| input            | output typStr                          | output val                            | output dep     |
| ---------------- | -------------------------------------- | ------------------------------------- | -------------- |
| `{}`             | `{}L`                                  | `{}`                                  | `{}`           |
| `{ n: n }`       | `{ n: number }L`                       | `{ n: '$n' }`                         | `{ n: ['n'] }` |
| `{ n }`          | `{ n: number }L`                       | `{ n: '$n' }`                         | `{ n: ['n'] }` |
| `{ a: 1, ...o }` | `{ a: number, p: string, q: boolean }` | `{ $mergeObjects: [{ a: 1 }, '$o'] }` | `['o']`        |

### Access Array or String Element

To access array or string element, you can use `expr[indexExpr]`. Error will be thrown when typ of expr is not assignable to `ArrayTyp`/`StringTyp` or typ of indexExpr is not assignable to `NumberTyp`. Accessing element from `AnyTyp` expr will give `AnyTyp` output.

| input         | output typStr | output val                       | output dep   |
| ------------- | ------------- | -------------------------------- | ------------ |
| `a[1]`        | `number`      | `{ $arrayElemAt: ['$a', 1] }`    | `['a']`      |
| `a[n]`        | `string`      | `{ $arrayElemAt: ['$a', '$n'] }` | `['a', 'n']` |
| `[1,2,3][-1]` | `numberL`     | `3`                              | `[]`         |
| `'apple'[0]`  | `stringL`     | `a`                              | `[]`         |

### Access Object Property

To access object property, you can use `expr.KEY` or `expr[STR]`. Error will be thrown when property key does not exist or typ of expr is not assignable to `ObjectTyp`. Accessing property from `AnyTyp` expr will give `AnyTyp` output.

| input              | output typStr | output val       | output dep |
| ------------------ | ------------- | ---------------- | ---------- |
| `o.p`              | `string`      | `$o.p`           | `['o']`    |
| `o['p']`           | `string`      | `$o.p`           | `['o']`    |
| `{ a: 1, b: 2 }.a` | `numberL`     | `1`              | `[]`       |
| `{ a: 1, ...o }.a` | `number`      | See `val1` below | `['o']`    |

```ts
// output val of { a: 1, ...o }.a
// ___ is the default reservedSuffix
const val1 = {
	$let: {
		vars: {
			o___: { $mergeObjects: [{ a: 1 }, '$o'] },
		},
		in: '$$o___.a',
	},
};
```

### Slice Array or String

To slice array or string element, you can use `expr[startIndexExpr:endIndexExpr]`. Error will be thrown when typ of expr is not assignable to `ArrayTyp`/`StringTyp` or typ of startIndexExpr/endIndexExpr is not assignable to `NumberTyp`. Slicing `AnyTyp`/`NeverTyp` expr is also erroneous. Either startIndexExpr or endIndexExpr can be ommited. If startIndexExpr is ommited, it means slice from the start. If endIndexExpr is ommited, it means slice till the end.

| input         | output typStr        | output val | output dep |
| ------------- | -------------------- | ---------- | ---------- |
| `[1,2,3][:2]` | `[numberL, numberL]` | `[1, 2]`   | `[[], []]` |
| `'apple'[1:]` | `stringL`            | `pple`     | `[]`       |

### Unary Operators

List of unary operators supported can be found [here](Operator.md#unary-operator).

| input   | output typStr | output val                  | output dep |
| ------- | ------------- | --------------------------- | ---------- |
| `-n`    | `number`      | `{ $multiply: [-1, '$n'] }` | `['n']`    |
| `!b`    | `boolean`     | `{ $not: '$b' }`            | `['b']`    |
| `\|s\|` | `number`      | `{ $strLenCP: '$s' }`       | `['s']`    |

### Binary Operators

List of binary operators supported can be found [here](Operator.md#binary-operator).

| input         | output typStr | output val                                     | output dep |
| ------------- | ------------- | ---------------------------------------------- | ---------- |
| `n + 1`       | `number`      | `{ $add: ['$n', 1] }`                          | `['n']`    |
| `n - 2 * 3`   | `boolean`     | `{ $subtract: ['$n', { $multiply: [2, 3] }] }` | `['n']`    |
| `(n - 2) * 3` | `number`      | `{ $multiply: [{ $subtract: ['$n', 2] }, 3] }` | `['n']`    |

### Ternary Operators

List of ternary operators supported can be found [here](Operator.md#ternary-operator).

| input       | output typStr | output val                      | output dep        |
| ----------- | ------------- | ------------------------------- | ----------------- |
| `b ? n : s` | `unknown`     | `{ $cond: ['$b', '$n', '$s'] }` | `['b', 'n', 's']` |

### Let Block

To contruct let block, you can use `{let assign ; expr}`. Note that variable definition is overridable in let blocks.

| input                                 | output typStr | output val       | output dep |
| ------------------------------------- | ------------- | ---------------- | ---------- |
| `{ let a = n + 2, b = n - 2; a * b }` | `number`      | See `val1` below | ['n']      |
| `{ let n = n + 2, n ** 2 }`           | `number`      | See `val2` below | ['n']      |

```ts
// output val of { let a = n + 2, b = n - 2; a * b }
const val1 = {
	$let: {
		vars: { a: { $add: ['$n', 2] }, b: { $subtract: ['$n', 2] } },
		in: { $multiply: ['$$a', '$$b'] },
	},
};
// output val of { let n = n + 2, n ** 2 }
const val2 = {
	$let: { vars: { n: { $add: ['$n', 2] } }, in: { $pow: ['$$n', 2] } },
};
```

### Cast Typ

To cast typ, you can use `expr as typExpr`. Since typ cast has no constraint, unexpected error may occur in parsing. This should be used carefully.

| input                        | output typStr | output val | output dep |
| ---------------------------- | ------------- | ---------- | ---------- |
| `n as { type: 'any' }`       | `any`         | `$n`       | `['n']`    |
| `u as { type: 'string' }`    | `string`      | `$u`       | `['u']`    |
| `null as { type: 'number' }` | `number`      | `null`     | `[]`       |

### Call Func

To call func, you can use `FUNCKEY()`. FuncKeys are case insensitive. List of funcs supported can be found [here](Func.md).

| input       | output typStr | output val              | output dep        |
| ----------- | ------------- | ----------------------- | ----------------- |
| `add(1, n)` | `number`      | `{ $add: [1, '$n'] } }` | `['b', 'n', 's']` |
| `ADD(1, n)` | `number`      | `{ $add: [1, '$n'] } }` | `['b', 'n', 's']` |

### Callback

Callback is a special func arg. It can only appear inside a Func Call.

| input                           | output typStr | output val     | output dep   |
| ------------------------------- | ------------- | -------------- | ------------ |
| `MAP(a, (x) => x + n)`          | `number`      | See val1 below | `['a', 'n']` |
| `FILTER(a, (y) => y > n)`       | `number`      | See val2 below | `['a', 'n']` |
| `REDUCE(a, n, (p, c) => p + c)` | `number`      | See val3 below | `['a', 'n']` |

```ts
// output val of MAP(a, (x) => x + n)
const val1 = { $map: { input: '$a', as: 'x', in: { $add: ['$$x', '$n'] } } };
// // output val of FILTER(a, (y) => y > n)
const val2 = {
	$filter: { input: '$a', as: 'y', cond: { $gt: ['$$y', '$n'] } },
};
// // output val of REDUCE(a, n, (p, c) => p + c)
const val3 = {
	$reduce: {
		input: '$a',
		initialValue: '$n',
		in: {
			$let: {
				vars: { p: '$$value', c: '$$this' },
				in: { $add: ['$$p', '$$c'] },
			},
		},
	},
};
```

### Call Stage

To call stage, you can use `>> STAGEKEY()`. StageKeys are case insensitive. Stage will also update the internal schema. This is useful for Pipleline Chain. List of stages supported can be found [here](Stage.md).

| input                | output typStr | output val                     | output dep |
| -------------------- | ------------- | ------------------------------ | ---------- |
| `>> SET({ one: 1 })` | `pipeline`    | `[{ $addFields: { one: 1 } }]` | `[]`       |
| `>> UNSET('n')`      | `pipeline`    | `[{ $project: { n: 0 } }]`     | `[]`       |

### Chain Pipeline

To chain pipelines, you can simply put multiple pipelines together.

| input                                       | output typ | output val       | output dep |
| ------------------------------------------- | ---------- | ---------------- | ---------- |
| `>> SET({ m: n + 1 }) >> GROUP({ _id: m })` | `pipeline` | See `val1` below | `['n']`    |
| `>> SET({ m: n + 1 }) >> MATCH(m > 0)`      | `pipeline` | See `val2` below | `['n']`    |

```ts
// output val of >> SET({ m: n + 1 }) >> GROUP({ _id: m })
const val1 = [
	{ $addFields: { m: { $add: ['$n', 1] } } },
	{ $group: { _id: '$m' } },
];

// output val of >> SET({ m: n + 1 }) >> MATCH(m > 0)
const val2 = [
	{ $addFields: { m: { $add: ['$n', 1] } } },
	{ $match: { expr: { $gt: ['$m', 0] } } },
];
```

# Reference

-   [Operator](Operator.md)
-   [Func](Func.md)
-   [Stage](Stage.md)
