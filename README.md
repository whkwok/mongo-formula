# Mongo Formula

Parser for MongoDB aggregation expression and pipeline written in a formula-like style.

## Overview

This project aims to simplify writing aggregation expression and pipeline.

### Type Error

One problem of using aggregation pipeline is that type error can only be discovered in runtime. For example, expression `{ $add: [$str, 2] }` is invalid if `$str` is a string.

### Readability

Another problem is that complex expression is less readable. For example, expression `{ $subtract: [{ $add: [1, 2] }, { $multiply: [3, 4, 5] }] }` could be compactly represented by `1 + 2 - 3 * 4 * 5` in common calculator syntax.

### Solution

To address above issues, a syntax is defined and the corresponding parser is builded.

## Getting Started

### Npm

```
npm install --save mongo-formula
```

### Yarn

```
yarn add mongo-formula
```

## Usage

### Setup

```ts
import { MongoFormulaParser, allFuncDict, allStageDict } from 'mongo-formula';
/* 
FuncDict/StageDict is the definition of functions/stages.
allFuncDict/allStageDict includes all the functions/stages defined in this library.
You may select only the required functions/stages to form a custom FuncDict/StageDict in order to reduce memory usage and bundle size.
*/
const schema = { num: { type: 'number' } }; // Schema is a type definition of the variables (document fields)
const options = {
	reservedSuffix: '___', // suffix reserved for variables used by this library. Default: ___
	undefinedTyp: { type: 'unknown' }, // typ of variables that are not defined in schema. Default: { type: 'unknown' }
	compatMode: true, // If true, some funcs/stages replaced by other more compatible funcs/stages. Default: true
};
const parser = new MongoFormulaParser(
	schema,
	allFuncDict,
	allStageDict,
	options
);
```

### Parse Valid Formula to Expression

```ts
parser.parse(`num + 1`);
/*
output: (other fields are omitted for simplicity)
{
	typ: { type: 'number' },
	val: { $add: ['$num', 1] },
	dep: ['num'],
};
*/
```

### Parse Valid Formula to Pipeline

```ts
parser.parse(`>> SET({ numPlusOne: num + 1 })`);
/*
output: (other fields are omitted for simplicity)
{
	typ: { type: 'pipeline' },
	val: [{ $addFields: { numPlusOne: { $add: ['$num', 1] } } }],
	dep: ['num'],
}
*/
```

### Parse Invalid Formula

```ts
parser.parse(`num + "1"`);
/*
throw:
SMARTADD: Invalid expr1 typ. Received "stringL". Expected "number".

note:
It is invalid because "1" is string literal (stringL) which is not assignable to number.
SMARTADD is the underlying function called when using +.
*/
```

### Parse Formula Template to Expression Creator

```ts
const createExpr = parser.parseTemplate([{ type: 'number' }])`${(
	...vs: any[]
) => vs[0]} + 1`;

createExpr(500);
/*
output: (other fields are omitted for simplicity)
{
	typ: { type: 'number' },
	val: { $add: [{ $ifNull: [null, 500] }, 1] },
	dep: [],
};

note:
{ $ifNull: [null, 500] } is identical to 500.
*/
```

## Cli

This library provides a simple cli to use the mongoFormulaParser.

To start the cli, you can simply run `mongo-formula` in your project root. FuncDict used is `allFuncDict`. StageDict used is `allStageDict`. Options used are the defaults. To set schema, you can use the command `setSchema`.

```
> setSchema({ num: { type: 'number' } })
setSchema:
{ num: { type: 'number' } }
> num + 1
{
  typ: { type: 'number' },
  val: { '$add': [ '$num', 1 ] },
  dep: [ 'num' ]
}
```

## Documentation

Full documentation can be found [here](https://gitlab.com/whkwok/mongo-formula/-/tree/master/docs).
