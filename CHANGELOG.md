# Change Log

## 1.0.2

-   Fix ESM build. (avoid import 'default' from './parse' in Parser.js)

## 1.0.1

-   Fix TypeScript declaration directiory (dist/types/src -> dist/types).

## 1.0.0

-   Initial Release.

## 0.0.\*

-   Beta release.
