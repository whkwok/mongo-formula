module.exports = {
	presets: [
		[
			'@babel/env',
			{
				targets: {
					browsers: '> 0.5%, last 2 versions, Firefox ESR, not dead',
				},
				useBuiltIns: 'usage',
				modules: 'cjs',
				corejs: { version: '3', proposal: true },
			},
		],
		['@babel/typescript'],
	],
	plugins: ['@babel/plugin-proposal-class-properties'],
};
