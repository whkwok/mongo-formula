module.exports = {
	presets: [
		[
			'@babel/env',
			{
				targets: { esmodules: true },
				useBuiltIns: 'usage',
				modules: false,
				corejs: { version: '3', proposal: true },
			},
		],
		['@babel/typescript'],
	],
	plugins: [
		'@babel/plugin-proposal-class-properties',
		'babel-plugin-transform-commonjs', // For converting jison output to esm
	],
};
