import { expect } from 'chai';
import createDebug from 'debug';

import { arithmeticFuncDict as funcDict } from '../../../src/func/arithmetic';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import { dateTyp, numberTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:arithmeticFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('arithmeticFuncDict', () => {
	const n: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const d: Expr = { typ: dateTyp(), val: '$d', dep: ['d'] };

	const successTestCases: TestCase[] = [
		{
			description: 'call ABS with valid args',
			funcKey: 'ABS',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $abs: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call ADD with valid args (number, number)',
			funcKey: 'ADD',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $add: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call ADD with valid args (number, number, number)',
			funcKey: 'ADD',
			args: [n, n, n],
			expr: {
				typ: { type: 'number' },
				val: { $add: ['$n', '$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call ADD with valid args (date, number)',
			funcKey: 'ADD',
			args: [d, n],
			expr: {
				typ: { type: 'date' },
				val: { $add: ['$d', '$n'] },
				dep: ['d', 'n'],
			},
		},
		{
			description: 'call SUBTRACT with valid args (number, number)',
			funcKey: 'SUBTRACT',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $subtract: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call SUBTRACT with valid args (date, number)',
			funcKey: 'SUBTRACT',
			args: [d, n],
			expr: {
				typ: { type: 'date' },
				val: { $subtract: ['$d', '$n'] },
				dep: ['d', 'n'],
			},
		},
		{
			description: 'call SUBTRACT with valid args (date, date)',
			funcKey: 'SUBTRACT',
			args: [d, d],
			expr: {
				typ: { type: 'number' },
				val: { $subtract: ['$d', '$d'] },
				dep: ['d'],
			},
		},
		{
			description: 'call MULTIPLY with valid args (number, number)',
			funcKey: 'MULTIPLY',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $multiply: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description:
				'call MULTIPLY with valid args (number, number, number)',
			funcKey: 'MULTIPLY',
			args: [n, n, n],
			expr: {
				typ: { type: 'number' },
				val: { $multiply: ['$n', '$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call DIVIDE with valid args (number, number)',
			funcKey: 'DIVIDE',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $divide: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call SAFEDIVIDE with valid args (number, number)',
			funcKey: 'SAFEDIVIDE',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: {
					$let: {
						vars: { v1___: '$n' },
						in: {
							$cond: [
								{ $eq: ['$$v1___', 0] },
								null,
								{ $divide: ['$n', '$$v1___'] },
							],
						},
					},
				},
				dep: ['n'],
			},
		},
		{
			description: 'call NEG with valid args (number)',
			funcKey: 'NEG',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $multiply: [-1, '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call POW with valid args (number, number)',
			funcKey: 'POW',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $pow: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call POW with valid args (number)',
			funcKey: 'CEIL',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $ceil: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call FLOOR with valid args (number)',
			funcKey: 'FLOOR',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $floor: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call LOG with valid args (number, number)',
			funcKey: 'LOG',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $log: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call LOG10 with valid args (number)',
			funcKey: 'LOG10',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $log10: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call LN with valid args (number)',
			funcKey: 'LN',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $ln: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call EXP with valid args (number)',
			funcKey: 'EXP',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $exp: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call MOD with valid args (number, number)',
			funcKey: 'MOD',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $mod: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call SQRT with valid args (number)',
			funcKey: 'SQRT',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $sqrt: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call TRUNC with valid args (number)',
			funcKey: 'TRUNC',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $trunc: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call SIGN with valid args (number)',
			funcKey: 'SIGN',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $cmp: ['$n', 0] },
				dep: ['n'],
			},
		},
		{
			description: 'call QUOTIENT with valid args (number, number)',
			funcKey: 'QUOTIENT',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $trunc: { $divide: ['$n', '$n'] } },
				dep: ['n'],
			},
		},
		{
			description: 'call SAFEQUOTIENT with valid args (number, number)',
			funcKey: 'SAFEQUOTIENT',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: {
					$trunc: {
						$let: {
							vars: { v1___: '$n' },
							in: {
								$cond: [
									{ $eq: ['$$v1___', 0] },
									null,
									{
										$divide: ['$n', '$$v1___'],
									},
								],
							},
						},
					},
				},
				dep: ['n'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('ADD', () => {
	const varExpr: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const zeroExpr: Expr = { typ: numberTyp(true), val: 0, dep: [] };
	const oneExpr: Expr = { typ: numberTyp(true), val: 1, dep: [] };
	const addOpExpr: Expr = {
		typ: numberTyp(),
		val: { $add: ['$n', '$n'] },
		dep: ['n'],
	};
	const nestedAddOpExpr: Expr = {
		typ: numberTyp(),
		val: { $add: ['$n', { $add: ['$n', 1] }] },
		dep: ['n'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with literal exprs',
			funcKey: 'ADD',
			args: [oneExpr, oneExpr],
			expr: { typ: { type: 'number', literal: true }, val: 2, dep: [] },
		},
		{
			description: 'call with literal expr and variable expr',
			funcKey: 'ADD',
			args: [varExpr, oneExpr, oneExpr],
			expr: {
				typ: { type: 'number' },
				val: { $add: ['$n', 2] },
				dep: ['n'],
			},
		},
		{
			description: 'call with variable expr and addOp expr',
			funcKey: 'ADD',
			args: [varExpr, addOpExpr],
			expr: {
				typ: { type: 'number' },
				val: { $add: ['$n', '$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call with variable Expr and nested addOp expr',
			funcKey: 'ADD',
			args: [varExpr, nestedAddOpExpr],
			expr: {
				typ: { type: 'number' },
				val: { $add: ['$n', '$n', '$n', 1] },
				dep: ['n'],
			},
		},
		{
			description: 'call with literal expr and nested addOp expr',
			funcKey: 'ADD',
			args: [oneExpr, nestedAddOpExpr],
			expr: {
				typ: { type: 'number' },
				val: { $add: [1, '$n', '$n', 1] },
				dep: ['n'],
			},
		},
		{
			description: 'simplify adding 0',
			funcKey: 'ADD',
			args: [zeroExpr, varExpr],
			expr: {
				typ: { type: 'number' },
				val: '$n',
				dep: ['n'],
			},
		},
	];

	runTestCases('Success', successTestCases);
});

describe('MULTIPLY', () => {
	const varExpr: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const zeroExpr: Expr = { typ: numberTyp(true), val: 0, dep: [] };
	const oneExpr: Expr = { typ: numberTyp(true), val: 1, dep: [] };
	const twoExpr: Expr = { typ: numberTyp(true), val: 2, dep: [] };
	const multiplyOpExpr: Expr = {
		typ: numberTyp(),
		val: { $multiply: ['$n', '$n'] },
		dep: ['n'],
	};
	const nestedMultiplyOpExpr: Expr = {
		typ: numberTyp(),
		val: { $multiply: ['$n', { $multiply: ['$n', 2] }] },
		dep: ['n'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with literal exprs',
			funcKey: 'MULTIPLY',
			args: [twoExpr, twoExpr],
			expr: { typ: { type: 'number', literal: true }, val: 4, dep: [] },
		},
		{
			description: 'call with literal exprs and variable expr',
			args: [varExpr, twoExpr, twoExpr],
			funcKey: 'MULTIPLY',
			expr: {
				typ: { type: 'number' },
				val: { $multiply: ['$n', 4] },
				dep: ['n'],
			},
		},
		{
			description: 'call with variable expr and multiplyOp expr',
			args: [varExpr, multiplyOpExpr],
			funcKey: 'MULTIPLY',
			expr: {
				typ: { type: 'number' },
				val: { $multiply: ['$n', '$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call with variable expr and nested multiplyOp expr',
			args: [varExpr, nestedMultiplyOpExpr],
			funcKey: 'MULTIPLY',
			expr: {
				typ: { type: 'number' },
				val: { $multiply: ['$n', '$n', '$n', 2] },
				dep: ['n'],
			},
		},
		{
			description: 'call with literal expr and nested multiplyOp expr',
			args: [oneExpr, nestedMultiplyOpExpr],
			funcKey: 'MULTIPLY',
			expr: {
				typ: { type: 'number' },
				val: { $multiply: ['$n', '$n', 2] },
				dep: ['n'],
			},
		},
		{
			description: 'simplify mutiplying by 1',
			args: [oneExpr, varExpr],
			funcKey: 'MULTIPLY',
			expr: {
				typ: { type: 'number' },
				val: '$n',
				dep: ['n'],
			},
		},
		{
			description: 'simplify mutiplying by 0',
			args: [zeroExpr, varExpr],
			funcKey: 'MULTIPLY',
			expr: {
				typ: { type: 'number', literal: true },
				val: 0,
				dep: [],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
