import { expect } from 'chai';
import createDebug from 'debug';

import { literalFuncDict as funcDict } from '../../../src/func/literal';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import {
	arrayTyp,
	booleanTyp,
	dateTyp,
	neverTyp,
	numberTyp,
	objectTyp,
	stringTyp,
} from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:literalFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('literalFuncDict', () => {
	const b: Expr = { typ: booleanTyp(), val: '$b', dep: ['b'] };
	const TRUE: Expr = { typ: booleanTyp(true), val: true, dep: [] };
	const n: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const ONE: Expr = { typ: numberTyp(true), val: 1, dep: [] };
	const s: Expr = { typ: stringTyp(), val: '$s', dep: ['s'] };
	const d: Expr = { typ: dateTyp(), val: '$d', dep: ['d'] };
	const firstOf2019: Expr = {
		typ: dateTyp(true),
		val: new Date('2019-1-1'),
		dep: [],
	};
	const a: Expr = { typ: arrayTyp(neverTyp()), val: '$a', dep: ['a'] };
	const an: Expr = {
		typ: arrayTyp(
			[numberTyp(true), numberTyp(true), numberTyp(true)],
			true
		),
		val: [1, 2, 3],
		dep: [],
	};
	const o: Expr = { typ: objectTyp({}), val: '$o', dep: ['o'] };
	const p: Expr = {
		typ: objectTyp(
			{ b: booleanTyp(true), n: numberTyp(true) },
			undefined,
			undefined,
			true
		),
		val: { b: false, n: 1 },
		dep: [],
	};

	const successTestCases: TestCase[] = [
		{
			description:
				'call LITERAL with valid args (number with string val)',
			funcKey: 'LITERAL',
			args: [n],
			expr: {
				typ: { type: 'string' },
				val: { $literal: '$n' },
				dep: [],
			},
		},
		{
			description:
				'call LITERAL with valid args (number with number val)',
			funcKey: 'LITERAL',
			args: [ONE],
			expr: {
				typ: { type: 'number' },
				val: { $literal: 1 },
				dep: [],
			},
		},
		{
			description:
				'call LITERAL with valid args (boolean with string val)',
			funcKey: 'LITERAL',
			args: [b],
			expr: {
				typ: { type: 'string' },
				val: { $literal: '$b' },
				dep: [],
			},
		},
		{
			description:
				'call LITERAL with valid args (boolean with boolean val)',
			funcKey: 'LITERAL',
			args: [TRUE],
			expr: {
				typ: { type: 'boolean' },
				val: { $literal: true },
				dep: [],
			},
		},
		{
			description:
				'call LITERAL with valid args (string with string val)',
			funcKey: 'LITERAL',
			args: [s],
			expr: {
				typ: { type: 'string' },
				val: { $literal: '$s' },
				dep: [],
			},
		},
		{
			description: 'call LITERAL with valid args (date with string val)',
			funcKey: 'LITERAL',
			args: [d],
			expr: {
				typ: { type: 'string' },
				val: { $literal: '$d' },
				dep: [],
			},
		},
		{
			description: 'call LITERAL with valid args (date with date val)',
			funcKey: 'LITERAL',
			args: [firstOf2019],
			expr: {
				typ: { type: 'date' },
				val: { $literal: new Date('2019-1-1') },
				dep: [],
			},
		},
		{
			description:
				'call LITERAL with valid args (object with string val)',
			funcKey: 'LITERAL',
			args: [o],
			expr: {
				typ: { type: 'string' },
				val: { $literal: '$o' },
				dep: [],
			},
		},
		{
			description:
				'call LITERAL with valid args (object with object val)',
			funcKey: 'LITERAL',
			args: [p],
			expr: {
				typ: {
					type: 'object',
					properties: {
						b: { type: 'boolean' },
						n: { type: 'number' },
					},
				},
				val: {
					$literal: { b: false, n: 1 },
				},
				dep: [],
			},
		},
		{
			description: 'call LITERAL with valid args (array with string val)',
			funcKey: 'LITERAL',
			args: [a],
			expr: {
				typ: { type: 'string' },
				val: { $literal: '$a' },
				dep: [],
			},
		},
		{
			description: 'call LITERAL with valid args (object with array val)',
			funcKey: 'LITERAL',
			args: [an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $literal: [1, 2, 3] },
				dep: [],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
