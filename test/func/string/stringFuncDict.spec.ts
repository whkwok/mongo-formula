import { expect } from 'chai';
import createDebug from 'debug';

import { stringFuncDict as funcDict } from '../../../src/func/string';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import { numberTyp, stringTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:stringFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('stringFuncDict', () => {
	const s: Expr = { typ: stringTyp(), val: '$s', dep: ['s'] };
	const n: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };

	const successTestCases: TestCase[] = [
		{
			description: 'call CONCAT with valid args (string, string)',
			funcKey: 'CONCAT',
			args: [s, s],
			expr: {
				typ: { type: 'string' },
				val: { $concat: ['$s', '$s'] },
				dep: ['s'],
			},
		},
		{
			description: 'call CONCAT with valid args (string, string, string)',
			funcKey: 'CONCAT',
			args: [s, s, s],
			expr: {
				typ: { type: 'string' },
				val: { $concat: ['$s', '$s', '$s'] },
				dep: ['s'],
			},
		},
		{
			description: 'call INDEXOFBYTES with valid args (string, string)',
			funcKey: 'INDEXOFBYTES',
			args: [s, s],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfBytes: ['$s', '$s'] },
				dep: ['s'],
			},
		},
		{
			description:
				'call INDEXOFBYTES with valid args (string, string, number)',
			funcKey: 'INDEXOFBYTES',
			args: [s, s, n],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfBytes: ['$s', '$s', '$n'] },
				dep: ['s', 'n'],
			},
		},
		{
			description:
				'call INDEXOFBYTES with valid args (string, string, number, number)',
			funcKey: 'INDEXOFBYTES',
			args: [s, s, n, n],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfBytes: ['$s', '$s', '$n', '$n'] },
				dep: ['s', 'n'],
			},
		},
		{
			description: 'call INDEXOFCP with valid args (string, string)',
			funcKey: 'INDEXOFCP',
			args: [s, s],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfCP: ['$s', '$s'] },
				dep: ['s'],
			},
		},
		{
			description:
				'call INDEXOFCP with valid args (string, string, number)',
			funcKey: 'INDEXOFCP',
			args: [s, s, n],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfCP: ['$s', '$s', '$n'] },
				dep: ['s', 'n'],
			},
		},
		{
			description:
				'call INDEXOFCP with valid args (string, string, number, number)',
			funcKey: 'INDEXOFCP',
			args: [s, s, n, n],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfCP: ['$s', '$s', '$n', '$n'] },
				dep: ['s', 'n'],
			},
		},
		{
			description: 'call LTRIM with valid args (string)',
			funcKey: 'LTRIM',
			args: [s],
			expr: {
				typ: { type: 'string' },
				val: { $ltrim: { input: '$s' } },
				dep: ['s'],
			},
		},
		{
			description: 'call LTRIM with valid args (string, string)',
			funcKey: 'LTRIM',
			args: [s, s],
			expr: {
				typ: { type: 'string' },
				val: { $ltrim: { input: '$s', chars: '$s' } },
				dep: ['s'],
			},
		},
		{
			description: 'call RTRIM with valid args (string)',
			funcKey: 'RTRIM',
			args: [s],
			expr: {
				typ: { type: 'string' },
				val: { $rtrim: { input: '$s' } },
				dep: ['s'],
			},
		},
		{
			description: 'call RTRIM with valid args (string, string)',
			funcKey: 'RTRIM',
			args: [s, s],
			expr: {
				typ: { type: 'string' },
				val: { $rtrim: { input: '$s', chars: '$s' } },
				dep: ['s'],
			},
		},
		{
			description: 'call TRIM with valid args (string)',
			funcKey: 'TRIM',
			args: [s],
			expr: {
				typ: { type: 'string' },
				val: { $trim: { input: '$s' } },
				dep: ['s'],
			},
		},
		{
			description: 'call TRIM with valid args (string, string)',
			funcKey: 'TRIM',
			args: [s, s],
			expr: {
				typ: { type: 'string' },
				val: { $trim: { input: '$s', chars: '$s' } },
				dep: ['s'],
			},
		},
		{
			description: 'call SPLIT with valid args (string, string)',
			funcKey: 'SPLIT',
			args: [s, s],
			expr: {
				typ: { type: 'array', items: { type: 'string' } },
				val: { $split: ['$s', '$s'] },
				dep: ['s'],
			},
		},
		{
			description: 'call STRLENBYTES with valid args (string)',
			funcKey: 'STRLENBYTES',
			args: [s],
			expr: {
				typ: { type: 'number' },
				val: { $strLenBytes: '$s' },
				dep: ['s'],
			},
		},
		{
			description: 'call STRLENCP with valid args (string)',
			funcKey: 'STRLENCP',
			args: [s],
			expr: {
				typ: { type: 'number' },
				val: { $strLenCP: '$s' },
				dep: ['s'],
			},
		},
		{
			description: 'call STRCASECMP with valid args (string, string)',
			funcKey: 'STRCASECMP',
			args: [s, s],
			expr: {
				typ: { type: 'number' },
				val: { $strcasecmp: ['$s', '$s'] },
				dep: ['s'],
			},
		},
		{
			description:
				'call SUBSTRBYTES with valid args (string, number, number)',
			funcKey: 'SUBSTRBYTES',
			args: [s, n, n],
			expr: {
				typ: { type: 'string' },
				val: { $substrBytes: ['$s', '$n', '$n'] },
				dep: ['s', 'n'],
			},
		},
		{
			description:
				'call SUBSTRCP with valid args (string, number, number)',
			funcKey: 'SUBSTRCP',
			args: [s, n, n],
			expr: {
				typ: { type: 'string' },
				val: { $substrCP: ['$s', '$n', '$n'] },
				dep: ['s', 'n'],
			},
		},
		{
			description: 'call TOLOWER with valid args (string)',
			funcKey: 'TOLOWER',
			args: [s],
			expr: {
				typ: { type: 'string' },
				val: { $toLower: '$s' },
				dep: ['s'],
			},
		},
		{
			description: 'call TOUPPER with valid args (string)',
			funcKey: 'TOUPPER',
			args: [s],
			expr: {
				typ: { type: 'string' },
				val: { $toUpper: '$s' },
				dep: ['s'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('CONCAT', () => {
	const varExpr: Expr = { typ: stringTyp(), val: '$s1', dep: ['s1'] };
	const literalExpr: Expr = { typ: stringTyp(true), val: 'hello', dep: [] };
	const concatOpExpr: Expr = {
		typ: stringTyp(),
		val: { $concat: ['$s1', '$s2'] },
		dep: ['s1', 's2'],
	};
	const nestedConcatOpExpr: Expr = {
		typ: stringTyp(),
		val: { $concat: ['hi', '$s3', { $concat: ['$s4', 'hello'] }] },
		dep: ['s3', 's4'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with variable expr and concatOp expr',
			funcKey: 'CONCAT',
			args: [varExpr, concatOpExpr],
			expr: {
				typ: { type: 'string' },
				val: { $concat: ['$s1', '$s1', '$s2'] },
				dep: ['s1', 's2'],
			},
		},
		{
			description: 'call with variable expr and nested concatOp expr',
			funcKey: 'CONCAT',
			args: [varExpr, nestedConcatOpExpr],
			expr: {
				typ: { type: 'string' },
				val: { $concat: ['$s1', 'hi', '$s3', '$s4', 'hello'] },
				dep: ['s1', 's3', 's4'],
			},
		},
		{
			description: 'call with literal exprs',
			funcKey: 'CONCAT',
			args: [literalExpr, literalExpr, literalExpr],
			expr: {
				typ: { type: 'string', literal: true },
				val: 'hellohellohello',
				dep: [],
			},
		},
		{
			description: 'call with literal exprs and variable expr',
			funcKey: 'CONCAT',
			args: [literalExpr, varExpr, literalExpr, literalExpr],
			expr: {
				typ: { type: 'string' },
				val: { $concat: ['hello', '$s1', 'hellohello'] },
				dep: ['s1'],
			},
		},
		{
			description: 'call with literal exprs and nested concatOp expr',
			funcKey: 'CONCAT',
			args: [literalExpr, nestedConcatOpExpr, literalExpr, literalExpr],
			expr: {
				typ: { type: 'string' },
				val: { $concat: ['hellohi', '$s3', '$s4', 'hellohellohello'] },
				dep: ['s3', 's4'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
