import { expect } from 'chai';
import createDebug from 'debug';

import { setFuncDict as funcDict } from '../../../src/func/set';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import { arrayTyp, booleanTyp, numberTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:setFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('setFuncDict', () => {
	const an: Expr = { typ: arrayTyp(numberTyp()), val: '$an', dep: ['an'] };
	const ab: Expr = { typ: arrayTyp(booleanTyp()), val: '$ab', dep: ['ab'] };

	const successTestCases: TestCase[] = [
		{
			description: 'call ALLELEMENTSTRUE with valid args (number[])',
			funcKey: 'ALLELEMENTSTRUE',
			args: [an],
			expr: {
				typ: { type: 'boolean' },
				val: { $allElementsTrue: '$an' },
				dep: ['an'],
			},
		},
		{
			description: 'call ANYELEMENTSTRUE with valid args (boolean[])',
			funcKey: 'ANYELEMENTTRUE',
			args: [ab],
			expr: {
				typ: { type: 'boolean' },
				val: { $anyElementTrue: '$ab' },
				dep: ['ab'],
			},
		},
		{
			description:
				'call SETDIFFERENCE with valid args (number[], number[])',
			funcKey: 'SETDIFFERENCE',
			args: [an, an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $setDifference: ['$an', '$an'] },
				dep: ['an'],
			},
		},
		{
			description:
				'call SETDIFFERENCE with valid args (boolean[], number[])',
			funcKey: 'SETDIFFERENCE',
			args: [ab, an],
			expr: {
				typ: { type: 'array', items: { type: 'boolean' } },
				val: { $setDifference: ['$ab', '$an'] },
				dep: ['ab', 'an'],
			},
		},
		{
			description: 'call SETEQUALS with valid args (number[], number[])',
			funcKey: 'SETEQUALS',
			args: [an, an],
			expr: {
				typ: { type: 'boolean' },
				val: { $setEquals: ['$an', '$an'] },
				dep: ['an'],
			},
		},
		{
			description:
				'call SETNOTEQUALS with valid args (number[], boolean[])',
			funcKey: 'SETNOTEQUALS',
			args: [an, ab],
			expr: {
				typ: { type: 'boolean' },
				val: { $not: { $setEquals: ['$an', '$ab'] } },
				dep: ['an', 'ab'],
			},
		},
		{
			description:
				'call SETISSUBSET with valid args (number[], boolean[])',
			funcKey: 'SETISSUBSET',
			args: [an, ab],
			expr: {
				typ: { type: 'boolean' },
				val: { $setIsSubset: ['$an', '$ab'] },
				dep: ['an', 'ab'],
			},
		},
		{
			description:
				'call SETISSUPERSET with valid args (number[], boolean[])',
			funcKey: 'SETISSUPERSET',
			args: [an, ab],
			expr: {
				typ: { type: 'boolean' },
				val: { $setIsSubset: ['$ab', '$an'] },
				dep: ['an', 'ab'],
			},
		},
		{
			description: 'call SETUNION with valid args (number[], number[])',
			funcKey: 'SETUNION',
			args: [an, an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $setUnion: ['$an', '$an'] },
				dep: ['an'],
			},
		},
		{
			description:
				'call SETUNION with valid args (number[], number[], number[])',
			funcKey: 'SETUNION',
			args: [an, an, an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $setUnion: ['$an', '$an', '$an'] },
				dep: ['an'],
			},
		},
		{
			description: 'call SETUNION with valid args (number[], boolean[])',
			funcKey: 'SETUNION',
			args: [an, ab],
			expr: {
				typ: { type: 'array', items: { type: 'unknown' } },
				val: { $setUnion: ['$an', '$ab'] },
				dep: ['an', 'ab'],
			},
		},
		{
			description:
				'call SETINTERSECTION with valid args (number[], number[])',
			funcKey: 'SETINTERSECTION',
			args: [an, an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $setIntersection: ['$an', '$an'] },
				dep: ['an'],
			},
		},
		{
			description:
				'call SETINTERSECTION with valid args (number[], number[], number[])',
			funcKey: 'SETINTERSECTION',
			args: [an, an, an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $setIntersection: ['$an', '$an', '$an'] },
				dep: ['an'],
			},
		},
		{
			description:
				'call SETINTERSECTION with valid args (number[], boolean[])',
			funcKey: 'SETINTERSECTION',
			args: [an, ab],
			expr: {
				typ: { type: 'array', items: { type: 'never' } },
				val: { $setIntersection: ['$an', '$ab'] },
				dep: ['an', 'ab'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('SETINTERSECTION', () => {
	const varExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: '$a1',
		dep: ['a1'],
	};
	const setIntersectionOpExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: { $setIntersection: ['$a1', '$a2'] },
		dep: ['a1', 'a2'],
	};
	const nestedSetIntersectionOpExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: {
			$setIntersection: ['$a3', { $setIntersection: ['$a4', [1, 2, 3]] }],
		},
		dep: ['a3', 'a4'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with variable expr and setIntersectionOp expr',
			funcKey: 'SETINTERSECTION',
			args: [varExpr, setIntersectionOpExpr],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $setIntersection: ['$a1', '$a1', '$a2'] },
				dep: ['a1', 'a2'],
			},
		},
		{
			description:
				'call with variable expr and nested setIntersectionOp expr',
			funcKey: 'SETINTERSECTION',
			args: [varExpr, nestedSetIntersectionOpExpr],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $setIntersection: ['$a1', '$a3', '$a4', [1, 2, 3]] },
				dep: ['a1', 'a3', 'a4'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('SETUNION', () => {
	const varExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: '$a1',
		dep: ['a1'],
	};
	const setUnionOpExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: { $setUnion: ['$a1', '$a2'] },
		dep: ['a1', 'a2'],
	};
	const nestedSetUnionOpExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: { $setUnion: ['$a3', { $setUnion: ['$a4', [1, 2, 3]] }] },
		dep: ['a3', 'a4'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with variable expr and setUnionOp expr',
			funcKey: 'SETUNION',
			args: [varExpr, setUnionOpExpr],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $setUnion: ['$a1', '$a1', '$a2'] },
				dep: ['a1', 'a2'],
			},
		},
		{
			description: 'call with variable Expr and nested setUnionOp expr',
			funcKey: 'SETUNION',
			args: [varExpr, nestedSetUnionOpExpr],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $setUnion: ['$a1', '$a3', '$a4', [1, 2, 3]] },
				dep: ['a1', 'a3', 'a4'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
