import { expect } from 'chai';
import createDebug from 'debug';

import { accumulatorFuncDict as funcDict } from '../../../src/func/accumulator';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { GROUP } from '../../../src/stage/GROUP';
import { Expr, ExtendedExpr, YY } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import { createRootScope, createStageScope } from '../../../src/util/scope';
import {
	arrayTyp,
	booleanTyp,
	numberTyp,
	stringTyp,
} from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	yy: YY;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const defaultYY: YY = {
	funcDict,
	stageDict: { GROUP },
	options: defaultParserOptions,
	scopeStack: [createRootScope({})],
};

const groupYY: YY = {
	funcDict,
	stageDict: { GROUP },
	options: defaultParserOptions,
	scopeStack: [
		createRootScope({}),
		createStageScope('GROUP', undefined, defaultYY),
	],
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:accumulatorFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				yy,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;

				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('accumulatorFuncDict', () => {
	const n: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const an: Expr = { typ: arrayTyp(numberTyp()), val: '$an', dep: ['an'] };
	const ab: Expr = { typ: arrayTyp(booleanTyp()), val: '$ab', dep: ['ab'] };
	const as: Expr = { typ: arrayTyp(stringTyp()), val: '$as', dep: ['as'] };

	const successTestCases: TestCase[] = [
		{
			description:
				'call AVG with valid args (number[]) (not in group-like stage scope)',
			funcKey: 'AVG',
			yy: defaultYY,
			args: [an],
			expr: {
				typ: { type: 'number' },
				val: { $avg: '$an' },
				dep: ['an'],
			},
		},
		{
			description:
				'call AVG with valid args (number) (in group-like stage scope)',
			funcKey: 'AVG',
			yy: groupYY,
			args: [n],
			expr: {
				typ: { type: 'accumulator', in: { type: 'number' } },
				val: { $avg: '$n' },
				dep: ['n'],
			},
		},
		{
			description:
				'call FIRST with valid args (string[]) (not in group-like stage scope)',
			funcKey: 'FIRST',
			yy: defaultYY,
			args: [as],
			expr: {
				typ: { type: 'string' },
				val: { $first: '$as' },
				dep: ['as'],
			},
		},
		{
			description:
				'call FIRST with valid args (number) (in group-like stage scope)',
			funcKey: 'FIRST',
			yy: groupYY,
			args: [n],
			expr: {
				typ: { type: 'accumulator', in: { type: 'number' } },
				val: { $first: '$n' },
				dep: ['n'],
			},
		},
		{
			description:
				'call LAST with valid args (string[]) (not in group-like stage scope)',
			funcKey: 'LAST',
			yy: defaultYY,
			args: [as],
			expr: {
				typ: { type: 'string' },
				val: { $last: '$as' },
				dep: ['as'],
			},
		},
		{
			description:
				'call LAST with valid args (number) (in group-like stage scope)',
			funcKey: 'LAST',
			yy: groupYY,
			args: [n],
			expr: {
				typ: { type: 'accumulator', in: { type: 'number' } },
				val: { $last: '$n' },
				dep: ['n'],
			},
		},
		{
			description:
				'call MAX with valid args (string[]) (not in group-like stage scope)',
			funcKey: 'MAX',
			yy: defaultYY,
			args: [as],
			expr: {
				typ: { type: 'string' },
				val: { $max: '$as' },
				dep: ['as'],
			},
		},
		{
			description:
				'call MAX with valid args (number) (in group-like stage scope)',
			funcKey: 'MAX',
			yy: groupYY,
			args: [n],
			expr: {
				typ: { type: 'accumulator', in: { type: 'number' } },
				val: { $max: '$n' },
				dep: ['n'],
			},
		},
		{
			description:
				'call MIN with valid args (boolean[]) (not in group-like stage scope)',
			funcKey: 'MIN',
			yy: defaultYY,
			args: [ab],
			expr: {
				typ: { type: 'boolean' },
				val: { $min: '$ab' },
				dep: ['ab'],
			},
		},
		{
			description:
				'call MIN with valid args (number) (in group-like stage scope)',
			funcKey: 'MIN',
			yy: groupYY,
			args: [n],
			expr: {
				typ: { type: 'accumulator', in: { type: 'number' } },
				val: { $min: '$n' },
				dep: ['n'],
			},
		},
		{
			description:
				'call STDDEVPOP with valid args (number[]) (not in group-like stage scope)',
			funcKey: 'STDDEVPOP',
			yy: defaultYY,
			args: [an],
			expr: {
				typ: { type: 'number' },
				val: { $stdDevPop: '$an' },
				dep: ['an'],
			},
		},
		{
			description:
				'call STDDEVPOP with valid args (number) (in group-like stage scope)',
			funcKey: 'STDDEVPOP',
			yy: groupYY,
			args: [n],
			expr: {
				typ: { type: 'accumulator', in: { type: 'number' } },
				val: { $stdDevPop: '$n' },
				dep: ['n'],
			},
		},
		{
			description:
				'call STDDEVSAMP with valid args (number[]) (not in group-like stage scope)',
			funcKey: 'STDDEVSAMP',
			yy: defaultYY,
			args: [an],
			expr: {
				typ: { type: 'number' },
				val: { $stdDevSamp: '$an' },
				dep: ['an'],
			},
		},
		{
			description:
				'call STDDEVSAMP with valid args (number[]) (in group-like stage scope)',
			funcKey: 'STDDEVSAMP',
			yy: groupYY,
			args: [an],
			expr: {
				typ: { type: 'accumulator', in: { type: 'number' } },
				val: { $stdDevSamp: '$an' },
				dep: ['an'],
			},
		},
		{
			description:
				'call SUM with valid args (number[]) (not in group-like stage scope)',
			funcKey: 'SUM',
			yy: defaultYY,
			args: [an],
			expr: {
				typ: { type: 'number' },
				val: { $sum: '$an' },
				dep: ['an'],
			},
		},
		{
			description:
				'call SUM with valid args (number) (in group-like stage scope)',
			funcKey: 'SUM',
			yy: groupYY,
			args: [n],
			expr: {
				typ: { type: 'accumulator', in: { type: 'number' } },
				val: { $sum: '$n' },
				dep: ['n'],
			},
		},
	];

	runTestCases('Success', successTestCases);
});
