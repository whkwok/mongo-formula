import { expect } from 'chai';
import createDebug from 'debug';

import { comparisonFuncDict as funcDict } from '../../../src/func/comparison';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import { stringTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:comparisonFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('comparisonFuncDict', () => {
	const s: Expr = { typ: stringTyp(), val: '$s', dep: ['s'] };
	const n: Expr = {
		typ: { type: 'number' },
		val: '$n',
		dep: ['n'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call CMP with valid args (string, string)',
			funcKey: 'CMP',
			args: [s, s],
			expr: {
				typ: { type: 'number' },
				val: { $cmp: ['$s', '$s'] },
				dep: ['s'],
			},
		},
		{
			description: 'call GT with valid args (number, number)',
			funcKey: 'GT',
			args: [n, n],
			expr: {
				typ: { type: 'boolean' },
				val: { $gt: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call GTE with valid args (number, number)',
			funcKey: 'GTE',
			args: [n, n],
			expr: {
				typ: { type: 'boolean' },
				val: { $gte: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call EQ with valid args (number, number)',
			funcKey: 'EQ',
			args: [n, n],
			expr: {
				typ: { type: 'boolean' },
				val: { $eq: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call LTE with valid args (number, number)',
			funcKey: 'LTE',
			args: [n, n],
			expr: {
				typ: { type: 'boolean' },
				val: { $lte: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call LT with valid args (number, number)',
			funcKey: 'LT',
			args: [n, n],
			expr: {
				typ: { type: 'boolean' },
				val: { $lt: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call NE with valid args (number, number)',
			funcKey: 'NE',
			args: [n, n],
			expr: {
				typ: { type: 'boolean' },
				val: { $ne: ['$n', '$n'] },
				dep: ['n'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
