import { expect } from 'chai';
import createDebug from 'debug';

import { overloadFuncDict as funcDict } from '../../../src/func/overload';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import {
	anyTyp,
	arrayTyp,
	dateTyp,
	neverTyp,
	numberTyp,
	stringTyp,
} from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:overloadFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('overloadFuncDict', () => {
	const any: Expr = { typ: anyTyp(), val: '$any', dep: ['any'] };
	const n: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const s: Expr = { typ: stringTyp(), val: '$s', dep: ['s'] };
	const d: Expr = { typ: dateTyp(), val: '$d', dep: ['d'] };
	const a: Expr = { typ: arrayTyp(neverTyp()), val: '$a', dep: ['a'] };

	const successTestCases: TestCase[] = [
		{
			description: 'call SMARTADD with valid args (number, number)',
			funcKey: 'SMARTADD',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $add: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call SMARTADD with valid args (date, number)',
			funcKey: 'SMARTADD',
			args: [d, n],
			expr: {
				typ: { type: 'date' },
				val: { $add: ['$d', '$n'] },
				dep: ['d', 'n'],
			},
		},
		{
			description: 'call SMARTADD with valid args (string, string)',
			funcKey: 'SMARTADD',
			args: [s, s],
			expr: {
				typ: { type: 'string' },
				val: { $concat: ['$s', '$s'] },
				dep: ['s'],
			},
		},
		{
			description: 'call SMARTADD with valid args (number, any)',
			funcKey: 'SMARTADD',
			args: [n, any],
			expr: {
				typ: { type: 'number' },
				val: { $add: ['$n', '$any'] },
				dep: ['n', 'any'],
			},
		},
		{
			description: 'call SMARTADD with valid args (date, any)',
			funcKey: 'SMARTADD',
			args: [d, any],
			expr: {
				typ: { type: 'date' },
				val: { $add: ['$d', '$any'] },
				dep: ['d', 'any'],
			},
		},
		{
			description: 'call SMARTADD with valid args (string, any)',
			funcKey: 'SMARTADD',
			args: [s, any],
			expr: {
				typ: { type: 'string' },
				val: { $concat: ['$s', '$any'] },
				dep: ['s', 'any'],
			},
		},
		{
			description: 'call SMARTADD with valid args (string, any)',
			funcKey: 'SMARTADD',
			args: [any, s],
			expr: {
				typ: { type: 'string' },
				val: { $concat: ['$any', '$s'] },
				dep: ['any', 's'],
			},
		},
		{
			description: 'call SMARTLEN with valid args (number)',
			funcKey: 'SMARTLEN',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $abs: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call SMARTLEN with valid args (string)',
			funcKey: 'SMARTLEN',
			args: [s],
			expr: {
				typ: { type: 'number' },
				val: { $strLenCP: '$s' },
				dep: ['s'],
			},
		},
		{
			description: 'call SMARTLEN with valid args (array)',
			funcKey: 'SMARTLEN',
			args: [a],
			expr: {
				typ: { type: 'number' },
				val: { $size: '$a' },
				dep: ['a'],
			},
		},
	];
	runTestCases('Success', successTestCases);

	const failTestCases: TestCase[] = [
		{
			description:
				'throw error when calling SMARTADD with invalid args (any, any)',
			funcKey: 'SMARTADD',
			args: [any, any],
			errName: 'OverloadErr',
			errDetails: {
				context: 'SMARTADD',
				receivedTyps: [{ type: 'any' }, { type: 'any' }],
			},
		},
		{
			description:
				'throw error when calling SMARTINDEX with invalid args (any, number)',
			funcKey: 'SMARTINDEX',
			args: [any, n],
			errName: 'OverloadErr',
			errDetails: {
				context: 'SMARTINDEX',
				receivedTyps: [{ type: 'any' }, { type: 'number' }],
			},
		},
		{
			description:
				'throw error when calling SMARTSLICE with invalid args (any, number, number)',
			funcKey: 'SMARTSLICE',
			args: [any, n, n],
			errName: 'OverloadErr',
			errDetails: {
				context: 'SMARTSLICE',
				receivedTyps: [
					{ type: 'any' },
					{ type: 'number' },
					{ type: 'number' },
				],
			},
		},
		{
			description:
				'throw error when calling SMARTLEN with invalid args (any)',
			funcKey: 'SMARTLEN',
			args: [any],
			errName: 'OverloadErr',
			errDetails: {
				context: 'SMARTLEN',
				receivedTyps: [{ type: 'any' }],
			},
		},
	];

	runTestCases('Fail', failTestCases);
});
