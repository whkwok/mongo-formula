import { expect } from 'chai';
import createDebug from 'debug';

import { typeFuncDict as funcDict } from '../../../src/func/type';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import { numberTyp, stringTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:typeFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('typeFuncDict', () => {
	const n: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const s: Expr = { typ: stringTyp(), val: '$s', dep: ['s'] };

	const successTestCases: TestCase[] = [
		{
			description: 'call CONVERT with valid args (number, string)',
			funcKey: 'CONVERT',
			args: [n, s],
			expr: {
				typ: { type: 'unknown' },
				val: { $convert: { input: '$n', to: '$s', onError: null } },
				dep: ['n', 's'],
			},
		},
		{
			description: 'call TOBOOL with valid args (number)',
			funcKey: 'TOBOOL',
			args: [n],
			expr: {
				typ: { type: 'boolean' },
				val: { $convert: { input: '$n', to: 'bool', onError: null } },
				dep: ['n'],
			},
		},
		{
			description: 'call TODATE with valid args (number)',
			funcKey: 'TODATE',
			args: [n],
			expr: {
				typ: { type: 'date' },
				val: { $convert: { input: '$n', to: 'date', onError: null } },
				dep: ['n'],
			},
		},
		{
			description: 'call TODECIMAL with valid args (number)',
			funcKey: 'TODECIMAL',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: {
					$convert: { input: '$n', to: 'decimal', onError: null },
				},
				dep: ['n'],
			},
		},
		{
			description: 'call TODOUBLE with valid args (number)',
			funcKey: 'TODOUBLE',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: {
					$convert: { input: '$n', to: 'double', onError: null },
				},
				dep: ['n'],
			},
		},
		{
			description: 'call TOINT with valid args (number)',
			funcKey: 'TOINT',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $convert: { input: '$n', to: 'int', onError: null } },
				dep: ['n'],
			},
		},
		{
			description: 'call TOLONG with valid args (number)',
			funcKey: 'TOLONG',
			args: [n],
			expr: {
				typ: { type: 'number' },
				val: { $convert: { input: '$n', to: 'long', onError: null } },
				dep: ['n'],
			},
		},
		{
			description: 'call TOSTRING with valid args (number)',
			funcKey: 'TOSTRING',
			args: [n],
			expr: {
				typ: { type: 'string' },
				val: { $convert: { input: '$n', to: 'string', onError: null } },
				dep: ['n'],
			},
		},
		{
			description: 'call TYPE with valid args (number)',
			funcKey: 'TYPE',
			args: [n],
			expr: {
				typ: { type: 'string' },
				val: { $type: '$n' },
				dep: ['n'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
