import { expect } from 'chai';
import createDebug from 'debug';

import { dateFuncDict as funcDict } from '../../../src/func/date';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import {
	datePartsTypProperties,
	dateTyp,
	isoDatePartsTypProperties,
	numberTyp,
	objectTyp,
	stringTyp,
} from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:dateFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('dateFuncDict', () => {
	const s: Expr = { typ: stringTyp(), val: '$s', dep: ['s'] };
	const d: Expr = { typ: dateTyp(), val: '$d', dep: ['d'] };
	const isoParts: Expr = {
		typ: objectTyp(isoDatePartsTypProperties()),
		val: '$isoParts',
		dep: ['isoParts'],
	};
	const isoWeekYearSubIsoParts: Expr = {
		typ: objectTyp({ isoWeekYear: numberTyp() }),
		val: '$isoWeekYearSubIsoParts',
		dep: ['isoWeekYearSubIsoParts'],
	};
	const nonIsoWeekYearSubIsoParts: Expr = {
		typ: objectTyp({
			isoWeek: numberTyp(),
			isoDayOfWeek: numberTyp(),
			hour: numberTyp(),
			minute: numberTyp(),
			second: numberTyp(),
			millisecond: numberTyp(),
		}),
		val: '$nonIsoWeekYearSubIsoParts',
		dep: ['nonIsoWeekYearSubIsoParts'],
	};
	const parts: Expr = {
		typ: objectTyp(datePartsTypProperties()),
		val: '$parts',
		dep: ['parts'],
	};
	const yearSubParts: Expr = {
		typ: objectTyp({ year: numberTyp() }),
		val: '$yearSubParts',
		dep: ['yearSubParts'],
	};
	const nonYearSubParts: Expr = {
		typ: objectTyp({
			month: numberTyp(),
			day: numberTyp(),
			hour: numberTyp(),
			minute: numberTyp(),
			second: numberTyp(),
			millisecond: numberTyp(),
		}),
		val: '$nonYearSubParts',
		dep: ['nonYearSubParts'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call YEAR with valid args (date)',
			funcKey: 'YEAR',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $year: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call MONTH with valid args (date)',
			funcKey: 'MONTH',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $month: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call WEEK with valid args (date)',
			funcKey: 'WEEK',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $week: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call DAYOFYEAR with valid args (date)',
			funcKey: 'DAYOFYEAR',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $dayOfYear: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call DAYOFMONTH with valid args (date)',
			funcKey: 'DAYOFMONTH',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $dayOfMonth: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call DAYOFWEEK with valid args (date)',
			funcKey: 'DAYOFWEEK',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $dayOfWeek: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call HOUR with valid args (date)',
			funcKey: 'HOUR',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $hour: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call MINUTE with valid args (date)',
			funcKey: 'MINUTE',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $minute: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call SECOND with valid args (date)',
			funcKey: 'SECOND',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $second: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call MILLISECOND with valid args (date)',
			funcKey: 'MILLISECOND',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $millisecond: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call ISOWEEKYEAR with valid args (date)',
			funcKey: 'ISOWEEKYEAR',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $isoWeekYear: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call ISOWEEK with valid args (date)',
			funcKey: 'ISOWEEK',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $isoWeek: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call ISODAYOFWEEK with valid args (date)',
			funcKey: 'ISODAYOFWEEK',
			args: [d],
			expr: {
				typ: { type: 'number' },
				val: { $isoDayOfWeek: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call DATEFROMSTRING with valid args (string)',
			funcKey: 'DATEFROMSTRING',
			args: [s],
			expr: {
				typ: { type: 'date' },
				val: {
					$dateFromString: {
						dateString: '$s',
						onError: null,
						onNull: null,
					},
				},
				dep: ['s'],
			},
		},
		{
			description: 'call DATETOSTRING with valid args (date)',
			funcKey: 'DATETOSTRING',
			args: [d],
			expr: {
				typ: { type: 'string' },
				val: { $dateToString: { date: '$d', onNull: null } },
				dep: ['d'],
			},
		},
		{
			description: 'call DATETOPARTS with valid args (date)',
			funcKey: 'DATETOPARTS',
			args: [d],
			expr: {
				typ: {
					type: 'object',
					properties: {
						year: { type: 'number' },
						month: { type: 'number' },
						day: { type: 'number' },
						hour: { type: 'number' },
						minute: { type: 'number' },
						second: { type: 'number' },
						millisecond: { type: 'number' },
					},
				},
				val: { $dateToParts: { date: '$d' } },
				dep: ['d'],
			},
		},
		{
			description: 'call DATETOISOPARTS with valid args (date)',
			funcKey: 'DATETOISOPARTS',
			args: [d],
			expr: {
				typ: {
					type: 'object',
					properties: {
						isoWeekYear: { type: 'number' },
						isoWeek: { type: 'number' },
						isoDayOfWeek: { type: 'number' },
						hour: { type: 'number' },
						minute: { type: 'number' },
						second: { type: 'number' },
						millisecond: { type: 'number' },
					},
				},
				val: { $dateToParts: { date: '$d', iso8601: true } },
				dep: ['d'],
			},
		},
		{
			description: 'call DATEFROMPARTS with valid args (parts)',
			funcKey: 'DATEFROMPARTS',
			args: [parts],
			expr: {
				typ: { type: 'date' },
				val: { $dateFromParts: '$parts' },
				dep: ['parts'],
			},
		},
		{
			description: 'call DATEFROMPARTS with valid args (yearSubParts)',
			funcKey: 'DATEFROMPARTS',
			args: [yearSubParts],
			expr: {
				typ: { type: 'date' },
				val: { $dateFromParts: '$yearSubParts' },
				dep: ['yearSubParts'],
			},
		},
		{
			description: 'call DATEFROMISOPARTS with valid args (isoParts)',
			funcKey: 'DATEFROMISOPARTS',
			args: [isoParts],
			expr: {
				typ: { type: 'date' },
				val: { $dateFromParts: '$isoParts' },
				dep: ['isoParts'],
			},
		},
		{
			description:
				'call DATEFROMISOPARTS with valid args (isoWeekYearSubIsoParts)',
			funcKey: 'DATEFROMISOPARTS',
			args: [isoWeekYearSubIsoParts],
			expr: {
				typ: { type: 'date' },
				val: { $dateFromParts: '$isoWeekYearSubIsoParts' },
				dep: ['isoWeekYearSubIsoParts'],
			},
		},
	];
	runTestCases('Success', successTestCases);

	const failTestCases: TestCase[] = [
		{
			description:
				'call DATEFROMPARTS with invalid args (nonYearSubParts)',
			funcKey: 'DATEFROMPARTS',
			args: [nonYearSubParts],
			errName: 'ExprTypErr',
			errDetails: {
				context: 'DATEFROMPARTS',
				index: 0,
				receivedTyp: {
					type: 'object',
					properties: {
						month: { type: 'number' },
						day: { type: 'number' },
						hour: { type: 'number' },
						minute: { type: 'number' },
						second: { type: 'number' },
						millisecond: { type: 'number' },
					},
				},
				expectedTyps: [
					{
						type: 'object',
						properties: {
							year: { type: 'number' },
							month: { type: 'number' },
							day: { type: 'number' },
							hour: { type: 'number' },
							minute: { type: 'number' },
							second: { type: 'number' },
							millisecond: { type: 'number' },
						},
						optional: [
							'month',
							'day',
							'hour',
							'minute',
							'second',
							'millisecond',
						],
					},
				],
			},
		},
		{
			description:
				'call ISODATEFROMPARTS with invalid args (nonIsoWeekYearSubIsoParts)',
			funcKey: 'DATEFROMISOPARTS',
			args: [nonIsoWeekYearSubIsoParts],
			errName: 'ExprTypErr',
			errDetails: {
				context: 'DATEFROMISOPARTS',
				index: 0,
				receivedTyp: {
					type: 'object',
					properties: {
						isoWeek: { type: 'number' },
						isoDayOfWeek: { type: 'number' },
						hour: { type: 'number' },
						minute: { type: 'number' },
						second: { type: 'number' },
						millisecond: { type: 'number' },
					},
				},
				expectedTyps: [
					{
						type: 'object',
						properties: {
							isoWeekYear: { type: 'number' },
							isoWeek: { type: 'number' },
							isoDayOfWeek: { type: 'number' },
							hour: { type: 'number' },
							minute: { type: 'number' },
							second: { type: 'number' },
							millisecond: { type: 'number' },
						},
						optional: [
							'isoWeek',
							'isoDayOfWeek',
							'hour',
							'minute',
							'second',
							'millisecond',
						],
					},
				],
			},
		},
	];
	runTestCases('Fail', failTestCases);
});
