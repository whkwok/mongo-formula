import { expect } from 'chai';
import createDebug from 'debug';

import { conditionalFuncDict as funcDict } from '../../../src/func/conditional';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import {
	booleanTyp,
	neverTyp,
	numberTyp,
	stringTyp,
} from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errName?: string;
	errExists?: boolean;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:conditionalFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('conditionalFuncDict', () => {
	const b: Expr = { typ: booleanTyp(), val: '$b', dep: ['b'] };
	const n: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const s: Expr = { typ: stringTyp(), val: '$s', dep: ['s'] };

	const successTestCases: TestCase[] = [
		{
			description: 'call COND with valid args (boolean, number, number)',
			funcKey: 'COND',
			args: [b, n, n],
			expr: {
				typ: { type: 'number' },
				val: { $cond: ['$b', '$n', '$n'] },
				dep: ['b', 'n'],
			},
		},
		{
			description: 'call COND with valid args (boolean, string, string)',
			funcKey: 'COND',
			args: [b, s, s],
			expr: {
				typ: { type: 'string' },
				val: { $cond: ['$b', '$s', '$s'] },
				dep: ['b', 's'],
			},
		},
		{
			description: 'call COND with valid args (boolean, string, number)',
			funcKey: 'COND',
			args: [b, s, n],
			expr: {
				typ: { type: 'unknown' },
				val: { $cond: ['$b', '$s', '$n'] },
				dep: ['b', 's', 'n'],
			},
		},
		{
			description: 'call IFNULL with valid args (number, number)',
			funcKey: 'IFNULL',
			args: [n, n],
			expr: {
				typ: { type: 'number' },
				val: { $ifNull: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call IFNULL with valid args (string, string)',
			funcKey: 'IFNULL',
			args: [s, s],
			expr: {
				typ: { type: 'string' },
				val: { $ifNull: ['$s', '$s'] },
				dep: ['s'],
			},
		},
		{
			description: 'call IFNULL with valid args (string, number)',
			funcKey: 'IFNULL',
			args: [s, n],
			expr: {
				typ: { type: 'unknown' },
				val: { $ifNull: ['$s', '$n'] },
				dep: ['s', 'n'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('COND', () => {
	const numberExpr: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const stringExpr: Expr = { typ: stringTyp(), val: '$s', dep: ['s'] };
	const booleanExpr: Expr = { typ: booleanTyp(), val: '$b', dep: ['b'] };
	const trueExpr: Expr = { typ: booleanTyp(true), val: true, dep: [] };
	const falseExpr: Expr = { typ: booleanTyp(true), val: false, dep: [] };

	const successTestCases: TestCase[] = [
		{
			description:
				'call with true expr, variable expr (number), and variable expr (string)',
			funcKey: 'COND',
			args: [trueExpr, numberExpr, stringExpr],
			expr: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
		},
		{
			description:
				'call with false expr, variable expr (number), and variable expr (string)',
			funcKey: 'COND',
			args: [falseExpr, numberExpr, stringExpr],
			expr: { typ: { type: 'string' }, val: '$s', dep: ['s'] },
		},
		{
			description:
				'call with variable expr (boolean), variable expr (number) and variable expr (string)',
			funcKey: 'COND',
			args: [booleanExpr, numberExpr, stringExpr],
			expr: {
				typ: { type: 'unknown' },
				val: { $cond: ['$b', '$n', '$s'] },
				dep: ['b', 'n', 's'],
			},
		},
		{
			description:
				'call with variable expr (boolean), variable expr (number) and variable expr (number)',
			funcKey: 'COND',
			args: [booleanExpr, numberExpr, numberExpr],
			expr: {
				typ: { type: 'number' },
				val: { $cond: ['$b', '$n', '$n'] },
				dep: ['b', 'n'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('IFNULL', () => {
	const numberExpr: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const stringExpr: Expr = { typ: stringTyp(), val: '$s', dep: ['s'] };
	const nullExpr: Expr = { typ: neverTyp(), val: null, dep: [] };

	const successTestCases: TestCase[] = [
		{
			description: 'call with null expr and variable expr (number)',
			funcKey: 'IFNULL',
			args: [nullExpr, numberExpr],
			expr: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
		},
		{
			description: 'call with null expr and variable expr (string)',
			funcKey: 'IFNULL',
			args: [nullExpr, stringExpr],
			expr: { typ: { type: 'string' }, val: '$s', dep: ['s'] },
		},
		{
			description:
				'call with variable (number) expr and variable expr (number)',
			funcKey: 'IFNULL',
			args: [numberExpr, numberExpr],
			expr: {
				typ: { type: 'number' },
				val: { $ifNull: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description:
				'call with variable (number) expr and variable expr (string)',
			funcKey: 'IFNULL',
			args: [numberExpr, stringExpr],
			expr: {
				typ: { type: 'unknown' },
				val: { $ifNull: ['$n', '$s'] },
				dep: ['n', 's'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
