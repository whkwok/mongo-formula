import { expect } from 'chai';
import createDebug from 'debug';

import { booleanFuncDict as funcDict } from '../../../src/func/boolean';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import { booleanTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:booleanFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('booleanFuncDict', () => {
	const b: Expr = {
		typ: { type: 'boolean' },
		val: '$b',
		dep: ['b'],
	};
	const n: Expr = {
		typ: { type: 'number' },
		val: '$n',
		dep: ['n'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call OR with valid args (boolean, number)',
			funcKey: 'OR',
			args: [b, n],
			expr: {
				typ: { type: 'boolean' },
				val: { $or: ['$b', '$n'] },
				dep: ['b', 'n'],
			},
		},
		{
			description: 'call OR with valid args (boolean, number, boolean)',
			funcKey: 'OR',
			args: [b, n, b],
			expr: {
				typ: { type: 'boolean' },
				val: { $or: ['$b', '$n', '$b'] },
				dep: ['b', 'n'],
			},
		},
		{
			description: 'call AND with valid args (number, boolean)',
			funcKey: 'AND',
			args: [n, b],
			expr: {
				typ: { type: 'boolean' },
				val: { $and: ['$n', '$b'] },
				dep: ['n', 'b'],
			},
		},
		{
			description: 'call AND with valid args (number, boolean, boolean)',
			funcKey: 'AND',
			args: [n, b, b],
			expr: {
				typ: { type: 'boolean' },
				val: { $and: ['$n', '$b', '$b'] },
				dep: ['n', 'b'],
			},
		},
		{
			description: 'call NOT with valid args (boolean)',
			funcKey: 'NOT',
			args: [b],
			expr: {
				typ: { type: 'boolean' },
				val: { $not: '$b' },
				dep: ['b'],
			},
		},
		{
			description: 'call NOT with valid args (number)',
			funcKey: 'NOT',
			args: [n],
			expr: {
				typ: { type: 'boolean' },
				val: { $not: '$n' },
				dep: ['n'],
			},
		},
	];

	runTestCases('Success', successTestCases);
});

describe('AND', () => {
	const varExpr: Expr = { typ: booleanTyp(), val: '$b', dep: ['b'] };
	const trueExpr: Expr = { typ: booleanTyp(true), val: true, dep: [] };
	const falseExpr: Expr = { typ: booleanTyp(true), val: false, dep: [] };
	const andOpExpr: Expr = {
		typ: booleanTyp(),
		val: { $and: ['$b', '$b'] },
		dep: ['b'],
	};
	const nestedAndOpExpr: Expr = {
		typ: booleanTyp(),
		val: { $and: ['$b', { $and: ['$b', true] }] },
		dep: ['b'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with true expr and false expr',
			funcKey: 'AND',
			args: [trueExpr, falseExpr],
			expr: {
				typ: { type: 'boolean', literal: true },
				val: false,
				dep: [],
			},
		},
		{
			description: 'call with true expr and variable expr',
			funcKey: 'AND',
			args: [trueExpr, varExpr],
			expr: { typ: { type: 'boolean' }, val: '$b', dep: ['b'] },
		},
		{
			description: 'call with false expr and variable expr',
			funcKey: 'AND',
			args: [falseExpr, varExpr],
			expr: {
				typ: { type: 'boolean', literal: true },
				val: false,
				dep: [],
			},
		},
		{
			description: 'call with variable expr and andOp expr',
			funcKey: 'AND',
			args: [varExpr, andOpExpr],
			expr: {
				typ: { type: 'boolean' },
				val: { $and: ['$b', '$b', '$b'] },
				dep: ['b'],
			},
		},
		{
			description: 'call with variable expr and nested andOp expr',
			funcKey: 'AND',
			args: [varExpr, nestedAndOpExpr],
			expr: {
				typ: { type: 'boolean' },
				val: { $and: ['$b', '$b', '$b'] },
				dep: ['b'],
			},
		},
		{
			description: 'call with true expr and nested andOp expr',
			funcKey: 'AND',
			args: [trueExpr, nestedAndOpExpr],
			expr: {
				typ: { type: 'boolean' },
				val: { $and: ['$b', '$b'] },
				dep: ['b'],
			},
		},
		{
			description: 'call with false expr and nested andOp expr',
			funcKey: 'AND',
			args: [falseExpr, nestedAndOpExpr],
			expr: {
				typ: { type: 'boolean', literal: true },
				val: false,
				dep: [],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('OR', () => {
	const varExpr: Expr = { typ: booleanTyp(), val: '$b', dep: ['b'] };
	const trueExpr: Expr = { typ: booleanTyp(true), val: true, dep: [] };
	const falseExpr: Expr = { typ: booleanTyp(true), val: false, dep: [] };
	const orOpExpr: Expr = {
		typ: booleanTyp(),
		val: { $or: ['$b', '$b'] },
		dep: ['b'],
	};
	const nestedOrOpExpr: Expr = {
		typ: booleanTyp(),
		val: { $or: ['$b', { $or: ['$b', false] }] },
		dep: ['b'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with true expr and false expr',
			funcKey: 'OR',
			args: [trueExpr, falseExpr],
			expr: {
				typ: { type: 'boolean', literal: true },
				val: true,
				dep: [],
			},
		},
		{
			description: 'call with true expr and variable expr',
			funcKey: 'OR',
			args: [trueExpr, varExpr],
			expr: {
				typ: { type: 'boolean', literal: true },
				val: true,
				dep: [],
			},
		},
		{
			description: 'call with false expr and variable expr',
			funcKey: 'OR',
			args: [falseExpr, varExpr],
			expr: { typ: { type: 'boolean' }, val: '$b', dep: ['b'] },
		},
		{
			description: 'call with variable expr and orOp expr',
			funcKey: 'OR',
			args: [varExpr, orOpExpr],
			expr: {
				typ: { type: 'boolean' },
				val: { $or: ['$b', '$b', '$b'] },
				dep: ['b'],
			},
		},
		{
			description: 'call with variable expr and nested orOp expr',
			funcKey: 'OR',
			args: [varExpr, nestedOrOpExpr],
			expr: {
				typ: { type: 'boolean' },
				val: { $or: ['$b', '$b', '$b'] },
				dep: ['b'],
			},
		},
		{
			description: 'call with true expr and nested orOp expr',
			funcKey: 'OR',
			args: [trueExpr, nestedOrOpExpr],
			expr: {
				typ: { type: 'boolean', literal: true },
				val: true,
				dep: [],
			},
		},
		{
			description: 'call with false expr and nested orOp expr',
			funcKey: 'OR',
			args: [falseExpr, nestedOrOpExpr],
			expr: {
				typ: { type: 'boolean' },
				val: { $or: ['$b', '$b'] },
				dep: ['b'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
