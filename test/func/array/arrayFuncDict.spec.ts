import { expect } from 'chai';
import createDebug from 'debug';

import { arrayFuncDict as funcDict } from '../../../src/func/array';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Callback, Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import {
	arrayTyp,
	booleanTyp,
	callbackTyp,
	neverTyp,
	numberTyp,
	stringTyp,
} from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:arrayFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('arrayFuncDict', () => {
	const n: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };
	const a: Expr = { typ: arrayTyp(neverTyp()), val: '$a', dep: ['a'] };
	const an: Expr = { typ: arrayTyp(numberTyp()), val: '$an', dep: ['an'] };
	const as: Expr = { typ: arrayTyp(stringTyp()), val: '$as', dep: ['as'] };

	const successTestCases: TestCase[] = [
		{
			description: 'call ARRAYELEMAT with valid args (number[])',
			funcKey: 'ARRAYELEMAT',
			args: [an, n],
			expr: {
				typ: { type: 'number' },
				val: { $arrayElemAt: ['$an', '$n'] },
				dep: ['an', 'n'],
			},
		},
		{
			description: 'call ARRAYELEMAT with valid args (string[])',
			funcKey: 'ARRAYELEMAT',
			args: [as, n],
			expr: {
				typ: { type: 'string' },
				val: { $arrayElemAt: ['$as', '$n'] },
				dep: ['as', 'n'],
			},
		},
		{
			description:
				'call CONCATARRAYS with valid args (number[], number[])',
			funcKey: 'CONCATARRAYS',
			args: [an, an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $concatArrays: ['$an', '$an'] },
				dep: ['an'],
			},
		},
		{
			description:
				'call CONCATARRAYS with valid args (string[], string[])',
			funcKey: 'CONCATARRAYS',
			args: [as, as],
			expr: {
				typ: { type: 'array', items: { type: 'string' } },
				val: { $concatArrays: ['$as', '$as'] },
				dep: ['as'],
			},
		},
		{
			description:
				'call CONCATARRAYS with valid args (number[], number[], number[])',
			funcKey: 'CONCATARRAYS',
			args: [an, an, an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $concatArrays: ['$an', '$an', '$an'] },
				dep: ['an'],
			},
		},
		{
			description:
				'call CONCATARRAYS with valid args (string[], number[])',
			funcKey: 'CONCATARRAYS',
			args: [as, an],
			expr: {
				typ: { type: 'array', items: { type: 'unknown' } },
				val: { $concatArrays: ['$as', '$an'] },
				dep: ['as', 'an'],
			},
		},
		{
			description: 'call CONCATARRAYS with valid args ([], number[])',
			funcKey: 'CONCATARRAYS',
			args: [a, an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $concatArrays: ['$a', '$an'] },
				dep: ['a', 'an'],
			},
		},
		{
			description: 'call IN with valid args (number, number[])',
			funcKey: 'IN',
			args: [n, an],
			expr: {
				typ: { type: 'boolean' },
				val: { $in: ['$n', '$an'] },
				dep: ['n', 'an'],
			},
		},
		{
			description: 'call IN with valid args (number, string[])',
			funcKey: 'IN',
			args: [n, as],
			expr: {
				typ: { type: 'boolean' },
				val: { $in: ['$n', '$as'] },
				dep: ['n', 'as'],
			},
		},
		{
			description: 'call INDEXOFARRAY with valid args (number[], number)',
			funcKey: 'INDEXOFARRAY',
			args: [an, n],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfArray: ['$an', '$n'] },
				dep: ['an', 'n'],
			},
		},
		{
			description: 'call INDEXOFARRAY with valid args (string[], number)',
			funcKey: 'INDEXOFARRAY',
			args: [as, n],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfArray: ['$as', '$n'] },
				dep: ['as', 'n'],
			},
		},
		{
			description:
				'call INDEXOFARRAY with valid args (string[], number, number)',
			funcKey: 'INDEXOFARRAY',
			args: [as, n, n],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfArray: ['$as', '$n', '$n'] },
				dep: ['as', 'n'],
			},
		},
		{
			description:
				'call INDEXOFARRAY with valid args (string[], number, number, number)',
			funcKey: 'INDEXOFARRAY',
			args: [as, n, n, n],
			expr: {
				typ: { type: 'number' },
				val: { $indexOfArray: ['$as', '$n', '$n', '$n'] },
				dep: ['as', 'n'],
			},
		},
		{
			description: 'call ISARRAY with valid args (number[])',
			funcKey: 'ISARRAY',
			args: [an],
			expr: {
				typ: { type: 'boolean' },
				val: { $isArray: '$an' },
				dep: ['an'],
			},
		},
		{
			description: 'call ISARRAY with valid args (number)',
			funcKey: 'ISARRAY',
			args: [n],
			expr: {
				typ: { type: 'boolean' },
				val: { $isArray: '$n' },
				dep: ['n'],
			},
		},
		{
			description: 'call RANGE with valid args (number, number)',
			funcKey: 'RANGE',
			args: [n, n],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $range: ['$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call RANGE with valid args (number, number, number)',
			funcKey: 'RANGE',
			args: [n, n, n],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $range: ['$n', '$n', '$n'] },
				dep: ['n'],
			},
		},
		{
			description: 'call REVERSEARRAY with valid args (number[])',
			funcKey: 'REVERSEARRAY',
			args: [an],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $reverseArray: '$an' },
				dep: ['an'],
			},
		},
		{
			description: 'call REVERSEARRAY with valid args (string[])',
			funcKey: 'REVERSEARRAY',
			args: [as],
			expr: {
				typ: { type: 'array', items: { type: 'string' } },
				val: { $reverseArray: '$as' },
				dep: ['as'],
			},
		},
		{
			description: 'call SIZE with valid args (number[])',
			funcKey: 'SIZE',
			args: [an],
			expr: {
				typ: { type: 'number' },
				val: { $size: '$an' },
				dep: ['an'],
			},
		},
		{
			description: 'call SIZE with valid args (string[])',
			funcKey: 'SIZE',
			args: [as],
			expr: {
				typ: { type: 'number' },
				val: { $size: '$as' },
				dep: ['as'],
			},
		},
		{
			description: 'call SLICE with valid args (number[], number)',
			funcKey: 'SLICE',
			args: [an, n],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $slice: ['$an', '$n'] },
				dep: ['an', 'n'],
			},
		},
		{
			description:
				'call SLICE with valid args (string[], number, number)',
			funcKey: 'SLICE',
			args: [as, n, n],
			expr: {
				typ: { type: 'array', items: { type: 'string' } },
				val: { $slice: ['$as', '$n', '$n'] },
				dep: ['as', 'n'],
			},
		},
		{
			description: 'call ZIP with valid args (number[], number[])',
			funcKey: 'ZIP',
			args: [an, an],
			expr: {
				typ: {
					type: 'array',
					items: { type: 'array', items: { type: 'number' } },
				},
				val: { $zip: { inputs: ['$an', '$an'] } },
				dep: ['an'],
			},
		},
		{
			description: 'call ZIP with valid args (string[], string[])',
			funcKey: 'ZIP',
			args: [as, as],
			expr: {
				typ: {
					type: 'array',
					items: { type: 'array', items: { type: 'string' } },
				},
				val: { $zip: { inputs: ['$as', '$as'] } },
				dep: ['as'],
			},
		},
		{
			description:
				'call ZIP with valid args (number[], number[], number[])',
			funcKey: 'ZIP',
			args: [an, an, an],
			expr: {
				typ: {
					type: 'array',
					items: { type: 'array', items: { type: 'number' } },
				},
				val: { $zip: { inputs: ['$an', '$an', '$an'] } },
				dep: ['an'],
			},
		},
		{
			description: 'call ZIP with valid args (number[], string[])',
			funcKey: 'ZIP',
			args: [an, as],
			expr: {
				typ: {
					type: 'array',
					items: { type: 'array', items: { type: 'unknown' } },
				},
				val: { $zip: { inputs: ['$an', '$as'] } },
				dep: ['an', 'as'],
			},
		},
		{
			description: 'call ZIP with valid args ([], string[])',
			funcKey: 'ZIP',
			args: [a, as],
			expr: {
				typ: {
					type: 'array',
					items: { type: 'array', items: { type: 'string' } },
				},
				val: { $zip: { inputs: ['$a', '$as'] } },
				dep: ['a', 'as'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('ARRAYELEMAT', () => {
	const varExpr: Expr = { typ: arrayTyp(numberTyp()), val: '$a', dep: ['a'] };
	const literalExpr: Expr = {
		typ: arrayTyp(
			[numberTyp(true), numberTyp(true), numberTyp(true)],
			true
		),
		val: [1, 2, 3],
		dep: [[], [], []],
	};
	const nestedLiteralExpr: Expr = {
		typ: arrayTyp(
			[
				arrayTyp([numberTyp(true), numberTyp(true)], true),
				arrayTyp([numberTyp()], true),
			],
			true
		),
		val: [[1, 2], ['$n']],
		dep: [[[], []], [['n']]],
	};
	const oneExpr: Expr = { typ: numberTyp(true), val: 1, dep: [] };
	const numberVarExpr: Expr = { typ: numberTyp(), val: '$n', dep: ['n'] };

	const successTestCases: TestCase[] = [
		{
			description:
				'call with variable expr (array) and variable expr (number)',
			funcKey: 'ARRAYELEMAT',
			args: [varExpr, numberVarExpr],
			expr: {
				typ: { type: 'number' },
				val: { $arrayElemAt: ['$a', '$n'] },
				dep: ['a', 'n'],
			},
		},
		{
			description:
				'call with variable expr (array) and literal expr (number)',
			funcKey: 'ARRAYELEMAT',
			args: [varExpr, oneExpr],
			expr: {
				typ: { type: 'number' },
				val: { $arrayElemAt: ['$a', 1] },
				dep: ['a'],
			},
		},
		{
			description:
				'call with literal expr (array) and variable expr (number)',
			funcKey: 'ARRAYELEMAT',
			args: [literalExpr, numberVarExpr],
			expr: {
				typ: { type: 'number' },
				val: { $arrayElemAt: [[1, 2, 3], '$n'] },
				dep: ['n'],
			},
		},
		{
			description:
				'call with literal expr (array) and literal expr (number)',
			funcKey: 'ARRAYELEMAT',
			args: [literalExpr, oneExpr],
			expr: {
				typ: { type: 'number', literal: true },
				val: 2,
				dep: [],
			},
		},
		{
			description:
				'call with nested literal expr (array) and literal expr (number)',
			funcKey: 'ARRAYELEMAT',
			args: [nestedLiteralExpr, oneExpr],
			expr: {
				typ: {
					type: 'array',
					items: [{ type: 'number' }],
					literal: true,
				},
				val: ['$n'],
				dep: [['n']],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('CONCATARRAYS', () => {
	const varExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: '$a1',
		dep: ['a1'],
	};
	const literalExpr1: Expr = {
		typ: arrayTyp(
			[numberTyp(true), numberTyp(true), numberTyp(true)],
			true
		),
		val: [1, 2, 3],
		dep: [[], [], []],
	};
	const literalExpr2: Expr = {
		typ: arrayTyp([numberTyp(true), numberTyp(true)], true),
		val: [4, 5],
		dep: [[], []],
	};
	const concatArraysOpExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: { $concatArrays: ['$a1', '$a2'] },
		dep: ['a1', 'a2'],
	};
	const nestedConcatArraysOpExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: {
			$concatArrays: [
				[6, 7],
				'$a3',
				{ $concatArrays: ['$a4', [1, 2, 3]] },
			],
		},
		dep: ['a3', 'a4'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with variable expr and concatArraysOp expr',
			funcKey: 'CONCATARRAYS',
			args: [varExpr, concatArraysOpExpr],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $concatArrays: ['$a1', '$a1', '$a2'] },
				dep: ['a1', 'a2'],
			},
		},
		{
			description:
				'call with variable expr and nested concatArraysOp expr',
			funcKey: 'CONCATARRAYS',
			args: [varExpr, nestedConcatArraysOpExpr],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: {
					$concatArrays: ['$a1', [6, 7], '$a3', '$a4', [1, 2, 3]],
				},
				dep: ['a1', 'a3', 'a4'],
			},
		},
		{
			description: 'call with literal exprs',
			args: [literalExpr1, literalExpr2, literalExpr1],
			funcKey: 'CONCATARRAYS',
			expr: {
				typ: {
					type: 'array',
					items: [
						{ type: 'number', literal: true },
						{ type: 'number', literal: true },
						{ type: 'number', literal: true },
						{ type: 'number', literal: true },
						{ type: 'number', literal: true },
						{ type: 'number', literal: true },
						{ type: 'number', literal: true },
						{ type: 'number', literal: true },
					],
					literal: true,
				},
				val: [1, 2, 3, 4, 5, 1, 2, 3],
				dep: [[], [], [], [], [], [], [], []],
			},
		},
		{
			description: 'call with literal exprs and variable expr',
			funcKey: 'CONCATARRAYS',
			args: [literalExpr1, varExpr, literalExpr2, literalExpr2],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $concatArrays: [[1, 2, 3], '$a1', [4, 5, 4, 5]] },
				dep: ['a1'],
			},
		},
		{
			description:
				'call with literal exprs and nested concatArraysOp expr',
			funcKey: 'CONCATARRAYS',
			args: [
				literalExpr1,
				nestedConcatArraysOpExpr,
				literalExpr2,
				literalExpr2,
			],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: {
					$concatArrays: [
						[1, 2, 3, 6, 7],
						'$a3',
						'$a4',
						[1, 2, 3, 4, 5, 4, 5],
					],
				},
				dep: ['a3', 'a4'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('MAP', () => {
	const varExpr: Expr = {
		typ: arrayTyp(numberTyp()),
		val: '$a',
		dep: ['a'],
	};
	const literalExpr: Expr = {
		typ: arrayTyp(
			[numberTyp(true), numberTyp(true), numberTyp(true)],
			true
		),
		val: [1, 2, 3],
		dep: [[], [], []],
	};
	const nestedLiteralExpr: Expr = {
		typ: arrayTyp(
			[
				arrayTyp([numberTyp(true), numberTyp(true)], true),
				arrayTyp([numberTyp()], true),
			],
			true
		),
		val: [[1, 2], ['$n']],
		dep: [[[], []], [['n']]],
	};
	const plusOneCallback: Callback = {
		typ: callbackTyp([numberTyp()], numberTyp()),
		val: { argKeys: ['item'], in: { $add: ['$$item', 1] } },
		dep: [],
	};
	const plusNCallback: Callback = {
		typ: callbackTyp([numberTyp()], numberTyp()),
		val: { argKeys: ['item'], in: { $add: ['$$item', '$n'] } },
		dep: ['n'],
	};
	const concatACallback: Callback = {
		typ: callbackTyp([arrayTyp(numberTyp())], arrayTyp(numberTyp())),
		val: { argKeys: ['item'], in: { $concatArrays: ['$$item', '$a'] } },
		dep: ['a'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with variable expr (array) and plusOneCallback',
			funcKey: 'MAP',
			args: [varExpr, plusOneCallback],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: {
					$map: {
						input: '$a',
						as: 'item',
						in: { $add: ['$$item', 1] },
					},
				},
				dep: ['a'],
			},
		},
		{
			description: 'call with variable expr (array) and plusNCallback',
			funcKey: 'MAP',
			args: [varExpr, plusNCallback],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: {
					$map: {
						input: '$a',
						as: 'item',
						in: { $add: ['$$item', '$n'] },
					},
				},
				dep: ['a', 'n'],
			},
		},
		{
			description: 'call with literal expr (array) and plusOneCallback',
			funcKey: 'MAP',
			args: [literalExpr, plusOneCallback],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: {
					$map: {
						input: [1, 2, 3],
						as: 'item',
						in: { $add: ['$$item', 1] },
					},
				},
				dep: [],
			},
		},
		{
			description: 'call with literal expr (array) and plusNCallback',
			funcKey: 'MAP',
			args: [literalExpr, plusNCallback],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: {
					$map: {
						input: [1, 2, 3],
						as: 'item',
						in: { $add: ['$$item', '$n'] },
					},
				},
				dep: ['n'],
			},
		},
		{
			description:
				'call with nested literal expr (array) and concatACallback',
			funcKey: 'MAP',
			args: [nestedLiteralExpr, concatACallback],
			expr: {
				typ: {
					type: 'array',
					items: { type: 'array', items: { type: 'number' } },
				},
				val: {
					$map: {
						input: [[1, 2], ['$n']],
						as: 'item',
						in: { $concatArrays: ['$$item', '$a'] },
					},
				},
				dep: ['n', 'a'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('REVERSEARRAY', () => {
	const varExpr: Expr = { typ: arrayTyp(numberTyp()), val: '$a', dep: ['a'] };
	const literalExpr: Expr = {
		typ: arrayTyp(
			[numberTyp(true), stringTyp(true), booleanTyp(true)],
			true
		),
		val: [1, '2', true],
		dep: [[], [], []],
	};
	const nestedLiteralExpr: Expr = {
		typ: arrayTyp(
			[
				arrayTyp([numberTyp(true), numberTyp(true)], true),
				arrayTyp([numberTyp()], true),
			],
			true
		),
		val: [[1, 2], ['$n']],
		dep: [[[], []], [['n']]],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call with variable expr',
			funcKey: 'REVERSEARRAY',
			args: [varExpr],
			expr: {
				typ: { type: 'array', items: { type: 'number' } },
				val: { $reverseArray: '$a' },
				dep: ['a'],
			},
		},
		{
			description: 'call with literal expr',
			funcKey: 'REVERSEARRAY',
			args: [literalExpr],
			expr: {
				typ: {
					type: 'array',
					items: [
						{ type: 'boolean', literal: true },
						{ type: 'string', literal: true },
						{ type: 'number', literal: true },
					],
					literal: true,
				},
				val: [true, '2', 1],
				dep: [[], [], []],
			},
		},
		{
			description: 'call with nested literal expr',
			funcKey: 'REVERSEARRAY',
			args: [nestedLiteralExpr],
			expr: {
				typ: {
					type: 'array',
					items: [
						{
							type: 'array',
							items: [{ type: 'number' }],
							literal: true,
						},
						{
							type: 'array',
							items: [
								{ type: 'number', literal: true },
								{ type: 'number', literal: true },
							],
							literal: true,
						},
					],
					literal: true,
				},
				val: [['$n'], [1, 2]],
				dep: [[['n']], [[], []]],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
