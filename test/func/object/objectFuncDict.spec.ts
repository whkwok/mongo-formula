import { expect } from 'chai';
import createDebug from 'debug';

import { objectFuncDict as funcDict } from '../../../src/func/object';
import { defaultParserOptions } from '../../../src/MongoFormulaParser';
import { Expr, ExtendedExpr } from '../../../src/types';
import { callFunc } from '../../../src/util/func';
import {
	anyTyp,
	booleanTyp,
	neverTyp,
	numberTyp,
	objectTyp,
	stringTyp,
} from '../../../src/util/typ';

interface TestCase {
	description: string;
	funcKey: keyof typeof funcDict;
	args: ExtendedExpr[];
	expr?: Expr;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

const yy = {
	funcDict,
	stageDict: {},
	scopeStack: [],
	options: defaultParserOptions,
};

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:objectFuncDict-test-${title}-${index}`
			);
			const {
				description,
				funcKey,
				args,
				expr,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _expr: Expr | undefined;
				try {
					_expr = callFunc(funcKey, args, yy);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_expr).to.deep.eq(expr);
				}
			});
		});
	});
}

describe('objectFuncDict', () => {
	const o1: Expr = {
		typ: objectTyp({ a: numberTyp(), b: numberTyp() }),
		val: '$o1',
		dep: ['o1'],
	};
	const o2: Expr = {
		typ: objectTyp({ b: stringTyp(), c: stringTyp() }),
		val: '$o2',
		dep: ['o2'],
	};
	const o3: Expr = {
		typ: objectTyp({ d: booleanTyp(), e: booleanTyp() }),
		val: '$o3',
		dep: ['o3'],
	};

	const successTestCases: TestCase[] = [
		{
			description: 'call MERGEOBJECTS with valid args (object, object)',
			funcKey: 'MERGEOBJECTS',
			args: [o1, o2],
			expr: {
				typ: {
					type: 'object',
					properties: {
						a: { type: 'number' },
						b: { type: 'string' },
						c: { type: 'string' },
					},
				},
				val: { $mergeObjects: ['$o1', '$o2'] },
				dep: ['o1', 'o2'],
			},
		},
		{
			description:
				'call MERGEOBJECTS with valid args (object, object, object)',
			funcKey: 'MERGEOBJECTS',
			args: [o2, o1, o3],
			expr: {
				typ: {
					type: 'object',
					properties: {
						a: { type: 'number' },
						b: { type: 'number' },
						c: { type: 'string' },
						d: { type: 'boolean' },
						e: { type: 'boolean' },
					},
				},
				val: { $mergeObjects: ['$o2', '$o1', '$o3'] },
				dep: ['o2', 'o1', 'o3'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('MERGEOBJECTS', () => {
	const varExpr: Expr = {
		typ: objectTyp({ a: numberTyp(), b: stringTyp() }),
		val: '$o1',
		dep: ['o1'],
	};
	const optionalVarExpr1: Expr = {
		typ: objectTyp({ a: numberTyp(), b: stringTyp(), c: stringTyp() }, [
			'a',
			'b',
			'c',
		]),
		val: '$p1',
		dep: ['p1'],
	};
	const optionalVarExpr2: Expr = {
		typ: objectTyp({ b: stringTyp(), c: stringTyp(), d: booleanTyp() }, [
			'c',
		]),
		val: '$p2',
		dep: ['p2'],
	};
	const additionalPropsVarExpr1: Expr = {
		typ: objectTyp(
			{ b: stringTyp(), c: stringTyp(), d: booleanTyp() },
			[],
			numberTyp()
		),
		val: '$p3',
		dep: ['p3'],
	};
	const additionalPropsVarExpr2: Expr = {
		typ: objectTyp(
			{ a: stringTyp(), c: stringTyp(), d: booleanTyp() },
			[],
			stringTyp()
		),
		val: '$p4',
		dep: ['p4'],
	};
	const literalExpr1: Expr = {
		typ: objectTyp(
			{ i: numberTyp(true), j: stringTyp(true) },
			undefined,
			undefined,
			true
		),
		val: { i: 123, j: 'abc' },
		dep: { i: [], j: [] },
	};
	const literalExpr2: Expr = {
		typ: objectTyp(
			{ j: numberTyp(true), k: stringTyp(true) },
			undefined,
			undefined,
			true
		),
		val: { j: 456, k: 'ghi' },
		dep: { j: [], k: [] },
	};
	const mergeObjectsOpExpr: Expr = {
		typ: objectTyp({ a: numberTyp(), b: stringTyp(), c: numberTyp() }),
		val: { $mergeObjects: ['$o1', '$o2'] },
		dep: ['o1', 'o2'],
	};
	const nestedMergeObjectsOpExpr: Expr = {
		typ: objectTyp({
			h: numberTyp(),
			d: numberTyp(),
			e: numberTyp(),
			f: stringTyp(),
			g: numberTyp(),
		}),
		val: {
			$mergeObjects: [
				{ h: 456 },
				'$o3',
				{ $mergeObjects: ['$o4', { g: 123 }] },
			],
		},
		dep: ['o3', 'o4'],
	};
	const anyExpr: Expr = { typ: anyTyp(), val: '$any', dep: ['any'] };
	const neverExpr: Expr = { typ: neverTyp(), val: '$never', dep: ['never'] };

	const successTestCases: TestCase[] = [
		{
			description: 'call with variable expr and mergeObjectsOp expr',
			funcKey: 'MERGEOBJECTS',
			args: [varExpr, mergeObjectsOpExpr],
			expr: {
				typ: {
					type: 'object',
					properties: {
						a: { type: 'number' },
						b: { type: 'string' },
						c: { type: 'number' },
					},
				},
				val: { $mergeObjects: ['$o1', '$o1', '$o2'] },
				dep: ['o1', 'o2'],
			},
		},
		{
			description:
				'call with variable expr and nested mergeobjectsOp expr',
			funcKey: 'MERGEOBJECTS',
			args: [varExpr, nestedMergeObjectsOpExpr],
			expr: {
				typ: {
					type: 'object',
					properties: {
						a: { type: 'number' },
						b: { type: 'string' },
						h: { type: 'number' },
						d: { type: 'number' },
						e: { type: 'number' },
						f: { type: 'string' },
						g: { type: 'number' },
					},
				},
				val: {
					$mergeObjects: [
						'$o1',
						{ h: 456 },
						'$o3',
						'$o4',
						{ g: 123 },
					],
				},
				dep: ['o1', 'o3', 'o4'],
			},
		},
		{
			description: 'call with literal exprs',
			funcKey: 'MERGEOBJECTS',
			args: [literalExpr1, literalExpr2],
			expr: {
				typ: {
					type: 'object',
					properties: {
						i: { type: 'number', literal: true },
						j: { type: 'number', literal: true },
						k: { type: 'string', literal: true },
					},
					literal: true,
				},
				val: { i: 123, j: 456, k: 'ghi' },
				dep: { i: [], j: [], k: [] },
			},
		},
		{
			description: 'call with literal exprs and variable expr',
			funcKey: 'MERGEOBJECTS',
			args: [literalExpr1, varExpr, literalExpr2],
			expr: {
				typ: {
					type: 'object',
					properties: {
						i: { type: 'number' },
						j: { type: 'number' },
						a: { type: 'number' },
						b: { type: 'string' },
						k: { type: 'string' },
					},
				},
				val: {
					$mergeObjects: [
						{ i: 123, j: 'abc' },
						'$o1',
						{ j: 456, k: 'ghi' },
					],
				},
				dep: ['o1'],
			},
		},
		{
			description:
				'call with literal exprs and nested mergeObjectsOp expr',
			funcKey: 'MERGEOBJECTS',
			args: [literalExpr1, nestedMergeObjectsOpExpr, literalExpr2],
			expr: {
				typ: {
					type: 'object',
					properties: {
						i: { type: 'number' },
						j: { type: 'number' },
						h: { type: 'number' },
						d: { type: 'number' },
						e: { type: 'number' },
						f: { type: 'string' },
						g: { type: 'number' },
						k: { type: 'string' },
					},
				},
				val: {
					$mergeObjects: [
						{ i: 123, j: 'abc', h: 456 },
						'$o3',
						'$o4',
						{ g: 123, j: 456, k: 'ghi' },
					],
				},
				dep: ['o3', 'o4'],
			},
		},
		{
			description: 'call with variable exprs (optional properties)',
			funcKey: 'MERGEOBJECTS',
			args: [optionalVarExpr1, optionalVarExpr2],
			expr: {
				typ: {
					type: 'object',
					properties: {
						a: { type: 'number' },
						b: { type: 'string' },
						c: { type: 'string' },
						d: { type: 'boolean' },
					},
					optional: ['a', 'c'],
				},
				val: { $mergeObjects: ['$p1', '$p2'] },
				dep: ['p1', 'p2'],
			},
		},
		{
			description: 'call with variable exprs (addtional properties)',
			funcKey: 'MERGEOBJECTS',
			args: [additionalPropsVarExpr1, additionalPropsVarExpr2],
			expr: {
				typ: {
					type: 'object',
					properties: {
						a: { type: 'string' },
						b: { type: 'string' },
						c: { type: 'string' },
						d: { type: 'boolean' },
					},
					additionalProperties: { type: 'unknown' },
				},
				val: { $mergeObjects: ['$p3', '$p4'] },
				dep: ['p3', 'p4'],
			},
		},
		{
			description: 'call with variable expr and any expr',
			funcKey: 'MERGEOBJECTS',
			args: [varExpr, anyExpr],
			expr: {
				typ: { type: 'any' },
				val: { $mergeObjects: ['$o1', '$any'] },
				dep: ['o1', 'any'],
			},
		},
		{
			description: 'call with variable expr and never expr',
			funcKey: 'MERGEOBJECTS',
			args: [varExpr, neverExpr],
			expr: {
				typ: { type: 'unknown' },
				val: { $mergeObjects: ['$o1', '$never'] },
				dep: ['o1', 'never'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
