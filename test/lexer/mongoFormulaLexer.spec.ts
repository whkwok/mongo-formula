import { expect } from 'chai';
import createDebug from 'debug';

import { mongoFormulaLexer } from '../../src/lexer';

interface TestCase {
	description: string;
	input: string;
	tokens?: string[];
	vals?: any[];
	errExists?: boolean;
	errDetails?: any;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:mongoFormulaLexer-test-${title}-${index}`
			);
			const {
				description,
				input,
				tokens,
				vals,
				errExists,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any,
					_tokens: string[] = [],
					_vals: any[] = [];
				try {
					mongoFormulaLexer.setInput(input);
					let token: string | undefined;
					while ((token = mongoFormulaLexer.lex())) {
						if (token) {
							_tokens.push(token);
							_vals.push(mongoFormulaLexer.yytext);
						}
					}
				} catch (err) {
					_err = err;
				}
				if (errExists || errDetails) {
					expect(_err).to.exist;
					if (errDetails)
						expect(_err.details).to.be.deep.equal(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					if (tokens) expect(_tokens).to.be.deep.equal(tokens);
					if (vals) expect(_vals).to.be.deep.equal(vals);
				}
			});
		});
	});
}

describe('mongoFormulaLexer', () => {
	const successTestCases: TestCase[] = [
		{
			description: 'lex empty string',
			input: '',
			tokens: ['EOF'],
			vals: [undefined],
		},
		{
			description: 'ignore whitespace',
			input: '         ',
			tokens: ['EOF'],
			vals: [undefined],
		},
		{
			description: 'ignore whitespace, tabs and newlines',
			input: `

			`,
			tokens: ['EOF'],
			vals: [undefined],
		},
		{
			description: 'lex integer to NUM',
			input: '5',
			tokens: ['NUM', 'EOF'],
			vals: [5, undefined],
		},
		{
			description: 'lex decimal fraction to NUM',
			input: '5.12',
			tokens: ['NUM', 'EOF'],
			vals: [5.12, undefined],
		},
		{
			description: 'lex percentage to NUM',
			input: '5%',
			tokens: ['NUM', 'EOF'],
			vals: [5 / 100, undefined],
		},
		{
			description: 'lex exponential notation to NUM (with e)',
			input: '5e3',
			tokens: ['NUM', 'EOF'],
			vals: [5e3, undefined],
		},
		{
			description: 'lex exponential notation to NUM (with e)',
			input: '5e-3',
			tokens: ['NUM', 'EOF'],
			vals: [5e-3, undefined],
		},
		{
			description: 'lex exponential notation to NUM (with e)',
			input: '5e+3',
			tokens: ['NUM', 'EOF'],
			vals: [5e3, undefined],
		},
		{
			description: 'lex single quoted characters to STR',
			input: "'5.12'",
			tokens: ['STR_S', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, '5.12', undefined, undefined],
		},
		{
			description: 'lex double quoted characters to STR',
			input: '"5.12"',
			tokens: ['STR_S', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, '5.12', undefined, undefined],
		},
		{
			description: 'lex double quoted single quotes to STR',
			input: `"''"`,
			tokens: ['STR_S', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, `''`, undefined, undefined],
		},
		{
			description: 'lex single quoted double quotes to STR',
			input: `'""'`,
			tokens: ['STR_S', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, `""`, undefined, undefined],
		},
		{
			description:
				'lex single quoted single quotes with escape character to STR',
			input: `'\\'\\''`,
			tokens: ['STR_S', 'STR', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, `'`, `'`, undefined, undefined],
		},
		{
			description:
				'lex double quoted double quotes with escape character to STR',
			input: `"\\"\\""`,
			tokens: ['STR_S', 'STR', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, `"`, `"`, undefined, undefined],
		},
		{
			description: 'lex linebreak in single quotes to STR',
			input: `'a\nb\n'`,
			tokens: ['STR_S', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, 'a\nb\n', undefined, undefined],
		},
		{
			description: 'lex linebreak in double quotes to STR',
			input: `"a\nb\n"`,
			tokens: ['STR_S', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, 'a\nb\n', undefined, undefined],
		},
		{
			description: 'lex \\ in single quotes to STR',
			input: `'\\\\'`,
			tokens: ['STR_S', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, '\\', undefined, undefined],
		},
		{
			description: 'lex \\ in dobule quotes to STR',
			input: `"\\\\"`,
			tokens: ['STR_S', 'STR', 'STR_E', 'EOF'],
			vals: [undefined, '\\', undefined, undefined],
		},
		{
			description: 'lex true to BOOL',
			input: 'true',
			tokens: ['BOOL', 'EOF'],
			vals: [true, undefined],
		},
		{
			description: 'lex false to BOOL',
			input: 'false',
			tokens: ['BOOL', 'EOF'],
			vals: [false, undefined],
		},
		{
			description: 'lex null to NULL',
			input: 'null',
			tokens: ['NULL', 'EOF'],
			vals: [null, undefined],
		},
		{
			description: 'lex + to +',
			input: '+',
			tokens: ['+', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex - to -',
			input: '-',
			tokens: ['-', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex * to *',
			input: '*',
			tokens: ['*', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex / to /',
			input: '/',
			tokens: ['/', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ** to **',
			input: '**',
			tokens: ['**', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex % to %',
			input: '%',
			tokens: ['%', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ( to (',
			input: '(',
			tokens: ['(', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ) to )',
			input: ')',
			tokens: [')', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex { to {',
			input: '{',
			tokens: ['{', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex } to }',
			input: '}',
			tokens: ['}', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex [ to [',
			input: '[',
			tokens: ['[', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ] to ]',
			input: ']',
			tokens: [']', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex < to <',
			input: '<',
			tokens: ['<', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex > to >',
			input: '>',
			tokens: ['>', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex != to !=',
			input: '!=',
			tokens: ['!=', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex <= to <=',
			input: '<=',
			tokens: ['<=', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex >= to >=',
			input: '>=',
			tokens: ['>=', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex == to ==',
			input: '==',
			tokens: ['==', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ! to !',
			input: '!',
			tokens: ['!', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex && to &&',
			input: '&&',
			tokens: ['&&', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex || to ||',
			input: '||',
			tokens: ['||', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ? to ?',
			input: '?',
			tokens: ['?', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex : to :',
			input: ':',
			tokens: [':', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ?? to ??',
			input: '??',
			tokens: ['??', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex let to let',
			input: 'let',
			tokens: ['let', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex = to =',
			input: '=',
			tokens: ['=', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ; to ;',
			input: ';',
			tokens: [';', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex {&&} to {&&}',
			input: '{&&}',
			tokens: ['{&&}', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex {&!} to {&!}',
			input: '{&!}',
			tokens: ['{&!}', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex {||} to {||}',
			input: '{||}',
			tokens: ['{||}', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex {!=} to {!=}',
			input: '{!=}',
			tokens: ['{!=}', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex {==} to {==}',
			input: '{==}',
			tokens: ['{==}', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex {<=} to {<=}',
			input: '{<=}',
			tokens: ['{<=}', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex {>=} to {>=}',
			input: '{>=}',
			tokens: ['{>=}', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex , to ,',
			input: ',',
			tokens: [',', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex | to |',
			input: '|',
			tokens: ['|', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex . to .',
			input: '.',
			tokens: ['.', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ... to ...',
			input: '...',
			tokens: ['...', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex in to in',
			input: 'in',
			tokens: ['in', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex as to as',
			input: 'as',
			tokens: ['as', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex >> to >>',
			input: '>>',
			tokens: ['>>', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex => to =>',
			input: '=>',
			tokens: ['=>', 'EOF'],
			vals: [undefined, undefined],
		},
		{
			description: 'lex ( to CB_S when trailing pattern matches',
			input: '(x) => x',
			tokens: ['CB_S', 'KEY', ')', '=>', 'KEY', 'EOF'],
			vals: [undefined, 'x', undefined, undefined, 'x', undefined],
		},
		{
			description: 'lex characters following by ( to KEY',
			input: 'KEY()',
			tokens: ['KEY', '(', ')', 'EOF'],
			vals: ['KEY', undefined, undefined, undefined],
		},
		{
			description: 'lex non-reserved word characters to KEY',
			input: 'key',
			tokens: ['KEY', 'EOF'],
			vals: ['key', undefined],
		},
		{
			description: 'lex nested KEY',
			input: 'A(B() + C(D())',
			tokens: [
				'KEY',
				'(',
				'KEY',
				'(',
				')',
				'+',
				'KEY',
				'(',
				'KEY',
				'(',
				')',
				')',
				'EOF',
			],
			vals: [
				'A',
				undefined,
				'B',
				undefined,
				undefined,
				undefined,
				'C',
				undefined,
				'D',
				undefined,
				undefined,
				undefined,
				undefined,
			],
		},
		{
			description: 'lex dot notation',
			input: 'o.key1.key2',
			tokens: ['KEY', '.', 'KEY', '.', 'KEY', 'EOF'],
			vals: ['o', undefined, 'key1', undefined, 'key2', undefined],
		},
	];

	runTestCases('Success', successTestCases);

	const failTestCases: TestCase[] = [
		{
			description: 'throw error when lexing unrecognized character',
			input: '   #',
			errDetails: {
				pos: 3,
				char: '#',
			},
		},
	];

	runTestCases('Fail', failTestCases);
});
