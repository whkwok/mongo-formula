import { expect } from 'chai';
import createDebug from 'debug';

import { AssignableExpr, Expr, ObjectExpr } from '../../../src/types';
import { objectExprAccess } from '../../../src/util/expr';

interface TestCase {
	description: string;
	expr: AssignableExpr<ObjectExpr>;
	key: string;
	result: Expr | undefined;
}

const RESERVED_SUFFIX = '___';

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:objectExprAccess-test-${title}-${index}`
			);
			const { description, expr, key, result } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				const _result = objectExprAccess(expr, key, RESERVED_SUFFIX);
				expect(_result).to.be.deep.eq(result);
			});
		});
	});
}

describe('objectExprAccess', function () {
	const successTestCases: TestCase[] = [
		{
			description: 'access variable object expr existing properties',
			expr: {
				typ: {
					type: 'object',
					properties: { k: { type: 'number' } },
				},
				val: '$o',
				dep: ['o'],
			},
			key: 'k',
			result: { typ: { type: 'number' }, val: '$o.k', dep: ['o'] },
		},
		{
			description:
				'not access variable object expr with non-existing properties',
			expr: {
				typ: {
					type: 'object',
					properties: { k: { type: 'number' } },
				},
				val: '$o',
				dep: ['o'],
			},
			key: 'i',
			result: undefined,
		},
		{
			description:
				'access variable object expr with additional properties',
			expr: {
				typ: {
					type: 'object',
					properties: { k: { type: 'number' } },
					additionalProperties: { type: 'string' },
				},
				val: '$o',
				dep: ['o'],
			},
			key: 'i',
			result: { typ: { type: 'string' }, val: '$o.i', dep: ['o'] },
		},
		{
			description: 'access variable expr (any) with any properties',
			expr: {
				typ: { type: 'any' },
				val: '$any',
				dep: ['any'],
			},
			key: 'k',
			result: { typ: { type: 'any' }, val: '$any.k', dep: ['any'] },
		},
		{
			description: 'not access variable expr (never) with any properties',
			expr: {
				typ: { type: 'never' },
				val: '$never',
				dep: ['never'],
			},
			key: 'k',
			result: undefined,
		},
		{
			description: 'access literal object expr with existing properties',
			expr: {
				typ: {
					type: 'object',
					properties: { a: { type: 'number', literal: true } },
					literal: true,
				},
				val: { a: 1 },
				dep: { a: [] },
			},
			key: 'a',
			result: { typ: { type: 'number', literal: true }, val: 1, dep: [] },
		},
		{
			description:
				'not access literal object expr with non-existing properties',
			expr: {
				typ: {
					type: 'object',
					properties: { a: { type: 'number', literal: true } },
					literal: true,
				},
				val: { a: 1 },
				dep: { a: [] },
			},
			key: 'b',
			result: undefined,
		},
		{
			description:
				'access non-let-op object expr with existing properties',
			expr: {
				typ: {
					type: 'object',
					properties: {
						a: { type: 'number' },
						b: { type: 'boolean' },
					},
				},
				val: { $mergeObjects: [{ a: 1 }, '$o'] },
				dep: ['o'],
			},
			key: 'b',
			result: {
				typ: { type: 'boolean' },
				val: {
					$let: {
						vars: { o___: { $mergeObjects: [{ a: 1 }, '$o'] } },
						in: '$$o___.b',
					},
				},
				dep: ['o'],
			},
		},
		{
			description:
				'access object-access-let op object expr with existing properties',
			expr: {
				typ: {
					type: 'object',
					properties: { c: { type: 'string' } },
				},
				val: {
					$let: {
						vars: { o___: { $mergeObjects: [{ a: 1 }, '$o'] } },
						in: '$$o___.b',
					},
				},
				dep: ['o'],
			},
			key: 'c',
			result: {
				typ: { type: 'string' },
				val: {
					$let: {
						vars: { o___: { $mergeObjects: [{ a: 1 }, '$o'] } },
						in: '$$o___.b.c',
					},
				},
				dep: ['o'],
			},
		},
		{
			description:
				'access ordinary-let op object expr with existing properties',
			expr: {
				typ: {
					type: 'object',
					properties: { k: { type: 'number' } },
				},
				val: { $let: { vars: { obj: '$o' }, in: '$$obj' } },
				dep: ['o'],
			},
			key: 'k',
			result: {
				typ: { type: 'number' },
				val: {
					$let: {
						vars: {
							o___: {
								$let: { vars: { obj: '$o' }, in: '$$obj' },
							},
						},
						in: '$$o___.k',
					},
				},
				dep: ['o'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
