import { expect } from 'chai';
import createDebug from 'debug';

import { Typ } from '../../../src/types';
import { isAssignableTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	t0: Typ;
	t1: Typ;
	result: boolean;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:compareTyp-test-${title}-${index}`
			);
			const { description, t0, t1, result } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				const _result = isAssignableTyp(t0, t1);
				expect(_result).to.eq(result);
			});
		});
	});
}

describe('isAssignableTyp', function () {
	const successTestCases: TestCase[] = [
		{
			description: 'assert that number is assignable to number',
			t0: { type: 'number' },
			t1: { type: 'number' },
			result: true,
		},
		{
			description: 'assert that number is not assignable to string',
			t0: { type: 'number' },
			t1: { type: 'string' },
			result: false,
		},
		{
			description: 'assert that number is assignable to unknown',
			t0: { type: 'number' },
			t1: { type: 'unknown' },
			result: true,
		},
		{
			description: 'assert that number is not assignable to never',
			t0: { type: 'number' },
			t1: { type: 'never' },
			result: false,
		},
		{
			description: 'assert that never is assignable to number',
			t0: { type: 'never' },
			t1: { type: 'number' },
			result: true,
		},
		{
			description: 'assert that never is assignable to never',
			t0: { type: 'never' },
			t1: { type: 'never' },
			result: true,
		},
		{
			description: 'assert that unknown is not assignable to number',
			t0: { type: 'unknown' },
			t1: { type: 'number' },
			result: false,
		},
		{
			description: 'assert that any is assignable to number',
			t0: { type: 'any' },
			t1: { type: 'number' },
			result: true,
		},
		{
			description: 'assert that number is assignable to any',
			t0: { type: 'number' },
			t1: { type: 'any' },
			result: true,
		},
		{
			description: 'assert that numberL is assignable to number',
			t0: { type: 'number', literal: true },
			t1: { type: 'number' },
			result: true,
		},
		{
			description: 'assert that number is not assignable to numberL',
			t0: { type: 'number' },
			t1: { type: 'number', literal: true },
			result: false,
		},
		{
			description: 'assert that numberL is assignable to numberL',
			t0: { type: 'number', literal: true },
			t1: { type: 'number', literal: true },
			result: true,
		},
		{
			description: 'assert that number[] is assignable to number[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'number' } },
			result: true,
		},
		{
			description: 'assert that number[] not is assignable to string[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'string' } },
			result: false,
		},
		{
			description: 'assert that number[] is assignable to unknown[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'unknown' } },
			result: true,
		},
		{
			description: 'assert that number[] not is assignable to [unknown]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: [{ type: 'unknown' }], literal: true },
			result: false,
		},
		{
			description:
				'assert that [number, number] is assignable to unknown[]',
			t0: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'number' }],
				literal: true,
			},
			t1: { type: 'array', items: { type: 'unknown' } },
			result: true,
		},
		{
			description: 'assert that unknown[] is not assignable to number[]',
			t0: { type: 'array', items: { type: 'unknown' } },
			t1: { type: 'array', items: { type: 'number' } },
			result: false,
		},
		{
			description:
				'assert that { n: number, b: boolean } is assignable to { n: number, b: boolean }',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			result: true,
		},
		{
			description:
				'assert that { n: number, b: boolean, s: string } is assignable to { n: number, b: boolean }',
			t0: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
					s: { type: 'string' },
				},
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			result: true,
		},
		{
			description:
				'assert that { n: number, b: boolean } is not assignable to { n: number, b: boolean, s: string }',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			t1: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
					s: { type: 'string' },
				},
			},
			result: false,
		},
		{
			description:
				'assert that { n: number, b: boolean } is assignable to {}',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			t1: { type: 'object', properties: {} },
			result: true,
		},
		{
			description:
				'assert that {} is not assignable to { n: number, b: boolean }',
			t0: { type: 'object', properties: {} },
			t1: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
				},
			},
			result: false,
		},
		{
			description:
				'assert that { n: number, b: boolean } is assignable to { n?: number, b: boolean }',
			t0: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
				},
			},
			t1: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
				},
				optional: ['n'],
			},
			result: true,
		},
		{
			description:
				'assert that { n?: number, b: boolean } is not assignable to { n: number, b: boolean }',
			t0: {
				type: 'object',
				properties: {
					n: { type: 'number' },
					b: { type: 'boolean' },
				},
				optional: ['n'],
			},
			t1: {
				type: 'object',
				properties: {
					n: { type: 'number' },
					b: { type: 'boolean' },
				},
			},
			result: false,
		},
		{
			description:
				'assert that { n?: number, b: boolean } is not assignable to { n: number, b?: boolean }',
			t0: {
				type: 'object',
				properties: {
					n: { type: 'number' },
					b: { type: 'boolean' },
				},
				optional: ['n'],
			},
			t1: {
				type: 'object',
				properties: {
					n: { type: 'number' },
					b: { type: 'boolean' },
				},
				optional: ['b'],
			},
			result: false,
		},
		{
			description:
				'assert that { a: number } is not assignable to { a?: string }',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' } },
				optional: ['a'],
			},
			result: false,
		},
		{
			description:
				'assert that { a?: string } is not assignable to { a: number }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' } },
				optional: ['a'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' } },
			},
			result: false,
		},
		{
			description:
				'assert that { a: number } is assignable to { a: number, b?: string }',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'string' } },
				optional: ['b'],
			},
			result: true,
		},
		{
			description:
				'assert that { a: number, b?: string } is assignable to { a: number }',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'string' } },
				optional: ['b'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' } },
			},
			result: true,
		},
		{
			description:
				'assert that { a: string, + number } is assignable to { a: string }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' } },
				additionalProperties: { type: 'number' },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' } },
			},
			result: true,
		},
		{
			description:
				'assert that { a: string } is assignable to { a: string, + number }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' } },
				additionalProperties: { type: 'number' },
			},
			result: true,
		},
		{
			description:
				'assert that { a: string, + number } is not assignable to { a: string, b: number }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' } },
				additionalProperties: { type: 'number' },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'number' } },
			},
			result: false,
		},
		{
			description:
				'assert that { a: string, b: number } is assignable to { a: string, + number }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'number' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' } },
				additionalProperties: { type: 'number' },
			},
			result: true,
		},
		{
			description:
				'assert that { a: string, + number } is assignable to { a: string, b?: number }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' } },
				additionalProperties: { type: 'number' },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'number' } },
				optional: ['b'],
			},
			result: true,
		},
		{
			description:
				'assert that { a: string, b?: number } is assignable to { a: string, + number }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'number' } },
				optional: ['b'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' } },
				additionalProperties: { type: 'number' },
			},
			result: true,
		},
		{
			description:
				'assert that { a: string, + string } is not assignable to { a: string, b?: number }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' } },
				additionalProperties: { type: 'string' },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'number' } },
				optional: ['b'],
			},
			result: false,
		},
		{
			description:
				'assert that { a: string, b: number } is not assignable to { a: string, + string }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'number' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' } },
				additionalProperties: { type: 'string' },
			},
			result: false,
		},
		{
			description:
				'assert that { a: string, b?: number } is not assignable to { a: string, + string }',
			t0: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'number' } },
				optional: ['b'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' } },
				additionalProperties: { type: 'string' },
			},
			result: false,
		},
		{
			description:
				'assert that accumulator(number) is assignable to accumulator(unknown)',
			t0: {
				type: 'accumulator',
				in: { type: 'number' },
			},
			t1: {
				type: 'accumulator',
				in: { type: 'unknown' },
			},
			result: true,
		},
		{
			description:
				'assert that accumulator(unknown) is assignable to accumulator(number)',
			t0: {
				type: 'accumulator',
				in: { type: 'unknown' },
			},
			t1: {
				type: 'accumulator',
				in: { type: 'number' },
			},
			result: false,
		},
		{
			description:
				'assert that (number, string) => boolean is assignable to (number, string) => boolean',
			t0: {
				type: 'callback',
				args: [{ type: 'number' }, { type: 'string' }],
				in: { type: 'boolean' },
			},
			t1: {
				type: 'callback',
				args: [{ type: 'number' }, { type: 'string' }],
				in: { type: 'boolean' },
			},
			result: true,
		},
		{
			description:
				'assert that (unknown, unknown) => boolean is assignable to (number, string) => boolean',
			t0: {
				type: 'callback',
				args: [{ type: 'unknown' }, { type: 'unknown' }],
				in: { type: 'boolean' },
			},
			t1: {
				type: 'callback',
				args: [{ type: 'number' }, { type: 'string' }],
				in: { type: 'boolean' },
			},
			result: true,
		},
		{
			description:
				'assert that (number, string, date) => boolean is assignable to (number, string) => boolean',
			t0: {
				type: 'callback',
				args: [
					{ type: 'number' },
					{ type: 'string' },
					{ type: 'date' },
				],
				in: { type: 'boolean' },
			},
			t1: {
				type: 'callback',
				args: [{ type: 'number' }, { type: 'string' }],
				in: { type: 'boolean' },
			},
			result: true,
		},
		{
			description:
				'assert that (number) => boolean is not assignable to (number, string) => boolean',
			t0: {
				type: 'callback',
				args: [{ type: 'number' }],
				in: { type: 'boolean' },
			},
			t1: {
				type: 'callback',
				args: [{ type: 'number' }, { type: 'string' }],
				in: { type: 'boolean' },
			},
			result: false,
		},
		{
			description:
				'assert that (number, string) => unknown is not assignable to (number, string) => boolean',
			t0: {
				type: 'callback',
				args: [{ type: 'number' }, { type: 'string' }],
				in: { type: 'unknown' },
			},
			t1: {
				type: 'callback',
				args: [{ type: 'number' }, { type: 'string' }],
				in: { type: 'boolean' },
			},
			result: false,
		},
	];
	runTestCases('Success', successTestCases);
});
