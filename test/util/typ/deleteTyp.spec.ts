import { expect } from 'chai';
import createDebug from 'debug';

import { ObjectTyp } from '../../../src/types';
import { deletePropertyTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	typ: ObjectTyp;
	key: string;
	newTyp: ObjectTyp;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:deleteTyp-test-${title}-${index}`
			);
			const { description, typ, key, newTyp } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				deletePropertyTyp(typ, key);
				expect(typ).to.deep.eq(newTyp);
			});
		});
	});
}

describe('deletePropertyTyp', function () {
	const successTestCases: TestCase[] = [
		{
			description: 'delete prop of objectTyp (shallow existing prop)',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
				},
				optional: ['b', 'c'],
			},
			key: 'a',
			newTyp: {
				type: 'object',
				properties: { b: { type: 'string' }, c: { type: 'boolean' } },
				optional: ['b', 'c'],
			},
		},
		{
			description:
				'delete prop of objectTyp (shallow existing optional prop)',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
				},
				optional: ['b', 'c'],
			},
			key: 'b',
			newTyp: {
				type: 'object',
				properties: { a: { type: 'number' }, c: { type: 'boolean' } },
				optional: ['c'],
			},
		},
		{
			description: 'delete prop of objectTyp (deep existing prop)',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
					d: {
						type: 'object',
						properties: { e: { type: 'string' } },
					},
				},
				optional: ['b', 'c'],
			},
			key: 'd.e',
			newTyp: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
					d: { type: 'object', properties: {} },
				},
				optional: ['b', 'c'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
