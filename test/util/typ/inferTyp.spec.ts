import { expect } from 'chai';
import createDebug from 'debug';

import { Typ } from '../../../src/types';
import { inferTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	val: any;
	typ: Typ;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:inferTyp-test-${title}-${index}`
			);
			const { description, val, typ } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				const _typ = inferTyp(val);
				expect(_typ).to.deep.eq(typ);
			});
		});
	});
}

describe('inferTyp', function () {
	const successTestCases: TestCase[] = [
		{
			description: 'infer boolean as booleanTyp',
			val: true,
			typ: { type: 'boolean', literal: true },
		},
		{
			description: 'infer number as numberTyp',
			val: 123,
			typ: { type: 'number', literal: true },
		},
		{
			description: 'infer string as stringTyp',
			val: '$abc',
			typ: { type: 'string', literal: true },
		},
		{
			description: 'infer null as neverTyp',
			val: null,
			typ: { type: 'never' },
		},
		{
			description: 'infer date as dateTyp',
			val: new Date(),
			typ: { type: 'date', literal: true },
		},
		{
			description: 'infer empty array as arrayTyp',
			val: [],
			typ: {
				type: 'array',
				items: [],
				literal: true,
			},
		},
		{
			description: 'infer string array as arrayTyp',
			val: ['a', 'b'],
			typ: {
				type: 'array',
				items: [
					{ type: 'string', literal: true },
					{ type: 'string', literal: true },
				],
				literal: true,
			},
		},
		{
			description: 'infer number string array as arrayTyp',
			val: [123, 'a'],
			typ: {
				type: 'array',
				items: [
					{ type: 'number', literal: true },
					{ type: 'string', literal: true },
				],
				literal: true,
			},
		},
		{
			description: 'infer null string array as arrayTyp',
			val: [null, 'a'],
			typ: {
				type: 'array',
				items: [{ type: 'never' }, { type: 'string', literal: true }],
				literal: true,
			},
		},
		{
			description: 'infer nested string array as arrayTyp',
			val: [
				['a', 'b'],
				['a', 'b', 'c'],
			],
			typ: {
				type: 'array',
				items: [
					{
						type: 'array',
						items: [
							{ type: 'string', literal: true },
							{ type: 'string', literal: true },
						],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{ type: 'string', literal: true },
							{ type: 'string', literal: true },
							{ type: 'string', literal: true },
						],
						literal: true,
					},
				],
				literal: true,
			},
		},
		{
			description: 'infer empty object as objectTyp',
			val: {},
			typ: {
				type: 'object',
				properties: {},
				literal: true,
			},
		},
		{
			description: 'infer simple object as objectTyp',
			val: { b: true, n: 123, m: null },
			typ: {
				type: 'object',
				properties: {
					b: { type: 'boolean', literal: true },
					n: { type: 'number', literal: true },
					m: { type: 'never' },
				},
				literal: true,
			},
		},
		{
			description: 'infer nested object as objectTyp',
			val: { b: true, n: 123, m: null, o: { s: 'abc', d: new Date() } },
			typ: {
				type: 'object',
				properties: {
					b: { type: 'boolean', literal: true },
					n: { type: 'number', literal: true },
					m: { type: 'never' },
					o: {
						type: 'object',
						properties: {
							s: { type: 'string', literal: true },
							d: { type: 'date', literal: true },
						},
						literal: true,
					},
				},
				literal: true,
			},
		},
	];
	runTestCases('Success', successTestCases);
});
