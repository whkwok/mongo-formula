import { expect } from 'chai';
import createDebug from 'debug';

import { isTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	input: any;
	result: boolean;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:isTyp-test-${title}-${index}`
			);
			const { description, input, result } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				const _result = isTyp(input);
				expect(_result).to.eq(result);
			});
		});
	});
}

describe('isTyp', function () {
	const successTestCases: TestCase[] = [
		{
			description: 'assert that boolean is Typ',
			input: { type: 'boolean' },
			result: true,
		},
		{
			description: 'assert that 123 is not Typ',
			input: 123,
			result: false,
		},
		{
			description: 'assert that stringL is Typ',
			input: { type: 'string', literal: true },
			result: true,
		},
		{
			description: 'assert that number[] is Typ',
			input: { type: 'array', items: { type: 'number' } },
			result: true,
		},
		{
			description: 'assert that [number] is Typ',
			input: {
				type: 'array',
				items: [{ type: 'number' }],
				literal: true,
			},
			result: true,
		},
		{
			description: 'assert that {} is Typ',
			input: {
				type: 'object',
				properties: {},
			},
			result: true,
		},
		{
			description:
				'assert that { a: number, b: { c?: date }, + boolean } is Typ',
			input: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: {
						type: 'object',
						properties: { c: { type: 'date' } },
						optional: ['c'],
					},
				},
				additionalProperties: { type: 'boolean' },
			},
			result: true,
		},
	];
	runTestCases('Success', successTestCases);
});
