import { expect } from 'chai';
import createDebug from 'debug';

import { Typ } from '../../../src/types';
import { getDisplayTypStr } from '../../../src/util/typ';

interface TestCase {
	description: string;
	typ: Typ;
	typStr: string;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:getDisplayTypStr-test-${title}-${index}`
			);
			const { description, typ, typStr } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				const _typStr = getDisplayTypStr(typ);
				expect(_typStr).to.eq(typStr);
			});
		});
	});
}

describe('getDisplayTypStr', function () {
	const successTestCases: TestCase[] = [
		{
			description: 'get display typStr of anyTyp',
			typ: { type: 'any' },
			typStr: 'any',
		},
		{
			description: 'get display typStr of booleanTyp',
			typ: { type: 'boolean' },
			typStr: 'boolean',
		},
		{
			description: 'get display typStr of booleanTyp (literal)',
			typ: { type: 'boolean', literal: true },
			typStr: 'booleanL',
		},
		{
			description: 'get display typStr of dateTyp',
			typ: { type: 'date' },
			typStr: 'date',
		},
		{
			description: 'get display typStr of dateTyp (literal)',
			typ: { type: 'date', literal: true },
			typStr: 'dateL',
		},
		{
			description: 'get display typStr of neverTyp',
			typ: { type: 'never' },
			typStr: 'never',
		},
		{
			description: 'get display typStr of numberTyp',
			typ: { type: 'number' },
			typStr: 'number',
		},
		{
			description: 'get display typStr of numberTyp (literal)',
			typ: { type: 'number', literal: true },
			typStr: 'numberL',
		},
		{
			description: 'get display typStr of stringTyp',
			typ: { type: 'string' },
			typStr: 'string',
		},
		{
			description: 'get display typStr of stringTyp (literal)',
			typ: { type: 'string', literal: true },
			typStr: 'stringL',
		},
		{
			description: 'get display typStr of unknownTyp',
			typ: { type: 'unknown' },
			typStr: 'unknown',
		},
		{
			description: 'get display typStr of string arrayTyp',
			typ: { type: 'array', items: { type: 'string' } },
			typStr: 'string[]',
		},
		{
			description: 'get display typStr of string arrayTyp (literal)',
			typ: {
				type: 'array',
				items: [{ type: 'string' }, { type: 'string' }],
				literal: true,
			},
			typStr: '[string, string]',
		},
		{
			description: 'get display typStr of string array arrayTyp',
			typ: {
				type: 'array',
				items: { type: 'array', items: { type: 'string' } },
			},
			typStr: 'string[][]',
		},
		{
			description:
				'get display typStr of string array arrayTyp (literal)',
			typ: {
				type: 'array',
				items: [
					{ type: 'array', items: { type: 'string' } },
					{ type: 'array', items: { type: 'string' } },
				],
				literal: true,
			},
			typStr: '[string[], string[]]',
		},
		{
			description: 'get display typStr of empty objectTyp',
			typ: {
				type: 'object',
				properties: {},
			},
			typStr: '{}',
		},
		{
			description: 'get display typStr of empty objectTyp (literal)',
			typ: {
				type: 'object',
				properties: {},
				literal: true,
			},
			typStr: '{}L',
		},
		{
			description: 'get display typStr of simple objectTyp',
			typ: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			typStr: '{ a: number, b: boolean }',
		},
		{
			description: 'get display typStr of simple objectTyp (literal)',
			typ: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				literal: true,
			},
			typStr: '{ a: number, b: boolean }L',
		},
		{
			description: 'get display typStr of optional objectTyp',
			typ: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['a'],
			},
			typStr: '{ a?: number, b: boolean }',
		},
		{
			description:
				'get display typStr of objectTyp with additionalProperties',
			typ: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['a'],
				additionalProperties: { type: 'number' },
			},
			typStr: '{ a?: number, b: boolean, + number }',
		},
		{
			description: 'get display typStr of nested objectTyp',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
					o: { type: 'object', properties: { c: { type: 'date' } } },
				},
				optional: ['a'],
			},
			typStr: '{ a?: number, b: boolean, o: { c: date } }',
		},
		{
			description: 'get display typStr of nested objectTyp (literal)',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
					o: { type: 'object', properties: { c: { type: 'date' } } },
				},
				literal: true,
			},
			typStr: '{ a: number, b: boolean, o: { c: date } }L',
		},
		{
			description: 'get display typStr of callbackTyp',
			typ: {
				type: 'callback',
				args: [{ type: 'number' }, { type: 'boolean' }],
				in: { type: 'string' },
			},
			typStr: '(number, boolean) => string',
		},
		{
			description: 'get display typStr of accumulatorTyp',
			typ: {
				type: 'accumulator',
				in: { type: 'number' },
			},
			typStr: 'accumulator(number)',
		},
		{
			description: 'get display typStr of pipelineTyp',
			typ: { type: 'pipeline' },
			typStr: 'pipeline',
		},
	];
	runTestCases('Success', successTestCases);
});
