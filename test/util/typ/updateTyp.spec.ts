import { expect } from 'chai';
import createDebug from 'debug';

import { ObjectTyp, Typ } from '../../../src/types';
import {
	arrayTyp,
	toNonLiteralTyp,
	updatePropertyTyp,
} from '../../../src/util/typ';

interface TestCase {
	description: string;
	typ: ObjectTyp;
	key: string;
	propertyTyp: Typ | ((t?: Typ) => Typ | undefined);
	newTyp: ObjectTyp;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:updateTyp-test-${title}-${index}`
			);
			const { description, typ, key, propertyTyp, newTyp } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				updatePropertyTyp(typ, key, propertyTyp);
				expect(typ).to.deep.eq(newTyp);
			});
		});
	});
}

describe('updatePropertyTyp', function () {
	const successTestCases: TestCase[] = [
		{
			description:
				'update prop of objectTyp (shallow existing prop) (const propertyTyp)',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
				},
				optional: ['b', 'c'],
			},
			key: 'a',
			propertyTyp: { type: 'string' },
			newTyp: {
				type: 'object',
				properties: {
					a: { type: 'string' },
					b: { type: 'string' },
					c: { type: 'boolean' },
				},
				optional: ['b', 'c'],
			},
		},
		{
			description:
				'update prop of objectTyp (shallow existing prop) (function propertyTyp)',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
				},
				optional: ['b', 'c'],
			},
			key: 'a',
			propertyTyp: (t) => (t ? arrayTyp(toNonLiteralTyp(t)) : undefined),
			newTyp: {
				type: 'object',
				properties: {
					a: { type: 'array', items: { type: 'number' } },
					b: { type: 'string' },
					c: { type: 'boolean' },
				},
				optional: ['b', 'c'],
			},
		},
		{
			description:
				'update prop of objectTyp (non-existing prop) (const prop)',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
				},
				optional: ['b', 'c'],
			},
			key: 'd',
			propertyTyp: { type: 'number' },
			newTyp: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
					d: { type: 'number' },
				},
				optional: ['b', 'c'],
			},
		},
		{
			description:
				'update prop of objectTyp (deep existing prop) (const prop)',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
					d: {
						type: 'object',
						properties: { e: { type: 'string' } },
					},
				},
				optional: ['b', 'c'],
			},
			key: 'd.e',
			propertyTyp: { type: 'number' },
			newTyp: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
					d: {
						type: 'object',
						properties: { e: { type: 'number' } },
					},
				},
				optional: ['b', 'c'],
			},
		},
		{
			description:
				'update prop of objectTyp (deep non-existing prop) (const prop)',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
				},
				optional: ['b', 'c'],
			},
			key: 'd.e',
			propertyTyp: { type: 'number' },
			newTyp: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'string' },
					c: { type: 'boolean' },
					d: {
						type: 'object',
						properties: { e: { type: 'number' } },
					},
				},
				optional: ['b', 'c'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
