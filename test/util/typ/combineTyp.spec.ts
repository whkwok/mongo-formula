import { expect } from 'chai';
import createDebug from 'debug';

import { Typ } from '../../../src/types';
import { intersectTyp, unionTyp } from '../../../src/util/typ';

interface TestCase {
	combineTyp: (t0: Typ, t1: Typ) => Typ;
	description: string;
	t0: Typ;
	t1: Typ;
	result: Typ;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:combineTyp-test-${title}-${index}`
			);
			const { description, combineTyp, t0, t1, result } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				const _result1 = combineTyp(t0, t1);
				const _result2 = combineTyp(t1, t0); // Symmetric operator
				expect(_result1).to.deep.eq(result);
				expect(_result2).to.deep.eq(result);
			});
		});
	});
}

describe('unionTyp', function () {
	const successTestCases: TestCase[] = [
		{
			combineTyp: unionTyp,
			description: 'union number and number',
			t0: { type: 'number' },
			t1: { type: 'number' },
			result: { type: 'number' },
		},
		{
			combineTyp: unionTyp,
			description: 'union number and string',
			t0: { type: 'number' },
			t1: { type: 'string' },
			result: { type: 'unknown' },
		},
		{
			combineTyp: unionTyp,
			description: 'union number and unknown',
			t0: { type: 'number' },
			t1: { type: 'unknown' },
			result: { type: 'unknown' },
		},
		{
			combineTyp: unionTyp,
			description: 'union number and never',
			t0: { type: 'number' },
			t1: { type: 'never' },
			result: { type: 'number' },
		},
		{
			combineTyp: unionTyp,
			description: 'union number and any',
			t0: { type: 'number' },
			t1: { type: 'any' },
			result: { type: 'any' },
		},
		{
			combineTyp: unionTyp,
			description: 'union unknown and never',
			t0: { type: 'unknown' },
			t1: { type: 'never' },
			result: { type: 'unknown' },
		},
		{
			combineTyp: unionTyp,
			description: 'union unknown and any',
			t0: { type: 'unknown' },
			t1: { type: 'any' },
			result: { type: 'any' },
		},
		{
			combineTyp: unionTyp,
			description: 'union never and any',
			t0: { type: 'never' },
			t1: { type: 'any' },
			result: { type: 'any' },
		},
		{
			combineTyp: unionTyp,
			description: 'union numberL and never',
			t0: { type: 'number', literal: true },
			t1: { type: 'never' },
			result: { type: 'number' },
		},
		{
			combineTyp: unionTyp,
			description: 'union numberL and unknown',
			t0: { type: 'number', literal: true },
			t1: { type: 'unknown' },
			result: { type: 'unknown' },
		},
		{
			combineTyp: unionTyp,
			description: 'union number[] and number[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'number' } },
			result: { type: 'array', items: { type: 'number' } },
		},
		{
			combineTyp: unionTyp,
			description: 'union number[] and string[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'string' } },
			result: { type: 'array', items: { type: 'unknown' } },
		},
		{
			combineTyp: unionTyp,
			description: 'union number[] and never[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'never' } },
			result: { type: 'array', items: { type: 'number' } },
		},
		{
			combineTyp: unionTyp,
			description: 'union never[] and never[]',
			t0: { type: 'array', items: { type: 'never' } },
			t1: { type: 'array', items: { type: 'never' } },
			result: { type: 'array', items: { type: 'never' } },
		},
		{
			combineTyp: unionTyp,
			description: 'union number[] and any[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'any' } },
			result: { type: 'array', items: { type: 'any' } },
		},
		{
			combineTyp: unionTyp,
			description: 'union [number, number] and unknown[]',
			t0: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'number' }],
				literal: true,
			},
			t1: { type: 'array', items: { type: 'unknown' } },
			result: { type: 'array', items: { type: 'unknown' } },
		},
		{
			combineTyp: unionTyp,
			description: 'union [number, number] and [number, number]',
			t0: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'number' }],
				literal: true,
			},
			t1: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'number' }],
				literal: true,
			},
			result: { type: 'array', items: { type: 'number' } },
		},
		{
			combineTyp: unionTyp,
			description: 'union [number, string] and never[]',
			t0: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'string' }],
				literal: true,
			},
			t1: { type: 'array', items: { type: 'never' } },
			result: { type: 'array', items: { type: 'unknown' } },
		},
		{
			combineTyp: unionTyp,
			description: 'union [number, string] and unknown[]',
			t0: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'string' }],
				literal: true,
			},
			t1: { type: 'array', items: { type: 'unknown' } },
			result: { type: 'array', items: { type: 'unknown' } },
		},
		{
			combineTyp: unionTyp,
			description: 'union same objectTyps',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
		},
		{
			combineTyp: unionTyp,
			description:
				'union different objectTyps without conflicted properties',
			t0: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
					s: { type: 'string' },
				},
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
		},
		{
			combineTyp: unionTyp,
			description:
				'union different objectTyps with conflicted properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, m: { type: 'boolean' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, m: { type: 'string' } },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'number' }, m: { type: 'unknown' } },
			},
		},
		{
			combineTyp: unionTyp,
			description: 'union non-empty objectTyp and empty objectTyp',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			t1: { type: 'object', properties: {} },
			result: { type: 'object', properties: {} },
		},
		{
			combineTyp: unionTyp,
			description:
				'union different objectTyps with non-conflicted optional properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['b'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['a', 'b'],
			},
			result: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['a', 'b'],
			},
		},
		{
			combineTyp: unionTyp,
			description:
				'union different objectTyps with conflicted optional properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['a'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'boolean' } },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'unknown' }, b: { type: 'boolean' } },
				optional: ['a'],
			},
		},
		{
			combineTyp: unionTyp,
			description:
				'union different objectTyps with non-conflicted additional properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'string' },
				optional: ['a'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'string' },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'unknown' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'string' },
				optional: ['a'],
			},
		},
		{
			combineTyp: unionTyp,
			description:
				'union different objectTyps with conflicted additional properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'string' },
				optional: ['a'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'number' },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'unknown' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'unknown' },
				optional: ['a'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});

describe('intersectTyp', function () {
	const successTestCases: TestCase[] = [
		{
			combineTyp: intersectTyp,
			description: 'intersect number and number',
			t0: { type: 'number' },
			t1: { type: 'number' },
			result: { type: 'number' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect number and string',
			t0: { type: 'number' },
			t1: { type: 'string' },
			result: { type: 'never' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect number and unknown',
			t0: { type: 'number' },
			t1: { type: 'unknown' },
			result: { type: 'number' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect number and never',
			t0: { type: 'number' },
			t1: { type: 'never' },
			result: { type: 'never' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect number and any',
			t0: { type: 'number' },
			t1: { type: 'any' },
			result: { type: 'any' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect unknown and never',
			t0: { type: 'unknown' },
			t1: { type: 'never' },
			result: { type: 'never' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect unknown and any',
			t0: { type: 'unknown' },
			t1: { type: 'any' },
			result: { type: 'any' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect never and any',
			t0: { type: 'never' },
			t1: { type: 'any' },
			result: { type: 'any' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect numberL and never',
			t0: { type: 'number', literal: true },
			t1: { type: 'never' },
			result: { type: 'never' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect numberL and unknown',
			t0: { type: 'number', literal: true },
			t1: { type: 'unknown' },
			result: { type: 'number' },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect number[] and number[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'number' } },
			result: { type: 'array', items: { type: 'number' } },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect number[] and string[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'string' } },
			result: { type: 'array', items: { type: 'never' } },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect number[] and never[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'never' } },
			result: { type: 'array', items: { type: 'never' } },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect never[] and never[]',
			t0: { type: 'array', items: { type: 'never' } },
			t1: { type: 'array', items: { type: 'never' } },
			result: { type: 'array', items: { type: 'never' } },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect number[] and any[]',
			t0: { type: 'array', items: { type: 'number' } },
			t1: { type: 'array', items: { type: 'any' } },
			result: { type: 'array', items: { type: 'any' } },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect [number, number] and unknown[]',
			t0: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'number' }],
				literal: true,
			},
			t1: { type: 'array', items: { type: 'unknown' } },
			result: { type: 'array', items: { type: 'number' } },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect [number, number] and [number, number]',
			t0: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'number' }],
				literal: true,
			},
			t1: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'number' }],
				literal: true,
			},
			result: { type: 'array', items: { type: 'number' } },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect [number, string] and never[]',
			t0: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'string' }],
				literal: true,
			},
			t1: { type: 'array', items: { type: 'never' } },
			result: { type: 'array', items: { type: 'never' } },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect [number, string] and unknown[]',
			t0: {
				type: 'array',
				items: [{ type: 'number' }, { type: 'string' }],
				literal: true,
			},
			t1: { type: 'array', items: { type: 'unknown' } },
			result: { type: 'array', items: { type: 'never' } },
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect same objectTyps',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
		},
		{
			combineTyp: intersectTyp,
			description:
				'intersect different objectTyps without conflicted properties',
			t0: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
					s: { type: 'string' },
				},
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			result: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
					s: { type: 'string' },
				},
			},
		},
		{
			combineTyp: intersectTyp,
			description:
				'intersect different objectTyps with conflicted properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, m: { type: 'boolean' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, m: { type: 'string' } },
			},
			result: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					m: { type: 'never' },
				},
			},
		},
		{
			combineTyp: intersectTyp,
			description: 'intersect non-empty objectTyp and empty objectTyp',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
			},
		},
		{
			combineTyp: intersectTyp,
			description:
				'intersect different objectTyps with non-conflicted optional properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['b'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['a', 'b'],
			},
			result: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['b'],
			},
		},
		{
			combineTyp: intersectTyp,
			description:
				'intersect different objectTyps with conflicted optional properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				optional: ['a'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'boolean' } },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'never' }, b: { type: 'boolean' } },
			},
		},
		{
			combineTyp: intersectTyp,
			description:
				'intersect different objectTyps with non-conflicted additional properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'string' },
				optional: ['a'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'string' },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'never' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'string' },
			},
		},
		{
			combineTyp: intersectTyp,
			description:
				'intersect different objectTyps with conflicted additional properties',
			t0: {
				type: 'object',
				properties: { a: { type: 'number' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'string' },
				optional: ['a'],
			},
			t1: {
				type: 'object',
				properties: { a: { type: 'string' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'number' },
			},
			result: {
				type: 'object',
				properties: { a: { type: 'never' }, b: { type: 'boolean' } },
				additionalProperties: { type: 'never' },
			},
		},
	];
	runTestCases('Success', successTestCases);
});
