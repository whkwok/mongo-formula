import { expect } from 'chai';
import createDebug from 'debug';

import { NonLiteralTyp, Typ } from '../../../src/types';
import { toNonLiteralTyp } from '../../../src/util/typ';

interface TestCase {
	description: string;
	typ: Typ;
	newTyp: NonLiteralTyp;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:convertTyp-test-${title}-${index}`
			);
			const { description, typ, newTyp } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				const _newTyp = toNonLiteralTyp(typ);
				expect(_newTyp).to.deep.eq(newTyp);
			});
		});
	});
}

describe('toNonLiteralTyp', function () {
	const successTestCases: TestCase[] = [
		{
			description: 'convert any to any',
			typ: { type: 'any' },
			newTyp: { type: 'any' },
		},
		{
			description: 'convert never to never',
			typ: { type: 'never' },
			newTyp: { type: 'never' },
		},
		{
			description: 'convert unknown to unknown',
			typ: { type: 'unknown' },
			newTyp: { type: 'unknown' },
		},
		{
			description: 'convert boolean to boolean',
			typ: { type: 'boolean' },
			newTyp: { type: 'boolean' },
		},
		{
			description: 'convert booleanL to boolean',
			typ: { type: 'boolean', literal: true },
			newTyp: { type: 'boolean' },
		},
		{
			description: 'convert date to date',
			typ: { type: 'date' },
			newTyp: { type: 'date' },
		},
		{
			description: 'convert dateL to date',
			typ: { type: 'date', literal: true },
			newTyp: { type: 'date' },
		},
		{
			description: 'convert number to number',
			typ: { type: 'number' },
			newTyp: { type: 'number' },
		},
		{
			description: 'convert numberL to number',
			typ: { type: 'number', literal: true },
			newTyp: { type: 'number' },
		},
		{
			description: 'convert string to string',
			typ: { type: 'string' },
			newTyp: { type: 'string' },
		},
		{
			description: 'convert stringL to string',
			typ: { type: 'string', literal: true },
			newTyp: { type: 'string' },
		},
		{
			description: 'convert string[] to string[]',
			typ: { type: 'array', items: { type: 'string' } },
			newTyp: { type: 'array', items: { type: 'string' } },
		},
		{
			description: 'convert [string, string] to string[]',
			typ: {
				type: 'array',
				items: [{ type: 'string' }, { type: 'string' }],
				literal: true,
			},
			newTyp: { type: 'array', items: { type: 'string' } },
		},
		{
			description: 'convert [stringL, string] to string[]',
			typ: {
				type: 'array',
				items: [{ type: 'string', literal: true }, { type: 'string' }],
				literal: true,
			},
			newTyp: { type: 'array', items: { type: 'string' } },
		},
		{
			description: 'convert {} to {}',
			typ: { type: 'object', properties: {} },
			newTyp: { type: 'object', properties: {} },
		},
		{
			description: 'convert {}L to {}',
			typ: { type: 'object', properties: {}, literal: true },
			newTyp: { type: 'object', properties: {} },
		},
		{
			description:
				'convert { a: stringL, b?: numberL }L to { a: string, b?: number }',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'string', literal: true },
					b: { type: 'number', literal: true },
				},
				optional: ['b'],
				literal: true,
			},
			newTyp: {
				type: 'object',
				properties: {
					a: { type: 'string' },
					b: { type: 'number' },
				},
				optional: ['b'],
			},
		},
	];
	runTestCases('Success', successTestCases);
});
