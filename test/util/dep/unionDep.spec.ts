import { expect } from 'chai';
import createDebug from 'debug';

import { Dep } from '../../../src/types';
import { unionDep } from '../../../src/util/dep';

interface TestCase {
	description: string;
	deps: (Dep | undefined)[];
	result: string[];
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:unionDep-test-${title}-${index}`
			);
			const { description, deps, result } = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				const _result = unionDep(...deps);
				expect(_result).to.be.deep.eq(result);
			});
		});
	});
}

describe('unionDep', function () {
	const successTestCases: TestCase[] = [
		{
			description: 'union empty deps',
			deps: [[], [], []],
			result: [],
		},
		{
			description: 'union non-overlapped simple deps',
			deps: [['a'], ['b'], ['c']],
			result: ['a', 'b', 'c'],
		},
		{
			description: 'union overlapped simple deps',
			deps: [
				['a', 'b'],
				['b', 'c'],
				['c', 'd'],
			],
			result: ['a', 'b', 'c', 'd'],
		},
		{
			description: 'union simple deps with undefined',
			deps: [undefined, ['a'], ['a']],
			result: ['a'],
		},
		{
			description: 'union simple deps and inline array deps',
			deps: [
				['a', 'b'],
				[
					['a', 'c'],
					['d', 'e'],
				],
				[
					['f', 'e'],
					['e', 'g'],
				],
			],
			result: ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
		},
		{
			description: 'union simple deps and inline object deps',
			deps: [['a', 'b'], { a: ['t', 'u'], b: ['i', 'j'] }, { c: ['k'] }],
			result: ['a', 'b', 't', 'u', 'i', 'j', 'k'],
		},
		{
			description:
				'union simple deps, inline array deps, inline object deps',
			deps: [
				['a', 'b'],
				[['o', 'p'], ['q']],
				{ a: ['t', 'u'], b: ['i', 'j'] },
				{ c: ['k'], d: [['e', 'f'], ['g'], { e: ['h'] }] },
			],
			result: [
				'a',
				'b',
				'o',
				'p',
				'q',
				't',
				'u',
				'i',
				'j',
				'k',
				'e',
				'f',
				'g',
				'h',
			],
		},
	];
	runTestCases('Success', successTestCases);
});
