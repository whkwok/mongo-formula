import { expect } from 'chai';
import createDebug from 'debug';

import { ArgsSig, ExtendedExprLoc } from '../../../src/types';
import { matchArgsSigs } from '../../../src/util/arg';

interface TestCase {
	description: string;
	args: ExtendedExprLoc[];
	argsSigs: ArgsSig[];
	result?: ArgsSig[];
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:matchArgsSigs-test-${title}-${index}`
			);
			const {
				description,
				args,
				argsSigs,
				result,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any, _result: ArgsSig[] | undefined;
				try {
					_result = matchArgsSigs('test', args, argsSigs);
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					if (result) expect(_result).to.deep.eq(result);
				}
			});
		});
	});
}

describe('matchArgsSigs', function () {
	const successTestCases: TestCase[] = [
		{
			description: 'match [(string, string)] argsSigs with [string] args',
			args: [{ typ: { type: 'string' }, val: '$s', dep: ['s'] }],
			argsSigs: [{ typs: [{ type: 'string' }, { type: 'string' }] }],
			result: [{ typs: [{ type: 'string' }, { type: 'string' }] }],
		},
		{
			description:
				'match [(string, string)] argsSigs with [string, string] args',
			args: [
				{ typ: { type: 'string' }, val: '$s', dep: ['s'] },
				{ typ: { type: 'string' }, val: '$s', dep: ['s'] },
			],
			argsSigs: [{ typs: [{ type: 'string' }, { type: 'string' }] }],
			result: [{ typs: [{ type: 'string' }, { type: 'string' }] }],
		},
		{
			description:
				'match [(string, string), (number, string)] argsSigs with [string] args',
			args: [{ typ: { type: 'string' }, val: '$s', dep: ['s'] }],
			argsSigs: [
				{ typs: [{ type: 'string' }, { type: 'string' }] },
				{ typs: [{ type: 'number' }, { type: 'string' }] },
			],
			result: [{ typs: [{ type: 'string' }, { type: 'string' }] }],
		},
		{
			description:
				'match [(string, string), (string, number)] argsSigs with [string] args',
			args: [{ typ: { type: 'string' }, val: '$s', dep: ['s'] }],
			argsSigs: [
				{ typs: [{ type: 'string' }, { type: 'string' }] },
				{ typs: [{ type: 'string' }, { type: 'number' }] },
			],
			result: [
				{ typs: [{ type: 'string' }, { type: 'string' }] },
				{ typs: [{ type: 'string' }, { type: 'number' }] },
			],
		},
		{
			description:
				'match [(number, number, ...number)] argsSig with [number, number, number] args',
			args: [
				{ typ: { type: 'number' }, val: '$n', dep: ['n'] },
				{ typ: { type: 'number' }, val: '$n', dep: ['n'] },
				{ typ: { type: 'number' }, val: '$n', dep: ['n'] },
			],
			argsSigs: [
				{
					typs: [{ type: 'number' }, { type: 'number' }],
					spreadTyp: { type: 'number' },
				},
			],
			result: [
				{
					typs: [{ type: 'number' }, { type: 'number' }],
					spreadTyp: { type: 'number' },
				},
			],
		},
	];
	runTestCases('Success', successTestCases);

	const failTestCases: TestCase[] = [
		{
			description:
				'throw err when matching [(string, string)] argsSigs with [number] args',
			args: [{ typ: { type: 'number' }, val: '$n', dep: ['n'] }],
			argsSigs: [{ typs: [{ type: 'string' }, { type: 'string' }] }],
			errName: 'ExprTypErr',
			errDetails: {
				index: 0,
				receivedTyp: { type: 'number' },
				expectedTyps: [{ type: 'string' }],
			},
		},
		{
			description:
				'throw err when matching [(string, string)] argsSigs with [string, number] args',
			args: [
				{ typ: { type: 'string' }, val: '$s', dep: ['s'] },
				{ typ: { type: 'number' }, val: '$n', dep: ['n'] },
			],
			argsSigs: [{ typs: [{ type: 'string' }, { type: 'string' }] }],
			errName: 'ExprTypErr',
			errDetails: {
				index: 1,
				receivedTyp: { type: 'number' },
				expectedTyps: [{ type: 'string' }],
			},
		},
		{
			description:
				'throw err when matching [(number, number, ...number)] argsSig with [number, number, string] args',
			args: [
				{ typ: { type: 'number' }, val: '$n', dep: ['n'] },
				{ typ: { type: 'number' }, val: '$n', dep: ['n'] },
				{ typ: { type: 'string' }, val: '$s', dep: ['s'] },
			],
			argsSigs: [
				{
					typs: [{ type: 'number' }, { type: 'number' }],
					spreadTyp: { type: 'number' },
				},
			],
			errName: 'ExprTypErr',
			errDetails: {
				index: 2,
				receivedTyp: { type: 'string' },
				expectedTyps: [{ type: 'number' }],
			},
		},
	];
	runTestCases('Fail', failTestCases);
});
