import { expect } from 'chai';
import createDebug from 'debug';
import { Collection, MongoClient, ObjectId } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';

import {
	Dep,
	MongoFormulaParser,
	ParseResult,
	Schema,
	allFuncDict,
	allStageDict,
	anyTyp,
	isPipelineTyp,
} from '../src';

interface TestCase {
	description: string;
	compatMode?: boolean;
	input: string;
	output?: any;
	dep: Dep;
	errExists?: boolean;
}

const RESERVED_SUFFIX = '___';
const UNDEFINED_VAR_TYP = anyTyp();
const _id = new ObjectId();

describe('e2e', () => {
	const schema: Schema = {
		num: { type: 'number' },
		bool: { type: 'boolean' },
		dat: { type: 'date' },
		str: { type: 'string' },
		arr: { type: 'array', items: { type: 'number' } },
		obj: {
			type: 'object',
			properties: { a: { type: 'number' }, b: { type: 'string' } },
			additionalProperties: { type: 'boolean' },
		},
	};
	const mongoFormulaParser = new MongoFormulaParser(
		schema,
		allFuncDict,
		allStageDict,
		{ reservedSuffix: RESERVED_SUFFIX, undefinedVarTyp: UNDEFINED_VAR_TYP }
	);
	let mongoServer: MongoMemoryServer;
	let mongoClient: MongoClient;
	let mongoCollection: Collection;

	function runTestCases(title: string, testCases: TestCase[]) {
		describe(title, () => {
			testCases.forEach((testCase: TestCase, index: number) => {
				const debug = createDebug(
					`mongo-formula:e2e-test-${title}-${index}`
				);
				const {
					description,
					compatMode = true,
					input,
					output,
					dep,
					errExists,
				} = testCase;
				const _description =
					'should be able to ' + description + ` [${index}]`;
				it(_description, async function () {
					debug(`--------  ${_description}  --------`);
					let _parsed: ParseResult | undefined,
						_err: any,
						_output: any,
						_dep: Dep | undefined;
					try {
						const parsed = mongoFormulaParser.parse(
							input,
							undefined,
							undefined,
							undefined,
							{ compatMode }
						);
						_parsed = parsed;
						_dep = _parsed.dep;
					} catch (err) {
						_err = err;
					}
					if (_parsed) {
						try {
							if (isPipelineTyp(_parsed.typ)) {
								_output = await mongoCollection
									.aggregate(_parsed.val)
									.toArray();
							} else {
								_output = (
									await mongoCollection
										.aggregate([
											{
												$addFields: {
													output: _parsed.val,
												},
											},
										])
										.toArray()
								)[0].output;
							}
						} catch (__err) {
							_err = __err;
						}
					}
					if (errExists) {
						expect(_err).to.exist;
					} else {
						if (_parsed) debug({ expr: _parsed });
						if (_err) debug({ errMsg: _err.message });
						expect(_err).to.not.exist;
						expect(_output).to.deep.eq(output);
						expect(_dep).to.deep.eq(dep);
					}
				});
			});
		});
	}

	before(async function () {
		this.timeout(60000);
		mongoServer = new MongoMemoryServer({
			binary: { version: 'latest' },
			instance: { storageEngine: 'wiredTiger' },
		});
		const uri = await mongoServer.getUri();
		mongoClient = new MongoClient(uri, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		});
		await mongoClient.connect();
		const db = mongoClient.db();
		mongoCollection = db.collection('docs');
		const doc = {
			_id,
			num: 1.234565656,
			str: 'string',
			nul: null,
			bool: true,
			dat: new Date('2019-01-02'),
			arr: [1, 3, 5, 7, 9, 11],
			obj: { a: 123, b: 'abc', c: true },
			one: 1,
			two: 2,
		};
		await mongoCollection.insertOne(doc);
	});

	after(async function () {
		await mongoClient.close();
		await mongoServer.stop();
	});

	const successTestCases: TestCase[] = [
		{
			description: 'get year from date',
			input: 'YEAR(dat)',
			output: 2019,
			dep: ['dat'],
		},
		{
			description: 'get null when safedivided by zero',
			input: 'SAFEDIVIDE(num, 0)',
			output: null,
			dep: ['num'],
		},
		{
			description: 'round number to integer',
			input: 'ROUND(num)',
			output: 1,
			dep: ['num'],
		},
		{
			description: 'round up number to integer',
			input: 'ROUNDUP(num)',
			output: 2,
			dep: ['num'],
		},
		{
			description: 'round down number to integer',
			input: 'ROUNDDOWN(num)',
			output: 1,
			dep: ['num'],
		},
		{
			description: 'round number to 1 dp',
			input: 'ROUND(num, 1)',
			output: 1.2,
			dep: ['num'],
		},
		{
			description: 'round number to 1 dp (non-compatMode)',
			compatMode: false,
			input: 'ROUND(num, 1)',
			output: 1.2,
			dep: ['num'],
		},
		{
			description: 'round up number to 1 dp',
			input: 'ROUNDUP(num, 1)',
			output: 1.3,
			dep: ['num'],
		},
		{
			description: 'round down number to 1 dp',
			input: 'ROUNDDOWN(num, 1)',
			output: 1.2,
			dep: ['num'],
		},
		{
			description: 'round number to 10 digit',
			input: 'ROUND(15, -1)',
			output: 20,
			dep: [],
		},
		{
			description: 'round up number to 10 digit',
			input: 'ROUNDUP(15, -1)',
			output: 20,
			dep: [],
		},
		{
			description: 'round down number to 10 digit',
			input: 'ROUNDDOWN(15, -1)',
			output: 10,
			dep: [],
		},
		{
			description: 'round number to nearest 2',
			input: 'MROUND(15, 2)',
			output: 16,
			dep: [],
		},
		{
			description: 'round number to nearest 2  (non-compatMode)',
			compatMode: false,
			input: 'MROUND(15, 2)',
			output: 16,
			dep: [],
		},
		{
			description: 'round up number to nearest 2',
			input: 'MROUNDUP(15, 2)',
			output: 16,
			dep: [],
		},
		{
			description: 'round down number to nearest 2',
			input: 'MROUNDDOWN(15, 2)',
			output: 14,
			dep: [],
		},
		{
			description: 'get one character from a string (non-negative index)',
			input: 'str[0]',
			output: 's',
			dep: ['str'],
		},
		{
			description: 'get one character from a string (negative index)',
			input: 'str[-1]',
			output: 'g',
			dep: ['str'],
		},
		{
			description: 'get one character from a string (out of bound index)',
			input: 'str[6]',
			output: '',
			dep: ['str'],
		},
		{
			description:
				'slice a substring from a string with two indices (non-negative indices)',
			input: 'str[2:4]',
			output: 'ri',
			dep: ['str'],
		},
		{
			description:
				'slice a substring from a string with two indices (negative indices)',
			input: 'str[-4:-1]',
			output: 'rin',
			dep: ['str'],
		},
		{
			description:
				'slice a substring from a string with two indices (out of bound indices)',
			input: 'str[8:10]',
			output: '',
			dep: ['str'],
		},
		{
			description:
				'slice a substring from a string with one end index (non-negative index)',
			input: 'str[:3]',
			output: 'str',
			dep: ['str'],
		},
		{
			description:
				'slice a substring from a string with one end index (negative index)',
			input: 'str[:-4]',
			output: 'st',
			dep: ['str'],
		},
		{
			description:
				'slice a substring from a string with one end index (out of bound index)',
			input: 'str[:10]',
			output: 'string',
			dep: ['str'],
		},
		{
			description:
				'slice a substring from a string with one start index (non-negative index)',
			input: 'str[3:]',
			output: 'ing',
			dep: ['str'],
		},
		{
			description:
				'slice a substring from a string with one start index (negative index)',
			input: 'str[-4:]',
			output: 'ring',
			dep: ['str'],
		},
		{
			description:
				'slice a substring from a string with one start index (out of bound index)',
			input: 'str[10:]',
			output: '',
			dep: ['str'],
		},
		{
			description: 'get one element from an array (non-negative index)',
			input: 'arr[0]',
			output: 1,
			dep: ['arr'],
		},
		{
			description: 'get one element from an array (negative index)',
			input: 'arr[-1]',
			output: 11,
			dep: ['arr'],
		},
		{
			description: 'get one element from an array (out of bound index)',
			input: 'arr[6]',
			output: undefined,
			dep: ['arr'],
		},
		{
			description: 'get one element from an array (variable index)',
			input: 'arr[one]',
			output: 3,
			dep: ['arr', 'one'],
		},
		{
			description:
				'slice an array with two indices (non-negative indices)',
			input: 'arr[2:4]',
			output: [5, 7],
			dep: ['arr'],
		},
		{
			description: 'slice an array with two indices (negative indices)',
			input: 'arr[-4:-1]',
			output: [5, 7, 9],
			dep: ['arr'],
		},
		{
			description:
				'slice an array with two indices (out of bound indices)',
			input: 'arr[8:10]',
			output: [],
			dep: ['arr'],
		},
		{
			description:
				'slice an array with one end index (non-negative index)',
			input: 'arr[:3]',
			output: [1, 3, 5],
			dep: ['arr'],
		},
		{
			description: 'slice an array with one end index (negative index)',
			input: 'arr[:-4]',
			output: [1, 3],
			dep: ['arr'],
		},
		{
			description:
				'slice an array with one end index (out of bound index)',
			input: 'arr[:10]',
			output: [1, 3, 5, 7, 9, 11],
			dep: ['arr'],
		},
		{
			description:
				'slice an array with one start index (non-negative index)',
			input: 'arr[3:]',
			output: [7, 9, 11],
			dep: ['arr'],
		},
		{
			description: 'slice an array with one start index (negative index)',
			input: 'arr[-4:]',
			output: [5, 7, 9, 11],
			dep: ['arr'],
		},
		{
			description:
				'slice an array with one start index (out of bound index)',
			input: 'arr[10:]',
			output: [],
			dep: ['arr'],
		},
		{
			description: 'slice an array with one start index (variable index)',
			input: 'arr[two:]',
			output: [5, 7, 9, 11],
			dep: ['arr', 'two'],
		},
		{
			description:
				'get existing property from an variable object (dot notation)',
			input: 'obj.a',
			output: 123,
			dep: ['obj'],
		},
		{
			description:
				'get existing property from an variable object (square brackets notation)',
			input: `obj['a']`,
			output: 123,
			dep: ['obj'],
		},
		{
			description:
				'get additional existing property from an variable object',
			input: 'obj.c',
			output: true,
			dep: ['obj'],
		},
		{
			description:
				'get additional non-existing property from an variable object',
			input: 'obj.d',
			output: undefined,
			dep: ['obj'],
		},
		{
			description: 'get property from an literal object',
			input: `{a: 456}.a`,
			output: 456,
			dep: [],
		},
		{
			description: 'get property from a nested object',
			input: `{a: 456, obj }.obj.a`,
			output: 123,
			dep: ['obj'],
		},
		{
			description: 'get property from a merged object',
			input: `{a: 456, ...obj }.a`,
			output: 123,
			dep: ['obj'],
		},
		{
			description: 'cast types (string -> date)',
			input: 'TODATE("2019-01-03")',
			output: new Date('2019-01-03'),
			dep: [],
		},
		{
			description:
				'return null when casting invalid type (string -> date)',
			input: 'TODATE(str)',
			output: null,
			dep: ['str'],
		},
		{
			description: 'return date with timezone as expression',
			input:
				'{let timezone = "GMT" ; DATETOPARTS(TODATE("2019-01-03"), timezone) }',
			output: {
				year: 2019,
				month: 1,
				day: 3,
				hour: 0,
				minute: 0,
				second: 0,
				millisecond: 0,
			},
			dep: [],
		},
		{
			description: 'map array',
			input: 'MAP(arr, (c) => c + one)',
			output: [2, 4, 6, 8, 10, 12],
			dep: ['arr', 'one'],
		},
		{
			description: 'filter array',
			input: 'FILTER(arr, (c) => c > one)',
			output: [3, 5, 7, 9, 11],
			dep: ['arr', 'one'],
		},
		{
			description: 'reduce array',
			input: 'REDUCE(arr, 0, (p, c) => p + c * two)',
			output: 72,
			dep: ['arr', 'two'],
		},
		{
			description: 'apply SET stage',
			input: '>> SET({ three: two + 1 })',
			output: [
				{
					_id,
					num: 1.234565656,
					str: 'string',
					nul: null,
					bool: true,
					dat: new Date('2019-01-02'),
					arr: [1, 3, 5, 7, 9, 11],
					obj: { a: 123, b: 'abc', c: true },
					one: 1,
					two: 2,
					three: 3,
				},
			],
			dep: ['two'],
		},
		{
			description: 'apply SET stage (non-compatMode)',
			compatMode: false,
			input: '>> SET({ three: two + 1 })',
			output: [
				{
					_id,
					num: 1.234565656,
					str: 'string',
					nul: null,
					bool: true,
					dat: new Date('2019-01-02'),
					arr: [1, 3, 5, 7, 9, 11],
					obj: { a: 123, b: 'abc', c: true },
					one: 1,
					two: 2,
					three: 3,
				},
			],
			dep: ['two'],
		},
		{
			description: 'apply UNSET stage',
			input: `>> UNSET('num', 'str', 'bool', 'dat')`,
			output: [
				{
					_id,
					nul: null,
					arr: [1, 3, 5, 7, 9, 11],
					obj: { a: 123, b: 'abc', c: true },
					one: 1,
					two: 2,
				},
			],
			dep: [],
		},
		{
			description: 'apply UNSET stage (non-compatMode)',
			compatMode: false,
			input: `>> UNSET('num', 'str', 'bool', 'dat')`,
			output: [
				{
					_id,
					nul: null,
					arr: [1, 3, 5, 7, 9, 11],
					obj: { a: 123, b: 'abc', c: true },
					one: 1,
					two: 2,
				},
			],
			dep: [],
		},
		{
			description: 'apply GROUP stage',
			input: '>> GROUP({ _id: one, v: FIRST(two), count: SUM(1) })',
			output: [{ _id: 1, v: 2, count: 1 }],
			dep: ['one', 'two'],
		},
	];

	runTestCases('Success', successTestCases);
});
