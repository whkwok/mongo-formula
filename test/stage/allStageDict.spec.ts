import { expect } from 'chai';
import createDebug from 'debug';

import { defaultParserOptions } from '../../src/MongoFormulaParser';
import { allStageDict as stageDict } from '../../src/stage';
import { Expr, ExtendedExpr, Pipeline, VarDict } from '../../src/types';
import { callStage } from '../../src/util/stage';

interface TestCase {
	description: string;
	compatMode?: boolean;
	stageKey: string;
	args: ExtendedExpr[];
	rootVarDict: VarDict;
	pipeline?: Pipeline;
	varDict?: VarDict;
	errExists?: boolean;
	errName?: string;
	errDetails?: any;
}

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:allStageDict-${title}-${index}`
			);
			const {
				description,
				compatMode = true,
				stageKey,
				args,
				rootVarDict,
				pipeline,
				varDict,
				errExists,
				errName,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			const yy = {
				funcDict: {},
				stageDict,
				scopeStack: [],
				options: { ...defaultParserOptions, compatMode },
			};
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _err: any,
					_pipeline: Pipeline | undefined,
					_varDict: VarDict | undefined;
				try {
					const _pipelineAndVarDict = callStage(
						stageKey,
						rootVarDict,
						args,
						yy
					);
					_pipeline = _pipelineAndVarDict[0];
					_varDict = _pipelineAndVarDict[1];
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					if (pipeline) expect(_pipeline).to.deep.eq(pipeline);
					if (varDict) expect(_varDict).to.deep.eq(varDict);
				}
			});
		});
	});
}

describe('allStageDict', () => {
	const n: Expr = { typ: { type: 'number' }, val: '$n', dep: ['n'] };

	const successTestCases: TestCase[] = [
		{
			description: 'call SET with valid args ({}L)',
			stageKey: 'SET',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: {
						type: 'object',
						properties: { a: { type: 'number' } },
						literal: true,
					},
					val: { a: { $add: ['$n', 1] } },
					dep: { a: ['n'] },
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $addFields: { a: { $add: ['$n', 1] } } }],
				dep: ['n'],
			},
			varDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
				a: { typ: { type: 'number' }, val: '$a', dep: ['n'] },
			},
		},
		{
			description: 'call SET with valid args ({}L) (non-compatMode)',
			compatMode: false,
			stageKey: 'SET',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: {
						type: 'object',
						properties: { a: { type: 'number' } },
						literal: true,
					},
					val: { a: { $add: ['$n', 1] } },
					dep: { a: ['n'] },
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $set: { a: { $add: ['$n', 1] } } }],
				dep: ['n'],
			},
			varDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
				a: { typ: { type: 'number' }, val: '$a', dep: ['n'] },
			},
		},
		{
			description: 'call UNSET with valid args (stringL)',
			stageKey: 'UNSET',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: { type: 'string', literal: true },
					val: 'n',
					dep: [],
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $project: { n: 0 } }],
				dep: [],
			},
			varDict: {},
		},
		{
			description:
				'call UNSET with valid args (stringL) (non-compatMode)',
			compatMode: false,
			stageKey: 'UNSET',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: { type: 'string', literal: true },
					val: 'n',
					dep: [],
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $unset: ['n'] }],
				dep: [],
			},
			varDict: {},
		},
		{
			description:
				'call GROUP with valid args ({_id: unknown, + accumulator(unknown) }L)',
			stageKey: 'GROUP',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
				m: { typ: { type: 'number' }, val: '$m', dep: ['m'] },
			},
			args: [
				{
					typ: {
						type: 'object',
						properties: {
							_id: { type: 'number' },
							a: { type: 'accumulator', in: { type: 'number' } },
						},
						literal: true,
					},
					val: { _id: '$n', a: { $avg: '$m' } },
					dep: { _id: ['n'], a: ['m'] },
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $group: { _id: '$n', a: { $avg: '$m' } } }],
				dep: ['n', 'm'],
			},
			varDict: {
				_id: { typ: { type: 'number' }, val: '$_id', dep: ['n'] },
				a: { typ: { type: 'number' }, val: '$a', dep: ['m'] },
			},
		},
		{
			description: 'call LIMIT with valid args (numberL)',
			stageKey: 'LIMIT',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: { type: 'number', literal: true },
					val: 123,
					dep: [],
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $limit: 123 }],
				dep: [],
			},
			varDict: { n: { typ: { type: 'number' }, val: '$n', dep: ['n'] } },
		},
		{
			description: 'call SKIP with valid args (numberL)',
			stageKey: 'SKIP',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: { type: 'number', literal: true },
					val: 123,
					dep: [],
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $skip: 123 }],
				dep: [],
			},
			varDict: { n: { typ: { type: 'number' }, val: '$n', dep: ['n'] } },
		},
		{
			description: 'call SAMPLE with valid args (numberL)',
			stageKey: 'SAMPLE',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: { type: 'number', literal: true },
					val: 123,
					dep: [],
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $sample: { size: 123 } }],
				dep: [],
			},
			varDict: { n: { typ: { type: 'number' }, val: '$n', dep: ['n'] } },
		},
		{
			description: 'call SORT with valid args ({ + numberL }L)',
			stageKey: 'SORT',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: {
						type: 'object',
						properties: { n: { type: 'number', literal: true } },
						literal: true,
					},
					val: { n: -1 },
					dep: ['n'],
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $sort: { n: -1 } }],
				dep: ['n'],
			},
			varDict: { n: { typ: { type: 'number' }, val: '$n', dep: ['n'] } },
		},
		{
			description: 'call MATCH with valid args (boolean)',
			stageKey: 'MATCH',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: { type: 'boolean' },
					val: { $gt: ['n', 50] },
					dep: ['n'],
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $match: { expr: { $gt: ['n', 50] } } }],
				dep: ['n'],
			},
			varDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
		},
		{
			description: 'call REPLACEROOT with valid args ({})',
			stageKey: 'REPLACEROOT',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
				p: {
					typ: {
						type: 'object',
						properties: { a: { type: 'number' } },
					},
					val: '$p',
					dep: ['p'],
				},
			},
			args: [
				{
					typ: {
						type: 'object',
						properties: { a: { type: 'number' } },
					},
					val: '$p',
					dep: ['p'],
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $replaceRoot: { newRoot: '$p' } }],
				dep: ['p'],
			},
			varDict: {
				a: { typ: { type: 'number' }, val: '$a', dep: ['p'] },
			},
		},
		{
			description: 'call UNWIND with valid args (stringL)',
			stageKey: 'UNWIND',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
				an: {
					typ: { type: 'array', items: { type: 'number' } },
					val: '$an',
					dep: ['an'],
				},
			},
			args: [
				{
					typ: { type: 'string', literal: true },
					val: 'an',
					dep: ['an'],
				},
			],
			pipeline: {
				typ: { type: 'pipeline' },
				val: [{ $unwind: '$an' }],
				dep: ['an'],
			},
			varDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
				an: { typ: { type: 'number' }, val: '$an', dep: ['an'] },
			},
		},
	];

	runTestCases('Success', successTestCases);

	const failTestCases: TestCase[] = [
		{
			description:
				'throw error when calling SET with invalid args (variable object)',
			stageKey: 'SET',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
				o: {
					typ: {
						type: 'object',
						properties: { m: { type: 'number' } },
					},
					val: '$o',
					dep: ['o'],
				},
			},
			args: [
				{
					typ: {
						type: 'object',
						properties: { m: { type: 'number' } },
					},
					val: '$o',
					dep: ['o'],
				},
			],
			errName: 'ExprTypErr',
			errDetails: {
				context: 'SET',
				index: 0,
				receivedTyp: {
					type: 'object',
					properties: { m: { type: 'number' } },
				},
				expectedTyps: [
					{
						type: 'object',
						properties: {},
						literal: true,
					},
				],
			},
		},
		{
			description:
				'throw error when calling GROUP with invalid args (literal object with non-accumulator)',
			stageKey: 'GROUP',
			rootVarDict: {
				n: { typ: { type: 'number' }, val: '$n', dep: ['n'] },
			},
			args: [
				{
					typ: {
						type: 'object',
						properties: {
							_id: { type: 'number' },
							a: { type: 'number', literal: true },
						},
						literal: true,
					},
					val: { _id: '$n', a: 123 },
					dep: { _id: ['n'], a: [] },
				},
			],
			errName: 'ExprTypErr',
			errDetails: {
				context: 'GROUP',
				index: 0,
				receivedTyp: {
					type: 'object',
					properties: {
						_id: { type: 'number' },
						a: { type: 'number', literal: true },
					},
					literal: true,
				},
				expectedTyps: [
					{
						type: 'object',
						properties: { _id: { type: 'unknown' } },
						additionalProperties: {
							type: 'accumulator',
							in: { type: 'unknown' },
						},
						literal: true,
					},
				],
			},
		},
	];
	runTestCases('Fail', failTestCases);
});
