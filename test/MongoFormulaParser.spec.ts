import { expect } from 'chai';
import createDebug from 'debug';

import {
	Dep,
	MongoFormulaParser,
	Schema,
	Typ,
	allFuncDict,
	allStageDict,
	unknownTyp,
} from '../src';

const RESERVED_SUFFIX = '___';

const UNDEFINED_VAR_TYP = unknownTyp();

interface TestCase {
	description: string;
	input: string;
	typ?: Typ;
	val?: any;
	dep?: Dep;
	errExists?: boolean;
	errName?: string;
	errMessage?: string | RegExp;
	errDetails?: any;
}

const schema: Schema = {
	n: { type: 'number' },
	b: { type: 'boolean' },
	s: { type: 'string' },
	d: { type: 'date' },
	a: { type: 'array', items: { type: 'number' } },
	o: {
		type: 'object',
		properties: { a: { type: 'number' }, b: { type: 'string' } },
	},
	p: {
		type: 'object',
		properties: {
			a: { type: 'number' },
			b: {
				type: 'object',
				properties: {
					c: { type: 'boolean' },
					d: { type: 'date' },
				},
			},
		},
	},
};

const mongoFormulaParser = new MongoFormulaParser(
	schema,
	allFuncDict,
	allStageDict,
	{ reservedSuffix: RESERVED_SUFFIX, undefinedVarTyp: UNDEFINED_VAR_TYP }
);

function runTestCases(title: string, testCases: TestCase[]) {
	describe(title, () => {
		testCases.forEach((testCase: TestCase, index: number) => {
			const debug = createDebug(
				`mongo-formula:MongoFormulaParser-test-${title}-${index}`
			);
			const {
				description,
				input,
				typ,
				val,
				dep,
				errExists,
				errName,
				errMessage,
				errDetails,
			} = testCase;
			const _description =
				'should be able to ' + description + ` [${index}]`;
			it(_description, function () {
				debug(`--------  ${_description}  --------`);
				let _typ: Typ, _val: any, _dep: Dep, _err: any;
				try {
					const result = mongoFormulaParser.parse(input);
					_typ = result.typ;
					_val = result.val;
					_dep = result.dep;
				} catch (err) {
					_err = err;
				}
				if (errExists || errName || errMessage || errDetails) {
					expect(_err).to.exist;
					if (errName) expect(_err.name).to.eq(errName);
					if (errMessage) {
						if (typeof errMessage === 'string')
							expect(_err.message).to.eq(errMessage);
						else expect(_err.message).to.match(errMessage);
					}
					if (errDetails)
						expect(_err.details).to.deep.contain(errDetails);
				} else {
					if (_err) debug(_err.message);
					expect(_err).to.not.exist;
					expect(_typ!).to.deep.eq(typ);
					expect(_val!).to.deep.eq(val);
					expect(_dep!).to.deep.eq(dep);
				}
			});
		});
	});
}

describe('MongoFormulaParser', () => {
	const singleTokenTestCases: TestCase[] = [
		{
			description: 'parse NUM to Expr',
			input: '5',
			typ: { type: 'number', literal: true },
			val: 5,
			dep: [],
		},
		{
			description: 'parse BOOL to Expr',
			input: 'true',
			typ: { type: 'boolean', literal: true },
			val: true,
			dep: [],
		},

		{
			description: 'parse NULL to Expr',
			input: 'null',
			typ: { type: 'never' },
			val: null,
			dep: [],
		},
		{
			description: 'parse KEY to var to Expr (defined)',
			input: 'n',
			typ: { type: 'number' },
			val: '$n',
			dep: ['n'],
		},
		{
			description: 'parse KEY to var to Expr (undefined)',
			input: 'u',
			typ: UNDEFINED_VAR_TYP,
			val: '$u',
			dep: ['u'],
		},
	];

	runTestCases('SingleToken', singleTokenTestCases);

	const stringConstructTestCases: TestCase[] = [
		{
			description: 'parse single quoted string',
			input: `'str'`,
			typ: { type: 'string', literal: true },
			val: 'str',
			dep: [],
		},
		{
			description: 'parse double quoted string',
			input: `"str"`,
			typ: { type: 'string', literal: true },
			val: 'str',
			dep: [],
		},
		{
			description: 'parse single quoted single quote',
			input: `'\\''`,
			typ: { type: 'string', literal: true },
			val: `'`,
			dep: [],
		},
		{
			description: 'parse double quoted single quote',
			input: `"'"`,
			typ: { type: 'string', literal: true },
			val: `'`,
			dep: [],
		},
		{
			description: 'parse single quoted double quote',
			input: `'"'`,
			typ: { type: 'string', literal: true },
			val: `"`,
			dep: [],
		},
		{
			description: 'parse double quoted double quote',
			input: `"\\""`,
			typ: { type: 'string', literal: true },
			val: `"`,
			dep: [],
		},
		{
			description: 'parse single quoted backslash',
			input: `'\\\\'`,
			typ: { type: 'string', literal: true },
			val: `\\`,
			dep: [],
		},
		{
			description: 'parse double quoted backslash',
			input: `"\\\\"`,
			typ: { type: 'string', literal: true },
			val: `\\`,
			dep: [],
		},
		{
			description: 'throw err when parsing string start with $',
			input: `'$123'`,
			errName: 'ExprValErr',
			errDetails: {
				receivedVal: '$123',
				reason:
					'String must not start with special character "$". Consider using LITERAL function.',
			},
		},
	];
	runTestCases('StringConstruct', stringConstructTestCases);

	const arrayConstructTestCases: TestCase[] = [
		{
			description: 'parse empty array',
			input: '[]',
			typ: { type: 'array', items: [], literal: true },
			val: [],
			dep: [],
		},
		{
			description: 'parse array of number',
			input: '[ 1, 2, 3 ]',
			typ: {
				type: 'array',
				items: [
					{ type: 'number', literal: true },
					{ type: 'number', literal: true },
					{ type: 'number', literal: true },
				],
				literal: true,
			},
			val: [1, 2, 3],
			dep: [[], [], []],
		},
		{
			description: 'parse array of string',
			input: '[ "1", "2", "3" ]',
			typ: {
				type: 'array',
				items: [
					{ type: 'string', literal: true },
					{ type: 'string', literal: true },
					{ type: 'string', literal: true },
				],
				literal: true,
			},
			val: ['1', '2', '3'],
			dep: [[], [], []],
		},
		{
			description: 'parse array of mixed',
			input: '[ "1", "2", 3 ]',
			typ: {
				type: 'array',
				items: [
					{ type: 'string', literal: true },
					{ type: 'string', literal: true },
					{ type: 'number', literal: true },
				],
				literal: true,
			},
			val: ['1', '2', 3],
			dep: [[], [], []],
		},
		{
			description: 'parse array of array of number',
			input: '[ [1], [2, 2], [3,3,3] ]',
			typ: {
				type: 'array',
				items: [
					{
						type: 'array',
						items: [{ type: 'number', literal: true }],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
						],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
						],
						literal: true,
					},
				],
				literal: true,
			},
			val: [[1], [2, 2], [3, 3, 3]],
			dep: [[[]], [[], []], [[], [], []]],
		},
		{
			description: 'parse array of array of number (with VAR)',
			input: '[ [n], [2, 2], [3,3,3] ]',
			typ: {
				type: 'array',
				items: [
					{
						type: 'array',
						items: [{ type: 'number' }],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
						],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
						],
						literal: true,
					},
				],
				literal: true,
			},
			val: [['$n'], [2, 2], [3, 3, 3]],
			dep: [[['n']], [[], []], [[], [], []]],
		},
		{
			description: 'parse array of array of number (with empty array)',
			input: '[ [], [1], [2, 2], [3,3,3] ]',
			typ: {
				type: 'array',
				items: [
					{
						type: 'array',
						items: [],
						literal: true,
					},
					{
						type: 'array',
						items: [{ type: 'number', literal: true }],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
						],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
						],
						literal: true,
					},
				],
				literal: true,
			},
			val: [[], [1], [2, 2], [3, 3, 3]],
			dep: [[], [[]], [[], []], [[], [], []]],
		},
		{
			description:
				'parse array of array of array of number (with empty array)',
			input: '[ [[]], [[1]], [[2, 2]], [[3,3,3]] ]',
			typ: {
				type: 'array',
				items: [
					{
						type: 'array',
						items: [{ type: 'array', items: [], literal: true }],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{
								type: 'array',
								items: [{ type: 'number', literal: true }],
								literal: true,
							},
						],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{
								type: 'array',
								items: [
									{ type: 'number', literal: true },
									{ type: 'number', literal: true },
								],
								literal: true,
							},
						],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{
								type: 'array',
								items: [
									{ type: 'number', literal: true },
									{ type: 'number', literal: true },
									{ type: 'number', literal: true },
								],
								literal: true,
							},
						],
						literal: true,
					},
				],
				literal: true,
			},
			val: [[[]], [[1]], [[2, 2]], [[3, 3, 3]]],
			dep: [[[]], [[[]]], [[[], []]], [[[], [], []]]],
		},
		{
			description: 'parse array of array of mixed',
			input: '[ [1], ["2", "2"], [3,3,3] ]',
			typ: {
				type: 'array',
				items: [
					{
						type: 'array',
						items: [{ type: 'number', literal: true }],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{ type: 'string', literal: true },
							{ type: 'string', literal: true },
						],
						literal: true,
					},
					{
						type: 'array',
						items: [
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
							{ type: 'number', literal: true },
						],
						literal: true,
					},
				],
				literal: true,
			},
			val: [[1], ['2', '2'], [3, 3, 3]],
			dep: [[[]], [[], []], [[], [], []]],
		},
		{
			description: 'parse array of object',
			input: '[{ a: 1, b: 2, c: "str" }, { b: 1 , c: 3, d: 5 }]',
			typ: {
				type: 'array',
				items: [
					{
						type: 'object',
						properties: {
							a: { type: 'number', literal: true },
							b: { type: 'number', literal: true },
							c: { type: 'string', literal: true },
						},
						literal: true,
					},
					{
						type: 'object',
						properties: {
							b: { type: 'number', literal: true },
							c: { type: 'number', literal: true },
							d: { type: 'number', literal: true },
						},
						literal: true,
					},
				],
				literal: true,
			},
			val: [
				{ a: 1, b: 2, c: 'str' },
				{ b: 1, c: 3, d: 5 },
			],
			dep: [
				{ a: [], b: [], c: [] },
				{ b: [], c: [], d: [] },
			],
		},

		{
			description: 'parse array with spread operator at the end',
			input: '[1,2,3, ...a]',
			typ: {
				type: 'array',
				items: { type: 'number' },
			},
			val: { $concatArrays: [[1, 2, 3], '$a'] },
			dep: ['a'],
		},
		{
			description: 'parse array with spread operator in the middle',
			input: '[1, ...a, 2, 3]',
			typ: {
				type: 'array',
				items: { type: 'number' },
			},
			val: { $concatArrays: [[1], '$a', [2, 3]] },
			dep: ['a'],
		},
		{
			description: 'parse array with spread operator at the start',
			input: '[...a, 1,2,3]',
			typ: {
				type: 'array',
				items: { type: 'number' },
			},
			val: { $concatArrays: ['$a', [1, 2, 3]] },
			dep: ['a'],
		},
	];
	runTestCases('ArrayConstruct', arrayConstructTestCases);

	const objectConstructTestCases: TestCase[] = [
		{
			description: 'parse empty object',
			input: '{}',
			typ: {
				type: 'object',
				properties: {},
				literal: true,
			},
			val: {},
			dep: {},
		},
		{
			description: 'parse simple object',
			input: '{ a: 1, b: "str" }',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number', literal: true },
					b: { type: 'string', literal: true },
				},
				literal: true,
			},
			val: { a: 1, b: 'str' },
			dep: { a: [], b: [] },
		},
		{
			description: 'parse nested object',
			input: '{ a: 1, b: "str", c: { d: 0, e: true } }',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number', literal: true },
					b: { type: 'string', literal: true },
					c: {
						type: 'object',
						properties: {
							d: { type: 'number', literal: true },
							e: { type: 'boolean', literal: true },
						},
						literal: true,
					},
				},
				literal: true,
			},
			val: { a: 1, b: 'str', c: { d: 0, e: true } },
			dep: {
				a: [],
				b: [],
				c: { d: [], e: [] },
			},
		},
		{
			description: 'parse nested object (with array)',
			input: '{ a: 1, b: "str", c: { d: 0, e: [1,2,3] } }',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number', literal: true },
					b: { type: 'string', literal: true },
					c: {
						type: 'object',
						properties: {
							d: { type: 'number', literal: true },
							e: {
								type: 'array',
								items: [
									{ type: 'number', literal: true },
									{ type: 'number', literal: true },
									{ type: 'number', literal: true },
								],
								literal: true,
							},
						},
						literal: true,
					},
				},
				literal: true,
			},
			val: { a: 1, b: 'str', c: { d: 0, e: [1, 2, 3] } },
			dep: { a: [], b: [], c: { d: [], e: [[], [], []] } },
		},
		{
			description: 'parse simple object with spread operator at the end',
			input: '{ x: 1, y: "str", ...o }',
			typ: {
				type: 'object',
				properties: {
					x: { type: 'number' },
					y: { type: 'string' },
					a: { type: 'number' },
					b: { type: 'string' },
				},
			},
			val: { $mergeObjects: [{ x: 1, y: 'str' }, '$o'] },
			dep: ['o'],
		},
		{
			description:
				'parse simple object with spread operator in the middle',
			input: '{ x: 1, ...o, b: n }',
			typ: {
				type: 'object',
				properties: {
					x: { type: 'number' },
					a: { type: 'number' },
					b: { type: 'number' },
				},
			},
			val: { $mergeObjects: [{ x: 1 }, '$o', { b: '$n' }] },
			dep: ['o', 'n'],
		},
		{
			description:
				'parse simple object with spread operator at the beginning',
			input: '{ ...o, x: 1, b: b }',
			typ: {
				type: 'object',
				properties: {
					a: { type: 'number' },
					b: { type: 'boolean' },
					x: { type: 'number' },
				},
			},
			val: { $mergeObjects: ['$o', { x: 1, b: '$b' }] },
			dep: ['o', 'b'],
		},

		{
			description: 'parse simple object with var key',
			input: '{ n, b }',
			typ: {
				type: 'object',
				properties: {
					n: { type: 'number' },
					b: { type: 'boolean' },
				},
				literal: true,
			},
			val: { n: '$n', b: '$b' },
			dep: { n: ['n'], b: ['b'] },
		},
	];
	runTestCases('ObjectConstruct', objectConstructTestCases);

	const objectAccessTestCases: TestCase[] = [
		{
			description:
				'parse simple object access with dot syntax (object variable)',
			input: 'o.a',
			typ: { type: 'number' },
			val: '$o.a',
			dep: ['o'],
		},
		{
			description:
				'parse nested object access with dot syntax (variable object)',
			input: 'p.b.c',
			typ: { type: 'boolean' },
			val: '$p.b.c',
			dep: ['p'],
		},
		{
			description:
				'parse simple object access with dot syntax (literal object)',
			input: '{ a: 0, b: 5 }.b',
			typ: { type: 'number', literal: true },
			val: 5,
			dep: [],
		},
		{
			description:
				'parse nested object access with dot syntax (variable object in literal object)',
			input: '{ a: 0, o: o }.o.a',
			typ: { type: 'number' },
			val: '$o.a',
			dep: ['o'],
		},
		{
			description:
				'parse nested object access with dot syntax (literal object in literal object)',
			input: '{ a: 0, o: { c: b, d: 5 } }.o.c',
			typ: { type: 'boolean' },
			val: '$b',
			dep: ['b'],
		},
		{
			description:
				'parse nested object access with dot syntax (variable object in merged object)',
			input: '{ i: 0, ...p }.b.c',
			typ: { type: 'boolean' },
			val: {
				$let: {
					vars: {
						[`o${RESERVED_SUFFIX}`]: {
							$mergeObjects: [{ i: 0 }, '$p'],
						},
					},
					in: `$$o${RESERVED_SUFFIX}.b.c`,
				},
			},
			dep: ['p'],
		},
		{
			description:
				'parse simple object access with square brackets syntax (object variable)',
			input: 'o["a"]',
			typ: { type: 'number' },
			val: '$o.a',
			dep: ['o'],
		},
		{
			description:
				'throw error when parsing invalid object access (array variable)',
			input: 'a["a"]',
			errName: 'ExprTypErr',
			errDetails: {
				receivedTyp: { type: 'array', items: { type: 'number' } },
				expectedTyps: [{ type: 'object', properties: {} }],
			},
		},
		{
			description:
				'throw error when parsing invalid object access (missing objectProperty)',
			input: 'o["c"]',
			errName: 'KeyErr',
			errDetails: {
				key: 'c',
				keyType: 'object',
				reason: 'Object property is not found.',
			},
		},
	];
	runTestCases('ObjectAccess', objectAccessTestCases);

	const letBlockTestCases: TestCase[] = [
		{
			description: 'parse simple let block',
			input: '{ let x = 1; x }',
			typ: { type: 'number' },
			val: { $let: { vars: { x: 1 }, in: '$$x' } },
			dep: [],
		},
		{
			description: 'parse nested let block',
			input: '{ let x = 1; { let y = 3; [x, y] } }',
			typ: { type: 'array', items: { type: 'number' } },
			val: {
				$let: {
					vars: { x: 1 },
					in: { $let: { vars: { y: 3 }, in: ['$$x', '$$y'] } },
				},
			},
			dep: [],
		},

		{
			description: 'parse let block with multiple assigns',
			input: '{ let x = 1, y = 3; [ x, y ] }',
			typ: { type: 'array', items: { type: 'number' } },
			val: { $let: { vars: { x: 1, y: 3 }, in: ['$$x', '$$y'] } },
			dep: [],
		},
		{
			description:
				'parse let block with multiple assigns (refering root variable in assign)',
			input: '{ let x = 1, y = x; y }',
			typ: UNDEFINED_VAR_TYP,
			val: { $let: { vars: { x: 1, y: '$x' }, in: '$$y' } },
			dep: ['x'],
		},
		{
			description:
				'parse simple let statement (override existing root VAR)',
			input: '{ let n = "abc"; n }',
			typ: { type: 'string' },
			val: { $let: { vars: { n: 'abc' }, in: '$$n' } },
			dep: [],
		},
		{
			description:
				'throw err when parsing let statement with conflicting assigns',
			input: '{ let x = 1, x = "str"; x }',
			errName: 'KeyErr',
			errDetails: {
				key: 'x',
				keyType: 'var',
				reason: 'Variable key has been already used in this scope.',
			},
		},
	];
	runTestCases('LetBlock', letBlockTestCases);

	const funcTestCases: TestCase[] = [
		{
			description: 'parse simple function',
			input: 'ADD(1, n)',
			typ: { type: 'number' },
			val: { $add: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse simple function (lowercase characters)',
			input: 'add(1, n)',
			typ: { type: 'number' },
			val: { $add: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse nested function',
			input: 'SUBTRACT(ADD(1, n), MULTIPLY(CEIL(n), n))',
			typ: { type: 'number' },
			val: {
				$subtract: [
					{ $add: [1, '$n'] },
					{ $multiply: [{ $ceil: '$n' }, '$n'] },
				],
			},
			dep: ['n'],
		},
	];
	runTestCases('Func', funcTestCases);

	const unaryOperatorTestCases: TestCase[] = [
		{
			description: 'parse - operator',
			input: '-n',
			typ: { type: 'number' },
			val: { $multiply: [-1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse ! operator',
			input: '!b',
			typ: { type: 'boolean' },
			val: { $not: '$b' },
			dep: ['b'],
		},
		{
			description: 'parse |/* expr */| operator',
			input: '|a|',
			typ: { type: 'number' },
			val: { $size: '$a' },
			dep: ['a'],
		},
	];
	runTestCases('UnaryOperator', unaryOperatorTestCases);

	const binaryOperatorTestCases: TestCase[] = [
		{
			description: 'parse + operator',
			input: '1 + n',
			typ: { type: 'number' },
			val: { $add: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse - operator',
			input: '1 - n',
			typ: { type: 'number' },
			val: { $subtract: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse * operator',
			input: '5 * n',
			typ: { type: 'number' },
			val: { $multiply: [5, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse / operator',
			input: '1 / n',
			typ: { type: 'number' },
			val: { $divide: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse % operator',
			input: '1 % n',
			typ: { type: 'number' },
			val: { $mod: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse ** operator',
			input: '1 ** n',
			typ: { type: 'number' },
			val: { $pow: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse in operator',
			input: '1 in a',
			typ: { type: 'boolean' },
			val: { $in: [1, '$a'] },
			dep: ['a'],
		},
		{
			description: 'parse > operator',
			input: '1 > n',
			typ: { type: 'boolean' },
			val: { $gt: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse >= operator',
			input: '1 >= n',
			typ: { type: 'boolean' },
			val: { $gte: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse == operator',
			input: '1 == n',
			typ: { type: 'boolean' },
			val: { $eq: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse <= operator',
			input: '1 <= n',
			typ: { type: 'boolean' },
			val: { $lte: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse < operator',
			input: '1 < n',
			typ: { type: 'boolean' },
			val: { $lt: [1, '$n'] },
			dep: ['n'],
		},
		{
			description: 'parse && operator',
			input: 'b && b',
			typ: { type: 'boolean' },
			val: { $and: ['$b', '$b'] },
			dep: ['b'],
		},
		{
			description: 'parse || operator',
			input: 'b || b',
			typ: { type: 'boolean' },
			val: { $or: ['$b', '$b'] },
			dep: ['b'],
		},
		{
			description: 'parse ?? operator',
			input: 'n ?? 1',
			typ: { type: 'number' },
			val: { $ifNull: ['$n', 1] },
			dep: ['n'],
		},
		{
			description: 'parse {||} operator',
			input: '[1,2,3] {||} a',
			typ: { type: 'array', items: { type: 'number' } },
			val: { $setUnion: [[1, 2, 3], '$a'] },
			dep: ['a'],
		},
		{
			description: 'parse {&&} operator',
			input: '[1,2,3] {&&} a',
			typ: { type: 'array', items: { type: 'number' } },
			val: { $setIntersection: [[1, 2, 3], '$a'] },
			dep: ['a'],
		},
		{
			description: 'parse {&!} operator',
			input: '[1,2,3] {&!} a',
			typ: { type: 'array', items: { type: 'number' } },
			val: { $setDifference: [[1, 2, 3], '$a'] },
			dep: ['a'],
		},
		{
			description: 'parse {<=} operator',
			input: '[1,2,3] {<=} a',
			typ: { type: 'boolean' },
			val: { $setIsSubset: [[1, 2, 3], '$a'] },
			dep: ['a'],
		},
		{
			description: 'parse {==} operator',
			input: '[1,2,3] {==} a',
			typ: { type: 'boolean' },
			val: { $setEquals: [[1, 2, 3], '$a'] },
			dep: ['a'],
		},
		{
			description: 'parse {>=} operator',
			input: '[1,2,3] {>=} a',
			typ: { type: 'boolean' },
			val: { $setIsSubset: ['$a', [1, 2, 3]] },
			dep: ['a'],
		},
		{
			description: 'parse {!=} operator',
			input: '[1,2,3] {!=} a',
			typ: { type: 'boolean' },
			val: { $not: { $setEquals: [[1, 2, 3], '$a'] } },
			dep: ['a'],
		},
	];
	runTestCases('BinaryOperator', binaryOperatorTestCases);

	const ternaryTestCases: TestCase[] = [
		{
			description: 'parse ? : operator',
			input: 'n ? b : 100',
			typ: { type: 'unknown' },
			val: { $cond: ['$n', '$b', 100] },
			dep: ['n', 'b'],
		},
	];
	runTestCases('TernaryOperator', ternaryTestCases);

	const precedenceTestCases: TestCase[] = [
		{
			description:
				'parse + and - operators with correct precedence (+ "=" -, left)',
			input: '1 + n - 2 + n',
			typ: { type: 'number' },
			val: { $add: [{ $subtract: [{ $add: [1, '$n'] }, 2] }, '$n'] },
			dep: ['n'],
		},
		{
			description:
				'parse * and / and % operators with correct precedence (* "=" / "=" %, left)',
			input: '5 * n / 2 % 4 * n',
			typ: { type: 'number' },
			val: {
				$multiply: [
					{
						$mod: [{ $divide: [{ $multiply: [5, '$n'] }, 2] }, 4],
					},
					'$n',
				],
			},
			dep: ['n'],
		},
		{
			description: 'parse ** operators with correct precedence (right)',
			input: 'n ** 2 ** n',
			typ: { type: 'number' },
			val: { $pow: ['$n', { $pow: [2, '$n'] }] },
			dep: ['n'],
		},
		{
			description:
				'parse + and * operators with correct precedence (+ "<" *)',
			input: '1 + n * n + n',
			typ: { type: 'number' },
			val: { $add: [1, { $multiply: ['$n', '$n'] }, '$n'] },
			dep: ['n'],
		},
		{
			description:
				'parse unary - and % operators with correct precedence (% "<" unary -)',
			input: '2 % -n',
			typ: { type: 'number' },
			val: { $mod: [2, { $multiply: [-1, '$n'] }] },
			dep: ['n'],
		},
		{
			description:
				'parse unary - and ** operators with correct precedence (unary - "<" **)',
			input: '-n**2',
			typ: { type: 'number' },
			val: { $multiply: [-1, { $pow: ['$n', 2] }] },
			dep: ['n'],
		},
		{
			description: 'parse () to override precedence (unary - "<" **)',
			input: '(-n)**2',
			typ: { type: 'number' },
			val: { $pow: [{ $multiply: [-1, '$n'] }, 2] },
			dep: ['n'],
		},
	];
	runTestCases('Precedence', precedenceTestCases);

	const asTypTestCases: TestCase[] = [
		{
			description: 'parse asTyp with valid typeKey (number as string)',
			input: `1 as { type: 'string' }`,
			typ: { type: 'string' },
			val: 1,
			dep: [],
		},
		{
			description: 'parse asTyp with valid typeKey (any as number)',
			input: `u as { type: 'number' }`,
			typ: { type: 'number' },
			val: '$u',
			dep: ['u'],
		},
		{
			description: 'parse asTyp together with other operator (+)',
			input: `n + u as { type: 'number' }`,
			typ: { type: 'number' },
			val: { $add: ['$n', '$u'] },
			dep: ['n', 'u'],
		},
	];
	runTestCases('AsTyp', asTypTestCases);

	const callbackTestCases: TestCase[] = [
		{
			description: 'parse MAP function',
			input: 'MAP(a, (item) => item + n)',
			typ: { type: 'array', items: { type: 'number' } },
			val: {
				$map: {
					input: '$a',
					as: 'item',
					in: { $add: ['$$item', '$n'] },
				},
			},
			dep: ['a', 'n'],
		},
		{
			description: 'parse FILTER function',
			input: 'FILTER(a, (item) => item > 1)',
			typ: { type: 'array', items: { type: 'number' } },
			val: {
				$filter: {
					input: '$a',
					as: 'item',
					cond: { $gt: ['$$item', 1] },
				},
			},
			dep: ['a'],
		},
		{
			description: 'parse REDUCE function',
			input: 'REDUCE(a, 1, (p, c) => p * c)',
			typ: { type: 'number' },
			val: {
				$reduce: {
					input: '$a',
					initialValue: 1,
					in: {
						$let: {
							vars: { p: '$$value', c: '$$this' },
							in: { $multiply: ['$$p', '$$c'] },
						},
					},
				},
			},
			dep: ['a'],
		},
		{
			description: 'parse nested MAP functions (different item keys)',
			input: 'MAP(MAP(a, (i) => i < 1), (j) => j && b)',
			typ: { type: 'array', items: { type: 'boolean' } },
			val: {
				$map: {
					input: {
						$map: {
							input: '$a',
							as: 'i',
							in: { $lt: ['$$i', 1] },
						},
					},
					as: 'j',
					in: { $and: ['$$j', '$b'] },
				},
			},
			dep: ['a', 'b'],
		},
		{
			description:
				'parse nested MAP and FILTER functions (reused item keys)',
			input: 'MAP(FILTER(a, (item) => item < 1), (item) => item + n)',
			typ: { type: 'array', items: { type: 'number' } },
			val: {
				$map: {
					input: {
						$filter: {
							input: '$a',
							as: 'item',
							cond: { $lt: ['$$item', 1] },
						},
					},
					as: 'item',
					in: { $add: ['$$item', '$n'] },
				},
			},
			dep: ['a', 'n'],
		},
	];
	runTestCases('Callback', callbackTestCases);

	const stageTestCases: TestCase[] = [
		{
			description: 'parse single stage',
			input: '>> SET({ u: 1 + n })',
			typ: { type: 'pipeline' },
			val: [{ $addFields: { u: { $add: [1, '$n'] } } }],
			dep: ['n'],
		},
		{
			description: 'parse multiple stages',
			input: '>> SET({ u: 1 + n }) >> SET({ v: u + 1 })',
			typ: { type: 'pipeline' },
			val: [
				{ $addFields: { u: { $add: [1, '$n'] } } },
				{ $addFields: { v: { $add: ['$u', 1] } } },
			],
			dep: ['n'],
		},
	];
	runTestCases('Func', stageTestCases);

	const literalTestCases: TestCase[] = [
		{
			description: 'parse LITERAL string starting with $',
			input: `LITERAL('$123')`,
			typ: { type: 'string' },
			val: { $literal: '$123' },
			dep: [],
		},
		{
			description: 'parse LITERAL negative number',
			input: `LITERAL(-1)`,
			typ: { type: 'number' },
			val: { $literal: -1 },
			dep: [],
		},
	];
	runTestCases('Stage', literalTestCases);

	const parseErrTestCases: TestCase[] = [
		{
			description: 'throw err when parsing invalid line (unknown token)',
			input: 'x $',
			errName: 'LexErr',
			errDetails: { pos: 2, char: '$' },
		},
		{
			description: 'throw err when parsing invalid line (unexpected EOF)',
			input: 'x +',
			errName: 'ParseErr',
			errDetails: { receivedToken: 'EOF' },
		},
	];
	runTestCases('ParseErr', parseErrTestCases);

	describe('ParseTemplate', () => {
		it('should be able to parse template with typ spec and null getVarVals', () => {
			const template = mongoFormulaParser.parseTemplate([
				{ type: 'boolean' },
				{ type: 'number' },
			])`${null} ? ${null} : n`;
			const result1 = template(true, { $add: [1, 2] });
			const result2 = template(false, 50);
			expect(result1.typ).to.deep.eq({ type: 'number' });
			expect(result1.val).to.deep.eq({
				$cond: [
					{ $ifNull: [null, true] },
					{ $ifNull: [null, { $add: [1, 2] }] },
					'$n',
				],
			});
			expect(result1.dep).to.deep.eq(['n']);
			expect(result2.typ).to.deep.eq({ type: 'number' });
			expect(result2.val).to.deep.eq({
				$cond: [
					{ $ifNull: [null, false] },
					{ $ifNull: [null, 50] },
					'$n',
				],
			});
			expect(result2.dep).to.deep.eq(['n']);
		});

		it('should be able to parse template with full spec and defined getVarVals', () => {
			const template = mongoFormulaParser.parseTemplate([
				{ typ: { type: 'boolean' }, key: 'key1', dep: ['myDep'] },
				{ typ: { type: 'number' }, key: 'key2' },
			])`${(...vs: any[]) => vs[0]} ? ${(...vs: any) => vs[1]} : n`;
			const result1 = template(true, { $add: [1, 2] });
			const result2 = template(false, 50);
			expect(result1.typ).to.deep.eq({ type: 'number' });
			expect(result1.val).to.deep.eq({
				$cond: [
					{ $ifNull: [null, true] },
					{ $ifNull: [null, { $add: [1, 2] }] },
					'$n',
				],
			});
			expect(result1.dep).to.deep.eq(['myDep', 'n']);
			expect(result2.typ).to.deep.eq({ type: 'number' });
			expect(result2.val).to.deep.eq({
				$cond: [
					{ $ifNull: [null, false] },
					{ $ifNull: [null, 50] },
					'$n',
				],
			});
			expect(result2.dep).to.deep.eq(['myDep', 'n']);
		});

		it('should be able to parse template with full spec and null getTemplateVarVals', () => {
			const template = mongoFormulaParser.parseTemplate([
				{ typ: { type: 'boolean' }, key: 'key1', dep: ['myDep'] },
				{ typ: { type: 'number' }, key: 'key2' },
			])`${null} ? ${null} : n`;
			const result1 = template(true, { $add: [1, 2] });
			const result2 = template(false, 50);
			expect(result1.typ).to.deep.eq({ type: 'number' });
			expect(result1.val).to.deep.eq({
				$cond: [
					{ $ifNull: [null, true] },
					{ $ifNull: [null, { $add: [1, 2] }] },
					'$n',
				],
			});
			expect(result1.dep).to.deep.eq(['myDep', 'n']);
			expect(result2.typ).to.deep.eq({ type: 'number' });
			expect(result2.val).to.deep.eq({
				$cond: [
					{ $ifNull: [null, false] },
					{ $ifNull: [null, 50] },
					'$n',
				],
			});
			expect(result2.dep).to.deep.eq(['myDep', 'n']);
		});
	});
});
