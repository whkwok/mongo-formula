import { BaseErr, ParseErr, TemplateErr, UnexpectedErr } from './errors';
import { Lexer, mongoFormulaLexer } from './lexer';
import { Parser } from './parser';
import {
	FuncDict,
	ParseInfo,
	ParseResult,
	ParserOptions,
	Schema,
	StageDict,
	TemplateVarSpec,
	YY,
} from './types';
import { deepClone } from './util/deepClone';
import { createRootScope, createTemplateScope } from './util/scope';
import { unknownTyp } from './util/typ';

export const defaultParserOptions: ParserOptions = {
	reservedSuffix: '___',
	undefinedVarTyp: unknownTyp(),
	compatMode: true,
};

export class MongoFormulaParser extends Parser {
	/**
	 * Internal states of parser
	 * @remarks used by jison builded parser.
	 */
	protected yy: YY;
	/**
	 * Lexer instance of parser
	 * @remarks used by jison builded parser.
	 */
	protected lexer: Lexer;
	/**
	 * Default type definition of variables.
	 */
	protected schema: Schema;
	/**
	 * Default definition of funcs.
	 */
	protected funcDict: FuncDict;
	/**
	 * Default definition of stages.
	 */
	protected stageDict: StageDict;
	/**
	 * Default options of parser.
	 */
	protected options: ParserOptions;

	constructor(
		schema: Schema = {},
		funcDict: FuncDict = {},
		stageDict: StageDict = {},
		options: Partial<ParserOptions> = defaultParserOptions
	) {
		super();
		this.yy = {
			funcDict: {},
			stageDict: {},
			scopeStack: [],
			options: defaultParserOptions,
		};
		this.lexer = mongoFormulaLexer;
		this.schema = schema;
		this.funcDict = funcDict;
		this.stageDict = stageDict;
		this.options = { ...defaultParserOptions, ...options };
	}

	/**
	 * Reset internal states of parser.
	 */
	protected reset(): void {
		this.yy.funcDict = {};
		this.yy.stageDict = {};
		this.yy.scopeStack = [];
		this.yy.options = defaultParserOptions;
		this.lexer.reset();
	}

	/**
	 * Throw ParseErr.
	 * @remark used by jison builded parser
	 */
	protected parseError(
		message: string,
		hash: {
			token: string;
			expected: string[]; // expected contain token with quotes
		}
	) {
		const { token, expected } = hash;
		throw new ParseErr({
			receivedToken: token,
			expectedTokens: expected.map((token) => token.slice(1, -1)), // slice off the quotes
		});
	}

	/**
	 * Get parseInfo from internal states of parser.
	 */
	protected getParseInfo(): ParseInfo {
		return {
			scopeStack: this.yy.scopeStack,
			tokenInfoStack: this.lexer.tokenInfoStack,
		};
	}

	/**
	 * Set schema of parser.
	 */
	setSchema(schema: Schema): this {
		this.schema = schema;
		return this;
	}

	/**
	 * Get schema of parser.
	 */
	getSchema(): Schema {
		return this.schema;
	}

	/**
	 * Set funcDict of parser.
	 */
	setFuncDict(funcDict: FuncDict): this {
		this.funcDict = funcDict;
		return this;
	}

	/**
	 * Get funcDict of parser.
	 */
	getFuncDict(): FuncDict {
		return this.funcDict;
	}

	/**
	 * Set stageDict of parser.
	 */
	setStageDict(stageDict: StageDict): this {
		this.stageDict = stageDict;
		return this;
	}

	/**
	 * Get stageDict of parser.
	 */
	getStageDict(): StageDict {
		return this.stageDict;
	}

	/**
	 * Set options of parser.
	 */
	setOptions(options: ParserOptions): this {
		this.options = options;
		return this;
	}

	/**
	 * Get options of parser.
	 */
	getOptions(): ParserOptions {
		return this.options;
	}

	/**
	 * Parse a formula.
	 * @param input - formula to be parsed.
	 * @param schema - schema for parsing. Default: this.schema
	 * @param funcDict - funcDict for parsing. Default: this.fincDict
	 * @param stageDict - stageDict for parsing. Default: this.stageDict
	 * @param options - options for parsing. Default: this.options
	 */
	parse(
		input: string,
		schema: Schema = this.schema,
		funcDict: FuncDict = this.funcDict,
		stageDict: StageDict = this.stageDict,
		options: Partial<ParserOptions> = this.options
	): ParseResult {
		try {
			this.yy.scopeStack.push(createRootScope(schema));
			this.yy.funcDict = funcDict;
			this.yy.stageDict = stageDict;
			this.yy.options = { ...this.options, ...options };
			const expr = super.parse(input);
			const info = this.getParseInfo();
			const result = { ...expr, info };
			this.reset();
			return result;
		} catch (err) {
			const _err = err instanceof BaseErr ? err : new UnexpectedErr(err);
			_err.info = this.getParseInfo();
			this.reset();
			throw _err;
		}
	}

	/**
	 * Parse a formula template.
	 * @param specs - specificiation of variables to be used in formula template.
	 * @param schema - schema for parsing. Default: this.schema
	 * @param funcDict - funcDict for parsing. Default: this.fincDict
	 * @param stageDict - stageDict for parsing. Default: this.stageDict
	 * @param options - options for parsing. Default: this.options
	 */
	parseTemplate(
		specs: TemplateVarSpec[],
		schema: Schema = this.schema,
		funcDict: FuncDict = this.funcDict,
		stageDict: StageDict = this.stageDict,
		options: ParserOptions = this.options
	) {
		const expectedVarsCount = specs.length;
		return <T extends (...args: any[]) => any>(
			formulaParts: TemplateStringsArray,
			...getVarVals: (T | null)[]
		) => {
			const receivedVarsCount = getVarVals.length;
			if (receivedVarsCount !== expectedVarsCount)
				throw new TemplateErr({
					specs,
					formulaParts,
					reason: 'Invalid number of template variables.',
				});

			const templateScope = createTemplateScope(specs);
			const varDict = templateScope.varDict;
			const varKeys = Object.keys(varDict);
			const formula = formulaParts.reduce(
				(_formula, formulaPart, i) =>
					`${_formula}${formulaPart}${varKeys[i] ?? ''}`,
				''
			);
			this.yy.scopeStack.push(templateScope);
			const parsed = this.parse(
				formula,
				schema,
				funcDict,
				stageDict,
				options
			);
			return (...args: Parameters<T>): ParseResult => {
				varKeys.forEach((key, i) => {
					const getVarVal = getVarVals[i] ?? ((..._args) => _args[i]);
					// { $ifNull: [null, v] } is identical to v
					varDict[key]!.val.$ifNull = [null, getVarVal(...args)];
				});
				return {
					typ: parsed.typ,
					val: deepClone(parsed.val),
					dep: parsed.dep,
					info: parsed.info,
				};
			};
		};
	}
}
