export interface Dictionary<T = any> {
	[key: string]: T | undefined;
}

export type Mutable<T> = { -readonly [K in keyof T]: T[K] };

export type Loc = [start: number, end: number];

export interface YYLoc {
	readonly first_line?: number;
	readonly last_line?: number;
	readonly first_column?: number;
	readonly last_column?: number;
	readonly range: Loc;
}

export interface TokenInfo {
	readonly token: string;
	readonly lexeme: string;
	readonly val: any;
	readonly loc: Loc;
}

export interface AnyTyp {
	readonly type: 'any';
	readonly literal?: undefined;
}

export interface NeverTyp {
	readonly type: 'never';
	readonly literal?: undefined;
}

export interface UnknownTyp {
	readonly type: 'unknown';
	readonly literal?: undefined;
}

export type BooleanTyp = NonLiteralBooleanTyp | LiteralBooleanTyp;

export interface NonLiteralBooleanTyp {
	readonly type: 'boolean';
	readonly literal?: undefined;
}

export interface LiteralBooleanTyp {
	readonly type: 'boolean';
	readonly literal: true;
}

export type DateTyp = NonLiteralDateTyp | LiteralDateTyp;

export interface NonLiteralDateTyp {
	readonly type: 'date';
	readonly literal?: undefined;
}

export interface LiteralDateTyp {
	readonly type: 'date';
	readonly literal: true;
}

export type NumberTyp = NonLiteralNumberTyp | LiteralNumberTyp;

export interface NonLiteralNumberTyp {
	readonly type: 'number';
	readonly literal?: undefined;
}

export interface LiteralNumberTyp {
	readonly type: 'number';
	readonly literal: true;
}

export type StringTyp = NonLiteralStringTyp | LiteralStringTyp;

export interface NonLiteralStringTyp {
	readonly type: 'string';
	readonly literal?: undefined;
}

export interface LiteralStringTyp {
	readonly type: 'string';
	readonly literal: true;
}

export type ArrayTyp = NonLiteralArrayTyp | LiteralArrayTyp;

export interface NonLiteralArrayTyp {
	readonly type: 'array';
	readonly items: NonLiteralTyp;
	readonly literal?: undefined;
}

export interface LiteralArrayTyp {
	readonly type: 'array';
	readonly items: Typ[];
	readonly literal: true;
}

export type ObjectTyp = NonLiteralObjectTyp | LiteralObjectTyp;

export interface NonLiteralObjectTyp {
	readonly type: 'object';
	readonly properties: Dictionary<NonLiteralTyp>;
	readonly additionalProperties?: NonLiteralTyp;
	readonly optional?: string[];
	readonly literal?: undefined;
}

export interface LiteralObjectTyp {
	readonly type: 'object';
	readonly properties: Dictionary<Typ>;
	readonly additionalProperties?: Typ;
	readonly optional?: string[];
	readonly literal: true;
}

export interface AccumulatorTyp {
	readonly type: 'accumulator';
	readonly in: NonLiteralTyp;
	readonly literal?: undefined;
}

export interface CallbackTyp {
	readonly type: 'callback';
	readonly args: Typ[];
	readonly in: Typ;
	readonly literal?: undefined;
}

export interface PipelineTyp {
	readonly type: 'pipeline';
	readonly literal?: undefined;
}

export type Typ =
	| AnyTyp
	| NeverTyp
	| UnknownTyp
	| BooleanTyp
	| DateTyp
	| NumberTyp
	| StringTyp
	| ArrayTyp
	| ObjectTyp
	| AccumulatorTyp
	| CallbackTyp
	| PipelineTyp;

export type LiteralTyp =
	| LiteralBooleanTyp
	| LiteralDateTyp
	| LiteralNumberTyp
	| LiteralStringTyp
	| LiteralArrayTyp
	| LiteralObjectTyp;

export type NonLiteralTyp =
	| AnyTyp
	| NeverTyp
	| UnknownTyp
	| NonLiteralBooleanTyp
	| NonLiteralDateTyp
	| NonLiteralNumberTyp
	| NonLiteralStringTyp
	| NonLiteralArrayTyp
	| NonLiteralObjectTyp
	| AccumulatorTyp
	| CallbackTyp
	| PipelineTyp;

export type AssignableTyp<T extends Typ> = T extends LiteralTyp
	? T
	: T | AnyTyp | NeverTyp;

export type LiteralObjectDep = Dictionary<Dep>;

export type LiteralArrayDep = Dep[];

export type Dep = string[] | LiteralArrayDep | LiteralObjectDep;

export interface CallbackVal {
	argKeys: string[];
	in: any;
}

export interface AnyExpr {
	readonly typ: AnyTyp;
	readonly val: any;
	readonly dep: Dep;
}

export interface NeverExpr {
	readonly typ: NeverTyp;
	readonly val: any;
	readonly dep: string[];
}

export interface UnknownExpr {
	readonly typ: UnknownTyp;
	readonly val: any;
	readonly dep: Dep;
}

export type BooleanExpr = NonLiteralBooleanExpr | LiteralBooleanExpr;

export interface NonLiteralBooleanExpr {
	readonly typ: NonLiteralBooleanTyp;
	readonly val: any;
	readonly dep: string[];
}

export interface LiteralBooleanExpr {
	readonly typ: LiteralBooleanTyp;
	readonly val: boolean;
	readonly dep: [];
}

export type DateExpr = NonLiteralDateExpr | LiteralDateExpr;

export interface NonLiteralDateExpr {
	readonly typ: DateTyp;
	readonly val: any;
	readonly dep: string[];
}

export interface LiteralDateExpr {
	readonly typ: DateTyp;
	readonly val: Date;
	readonly dep: [];
}

export type NumberExpr = NonLiteralNumberExpr | LiteralNumberExpr;

export interface NonLiteralNumberExpr {
	readonly typ: NonLiteralNumberTyp;
	readonly val: any;
	readonly dep: string[];
}

export interface LiteralNumberExpr {
	readonly typ: NumberTyp;
	readonly val: number;
	readonly dep: [];
}

export type StringExpr = NonLiteralStringExpr | LiteralStringExpr;

export interface NonLiteralStringExpr {
	readonly typ: NonLiteralStringTyp;
	readonly val: any;
	readonly dep: string[];
}

export interface LiteralStringExpr {
	readonly typ: LiteralStringTyp;
	readonly val: string;
	readonly dep: [];
}

export type ArrayExpr = NonLiteralArrayExpr | LiteralArrayExpr;

export interface NonLiteralArrayExpr {
	readonly typ: NonLiteralArrayTyp;
	readonly val: any;
	readonly dep: string[];
}

export interface LiteralArrayExpr {
	readonly typ: LiteralArrayTyp;
	readonly val: any[];
	readonly dep: LiteralArrayDep;
}

export type ObjectExpr = NonLiteralObjectExpr | LiteralObjectExpr;

export interface NonLiteralObjectExpr {
	readonly typ: NonLiteralObjectTyp;
	readonly val: any;
	readonly dep: string[];
}

export interface LiteralObjectExpr {
	readonly typ: LiteralObjectTyp;
	readonly val: Dictionary;
	readonly dep: LiteralObjectDep;
}

export interface UnspecifiedExpr {
	typ: Typ;
	val: any;
	dep: Dep;
}

export type Expr =
	| AnyExpr
	| BooleanExpr
	| DateExpr
	| NeverExpr
	| NumberExpr
	| StringExpr
	| UnknownExpr
	| ArrayExpr
	| ObjectExpr
	| UnspecifiedExpr;

export type LiteralExpr =
	| LiteralBooleanExpr
	| LiteralDateExpr
	| LiteralNumberExpr
	| LiteralStringExpr
	| LiteralArrayExpr
	| LiteralObjectExpr;

export type NonLiteralExpr =
	| AnyExpr
	| NeverExpr
	| UnknownExpr
	| NonLiteralBooleanExpr
	| NonLiteralDateExpr
	| NonLiteralNumberExpr
	| NonLiteralStringExpr
	| NonLiteralArrayExpr
	| NonLiteralObjectExpr;

export interface Accumulator {
	readonly typ: AccumulatorTyp;
	readonly val: any;
	readonly dep: Dep;
}

export interface Callback {
	readonly typ: CallbackTyp;
	readonly val: CallbackVal;
	readonly dep: Dep;
}

export interface Pipeline {
	readonly typ: PipelineTyp;
	readonly val: any[];
	readonly dep: string[];
}

export type ExtendedExpr = Expr | Accumulator | Callback | Pipeline;

export type ExtendedExprLoc = ExtendedExpr & { loc?: Loc };

export type AssignableExpr<E extends ExtendedExpr> = E extends LiteralExpr
	? E
	: E | AnyExpr | NeverExpr;

export interface Var {
	readonly typ: Typ;
	readonly val: any;
	readonly dep: Dep;
}

export type VarDict = Dictionary<Var>;

export interface ParseInfo {
	readonly scopeStack: Scope[];
	readonly tokenInfoStack: TokenInfo[];
}

export interface ParseResult {
	typ: Typ;
	val: any;
	dep: Dep;
	info: ParseInfo;
}

export interface ParserOptions {
	readonly reservedSuffix: string;
	readonly undefinedVarTyp: Typ;
	readonly compatMode: boolean;
}

export interface YY {
	funcDict: FuncDict;
	stageDict: StageDict;
	scopeStack: Scope[];
	options: ParserOptions;
}

export interface ArgsSig {
	readonly typs: (Typ | ((...args: any[]) => Typ))[];
	readonly spreadTyp?: Typ | ((...args: any[]) => Typ);
	readonly minLen?: number;
	readonly maxLen?: number;
}

export type Func = {
	readonly argsSigs: ArgsSig[];
	readonly expr: (...args: any[]) => Expr;
	readonly onFuncArg?: (funcScope: FuncScope) => void;
	readonly onFuncEnd?: (funcScope: FuncScope) => void;
};

export type FuncDict = Dictionary<Func | ((yy: YY) => Func)>;

export interface Stage {
	readonly argsSigs: ArgsSig[];
	readonly pipeline: (...args: any[]) => Pipeline;
	readonly varDict?: (varDict: VarDict, ...args: any[]) => VarDict;
	readonly onStageArg?: (stageScope: StageScope) => void;
	readonly onStageEnd?: (stageScope: StageScope) => void;
}

export type StageDict = Dictionary<Stage | ((yy: YY) => Stage)>;

export interface RootScope {
	readonly name: 'ROOT';
	varDict: VarDict;
}

export interface ArrayScope {
	readonly name: 'ARRAY';
	readonly varDict?: undefined;
	literalExpr: LiteralArrayExpr;
	combiningExprs: Expr[];
}

export interface ObjectScope {
	readonly name: 'OBJECT';
	readonly varDict?: undefined;
	literalExpr: LiteralObjectExpr;
	combiningExprs: Expr[];
}

export interface LetScope {
	readonly name: 'LET';
	readonly varDict: VarDict;
	readonly vars: Dictionary<any>;
	assignCompleted: boolean;
}

export interface FuncScope {
	readonly name: 'FUNC';
	readonly varDict?: undefined;
	readonly funcKey: string;
	readonly func: Func;
	readonly args: ExtendedExprLoc[];
	argsSigs: ArgsSig[];
}

export interface StageScope {
	readonly name: 'STAGE';
	readonly varDict?: undefined;
	readonly stageKey: string;
	readonly stage: Stage;
	readonly args: ExtendedExprLoc[];
	argsSigs: ArgsSig[];
}

export interface CallbackScope {
	readonly name: 'CALLBACK';
	readonly varDict: VarDict;
	readonly funcKey: string;
	readonly argTyps: Typ[];
	readonly argKeys: string[];
}

export interface TemplateScope {
	readonly name: 'TEMPLATE';
	readonly varDict: VarDict;
}

export type Scope =
	| RootScope
	| LetScope
	| ArrayScope
	| ObjectScope
	| FuncScope
	| StageScope
	| CallbackScope
	| TemplateScope;

export type Schema = Dictionary<NonLiteralTyp>;

export type TemplateVarSpec =
	| Typ
	| {
			readonly typ: Typ;
			readonly key?: string;
			readonly dep?: Dep;
	  };
