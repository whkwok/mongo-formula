import createDebug from 'debug';

import { Lexer } from './Lexer';

const debug = createDebug('mongo-formula:mongoFormulaLexer');

enum LexerState {
	ROOT = 0, // 0 is the default state of Lexer
	SINGLE_QUOTING,
	SINGLE_QUOTE_ESCAPING,
	DOUBLE_QUOTING,
	DOUBLE_QUOTE_ESCAPING,
}

const mongoFormulaLexer = new Lexer();

/* skip whitespaces */
mongoFormulaLexer.addRule({ pattern: /\s+|\t+/ });

/* NUM (Support decimal fractions, exponential notation, percentage.) */
mongoFormulaLexer.addRule({
	pattern: /[0-9]+(?:\.[0-9]+)?(?:e[-+]?[0-9]+)?%?/,
	action: function (lexeme: string) {
		const val = lexeme.endsWith('%')
			? Number(lexeme.slice(0, -1)) / 100
			: Number(lexeme);
		debug(`${lexeme} -> NUM`);
		return { token: 'NUM', val };
	},
});

/* BOOL (true, false) */
mongoFormulaLexer.addRule({
	pattern: /true|false/,
	action: function (lexeme: string) {
		const val = lexeme === 'true';
		debug(`${lexeme} -> BOOL`);
		return { token: 'BOOL', val };
	},
});

/* NULL */
mongoFormulaLexer.addRule({
	pattern: /null/,
	action: function () {
		debug(`null -> NULL`);
		return { token: 'NULL', val: null };
	},
});

/* SYMBOL */
/*
	length = 4
		{&&} {||} {&!} {!=} {<=} {>=} {==}
	length = 3
		... let
	length = 2
		** != <= >= == ?? && || => >> as in
	length = 1
		+ - * / ( ) { } [ ] . , % ; : ! ? | < > =
*/
mongoFormulaLexer.addRule({
	pattern: /{(?:&[&!]|\|\||[!<>=]=)}|\.\.\.|let|[!<>=]=|\*\*|\?\?|&&|\|\||=>|>>|as|in|[+\-*/(){}[\].,%;:!?|<>=]/,
	action: function (lexeme: string) {
		debug(`${lexeme} -> ${lexeme}`);
		return { token: lexeme };
	},
});

/* CB */
mongoFormulaLexer.addRule({
	pattern: /\(/,
	trailingPattern: /[^()]*\)\s*=>/,
	action: function () {
		debug(`( -> CB_S`);
		return { token: 'CB_S' };
	},
});

/* KEY */
mongoFormulaLexer.addRule({
	pattern: /\w+/,
	action: function (lexeme: string) {
		debug(`${lexeme} -> KEY`);
		return { token: 'KEY', val: lexeme };
	},
});

/* STR (Support single quotes, double quotes.) */
// single quotes
mongoFormulaLexer.addRule({
	pattern: /'/,
	action: function () {
		debug(`' -> STR_S`);
		return { token: 'STR_S', nextState: LexerState.SINGLE_QUOTING };
	},
});

mongoFormulaLexer.addRule({
	pattern: /[^'\\]*/,
	action: function (lexeme: string) {
		debug(`${lexeme} -> STR`);
		return { token: 'STR', val: lexeme };
	},
	state: LexerState.SINGLE_QUOTING,
});

mongoFormulaLexer.addRule({
	pattern: /[^']*?\\/,
	action: function (lexeme: string) {
		debug(`${lexeme} -> STR`);
		const val = lexeme.slice(0, -1);
		if (!val) return { nextState: LexerState.SINGLE_QUOTE_ESCAPING };
		return {
			token: 'STR',
			val,
			nextState: LexerState.SINGLE_QUOTE_ESCAPING,
		};
	},
	state: LexerState.SINGLE_QUOTING,
});

mongoFormulaLexer.addRule({
	pattern: /./,
	action: function (lexeme: string) {
		debug(`${lexeme} -> STR`);
		return {
			token: 'STR',
			val: lexeme,
			nextState: LexerState.SINGLE_QUOTING,
		};
	},
	state: LexerState.SINGLE_QUOTE_ESCAPING,
});

mongoFormulaLexer.addRule({
	pattern: /'/,
	action: function () {
		debug(`' -> STR_E`);
		return { token: 'STR_E', nextState: LexerState.ROOT };
	},
	state: LexerState.SINGLE_QUOTING,
});

// double quotes
mongoFormulaLexer.addRule({
	pattern: /"/,
	action: function () {
		debug(`" -> STR_S`);
		return { token: 'STR_S', nextState: LexerState.DOUBLE_QUOTING };
	},
});

mongoFormulaLexer.addRule({
	pattern: /[^"\\]*/,
	action: function (lexeme: string) {
		debug(`${lexeme} -> STR`);
		return { token: 'STR', val: lexeme };
	},
	state: LexerState.DOUBLE_QUOTING,
});

mongoFormulaLexer.addRule({
	pattern: /[^"]*?\\/,
	action: function (lexeme: string) {
		debug(`${lexeme} -> STR`);
		const val = lexeme.slice(0, -1);
		if (!val) return { nextState: LexerState.DOUBLE_QUOTE_ESCAPING };
		return {
			token: 'STR',
			val,
			nextState: LexerState.DOUBLE_QUOTE_ESCAPING,
		};
	},
	state: LexerState.DOUBLE_QUOTING,
});

mongoFormulaLexer.addRule({
	pattern: /./,
	action: function (lexeme: string) {
		debug(`${lexeme} -> STR`);
		return {
			token: 'STR',
			val: lexeme,
			nextState: LexerState.DOUBLE_QUOTING,
		};
	},
	state: LexerState.DOUBLE_QUOTE_ESCAPING,
});

mongoFormulaLexer.addRule({
	pattern: /"/,
	action: function () {
		debug(`" -> STR_E`);
		return { token: 'STR_E', nextState: LexerState.ROOT };
	},
	state: LexerState.DOUBLE_QUOTING,
});

/* EOF */
mongoFormulaLexer.addRule({
	pattern: /$/,
	action: function () {
		debug('-> EOF');
		return { token: 'EOF' };
	},
});

export { mongoFormulaLexer };
