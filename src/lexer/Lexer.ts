import { LexErr } from '../errors';
import { Loc, TokenInfo, YYLoc } from '../types';

interface ActionResult {
	token?: string;
	val?: any;
	nextState?: number;
}

type Action = (lexeme: string) => ActionResult | void;

interface RuleDescription {
	pattern: RegExp;
	trailingPattern?: RegExp;
	action?: Action;
	state?: number;
}

interface Rule {
	fullPattern: RegExp;
	withTrailingPattern: boolean;
	action?: Action;
}

function errAction(this: Lexer) {
	throw new LexErr({ pos: this.pos, char: this.input[this.pos] });
}

export class Lexer {
	state: number;
	rules: { [state: number]: Rule[] };
	tokenInfoStack: TokenInfo[];
	input: string;
	pos: number; // position of cursor
	yytext: any; // the value corresponding to the token
	yylloc: YYLoc;
	options: { ranges: true };

	constructor() {
		this.state = 0;
		this.rules = {};
		this.tokenInfoStack = [];
		this.pos = 0;
		this.input = '';
		this.yytext = null;
		this.yylloc = { range: [0, 0] };
		this.options = { ranges: true }; // ranges options is used by Jison Parser
	}
	// Jison Parser use Object.create(this.lexer) to instantiate a new lexer in parse method.
	// In order to notice the change in lexer state from our lexer instance, all methods below use arrow function to bind this.

	reset = (): void => {
		this.state = 0;
		this.tokenInfoStack = [];
		this.input = '';
		this.pos = 0;
		this.yytext = null;
		this.yylloc = { range: [0, 0] };
	};

	setInput = (input: string): void => {
		this.reset();
		this.input = input;
	};

	addRule = ({
		pattern,
		trailingPattern,
		action,
		state = 0,
	}: RuleDescription): void => {
		let source = pattern.source;
		const flags = pattern.flags;
		if (trailingPattern) {
			if (trailingPattern.flags !== flags)
				throw new Error(
					'trailingPattern must have same flags as pattern.'
				);
			source += '(?=(' + trailingPattern.source + '))';
		}
		// Add sticky to fullPattern
		const fullPattern = new RegExp(
			source,
			pattern.sticky ? flags : flags + 'y'
		);
		if (!this.rules[state]) this.rules[state] = [];
		this.rules[state].push({
			fullPattern,
			withTrailingPattern: !!trailingPattern,
			action,
		});
	};

	lex = (): string | undefined => {
		const l = this.input.length;
		while (this.pos <= l) {
			const currentPos = this.pos;
			const [lexeme, lexemeLength, actionResult] = this.applyBestRule();
			this.pos = currentPos + (lexemeLength ? lexemeLength : 1); // Minimum pos move is 1
			if (actionResult) {
				const { token, val, nextState } = actionResult;
				if (nextState !== undefined) this.state = nextState;
				if (token !== undefined) {
					const loc: Loc = [currentPos, currentPos + lexemeLength];
					this.yytext = val;
					this.yylloc = { range: loc };
					this.tokenInfoStack.push({
						token,
						lexeme,
						val,
						loc,
					});
					return token;
				}
			}
		}
	};

	private applyBestRule = (): [string, number, ActionResult | undefined] => {
		// Find the "best" (longest lexeme matched) rule and apply it.
		// If there are more than one rules having the same longest lexeme length, the one appear first is chosen.
		let bestLexeme = '';
		let bestLexemeLength: number | undefined;
		let bestAction: Action | undefined = errAction;
		const _rules = this.rules[this.state];
		if (_rules) {
			for (let i = 0, l = _rules.length; i < l; ++i) {
				const { fullPattern, withTrailingPattern, action } = _rules[i];
				fullPattern.lastIndex = this.pos;
				const result = fullPattern.exec(this.input);
				if (result) {
					const lexeme = result[0];
					const lexemeLength = lexeme.length;
					const trailingLength = withTrailingPattern
						? result[result.length - 1].length
						: 0;
					if (
						bestLexemeLength === undefined ||
						lexemeLength + trailingLength > bestLexemeLength
					) {
						bestLexeme = lexeme;
						bestAction = action;
						bestLexemeLength = lexemeLength;
					}
				}
			}
		}
		bestLexemeLength ??= 0;
		const actionResult = bestAction?.call(this, bestLexeme) as
			| ActionResult
			| undefined;

		return [bestLexeme, bestLexemeLength, actionResult];
	};
}
