%right '?'
%left '||'
%left '&&'
%left '==' '!='
%left '>' '<' '>=' '<=' 'in'
%left '+' '-' 
%left '*' '/' '%'
%left NEG
%right '**'
%nonassoc '{==}' '{!=}' '{>=}' '{<=}'
%left '{||}'
%left '{&&}' '{&!}'
%left '??'
%left '!'
%left '[' '.'
%left ','
%left 'as'

%{
    const createDebug = require('debug');
    const {
        parse_var,
        parse_array_start,
        parse_array_elems,
        parse_array_elems_spread,
        parse_array_end,
        parse_object_start,
        parse_object_entries,
        parse_object_entries_spread,
        parse_object_end,
        parse_object_access,
        parse_let_stmt_start,
        parse_asgmts,
        parse_let_stmt_end,
        parse_let_block_end,
        parse_func_start,
        parse_func_args,
        parse_func_end,
        parse_cb_start,
        parse_cb_arg_keys,
        parse_cb_end,
        parse_expr_NUM,
        parse_expr_BOOL,
        parse_expr_NULL,
        parse_expr_string,
        parse_expr_var,
        parse_as_typ,
        parse_stage_start,
        parse_stage_args,
        parse_stage_end,
        parse_pl_combine,
        callFunc,
        zeroExpr,
    } = require('./parse');
    const debug = createDebug('mongo-formula:Parser');
%}

%%

entry
    : expr EOF
        {
            debug('expr EOF -> entry');
            return $1;
        }
    | pl EOF
        {
            debug('pl EOF -> entry');
            return $1;
        }
    ;

var
    : KEY
        {
            debug('KEY -> var');
            $$ = parse_var($1, @1.range, yy);
        }
    ;

string_content
    : STR
        {
            debug('STR -> string_content');
            $$ = $1;
        }
    | string_content string_content
        {
            debug('string_content string_content -> string_content');
            $$ = $1 + $2;
        }
    ;

string
    : STR_S string_content STR_E
        {
            debug('STR_S string_content STR_E -> string');
            $$ = $2;
        }
    ;

array_start
    : '['
        {
            debug('[ -> array_start');
            parse_array_start(yy);
        }
    ;

array_elems
    : /* empty */
        {
            debug('/* empty */ -> array_elems');
        }
    | expr
        {
            debug('expr -> array_elems');
            parse_array_elems($1, yy);
        }
    | '...' expr
        {
            debug('... expr -> array_elems');
            parse_array_elems_spread($2, @2.range, yy);
        }
    | array_elems ',' array_elems
        {
            debug('array_elems, array_elems -> array_elems');
        }
    ;

object_start
    : '{'
        {
            debug('{ -> object_start');
            parse_object_start(yy);
        }
    ;

object_entries
    : /* empty */
        {
            debug('/* empty */ -> object_entries');
        }
    | var
        {
            debug('var -> object_entries');
            parse_object_entries($1, parse_expr_var($1, yy), yy);
        }
    | KEY ':' expr
        {
            debug('KEY : expr -> object_entries');
            parse_object_entries($1, $3, yy);
        }
    | string ':' expr
        {
            debug('string : expr -> object_entries');
            parse_object_entries($1, $3, yy);
        }
    | '...' expr
        {
            debug('... expr -> object_entries');
            parse_object_entries_spread($2, @2.range, yy);
        }
    | object_entries ',' object_entries
        {
            debug('object_entries , object_entries -> object_entries');
        }
    ;

let_stmt_start
    : 'let'
        {
            debug('let -> let_stmt_start');
            parse_let_stmt_start(yy);
        }
    ;

asgmts
    : var '=' expr
        {
            debug('var = expr -> asgmts');
            parse_asgmts($1, @1.range, $3, yy);
        }
    | asgmts ',' asgmts
        {
            debug('asgmts , asgmts -> asgmts');
        }
    ;

let_stmt
    : let_stmt_start asgmts ';'
        {
            debug('let_stmt_start asgmts ; -> let_stmt');
            parse_let_stmt_end(yy);
        }
    ;

func_start
    : KEY '('
        {
            debug('KEY ( -> func_start');
            parse_func_start($1, @1.range, yy);
        }
    ;

func_args
    : /* empty */
        {
            debug('/* empty */ -> func_args');
        }
    | expr
        {
            debug('expr -> func_args');
            parse_func_args($1, @1.range, yy);
        }
    | cb
        {
            debug('cb -> func_args');
            parse_func_args($1, @1.range, yy);
        }
    | func_args ',' func_args
        {
            debug('func_args , func_args -> func_args');
        }
    ;

cb_start
    : 'CB_S'
        {
            debug('CB_S -> cb_start');
            parse_cb_start(yy);
        }
    ;

cb_arg_keys
    : /* empty */
        {
            debug('/* empty */ -> cb_arg_keys');
        }
    | var
        {
            debug('var -> cb_arg_keys');
            parse_cb_arg_keys($1, yy);
        }
    | cb_arg_keys ',' cb_arg_keys
        {
            debug('cb_arg_keys , cb_arg_keys -> cb_arg_keys');
        }
    ;

cb
    : cb_start cb_arg_keys ')' '=>' expr
        {
            debug('cb_start cb_arg_keys ) => expr -> cb');
            $$ = parse_cb_end($5, yy);
        }
    ;

expr
    : NUM
        {
            debug('NUM -> expr');
            $$ = parse_expr_NUM($1);
        }
    | BOOL
        {
            debug('BOOL -> expr');
            $$ = parse_expr_BOOL($1);
        }
    | NULL
        {
            debug('NULL -> expr');
            $$ = parse_expr_NULL($1); 
        }
    | string
        {
            debug('string -> expr');
            $$ = parse_expr_string($1, @1.range, yy);
        }
    | var
        {
            debug('var -> expr');
            $$ = parse_expr_var($1, yy);
        }
    | '(' expr ')'
        {
            debug('( expr ) -> expr');
            $$ = $2;
        }
    | object_start object_entries '}'
        {
            debug('object_start object_entries } -> expr');
            $$ = parse_object_end(yy);
        }
    | array_start array_elems ']'
        {
            debug('array_start array_elems ] -> expr');
            $$ = parse_array_end(yy);
        }
    | expr '.' KEY
        {
            debug('expr . KEY -> expr');
            $$ = parse_object_access($1, @1.range, $3, @3.range, yy);
        }
    | expr '[' string ']'
        {
            debug('expr [ string ] -> expr');
            $$ = parse_object_access($1, @1.range, $3, @3.range, yy);
        }
    | expr '[' expr ']'
        {
            debug('expr [ expr ] -> expr');
            $$ = callFunc('SMARTINDEX', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '[' expr ':' expr ']'
        {
            debug('expr [ expr : expr ] -> expr');
            $$ = callFunc('SMARTSLICE', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }, { ...$5, loc: @5.range }], yy);
        }
    | expr '[' ':' expr ']'
        {
            debug('expr [ : expr ] -> expr');
            $$ = callFunc('SMARTSLICE', [{ ...$1, loc: @1.range }, zeroExpr, { ...$4, loc: @4.range }], yy);
        }
    | expr '[' expr ':' ']'
        {
            debug('expr [ expr : ] -> expr');
            $$ = callFunc('SMARTSLICE', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | '!' expr
        {
            debug('! expr -> expr');
            $$ = callFunc('NOT', [{ ...$2, loc: @2.range }], yy);
        }
    | expr '??' expr
        {
            debug('expr ?? expr -> expr');
            $$ = callFunc('IFNULL', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '{||}' expr
        {
            debug('expr {||} expr -> expr');
            $$ = callFunc('SETUNION', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '{&&}' expr
        {
            debug('expr {&&} expr -> expr');
            $$ = callFunc('SETINTERSECTION', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '{&!}' expr
        {
            debug('expr {&!} expr -> expr');
            $$ = callFunc('SETDIFFERENCE', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '{<=}' expr
        {
            debug('expr {<=} expr -> expr');
            $$ = callFunc('SETISSUBSET', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '{>=}' expr
        {
            debug('expr {>=} expr -> expr');
            $$ = callFunc('SETISSUPERSET', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '{==}' expr
        {
            debug('expr {==} expr -> expr');
            $$ = callFunc('SETEQUALS', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '{!=}' expr
        {
            debug('expr {!=} expr -> expr');
            $$ = callFunc('SETNOTEQUALS', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '**' expr
        {
            debug('expr ** expr -> expr');
            $$ = callFunc('POW', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '*' expr
        {
            debug('expr * expr -> expr');
            $$ = callFunc('MULTIPLY', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '/' expr
        {
            debug('expr / expr -> expr');
            $$ = callFunc('DIVIDE', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '%' expr
        {
            debug('expr % expr -> expr');
            $$ = callFunc('MOD', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | '-' expr %prec NEG
        {
            debug('- expr -> expr');
            $$ = callFunc('NEG', [{ ...$2, loc: @2.range }], yy);
        }
    | expr '+' expr
        {
            debug('expr + expr -> expr');
            $$ = callFunc('SMARTADD', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '-' expr
        {
            debug('expr - expr -> expr');
            $$ = callFunc('SUBTRACT', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr 'in' expr
        {
            debug('expr in expr -> expr');
            $$ = callFunc('IN', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '>' expr
        {
            debug('expr > expr -> expr');
            $$ = callFunc('GT', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '<' expr
        {
            debug('expr < expr -> expr');
            $$ = callFunc('LT', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '>=' expr
        {
            debug('expr >= expr -> expr');
            $$ = callFunc('GTE', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '<=' expr
        {
            debug('expr <= expr -> expr');
            $$ = callFunc('LTE', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '==' expr
        {
            debug('expr == expr -> expr');
            $$ = callFunc('EQ', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '!=' expr
        {
            debug('expr != expr -> expr');
            $$ = callFunc('NE', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '&&' expr
        {
            debug('expr && expr -> expr');
            $$ = callFunc('AND', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | expr '||' expr
        {
            debug('expr || expr -> expr');
            $$ = callFunc('OR', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }], yy);
        }
    | '|' expr '|'
        {
            debug('| expr |-> expr');
            $$ = callFunc('SMARTLEN', [{ ...$2, loc: @2.range }], yy);
        }
    | expr '?' expr ':' expr
        {
            debug('expr ? expr : expr -> expr');
            $$ = callFunc('COND', [{ ...$1, loc: @1.range }, { ...$3, loc: @3.range }, { ...$5, loc: @5.range }], yy);
        }
    | '{' let_stmt expr '}'
        {
            debug('{ let_stmt expr } -> expr');
            $$ = parse_let_block_end($3, yy);
        }
    | func_start func_args ')'
        {
            debug('func_start func_args ) -> expr');
            $$ = parse_func_end(yy);
        }
    | expr as expr
        {
            debug('expr as expr -> expr');
            $$ = parse_as_typ($1, $3, @3.range);
        }
    ;

stage_start
    : '>>' KEY '('
        {
            debug('>> KEY ( -> stage_start');
            parse_stage_start($2, @2.range, yy);
        }
    ;

stage_args
    : /* empty */
        {
            debug('/* empty */ -> stage_args');
        }
    | expr
        {
            debug('expr -> stage_args');
            parse_stage_args($1, @1.range, yy);
        }
    | stage_args ',' stage_args
        {
            debug('stage_args , stage_args -> stage_args');
        }
    ;

pl 
    : stage_start stage_args ')'
        {
            debug('stage_start stage_args ) -> pl');
            $$ = parse_stage_end(yy);
        }
    | pl pl
        {
            debug('pl pl -> pl');
            $$ = parse_pl_combine($1, $2);
        }
    ;