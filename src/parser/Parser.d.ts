export class Parser {
	parse(input: string): any;
	protected parseError(message: string, hash: any): void;
}
