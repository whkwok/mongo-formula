import createDebug from 'debug';

import { ExprTypErr, ExprValErr, KeyErr } from '../errors';
import {
	AssignableTyp,
	Callback,
	Expr,
	ExtendedExpr,
	Loc,
	Pipeline,
	Typ,
	YY,
} from '../types';
import { getExpectedArgTyp, matchArgsSigs } from '../util/arg';
import { unionDep } from '../util/dep';
import { objectExprAccess } from '../util/expr';
import { callFunc } from '../util/func';
import {
	completeArrayScope,
	completeCallbackScope,
	completeFuncScope,
	completeLetScope,
	completeObjectScope,
	completeStageScope,
	createArrayScope,
	createCallbackScope,
	createFuncScope,
	createLetScope,
	createObjectScope,
	createStageScope,
	findScope,
} from '../util/scope';
import {
	arrayTyp,
	booleanTyp,
	isAssignableTyp,
	isCallbackTyp,
	isTyp,
	neverTyp,
	numberTyp,
	objectTyp,
	pipelineTyp,
	stringTyp,
	unknownTyp,
} from '../util/typ';
import { isStringVal } from '../util/val';
import { isVarKey } from '../util/var';

const debug = createDebug('mongo-formula:parse');

function checkExprTyp<T extends Typ>(
	expr: ExtendedExpr,
	loc: Loc | undefined,
	expectedTyp: T
): expr is Expr & { typ: AssignableTyp<T> } {
	const typ = expr.typ;
	if (!isAssignableTyp(typ, expectedTyp))
		throw new ExprTypErr({
			receivedTyp: typ,
			expectedTyps: [expectedTyp],
			loc,
		});
	return true;
}

function checkExprVal(
	expr: ExtendedExpr,
	exprLoc: Loc | undefined,
	expectedVal: (val: any) => boolean,
	errReason: string
): void {
	const val = expr.val;
	if (!expectedVal(val))
		throw new ExprValErr({
			receivedVal: val,
			loc: exprLoc,
			reason: errReason,
		});
}

function checkKey(
	key: string,
	keyLoc: Loc | undefined,
	expectedKey: (key: string) => boolean,
	keyType: 'object' | 'var',
	errReason: string
): void {
	if (!expectedKey(key))
		throw new KeyErr({ key, keyType, loc: keyLoc, reason: errReason });
}

export function parse_expr_NUM(val: number): Expr {
	return { typ: numberTyp(true), val, dep: [] };
}

export function parse_expr_string(
	val: string,
	valLoc: Loc,
	{ scopeStack }: YY,
	literalFuncKey: string = 'LITERAL'
): Expr {
	const scope = scopeStack[scopeStack.length - 1];
	const expr = { typ: stringTyp(true), val, dep: [] };
	checkExprVal(
		expr,
		valLoc,
		(v) =>
			isStringVal(
				v,
				scope.name === 'FUNC' && scope.funcKey === literalFuncKey
			),
		`String must not start with special character "$". Consider using ${literalFuncKey} function.`
	);
	return expr;
}

export function parse_expr_BOOL(val: boolean): Expr {
	return { typ: booleanTyp(true), val, dep: [] };
}

export function parse_expr_NULL(val: null): Expr {
	return { typ: neverTyp(), val, dep: [] };
}

export function parse_var(
	key: string,
	keyLoc: Loc,
	{ options: { reservedSuffix } }: YY
): string {
	checkKey(
		key,
		keyLoc,
		(k) => isVarKey(k, reservedSuffix),
		'var',
		`Variable key must start with lowercase character and not end with ${reservedSuffix}.`
	);
	return key;
}

export function parse_expr_var(
	key: string,
	{ scopeStack, options: { undefinedVarTyp } }: YY
): Expr {
	for (let i = scopeStack.length - 1; i >= 0; --i) {
		const scope = scopeStack[i];
		if (scope.name === 'LET' && !scope.assignCompleted) continue;
		const _var = scope.varDict?.[key];
		if (_var) return { typ: _var.typ, val: _var.val, dep: _var.dep };
	}
	return { typ: undefinedVarTyp, val: '$' + key, dep: [key] };
}

export function parse_array_start({ scopeStack }: YY): void {
	const newScope = createArrayScope();
	debug(`START SCOPE ARRAY (${scopeStack.length})`);
	scopeStack.push(newScope);
}

export function parse_array_elems(expr: Expr, { scopeStack }: YY): void {
	const {
		literalExpr: { typ: literalTyp, val: literalVal, dep: literalDep },
	} = findScope(scopeStack, 'ARRAY', 1);
	literalTyp.items.push(expr.typ);
	literalVal.push(expr.val);
	literalDep.push(expr.dep);
}

export function parse_array_elems_spread(
	expr: Expr,
	exprLoc: Loc,
	{ scopeStack }: YY
): void {
	const scope = findScope(scopeStack, 'ARRAY', 1);
	if (checkExprTyp(expr, exprLoc, arrayTyp(unknownTyp()))) {
		const { literalExpr, combiningExprs } = scope;
		// Push non-empty literalExpr
		if (literalExpr.typ.items) {
			combiningExprs.push(literalExpr);
			// Reset literalExpr
			scope.literalExpr = { typ: arrayTyp([], true), val: [], dep: [] };
		}
		combiningExprs.push(expr);
	}
}

export function parse_array_end(
	yy: YY,
	concatArraysFuncKey: string = 'CONCATARRAYS'
): Expr {
	const { scopeStack } = yy;
	const scope = findScope(scopeStack, 'ARRAY', 1);
	const expr = completeArrayScope(scope, concatArraysFuncKey, yy);
	scopeStack.pop();
	debug(`END SCOPE ARRAY (${scopeStack.length})`);
	return expr;
}

export function parse_object_start({ scopeStack }: YY): void {
	const newScope = createObjectScope();
	debug(`START SCOPE OBJECT (${scopeStack.length})`);
	scopeStack.push(newScope);
}

export function parse_object_entries(
	key: string,
	expr: Expr,
	{ scopeStack }: YY
): void {
	const {
		literalExpr: { typ: literalTyp, val: literalVal, dep: literalDep },
	} = findScope(scopeStack, 'OBJECT', 1);
	literalTyp.properties[key] = expr.typ;
	literalVal[key] = expr.val;
	literalDep[key] = expr.dep;
}

export function parse_object_entries_spread(
	expr: Expr,
	exprLoc: Loc,
	{ scopeStack }: YY
): void {
	const scope = findScope(scopeStack, 'OBJECT', 1);
	if (checkExprTyp(expr, exprLoc, objectTyp({}))) {
		const { literalExpr, combiningExprs } = scope;
		// Push non-empty literalExpr
		if (literalExpr.typ.properties) {
			combiningExprs.push(literalExpr);
			// Reset literalExpr
			scope.literalExpr = {
				typ: objectTyp({}, undefined, undefined, true),
				val: {},
				dep: {},
			};
		}
		combiningExprs.push(expr);
	}
}

export function parse_object_end(
	yy: YY,
	mergeObjectsFuncKey: string = 'MERGEOBJECTS'
): Expr {
	const { scopeStack } = yy;
	const scope = findScope(scopeStack, 'OBJECT', 1);
	const expr = completeObjectScope(scope, mergeObjectsFuncKey, yy);
	scopeStack.pop();
	debug(`END SCOPE OBJECT (${scopeStack.length})`);
	return expr;
}

export function parse_object_access(
	expr: Expr,
	exprLoc: Loc,
	key: string,
	keyLoc: Loc,
	{ options: { reservedSuffix } }: YY
): Expr | undefined {
	if (checkExprTyp(expr, exprLoc, objectTyp({}))) {
		const resultExpr = objectExprAccess(expr, key, reservedSuffix);
		checkKey(
			key,
			keyLoc,
			() => !!resultExpr,
			'object',
			'Object property is not found.'
		);
		return resultExpr;
	}
}

export function parse_let_stmt_start({ scopeStack }: YY): void {
	const newScope = createLetScope();
	debug(`START SCOPE LET (${scopeStack.length})`);
	scopeStack.push(newScope);
}

export function parse_asgmts(
	key: string,
	keyLoc: Loc,
	expr: Expr,
	{ scopeStack }: YY
): void {
	const { varDict, vars } = findScope(scopeStack, 'LET', 1);
	checkKey(
		key,
		keyLoc,
		(k) => !varDict[k],
		'var',
		'Variable key has been already used in this scope.'
	);
	vars[key] = expr.val;
	varDict[key] = {
		typ: expr.typ,
		val: '$$' + key,
		dep: expr.dep,
	};
}

export function parse_let_stmt_end({ scopeStack }: YY): void {
	const scope = findScope(scopeStack, 'LET', 1);
	scope.assignCompleted = true;
}

export function parse_let_block_end(inExpr: Expr, { scopeStack }: YY): Expr {
	const scope = findScope(scopeStack, 'LET', 1);
	const expr = completeLetScope(scope, inExpr);
	scopeStack.pop();
	debug(`END SCOPE LET (${scopeStack.length})`);
	return expr;
}

export function parse_func_start(
	funcKey: string,
	funcKeyLoc: Loc,
	yy: YY
): void {
	const scopeStack = yy.scopeStack;
	const newScope = createFuncScope(funcKey, funcKeyLoc, yy);
	debug(`START SCOPE FUNC (${newScope.funcKey}) (${scopeStack.length})`);
	scopeStack.push(newScope);
}

export function parse_func_args(
	expr: ExtendedExpr,
	exprLoc: Loc,
	{ scopeStack }: YY
): void {
	const scope = findScope(scopeStack, 'FUNC', 1);
	const { funcKey, func, args, argsSigs } = scope;
	args.push({ ...expr, loc: exprLoc });
	scope.argsSigs = matchArgsSigs(funcKey, args, argsSigs);
	func.onFuncArg?.(scope);
}

export function parse_func_end({ scopeStack }: YY): Expr {
	const scope = findScope(scopeStack, 'FUNC', 1);
	const expr = completeFuncScope(scope, true);
	scopeStack.pop();
	debug(`END SCOPE FUNC (${scope.funcKey}) (${scopeStack.length})`);
	return expr;
}

export function parse_stage_start(
	stageKey: string,
	stageKeyLoc: Loc,
	yy: YY
): void {
	const scopeStack = yy.scopeStack;
	const newScope = createStageScope(stageKey, stageKeyLoc, yy);
	debug(`START SCOPE STAGE (${newScope.stageKey}) (${scopeStack.length})`);
	scopeStack.push(newScope);
}

export function parse_stage_args(
	expr: ExtendedExpr,
	exprLoc: Loc,
	{ scopeStack }: YY
): void {
	const scope = findScope(scopeStack, 'STAGE', 1);
	const { stageKey, args, stage, argsSigs } = scope;
	args.push({ ...expr, loc: exprLoc });
	scope.argsSigs = matchArgsSigs(stageKey, args, argsSigs);
	stage.onStageArg?.(scope);
}

export function parse_stage_end({ scopeStack }: YY): Pipeline {
	const scope = findScope(scopeStack, 'STAGE', 1);
	const rootScope = findScope(scopeStack, 'ROOT');
	const [pipeline, varDict] = completeStageScope(
		scope,
		rootScope.varDict,
		true
	);
	rootScope.varDict = varDict;
	scopeStack.pop();
	debug(`END SCOPE STAGE (${scope.stageKey}) (${scopeStack.length})`);
	return pipeline;
}

export function parse_cb_start({ scopeStack }: YY): void {
	const scope = findScope(scopeStack, 'FUNC', 1);
	const { funcKey, args, argsSigs } = scope;
	const index = args.length;
	const expectedArgTyps = argsSigs
		.map((sig) => getExpectedArgTyp(args, sig, index))
		.filter(isCallbackTyp);
	const newScope = createCallbackScope(
		funcKey,
		expectedArgTyps.length === 1 ? expectedArgTyps[0].args : []
	);
	debug(`START SCOPE CALLBACK (${scopeStack.length})`);
	scopeStack.push(newScope);
}

export function parse_cb_arg_keys(key: string, { scopeStack }: YY): void {
	const scope = findScope(scopeStack, 'CALLBACK', 1);
	const { argKeys, argTyps, varDict } = scope;
	const argTyp = argTyps[argKeys.length] ?? unknownTyp();
	argKeys.push(key);
	varDict[key] = { typ: argTyp, val: '$$' + key, dep: [] };
}

export function parse_cb_end(inExpr: Expr, { scopeStack }: YY): Callback {
	const scope = findScope(scopeStack, 'CALLBACK', 1);
	const callback = completeCallbackScope(scope, inExpr);
	scopeStack.pop();
	debug(`END SCOPE CALLBACK (${scopeStack.length})`);
	return callback;
}

export function parse_as_typ(expr: Expr, typExpr: Expr, typExprLoc: Loc): Expr {
	const typ = typExpr.val;
	checkExprVal(typExpr, typExprLoc, isTyp, 'Val must be a valid typ.');
	return { typ, val: expr.val, dep: expr.dep };
}

export function parse_pl_combine(pl0: Pipeline, pl1: Pipeline): Pipeline {
	return {
		typ: pipelineTyp(),
		val: [...pl0.val, ...pl1.val],
		dep: unionDep(pl0.dep, pl1.dep),
	};
}

export const zeroExpr: Expr = { typ: numberTyp(true), val: 0, dep: [] };

export { callFunc }; // Re-export for Parse.jison
