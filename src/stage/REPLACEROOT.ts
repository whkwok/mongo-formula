import { AssignableExpr, Expr, ObjectExpr, Pipeline, VarDict } from '../types';
import { unionDep } from '../util/dep';
import { isAnyExpr, isNeverExpr } from '../util/expr';
import { sigO } from '../util/sigs';
import { pipelineTyp } from '../util/typ';

export const replaceRoot = (v: any) => ({ $replaceRoot: { newRoot: v } });

export const replaceRootP = (e: Expr): Pipeline => ({
	typ: pipelineTyp(),
	val: [replaceRoot(e.val)],
	dep: unionDep(e.dep),
});

export function replaceRootVD(
	_: VarDict,
	newRoot: AssignableExpr<ObjectExpr>
): VarDict {
	if (isAnyExpr(newRoot) || isNeverExpr(newRoot)) return {};
	const newVarDict: VarDict = {};
	const newRootTypProps = newRoot.typ.properties;
	Object.keys(newRootTypProps).forEach((key) => {
		const propTyp = newRootTypProps[key]!;
		newVarDict[key] = {
			typ: propTyp,
			val: '$' + key,
			dep: unionDep(newRoot.dep),
		};
	});
	return newVarDict;
}

export const REPLACEROOT = {
	argsSigs: [sigO],
	pipeline: replaceRootP,
	varDict: replaceRootVD,
};
