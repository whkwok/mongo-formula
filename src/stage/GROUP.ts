import {
	ArgsSig,
	Dictionary,
	Expr,
	LiteralObjectExpr,
	Pipeline,
	VarDict,
} from '../types';
import { unionDep } from '../util/dep';
import { literalObjectExprAccess } from '../util/expr';
import {
	accumulatorTyp,
	isAccumulatorTyp,
	objectTyp,
	pipelineTyp,
	unknownTyp,
} from '../util/typ';

export const group = (v: Dictionary) => ({ $group: v });

export const groupP = (e: Expr): Pipeline => ({
	typ: pipelineTyp(),
	val: [group(e.val)],
	dep: unionDep(e.dep),
});

export function groupVD(_: VarDict, fields: LiteralObjectExpr): VarDict {
	const newVarDict: VarDict = {};
	Object.keys(fields.val).forEach((key) => {
		const argProp = literalObjectExprAccess(fields, key)!; // it must be defined
		const argPropTyp = argProp.typ;
		newVarDict[key] = {
			typ: isAccumulatorTyp(argPropTyp) ? argPropTyp.in : argPropTyp,
			val: '$' + key,
			dep: argProp.dep,
		};
	});
	return newVarDict;
}

const sigGroup: ArgsSig = {
	typs: [
		objectTyp(
			{ _id: unknownTyp() },
			undefined,
			accumulatorTyp(unknownTyp()),
			true
		),
	],
	minLen: 1,
	maxLen: 1,
};

export const GROUP = {
	argsSigs: [sigGroup],
	pipeline: groupP,
	varDict: groupVD,
};
