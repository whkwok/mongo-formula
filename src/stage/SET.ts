import {
	Dictionary,
	Expr,
	LiteralObjectExpr,
	Pipeline,
	VarDict,
	YY,
} from '../types';
import { unionDep } from '../util/dep';
import { literalObjectExprAccess } from '../util/expr';
import { sigOL } from '../util/sigs';
import { pipelineTyp } from '../util/typ';

// Use $addFields to implement $unset for better compatibility
export const getSet = (compatMode: boolean) => {
	if (compatMode) return (v: Dictionary) => ({ $addFields: v });
	return (v: Dictionary) => ({ $set: v });
};

export const getSetP = (compatMode: boolean) => {
	const set = getSet(compatMode);
	return (e: Expr): Pipeline => ({
		typ: pipelineTyp(),
		val: [set(e.val)],
		dep: unionDep(e.dep),
	});
};

export function setVD(varDict: VarDict, fields: LiteralObjectExpr): VarDict {
	const newVarDict: VarDict = { ...varDict };
	Object.keys(fields.val).forEach((key) => {
		const argProp = literalObjectExprAccess(fields, key)!; // it must be defined
		newVarDict[key] = {
			typ: argProp.typ,
			val: '$' + key,
			dep: argProp.dep,
		};
	});
	return newVarDict;
}

export const SET = ({ options: { compatMode } }: YY) => {
	const setP = getSetP(compatMode);
	return {
		argsSigs: [sigOL],
		pipeline: setP,
		varDict: setVD,
	};
};
