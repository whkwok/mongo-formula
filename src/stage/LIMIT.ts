import { Expr, Pipeline } from '../types';
import { sigNL } from '../util/sigs';
import { pipelineTyp } from '../util/typ';

export const limit = (v: number) => ({ $limit: v });

export const limitP = (e: Expr): Pipeline => ({
	typ: pipelineTyp(),
	val: [limit(e.val)],
	dep: [],
});

export const LIMIT = {
	argsSigs: [sigNL],
	pipeline: limitP,
};
