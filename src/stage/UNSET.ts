import { Dictionary, Expr, LiteralStringExpr, VarDict, YY } from '../types';
import { sigSLSLs } from '../util/sigs';
import { deletePropertyTyp, isObjectTyp, pipelineTyp } from '../util/typ';

// Use $project to implement $unset for better compatibility
export const getUnset = (compatMode: boolean) => {
	if (compatMode)
		return (...vs: string[]) => {
			const projectSpec: Dictionary<0> = {};
			vs.forEach((v) => {
				projectSpec[v] = 0;
			});
			return { $project: projectSpec };
		};
	return (...vs: string[]) => ({ $unset: vs });
};

export const getUnsetP = (compatMode: boolean) => {
	const unset = getUnset(compatMode);
	return (...es: Expr[]) => ({
		typ: pipelineTyp(),
		val: [unset(...es.map((e) => e.val))],
		dep: [],
	});
};

export function unsetVD(
	varDict: VarDict,
	...keyExprs: LiteralStringExpr[]
): VarDict {
	const newVarDict: VarDict = { ...varDict };
	keyExprs.forEach(({ val: key }) => {
		const dotIndex = key.indexOf('.');
		if (dotIndex !== -1) {
			const subKey = key.slice(0, dotIndex);
			const restKey = key.slice(dotIndex + 1);
			const _var = newVarDict[subKey];
			if (_var) {
				const typ = _var.typ;
				if (isObjectTyp(typ)) deletePropertyTyp(typ, restKey);
			}
		} else delete newVarDict[key];
	});
	return newVarDict;
}

export const UNSET = ({ options: { compatMode } }: YY) => {
	const unsetP = getUnsetP(compatMode);
	return {
		argsSigs: [sigSLSLs],
		pipeline: unsetP,
		varDict: unsetVD,
	};
};
