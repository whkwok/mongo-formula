import { Expr, LiteralStringExpr, Pipeline, Typ, VarDict } from '../types';
import { unionDep } from '../util/dep';
import { sigSL } from '../util/sigs';
import {
	isArrayTyp,
	isObjectTyp,
	pipelineTyp,
	toNonLiteralTyp,
	updatePropertyTyp,
} from '../util/typ';

export const unwind = (v: string) => ({ $unwind: '$' + v });

export const unwindP = (e: Expr): Pipeline => ({
	typ: pipelineTyp(),
	val: [unwind(e.val)],
	dep: unionDep(e.dep),
});

function getUnwindedTyp(typ: Typ | undefined) {
	if (!typ) return;
	const _typ = toNonLiteralTyp(typ);
	return isArrayTyp(_typ) ? _typ.items : _typ;
}

export function unwindVD(
	varDict: VarDict,
	keyExpr: LiteralStringExpr
): VarDict {
	const newVarDict: VarDict = { ...varDict };
	const key = keyExpr.val;
	const dotIndex = key.indexOf('.');
	if (dotIndex !== -1) {
		const subKey = key.slice(0, dotIndex);
		const restKey = key.slice(dotIndex + 1);
		const _var = newVarDict[subKey];
		if (_var) {
			const typ = _var.typ;
			if (isObjectTyp(typ))
				updatePropertyTyp(typ, restKey, getUnwindedTyp);
		}
	} else {
		const _var = newVarDict[key];
		if (_var)
			newVarDict[key] = {
				typ: getUnwindedTyp(_var.typ)!,
				val: _var.val,
				dep: _var.dep,
			};
	}
	return newVarDict;
}

export const UNWIND = {
	argsSigs: [sigSL],
	pipeline: unwindP,
	varDict: unwindVD,
};
