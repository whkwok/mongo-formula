import { Expr, Pipeline } from '../types';
import { unionDep } from '../util/dep';
import { sigB } from '../util/sigs';
import { pipelineTyp } from '../util/typ';

export const match = (v: any) => ({ $match: { expr: v } });

export const matchP = (e: Expr): Pipeline => ({
	typ: pipelineTyp(),
	val: [match(e.val)],
	dep: unionDep(e.dep),
});

export const MATCH = {
	argsSigs: [sigB],
	pipeline: matchP,
};
