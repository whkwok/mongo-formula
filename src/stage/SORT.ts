import { ArgsSig, Dictionary, Expr, Pipeline } from '../types';
import { unionDep } from '../util/dep';
import { numberTyp, objectTyp, pipelineTyp } from '../util/typ';

export const sort = (v: Dictionary) => ({ $sort: v });

export const sortP = (e: Expr): Pipeline => ({
	typ: pipelineTyp(),
	val: [sort(e.val)],
	dep: unionDep(e.dep),
});

const sigSort: ArgsSig = {
	typs: [objectTyp({}, undefined, numberTyp(true), true)],
	minLen: 1,
	maxLen: 1,
};

export const SORT = {
	argsSigs: [sigSort],
	pipeline: sortP,
};
