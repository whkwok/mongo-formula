import { Expr, Pipeline } from '../types';
import { sigNL } from '../util/sigs';
import { pipelineTyp } from '../util/typ';

export const sample = (v: number) => ({ $sample: { size: v } });

export const sampleP = (e: Expr): Pipeline => ({
	typ: pipelineTyp(),
	val: [sample(e.val)],
	dep: [],
});

export const SAMPLE = {
	argsSigs: [sigNL],
	pipeline: sampleP,
};
