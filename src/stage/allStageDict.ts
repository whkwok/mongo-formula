import { StageDict } from '../types';
import { GROUP } from './GROUP';
import { LIMIT } from './LIMIT';
import { MATCH } from './MATCH';
import { REPLACEROOT } from './REPLACEROOT';
import { SAMPLE } from './SAMPLE';
import { SET } from './SET';
import { SKIP } from './SKIP';
import { SORT } from './SORT';
import { UNSET } from './UNSET';
import { UNWIND } from './UNWIND';

export const allStageDict: StageDict = {
	GROUP,
	LIMIT,
	MATCH,
	REPLACEROOT,
	SAMPLE,
	SET,
	SKIP,
	SORT,
	UNSET,
	UNWIND,
};
