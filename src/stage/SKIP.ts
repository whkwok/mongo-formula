import { Expr, Pipeline } from '../types';
import { sigNL } from '../util/sigs';
import { pipelineTyp } from '../util/typ';

export const skip = (v: number) => ({ $skip: v });

export const skipP = (e: Expr): Pipeline => ({
	typ: pipelineTyp(),
	val: [skip(e.val)],
	dep: [],
});

export const SKIP = {
	argsSigs: [sigNL],
	pipeline: skipP,
};
