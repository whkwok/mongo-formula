import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';
import { isBooleanVal } from '../../util/val';

export const not = (v: any) => ({ $not: v });

export const _not = (v: any) => (isBooleanVal(v) ? !v : { $not: v });

export const notE = (e: Expr): Expr => {
	const val = _not(e.val);
	if (isBooleanVal(val)) return { typ: booleanTyp(true), val, dep: [] };
	return { typ: booleanTyp(), val, dep: e.dep };
};

export const NOT = {
	argsSigs: [sigU],
	expr: notE,
};
