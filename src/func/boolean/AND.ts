import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUUs } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';
import { isBooleanVal, isOpVal } from '../../util/val';

export const and = (...vs: any[]) => ({ $and: vs });

// Accumulate boolean value and flatten $and expression
const getAndVals = (vs: any[]): false | any[] => {
	const _vs: any[] = [];
	for (let i = 0, l = vs.length; i < l; ++i) {
		const v = vs[i];
		if (isBooleanVal(v)) {
			if (v) continue;
			return false;
		} else if (isOpVal(v, '$and') && v.$and instanceof Array) {
			const vals = getAndVals(v.$and);
			if (typeof vals === 'boolean') return false;
			_vs.push(...vals);
		} else _vs.push(v);
	}
	return _vs;
};

export const _and = (...vs: any[]) => {
	const vals = getAndVals(vs);
	if (vals === false) return false;
	const valCount = vals.length;
	if (valCount === 0) return true;
	if (valCount === 1) return vals[0];
	return and(...vals);
};

export const andE = (...es: Expr[]) => {
	const val = _and(...es.map((e) => e.val));
	if (isBooleanVal(val)) return { typ: booleanTyp(true), val, dep: [] };
	return { typ: booleanTyp(), val, dep: unionDep(...es.map((e) => e.dep)) };
};

export const AND = {
	argsSigs: [sigUUs],
	expr: andE,
};
