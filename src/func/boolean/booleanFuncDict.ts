import { AND } from './AND';
import { NOT } from './NOT';
import { OR } from './OR';

export const booleanFuncDict = { AND, NOT, OR };
