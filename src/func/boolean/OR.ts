import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUUs } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';
import { isBooleanVal, isOpVal } from '../../util/val';

export const or = (...vs: any[]) => ({ $or: vs });

// Accumulate boolean value and flatten $or expression
const getOrVals = (vs: any[]): true | any[] => {
	const _vs: any[] = [];
	for (let i = 0, l = vs.length; i < l; ++i) {
		const v = vs[i];
		if (isBooleanVal(v)) {
			if (v) return true;
			continue;
		} else if (isOpVal(v, '$or') && v.$or instanceof Array) {
			const vals = getOrVals(v.$or);
			if (vals === true) return true;
			_vs.push(...vals);
		} else _vs.push(v);
	}
	return _vs;
};

export const _or = (...vs: any[]) => {
	const vals = getOrVals(vs);
	if (vals === true) return true;
	const valCount = vals.length;
	if (valCount === 0) return false;
	if (valCount === 1) return vals[0];
	return or(...vals);
};

export const orE = (...es: Expr[]): Expr => {
	const val = _or(...es.map((e) => e.val));
	if (isBooleanVal(val)) return { typ: booleanTyp(true), val, dep: [] };
	return { typ: booleanTyp(), val, dep: unionDep(...es.map((e) => e.dep)) };
};

export const OR = {
	argsSigs: [sigUUs],
	expr: orE,
};
