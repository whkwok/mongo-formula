import { AssignableExpr, StringExpr } from '../../types';
import { sigS } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal, isStringVal } from '../../util/val';

export const strLenCP = (v: any) => ({ $strLenCP: v });

export const _strLenCP = (v: any) => (isStringVal(v) ? v.length : strLenCP(v));

export const strLenCPE = (e: AssignableExpr<StringExpr>) => {
	const val = _strLenCP(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const STRLENCP = {
	argsSigs: [sigS],
	expr: strLenCPE,
};
