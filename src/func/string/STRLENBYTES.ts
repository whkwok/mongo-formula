import { AssignableExpr, StringExpr } from '../../types';
import { sigS } from '../../util/sigs';
import { numberTyp } from '../../util/typ';

export const strLenBytes = (v: any) => ({ $strLenBytes: v });

export const strLenBytesE = (e: AssignableExpr<StringExpr>) => ({
	typ: numberTyp(),
	val: strLenBytes(e.val),
	dep: e.dep,
});

export const STRLENBYTES = {
	argsSigs: [sigS],
	expr: strLenBytesE,
};
