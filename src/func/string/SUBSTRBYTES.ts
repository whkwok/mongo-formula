import { AssignableExpr, NumberExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSNN } from '../../util/sigs';
import { stringTyp } from '../../util/typ';

export const substrBytes = (v0: any, v1: any, v2: any) => ({
	$substrBytes: [v0, v1, v2],
});

export const substrBytesE = (
	e0: AssignableExpr<StringExpr>,
	e1: AssignableExpr<NumberExpr>,
	e2: AssignableExpr<NumberExpr>
) => ({
	typ: stringTyp(),
	val: substrBytes(e0.val, e1.val, e2.val),
	dep: unionDep(e0.dep, e1.dep, e2.dep),
});

export const SUBSTRBYTES = {
	argsSigs: [sigSNN],
	expr: substrBytesE,
};
