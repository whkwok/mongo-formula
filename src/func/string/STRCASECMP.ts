import { AssignableExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSS } from '../../util/sigs';
import { numberTyp } from '../../util/typ';

export const strcasecmp = (v0: any, v1: any) => ({ $strcasecmp: [v0, v1] });

export const strcasecmpE = (
	e0: AssignableExpr<StringExpr>,
	e1: AssignableExpr<StringExpr>
) => ({
	typ: numberTyp(),
	val: strcasecmp(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const STRCASECMP = {
	argsSigs: [sigSS],
	expr: strcasecmpE,
};
