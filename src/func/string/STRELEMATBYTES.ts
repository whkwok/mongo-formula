import { AssignableExpr, NumberExpr, StringExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSN } from '../../util/sigs';
import { stringTyp } from '../../util/typ';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { add } from '../arithmetic/ADD';
import { _trunc } from '../arithmetic/TRUNC';
import { and } from '../boolean/AND';
import { gte } from '../comparison/GTE';
import { lt } from '../comparison/LT';
import { cond } from '../conditional/COND';
import { strLenBytes } from './STRLENBYTES';
import { substrBytes } from './SUBSTRBYTES';

// Support negative index and out of bound index.
export const getStrElemAtBytes = (reservedSuffix: string) => (
	v0: any,
	v1: any
) => {
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	const [pVK, pVV] = getReservedVarKeyVal('p', reservedSuffix);
	const [lVK, lVV] = getReservedVarKeyVal('l', reservedSuffix);
	const [absPVK, absPVV] = getReservedVarKeyVal('absP', reservedSuffix);
	return _let(
		{ [v0VK]: v0, [pVK]: _trunc(v1) },
		_let(
			{ [lVK]: strLenBytes(v0VV) },
			_let(
				{ [absPVK]: cond(gte(pVV, 0), pVV, add(lVV, pVV)) },
				cond(
					and(gte(absPVV, 0), lt(absPVV, lVV)),
					substrBytes(v0VV, absPVV, 1),
					''
				)
			)
		)
	);
};

export const getStrElemAtBytesE = (reservedSuffix: string) => {
	const strElemAtBytes = getStrElemAtBytes(reservedSuffix);
	return (
		e0: AssignableExpr<StringExpr>,
		e1: AssignableExpr<NumberExpr>
	) => ({
		typ: stringTyp(),
		val: strElemAtBytes(e0.val, e1.val),
		dep: unionDep(e0.dep, e1.dep),
	});
};

export const STRELEMATBYTES = ({ options: { reservedSuffix } }: YY) => {
	const strElemAtBytesE = getStrElemAtBytesE(reservedSuffix);
	return {
		argsSigs: [sigSN],
		expr: strElemAtBytesE,
	};
};
