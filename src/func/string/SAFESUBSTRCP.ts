import { AssignableExpr, NumberExpr, StringExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSNNo } from '../../util/sigs';
import { stringTyp } from '../../util/typ';
import { isNumberVal, isStringVal } from '../../util/val';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { max } from '../accumulator/MAX';
import { min } from '../accumulator/MIN';
import { add } from '../arithmetic/ADD';
import { subtract } from '../arithmetic/SUBTRACT';
import { _trunc } from '../arithmetic/TRUNC';
import { gt } from '../comparison/GT';
import { gte } from '../comparison/GTE';
import { cond } from '../conditional/COND';
import { strLenCP } from './STRLENCP';
import { substrCP } from './SUBSTRCP';

// Safe Version of SubStrCP. Support negative index, out of bound index, optional end index.
export const getSafeSubStrCP = (reservedSuffix: string) => (
	v0: any,
	v1: any,
	v2?: any
) => {
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	const [lVK, lVV] = getReservedVarKeyVal('l', reservedSuffix);
	const [p0VK, p0VV] = getReservedVarKeyVal('p0', reservedSuffix);
	const [p1VK, p1VV] = getReservedVarKeyVal('p1', reservedSuffix);
	const [absP0VK, absP0VV] = getReservedVarKeyVal('absP0', reservedSuffix);
	const [absP1VK, absP1VV] = getReservedVarKeyVal('absP1', reservedSuffix);

	return _let(
		{ [v0VK]: v0 },
		_let(
			{ [lVK]: strLenCP(v0VV) },
			_let(
				{
					[p0VK]: _trunc(v1),
					[p1VK]: v2 !== undefined ? _trunc(v2) : lVV,
				},
				_let(
					{
						[absP0VK]: cond(
							gte(p0VV, 0),
							min([p0VV, lVV]),
							max([add(lVV, p0VV), 0])
						),
						[absP1VK]: cond(
							gte(p1VV, 0),
							min([p1VV, lVV]),
							max([add(lVV, p1VV), 0])
						),
					},
					cond(
						gt(absP1VV, absP0VV),
						substrCP(v0VV, absP0VV, subtract(absP1VV, absP0VV)),
						''
					)
				)
			)
		)
	);
};

export const _getSafeSubStrCP = (reservedSuffix: string) => {
	const safeSubStrCP = getSafeSubStrCP(reservedSuffix);
	return (v0: any, v1: any, v2?: any) => {
		if (
			isStringVal(v0) &&
			isNumberVal(v1) &&
			(v2 === undefined || isNumberVal(v2))
		)
			return v0.slice(v1, v2);
		return safeSubStrCP(v0, v1, v2);
	};
};

export const getSafeSubStrCPE = (reservedSuffix: string) => {
	const _safeSubStrCP = _getSafeSubStrCP(reservedSuffix);
	return (
		e0: AssignableExpr<StringExpr>,
		e1: AssignableExpr<NumberExpr>,
		e2?: AssignableExpr<NumberExpr>
	) => {
		const val = _safeSubStrCP(e0.val, e1.val, e2?.val);
		if (isStringVal(val)) return { typ: stringTyp(true), val, dep: [] };
		return {
			typ: stringTyp(),
			val,
			dep: unionDep(e0.dep, e1.dep, e2?.dep),
		};
	};
};

export const SAFESUBSTRCP = ({ options: { reservedSuffix } }: YY) => {
	const safeSubStrCPE = getSafeSubStrCPE(reservedSuffix);
	return {
		argsSigs: [sigSNNo],
		expr: safeSubStrCPE,
	};
};
