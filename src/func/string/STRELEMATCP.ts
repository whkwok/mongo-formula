import { AssignableExpr, NumberExpr, StringExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSN } from '../../util/sigs';
import { stringTyp } from '../../util/typ';
import { isNumberVal, isStringVal } from '../../util/val';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { add } from '../arithmetic/ADD';
import { _trunc } from '../arithmetic/TRUNC';
import { and } from '../boolean/AND';
import { gte } from '../comparison/GTE';
import { lt } from '../comparison/LT';
import { cond } from '../conditional/COND';
import { literal } from '../literal/LITERAL';
import { strLenCP } from './STRLENCP';
import { substrCP } from './SUBSTRCP';

// Support negative index and out of bound index.
export const getStrElemAtCP = (reservedSuffix: string) => (
	v0: any,
	v1: any
) => {
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	const [pVK, pVV] = getReservedVarKeyVal('p', reservedSuffix);
	const [lVK, lVV] = getReservedVarKeyVal('l', reservedSuffix);
	const [absPVK, absPVV] = getReservedVarKeyVal('absP', reservedSuffix);
	return _let(
		{ [v0VK]: v0, [pVK]: _trunc(v1) },
		_let(
			{ [lVK]: strLenCP(v0VV) },
			_let(
				{ [absPVK]: cond(gte(pVV, 0), pVV, add(lVV, pVV)) },
				cond(
					and(gte(absPVV, 0), lt(absPVV, lVV)),
					substrCP(v0VV, absPVV, 1),
					''
				)
			)
		)
	);
};

export const _getStrElemAtCP = (reservedSuffix: string) => {
	const strElemAtCP = getStrElemAtCP(reservedSuffix);
	return (v0: any, v1: any) => {
		if (isStringVal(v0) && isNumberVal(v1)) {
			const _v = v0[v1 >= 0 ? v1 : v0.length + v1];
			return _v.indexOf('$') === 0 ? literal(_v) : _v;
		}
		return strElemAtCP(v0, v1);
	};
};

export const getStrElemAtCPE = (reservedSuffix: string) => {
	const _strElemAtCP = _getStrElemAtCP(reservedSuffix);
	return (e0: AssignableExpr<StringExpr>, e1: AssignableExpr<NumberExpr>) => {
		const val = _strElemAtCP(e0.val, e1.val);
		if (isStringVal(val)) return { typ: stringTyp(true), val, dep: [] };
		return { typ: stringTyp(), val, dep: unionDep(e0.dep, e1.dep) };
	};
};

export const STRELEMATCP = ({ options: { reservedSuffix } }: YY) => {
	const strElemAtCPE = getStrElemAtCPE(reservedSuffix);
	return {
		argsSigs: [sigSN],
		expr: strElemAtCPE,
	};
};
