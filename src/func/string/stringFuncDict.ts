import { CONCAT } from './CONCAT';
import { INDEXOFBYTES } from './INDEXOFBYTES';
import { INDEXOFCP } from './INDEXOFCP';
import { LTRIM } from './LTRIM';
import { RTRIM } from './RTRIM';
import { SAFESUBSTRBYTES } from './SAFESUBSTRBYTES';
import { SAFESUBSTRCP } from './SAFESUBSTRCP';
import { SPLIT } from './SPLIT';
import { STRCASECMP } from './STRCASECMP';
import { STRELEMATBYTES } from './STRELEMATBYTES';
import { STRELEMATCP } from './STRELEMATCP';
import { STRLENBYTES } from './STRLENBYTES';
import { STRLENCP } from './STRLENCP';
import { SUBSTRBYTES } from './SUBSTRBYTES';
import { SUBSTRCP } from './SUBSTRCP';
import { TOLOWER } from './TOLOWER';
import { TOUPPER } from './TOUPPER';
import { TRIM } from './TRIM';

export const stringFuncDict = {
	CONCAT,
	INDEXOFBYTES,
	INDEXOFCP,
	LTRIM,
	RTRIM,
	SAFESUBSTRBYTES,
	SAFESUBSTRCP,
	SPLIT,
	STRCASECMP,
	STRELEMATBYTES,
	STRELEMATCP,
	STRLENBYTES,
	STRLENCP,
	SUBSTRBYTES,
	SUBSTRCP,
	TOLOWER,
	TOUPPER,
	TRIM,
};
