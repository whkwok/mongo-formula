import { AssignableExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSSs } from '../../util/sigs';
import { stringTyp } from '../../util/typ';
import { isOpVal, isStringVal } from '../../util/val';

export const concat = (...vs: any[]) => ({ $concat: vs });

// Combine (non-variable) string value and flatten $concat expression
const getConcatVals = (str: string, vs: any[]): [any[], string] => {
	const _vs: any[] = [];
	for (let i = 0, l = vs.length; i < l; ++i) {
		const v = vs[i];
		if (isStringVal(v)) str += v;
		else if (isOpVal(v, '$concat') && v.$concat instanceof Array) {
			const [__vs, __str] = getConcatVals(str, v.$concat);
			str = __str;
			_vs.push(...__vs);
		} else {
			if (str) {
				_vs.push(str, v);
				str = '';
			} else _vs.push(v);
		}
	}
	return [_vs, str];
};

export const _concat = (...vs: any[]) => {
	const [_vs, str] = getConcatVals('', vs);
	if (str) _vs.push(str);
	if (_vs.length === 0) return '';
	if (_vs.length === 1) return _vs[0];
	return { $concat: _vs };
};

export const concatE = (...es: AssignableExpr<StringExpr>[]) => {
	const val = _concat(...es.map((e) => e.val));
	if (isStringVal(val)) return { typ: stringTyp(true), val, dep: [] };
	return { typ: stringTyp(), val, dep: unionDep(...es.map((e) => e.dep)) };
};

export const CONCAT = {
	argsSigs: [sigSSs],
	expr: concatE,
};
