import { AssignableExpr, NumberExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSNN } from '../../util/sigs';
import { stringTyp } from '../../util/typ';
import { isNumberVal, isStringVal } from '../../util/val';
import { literal } from '../literal/LITERAL';

export const substrCP = (v0: any, v1: any, v2: any) => ({
	$substrCP: [v0, v1, v2],
});

export const _substrCP = (v0: any, v1: any, v2: any) => {
	if (isStringVal(v0) && isNumberVal(v1) && isNumberVal(v2)) {
		const _v = v0.slice(v1, v2);
		return _v.indexOf('$') === 0 ? literal(_v) : _v;
	}
	return substrCP(v0, v1, v2);
};

export const substrCPE = (
	e0: AssignableExpr<StringExpr>,
	e1: AssignableExpr<NumberExpr>,
	e2: AssignableExpr<NumberExpr>
) => {
	const val = _substrCP(e0.val, e1.val, e2.val);
	if (isStringVal(val)) return { typ: stringTyp(true), val, dep: [] };
	return { typ: stringTyp(), val, dep: unionDep(e0.dep, e1.dep, e2.dep) };
};

export const SUBSTRCP = {
	argsSigs: [sigSNN],
	expr: substrCPE,
};
