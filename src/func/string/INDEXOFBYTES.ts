import { AssignableExpr, NumberExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSSNoNo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { numberTyp } from '../../util/typ';

export const indexOfBytes = (v0: any, v1: any, v2?: any, v3?: any) => ({
	$indexOfBytes: trimUndefined([v0, v1, v2, v3]),
});

export const indexOfBytesE = (
	e0: AssignableExpr<StringExpr>,
	e1: AssignableExpr<StringExpr>,
	e2?: AssignableExpr<NumberExpr>,
	e3?: AssignableExpr<NumberExpr>
) => ({
	typ: numberTyp(),
	val: indexOfBytes(e0.val, e1.val, e2?.val, e3?.val),
	dep: unionDep(e0.dep, e1.dep, e2?.dep, e3?.dep),
});

export const INDEXOFBYTES = {
	argsSigs: [sigSSNoNo],
	expr: indexOfBytesE,
};
