import { AssignableExpr, StringExpr } from '../../types';
import { sigS } from '../../util/sigs';
import { stringTyp } from '../../util/typ';
import { isStringVal } from '../../util/val';

export const toUpper = (v: any) => ({ $toUpper: v });

export const _toUpper = (v: any) =>
	isStringVal(v) ? v.toUpperCase() : toUpper(v);

export const toUpperE = (e: AssignableExpr<StringExpr>) => {
	const val = _toUpper(e.val);
	if (isStringVal(val)) return { typ: stringTyp(true), val, dep: [] };
	return { typ: stringTyp(), val, dep: e.dep };
};

export const TOUPPER = {
	argsSigs: [sigS],
	expr: toUpperE,
};
