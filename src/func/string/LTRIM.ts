import { AssignableExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { stringTyp } from '../../util/typ';
import { isStringVal } from '../../util/val';
import { literal } from '../literal/LITERAL';

export const ltrim = (v0: any, v1?: any) => ({
	$ltrim: trimUndefined({
		input: v0,
		chars: v1,
	}),
});

// ltrim may cause string starting with '$'
export const _ltrim = (v0: any, v1?: any) => {
	if (isStringVal(v0) && v1 === undefined) {
		const _v = v0.trimLeft();
		return _v.indexOf('$') === 0 ? literal(_v) : _v;
	}
	return ltrim(v0, v1);
};

export const ltrimE = (
	e0: AssignableExpr<StringExpr>,
	e1?: AssignableExpr<StringExpr>
) => {
	const val = _ltrim(e0.val, e1?.val);
	if (isStringVal(val)) return { typ: stringTyp(true), val, dep: [] };
	return { typ: stringTyp(), val, dep: unionDep(e0.dep, e1?.dep) };
};

export const LTRIM = {
	argsSigs: [sigSSo],
	expr: ltrimE,
};
