import { AssignableExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSS } from '../../util/sigs';
import { arrayTyp, stringTyp } from '../../util/typ';
import { isArrayVal, isStringVal } from '../../util/val';
import { literal } from '../literal/LITERAL';

export const split = (v0: any, v1: any) => ({ $split: [v0, v1] });

// split may cause string starting with $
export const _split = (v0: any, v1: any) => {
	if (isStringVal(v0) && isStringVal(v1)) {
		return v0
			.split(v1)
			.map((_v) => (_v.indexOf('$') === 0 ? literal(_v) : _v));
	}
	return split(v0, v1);
};

export const splitE = (
	e0: AssignableExpr<StringExpr>,
	e1: AssignableExpr<StringExpr>
) => {
	const val = _split(e0.val, e1.val);
	if (isArrayVal(val))
		return {
			typ: arrayTyp(
				val.map((v) => stringTyp(isStringVal(v))),
				true
			),
			val,
			dep: val.map(() => []),
		};
	return { typ: arrayTyp(stringTyp()), val, dep: unionDep(e0.dep, e1.dep) };
};

export const SPLIT = {
	argsSigs: [sigSS],
	expr: splitE,
};
