import { AssignableExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { stringTyp } from '../../util/typ';
import { isStringVal } from '../../util/val';

export const rtrim = (v0: any, v1?: any) => ({
	$rtrim: trimUndefined({
		input: v0,
		chars: v1,
	}),
});

export const _rtrim = (v0: any, v1?: any) =>
	isStringVal(v0) && v1 === undefined ? v0.trimRight() : rtrim(v0, v1);

export const rtrimE = (
	e0: AssignableExpr<StringExpr>,
	e1?: AssignableExpr<StringExpr>
) => {
	const val = _rtrim(e0.val, e1?.val);
	if (isStringVal(val)) return { typ: stringTyp(true), val, dep: [] };
	return { typ: stringTyp(), val, dep: unionDep(e0.dep, e1?.dep) };
};

export const RTRIM = {
	argsSigs: [sigSSo],
	expr: rtrimE,
};
