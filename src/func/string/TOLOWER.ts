import { AssignableExpr, StringExpr } from '../../types';
import { sigS } from '../../util/sigs';
import { stringTyp } from '../../util/typ';
import { isStringVal } from '../../util/val';

export const toLower = (v: any) => ({ $toLower: v });

export const _toLower = (v: any) =>
	isStringVal(v) ? v.toLowerCase() : toLower(v);

export const toLowerE = (e: AssignableExpr<StringExpr>) => {
	const val = _toLower(e.val);
	if (isStringVal(val)) return { typ: stringTyp(true), val, dep: [] };
	return { typ: stringTyp(), val, dep: e.dep };
};

export const TOLOWER = {
	argsSigs: [sigS],
	expr: toLowerE,
};
