import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { convert } from './CONVERT';

export const toLong = (v: any) => convert(v, 'long');

export const toLongE = (e: Expr) => ({
	typ: numberTyp(),
	val: toLong(e.val),
	dep: e.dep,
});

export const TOLONG = {
	argsSigs: [sigU],
	expr: toLongE,
};
