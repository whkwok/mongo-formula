import { AssignableExpr, Expr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUS } from '../../util/sigs';
import {
	booleanTyp,
	dateTyp,
	numberTyp,
	stringTyp,
	unknownTyp,
} from '../../util/typ/typs';

export const convert = (v0: any, v1: any) => ({
	$convert: { input: v0, to: v1, onError: null },
});

const convertTyp = (e0: Expr, e1: AssignableExpr<StringExpr>) => {
	const v1 = e1.val;
	if (v1 === 'bool') return booleanTyp();
	if (v1 === 'date') return dateTyp();
	if (v1 === 'decimal') return numberTyp();
	if (v1 === 'double') return numberTyp();
	if (v1 === 'int') return numberTyp();
	if (v1 === 'long') return numberTyp();
	if (v1 === 'string') return stringTyp();
	return unknownTyp();
};

export const convertE = (e0: Expr, e1: AssignableExpr<StringExpr>) => ({
	typ: convertTyp(e0, e1),
	val: convert(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const CONVERT = {
	argsSigs: [sigUS],
	expr: convertE,
};
