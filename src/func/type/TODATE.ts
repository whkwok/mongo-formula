import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { dateTyp } from '../../util/typ';
import { convert } from './CONVERT';

export const toDate = (v: any) => convert(v, 'date');

export const toDateE = (e: Expr) => ({
	typ: dateTyp(),
	val: toDate(e.val),
	dep: e.dep,
});

export const TODATE = {
	argsSigs: [sigU],
	expr: toDateE,
};
