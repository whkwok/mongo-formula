import { CONVERT } from './CONVERT';
import { TOBOOL } from './TOBOOL';
import { TODATE } from './TODATE';
import { TODECIMAL } from './TODECIMAL';
import { TODOUBLE } from './TODOUBLE';
import { TOINT } from './TOINT';
import { TOLONG } from './TOLONG';
import { TOSTRING } from './TOSTRING';
import { TYPE } from './TYPE';

export const typeFuncDict = {
	CONVERT,
	TOBOOL,
	TODATE,
	TODECIMAL,
	TODOUBLE,
	TOINT,
	TOLONG,
	TOSTRING,
	TYPE,
};
