import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { stringTyp } from '../../util/typ';
import { convert } from './CONVERT';

export const toString = (v: any) => convert(v, 'string');

export const toStringE = (e: Expr) => ({
	typ: stringTyp(),
	val: toString(e.val),
	dep: e.dep,
});

export const TOSTRING = {
	argsSigs: [sigU],
	expr: toStringE,
};
