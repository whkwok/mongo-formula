import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';
import { convert } from './CONVERT';

export const toBool = (v: any) => convert(v, 'bool');

export const toBoolE = (e: Expr) => ({
	typ: booleanTyp(),
	val: toBool(e.val),
	dep: e.dep,
});

export const TOBOOL = {
	argsSigs: [sigU],
	expr: toBoolE,
};
