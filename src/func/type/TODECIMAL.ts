import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { convert } from './CONVERT';

export const toDecimal = (v: any) => convert(v, 'decimal');

export const toDecimalE = (e: Expr) => ({
	typ: numberTyp(),
	val: toDecimal(e.val),
	dep: e.dep,
});

export const TODECIMAL = {
	argsSigs: [sigU],
	expr: toDecimalE,
};
