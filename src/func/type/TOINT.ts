import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { convert } from './CONVERT';

export const toInt = (v: any) => convert(v, 'int');

export const toIntE = (e: Expr) => ({
	typ: numberTyp(),
	val: toInt(e.val),
	dep: e.dep,
});

export const TOINT = {
	argsSigs: [sigU],
	expr: toIntE,
};
