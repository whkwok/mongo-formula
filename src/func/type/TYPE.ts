import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { stringTyp } from '../../util/typ';

export const _type = (v: any) => ({ $type: v });

export const typeE = (e: Expr) => ({
	typ: stringTyp(),
	val: _type(e.val),
	dep: e.dep,
});

export const TYPE = {
	argsSigs: [sigU],
	expr: typeE,
};
