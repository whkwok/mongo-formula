import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { convert } from './CONVERT';

export const toDouble = (v: any) => convert(v, 'double');

export const toDoubleE = (e: Expr) => ({
	typ: numberTyp(),
	val: toDouble(e.val),
	dep: e.dep,
});

export const TODOUBLE = {
	argsSigs: [sigU],
	expr: toDoubleE,
};
