import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { isBooleanExpr, isLiteralExpr } from '../../util/expr';
import { sigUUU } from '../../util/sigs';
import { unionTyp } from '../../util/typ';

export const cond = (v0: any, v1: any, v2: any) => ({ $cond: [v0, v1, v2] });

export const condE = (e0: Expr, e1: Expr, e2: Expr) => {
	if (isLiteralExpr(e0) && isBooleanExpr(e0))
		return e0.val
			? { typ: e1.typ, val: e1.val, dep: e1.dep }
			: { typ: e2.typ, val: e2.val, dep: e2.dep };
	return {
		typ: unionTyp(e1.typ, e2.typ),
		val: cond(e0.val, e1.val, e2.val),
		dep: unionDep(e0.dep, e1.dep, e2.dep),
	};
};

export const COND = {
	argsSigs: [sigUUU],
	expr: condE,
};
