import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUU } from '../../util/sigs';
import { unionTyp } from '../../util/typ';
import { isNullVal } from '../../util/val';

const ifNull = (v0: any, v1: any) => ({ $ifNull: [v0, v1] });

export const ifNullE = (e0: Expr, e1: Expr) => {
	if (isNullVal(e0.val))
		return e0.val === null
			? { typ: e1.typ, val: e1.val, dep: e1.dep }
			: { typ: e0.typ, val: e0.val, dep: e0.dep };
	return {
		typ: unionTyp(e0.typ, e1.typ),
		val: ifNull(e0.val, e1.val),
		dep: unionDep(e0.dep, e1.dep),
	};
};

export const IFNULL = {
	argsSigs: [sigUU],
	expr: ifNullE,
};
