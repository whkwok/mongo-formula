import { COND } from './COND';
import { IFNULL } from './IFNULL';

export const conditionalFuncDict = { COND, IFNULL };
