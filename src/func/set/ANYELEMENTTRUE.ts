import { ArrayExpr, AssignableExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigA } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const anyElementTrue = (v: any) => ({ $anyElementTrue: v });

export const anyElementTrueE = (e: AssignableExpr<ArrayExpr>) => ({
	typ: booleanTyp(),
	val: anyElementTrue(e.val),
	dep: unionDep(e.dep),
});

export const ANYELEMENTTRUE = {
	argsSigs: [sigA],
	expr: anyElementTrueE,
};
