import { ArrayExpr, AssignableExpr, Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAA } from '../../util/sigs';
import { toNonLiteralTyp } from '../../util/typ';

export const setDifference = (v0: any, v1: any) => ({
	$setDifference: [v0, v1],
});

export const setDifferenceE = (
	e0: AssignableExpr<ArrayExpr>,
	e1: AssignableExpr<ArrayExpr>
): Expr => ({
	typ: toNonLiteralTyp(e0.typ),
	val: setDifference(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const SETDIFFERENCE = {
	argsSigs: [sigAA],
	expr: setDifferenceE,
};
