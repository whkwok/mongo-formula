import { ArrayExpr, AssignableExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigA } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const allElementsTrue = (v: any) => ({ $allElementsTrue: v });

export const allElementsTrueE = (e: AssignableExpr<ArrayExpr>) => ({
	typ: booleanTyp(),
	val: allElementsTrue(e.val),
	dep: unionDep(e.dep),
});

export const ALLELEMENTSTRUE = {
	argsSigs: [sigA],
	expr: allElementsTrueE,
};
