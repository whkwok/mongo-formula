import { ArrayExpr, AssignableExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAA } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';
import { setIsSubset } from './SETISSUBSET';

export const setIsSuperset = (v0: any, v1: any) => setIsSubset(v1, v0);

export const setIsSupersetE = (
	e0: AssignableExpr<ArrayExpr>,
	e1: AssignableExpr<ArrayExpr>
) => ({
	typ: booleanTyp(),
	val: setIsSuperset(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const SETISSUPERSET = {
	argsSigs: [sigAA],
	expr: setIsSupersetE,
};
