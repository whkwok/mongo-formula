import { ArrayExpr, AssignableExpr, Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAAs } from '../../util/sigs';
import { unionTyp } from '../../util/typ';

export const setUnion = (...vs: any[]) => ({ $setUnion: vs });

// Flatten $setUnion expression
const getSetUnionVals = (vs: any[]): any[] => {
	const _vs: any[] = [];
	for (let i = 0, l = vs.length; i < l; ++i) {
		const v = vs[i];
		if (typeof v === 'object' && v.$setUnion instanceof Array) {
			const vals = getSetUnionVals(v.$setUnion);
			_vs.push(...vals);
		} else _vs.push(v);
	}
	return _vs;
};

export const _setUnion = (...vs: any[]) => {
	return setUnion(...getSetUnionVals(vs));
};

export const setUnionE = (...es: AssignableExpr<ArrayExpr>[]): Expr => ({
	typ: unionTyp(...es.map((e) => e.typ)),
	val: _setUnion(...es.map((e) => e.val)),
	dep: unionDep(...es.map((e) => e.dep)),
});

export const SETUNION = {
	argsSigs: [sigAAs],
	expr: setUnionE,
};
