import { ArrayExpr, AssignableExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAA } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';
import { not } from '../boolean/NOT';
import { setEquals } from './SETEQUALS';

export const setNotEquals = (v0: any, v1: any) => not(setEquals(v0, v1));

export const setNotEqualsE = (
	e0: AssignableExpr<ArrayExpr>,
	e1: AssignableExpr<ArrayExpr>
) => ({
	typ: booleanTyp(),
	val: setNotEquals(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const SETNOTEQUALS = {
	argsSigs: [sigAA],
	expr: setNotEqualsE,
};
