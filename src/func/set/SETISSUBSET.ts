import { ArrayExpr, AssignableExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAA } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const setIsSubset = (v0: any, v1: any) => ({ $setIsSubset: [v0, v1] });

export const setIsSubsetE = (
	e0: AssignableExpr<ArrayExpr>,
	e1: AssignableExpr<ArrayExpr>
) => ({
	typ: booleanTyp(),
	val: setIsSubset(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const SETISSUBSET = {
	argsSigs: [sigAA],
	expr: setIsSubsetE,
};
