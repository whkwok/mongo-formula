import { ArrayExpr, AssignableExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAA } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const setEquals = (v0: any, v1: any) => ({ $setEquals: [v0, v1] });

export const setEqualsE = (
	e0: AssignableExpr<ArrayExpr>,
	e1: AssignableExpr<ArrayExpr>
) => ({
	typ: booleanTyp(),
	val: setEquals(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const SETEQUALS = {
	argsSigs: [sigAA],
	expr: setEqualsE,
};
