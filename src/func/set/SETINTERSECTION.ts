import { ArrayExpr, AssignableExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAAs } from '../../util/sigs';
import { intersectTyp } from '../../util/typ';

export const setIntersection = (...vs: any[]) => ({ $setIntersection: vs });

// Flatten $setIntersection expression
const getSetIntersectionVals = (vs: any[]): any[] => {
	const _vs: any[] = [];
	for (let i = 0, l = vs.length; i < l; ++i) {
		const v = vs[i];
		if (typeof v === 'object' && v.$setIntersection instanceof Array) {
			const vals = getSetIntersectionVals(v.$setIntersection);
			_vs.push(...vals);
		} else _vs.push(v);
	}
	return _vs;
};

export const _setIntersection = (...vs: any[]) => {
	return setIntersection(...getSetIntersectionVals(vs));
};

export const setIntersectionE = (...es: AssignableExpr<ArrayExpr>[]) => ({
	typ: intersectTyp(...es.map((e) => e.typ)),
	val: _setIntersection(...es.map((e) => e.val)),
	dep: unionDep(...es.map((e) => e.dep)),
});

export const SETINTERSECTION = {
	argsSigs: [sigAAs],
	expr: setIntersectionE,
};
