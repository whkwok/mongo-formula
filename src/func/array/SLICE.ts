import {
	ArrayExpr,
	AssignableExpr,
	LiteralArrayExpr,
	NumberExpr,
} from '../../types';
import { unionDep } from '../../util/dep';
import { sigANNo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { arrayTyp, toNonLiteralTyp } from '../../util/typ';
import { isArrayVal, isNumberVal } from '../../util/val';

export const slice = (v0: any, v1: any, v2?: any) => ({
	$slice: trimUndefined([v0, v1, v2]),
});

export const _slice = (v0: any, v1: any, v2?: any) =>
	isArrayVal(v0) && isNumberVal(v1) && (v2 === undefined || isNumberVal(v2))
		? v0.slice(v1, v2)
		: slice(v0, v1, v2);

export const sliceE = (
	e0: AssignableExpr<ArrayExpr>,
	e1: AssignableExpr<NumberExpr>,
	e2?: AssignableExpr<NumberExpr>
) => {
	const val = _slice(e0.val, e1.val, e2?.val);
	if (isArrayVal(val))
		return {
			typ: arrayTyp(
				(e0 as LiteralArrayExpr).typ.items.slice(e1.val, e2?.val),
				true
			),
			val,
			dep: (e0 as LiteralArrayExpr).dep.slice(e1.val, e2?.val),
		};
	return {
		typ: toNonLiteralTyp(e0.typ),
		val,
		dep: unionDep(e0.dep, e1.dep, e2?.dep),
	};
};

export const SLICE = {
	argsSigs: [sigANNo],
	expr: sliceE,
};
