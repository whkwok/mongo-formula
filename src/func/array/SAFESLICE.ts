import {
	ArrayExpr,
	AssignableExpr,
	LiteralArrayExpr,
	NumberExpr,
	YY,
} from '../../types';
import { unionDep } from '../../util/dep';
import { sigANNo } from '../../util/sigs';
import { arrayTyp, toNonLiteralTyp } from '../../util/typ';
import { isArrayVal, isNumberVal } from '../../util/val';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { max } from '../accumulator/MAX';
import { min } from '../accumulator/MIN';
import { add } from '../arithmetic/ADD';
import { subtract } from '../arithmetic/SUBTRACT';
import { _trunc } from '../arithmetic/TRUNC';
import { gt } from '../comparison/GT';
import { gte } from '../comparison/GTE';
import { cond } from '../conditional/COND';
import { size } from './SIZE';
import { _slice } from './SLICE';

export const getSafeSlice = (reservedSuffix: string) => (
	v0: any,
	v1: any,
	v2?: any
) => {
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	const [lVK, lVV] = getReservedVarKeyVal('l', reservedSuffix);
	const [p0VK, p0VV] = getReservedVarKeyVal('p0', reservedSuffix);
	const [p1VK, p1VV] = getReservedVarKeyVal('p1', reservedSuffix);
	const [absP0VK, absP0VV] = getReservedVarKeyVal('absP0', reservedSuffix);
	const [absP1VK, absP1VV] = getReservedVarKeyVal('absP1', reservedSuffix);
	return _let(
		{ [v0VK]: v0 },
		_let(
			{ [lVK]: size(v0VV) },
			_let(
				{
					[p0VK]: _trunc(v1),
					[p1VK]: v2 !== undefined ? _trunc(v2) : lVV,
				},
				_let(
					{
						[absP0VK]: cond(
							gte(p0VV, 0),
							min([p0VV, lVV]),
							max([add(lVV, p0VV), 0])
						),
						[absP1VK]: cond(
							gte(p1VV, 0),
							min([p1VV, lVV]),
							max([add(lVV, p1VV), 0])
						),
					},
					cond(
						gt(absP1VV, absP0VV),
						_slice(v0VV, absP0VV, subtract(absP1VV, absP0VV)),
						[]
					)
				)
			)
		)
	);
};

export const _getSafeSlice = (reservedSuffix: string) => {
	const safeSlice = getSafeSlice(reservedSuffix);
	return (v0: any, v1: any, v2?: any) => {
		if (
			isArrayVal(v0) &&
			isNumberVal(v2) &&
			(v2 === undefined || isNumberVal(v2))
		)
			return v0.slice(v1, v2);
		return safeSlice(v0, v1, v2);
	};
};

export const getSafeSliceE = (reservedSuffix: string) => {
	const _safeSlice = _getSafeSlice(reservedSuffix);
	return (
		e0: AssignableExpr<ArrayExpr>,
		e1: AssignableExpr<NumberExpr>,
		e2?: AssignableExpr<NumberExpr>
	) => {
		const val = _safeSlice(e0.val, e1.val, e2?.val);
		if (isArrayVal(val))
			return {
				typ: arrayTyp(
					(e0 as LiteralArrayExpr).typ.items.slice(e1.val, e2?.val),
					true
				),
				val,
				dep: (e0 as LiteralArrayExpr).dep.slice(e1.val, e2?.val),
			};
		return {
			typ: toNonLiteralTyp(e0.typ),
			val,
			dep: unionDep(e0.dep, e1.dep, e2?.dep),
		};
	};
};

export const SAFESLICE = ({ options: { reservedSuffix } }: YY) => {
	const safeSliceE = getSafeSliceE(reservedSuffix);
	return {
		argsSigs: [sigANNo],
		expr: safeSliceE,
	};
};
