import { ArrayExpr, AssignableExpr, LiteralArrayExpr } from '../../types';
import { sigA } from '../../util/sigs';
import { arrayTyp, toNonLiteralTyp } from '../../util/typ';
import { isArrayVal } from '../../util/val';

export const reverseArray = (v: any) => ({ $reverseArray: v });

export const _reverseArray = (v: any) =>
	isArrayVal(v) ? v.slice().reverse() : reverseArray(v);

export const reverseArrayE = (e: AssignableExpr<ArrayExpr>) => {
	const val = _reverseArray(e.val);
	if (isArrayVal(val))
		return {
			typ: arrayTyp(
				(e as LiteralArrayExpr).typ.items.slice().reverse(),
				true
			),
			val,
			dep: (e as LiteralArrayExpr).dep.slice().reverse(),
		};
	return { typ: toNonLiteralTyp(e.typ), val, dep: e.dep };
};

export const REVERSEARRAY = {
	argsSigs: [sigA],
	expr: reverseArrayE,
};
