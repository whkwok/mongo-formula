import { AssignableExpr, NumberExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNNNo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { arrayTyp, numberTyp } from '../../util/typ';

export const range = (v0: any, v1: any, v2?: any) => ({
	$range: trimUndefined([v0, v1, v2]),
});

export const rangeE = (
	e0: AssignableExpr<NumberExpr>,
	e1: AssignableExpr<NumberExpr>,
	e2?: AssignableExpr<NumberExpr>
) => ({
	typ: arrayTyp(numberTyp()),
	val: range(e0.val, e1.val, e2?.val),
	dep: unionDep(e0.dep, e1.dep, e2?.dep),
});

export const RANGE = {
	argsSigs: [sigNNNo],
	expr: rangeE,
};
