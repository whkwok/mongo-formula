import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigU } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';
import { isArrayVal } from '../../util/val';

export const isArray = (v: any) => ({ $isArray: v });

export const _isArray = (v: any) => (isArrayVal(v) ? true : isArray(v));

export const isArrayE = (e: Expr) => ({
	typ: booleanTyp(),
	val: _isArray(e.val),
	dep: unionDep(e.dep),
});

export const ISARRAY = {
	argsSigs: [sigU],
	expr: isArrayE,
};
