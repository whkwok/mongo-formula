import { ArrayExpr, AssignableExpr, LiteralArrayExpr, Typ } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAAs } from '../../util/sigs';
import { arrayTyp, unionTyp } from '../../util/typ';
import { isArrayVal, isOpVal } from '../../util/val';

export const concatArrays = (...vs: any[]) => ({ $concatArrays: vs });

// Combine (non-variable) array value and flatten $concatArrays expression
const getConcatArraysVals = (arr: any[], vs: any[]): [any[], any[]] => {
	const _vs: any[] = [];
	for (let i = 0, l = vs.length; i < l; ++i) {
		const v = vs[i];
		if (isArrayVal(v)) arr.push(...v);
		else if (
			isOpVal(v, '$concatArrays') &&
			v.$concatArrays instanceof Array
		) {
			const [__vs, __arr] = getConcatArraysVals(arr, v.$concatArrays);
			arr = __arr;
			_vs.push(...__vs);
		} else {
			if (arr.length) {
				_vs.push(arr, v);
				arr = [];
			} else _vs.push(v);
		}
	}
	return [_vs, arr];
};

export const _concatArrays = (...vs: any[]) => {
	const [_vs, arr] = getConcatArraysVals([], vs);
	if (arr.length) _vs.push(arr);
	if (_vs.length === 0) return [];
	if (_vs.length === 1) return _vs[0];
	return { $concatArrays: _vs };
};

export const concatArraysE = (...es: AssignableExpr<ArrayExpr>[]) => {
	const val = _concatArrays(...es.map((e) => e.val));
	if (isArrayVal(val))
		return {
			typ: arrayTyp(
				(es as LiteralArrayExpr[]).flatMap((e) => e.typ.items),
				true
			),
			val,
			dep: (es as LiteralArrayExpr[]).flatMap((e) => e.dep),
		};
	return {
		typ: unionTyp(...es.map((e) => e.typ as Typ)),
		val,
		dep: unionDep(...es.map((e) => e.dep)),
	};
};

export const CONCATARRAYS = {
	argsSigs: [sigAAs],
	expr: concatArraysE,
};
