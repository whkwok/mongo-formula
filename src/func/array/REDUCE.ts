import {
	ArgsSig,
	ArrayExpr,
	AssignableExpr,
	Callback,
	Expr,
} from '../../types';
import { unionDep } from '../../util/dep';
import {
	arrayTyp,
	callbackTyp,
	getItemsTyp,
	toNonLiteralTyp,
	unknownTyp,
} from '../../util/typ';
import { _let } from '../../util/var';

export const reduce = (v0: any, v1: any, v2: string, v3: string, v4: any) => ({
	$reduce: {
		input: v0,
		initialValue: v1,
		in: _let({ [v2]: '$$value', [v3]: '$$this' }, v4),
	},
});

export const reduceE = (
	e0: AssignableExpr<ArrayExpr>,
	e1: Expr,
	e2: Callback
) => {
	return {
		typ: toNonLiteralTyp(e2.typ.in),
		val: reduce(
			e0.val,
			e1.val,
			e2.val.argKeys[0],
			e2.val.argKeys[1],
			e2.val.in
		),
		dep: unionDep(e0.dep, e1.dep, e2.dep),
	};
};

const sigReduce: ArgsSig = {
	typs: [
		arrayTyp(unknownTyp()),
		unknownTyp(),
		(...args) => {
			const cumulativeTyp = toNonLiteralTyp(args[1].typ);
			const currentTyp = getItemsTyp(
				(args[0] as AssignableExpr<ArrayExpr>).typ
			);
			return callbackTyp([currentTyp, cumulativeTyp], cumulativeTyp);
		},
	],
	maxLen: 3,
	minLen: 3,
};

export const REDUCE = {
	argsSigs: [sigReduce],
	expr: reduceE,
};
