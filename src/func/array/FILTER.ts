import { ArgsSig, ArrayExpr, AssignableExpr, Callback } from '../../types';
import { unionDep } from '../../util/dep';
import {
	arrayTyp,
	booleanTyp,
	callbackTyp,
	getItemsTyp,
	toNonLiteralTyp,
	unknownTyp,
} from '../../util/typ';

export const filter = (v0: any, v1: any, v2: any) => ({
	$filter: { input: v0, as: v1, cond: v2 },
});

export const filterE = (e0: AssignableExpr<ArrayExpr>, e1: Callback) => {
	return {
		typ: toNonLiteralTyp(e0.typ),
		val: filter(e0.val, e1.val.argKeys[0], e1.val.in),
		dep: unionDep(e0.dep, e1.dep),
	};
};

const sigFilter: ArgsSig = {
	typs: [
		arrayTyp(unknownTyp()),
		(...args) =>
			callbackTyp(
				[getItemsTyp((args[0] as AssignableExpr<ArrayExpr>).typ)],
				booleanTyp()
			),
	],
	maxLen: 2,
	minLen: 2,
};

export const FILTER = {
	argsSigs: [sigFilter],
	expr: filterE,
};
