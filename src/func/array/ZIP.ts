import { ArrayExpr, AssignableExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAAs } from '../../util/sigs';
import { arrayTyp, unionTyp } from '../../util/typ';

export const zip = (...vs: any[]) => ({ $zip: { inputs: vs } });

export const zipE = (...es: AssignableExpr<ArrayExpr>[]) => ({
	typ: arrayTyp(unionTyp(...es.map((e) => e.typ))),
	val: zip(...es.map((e) => e.val)),
	dep: unionDep(...es.map((e) => e.dep)),
});

export const ZIP = {
	argsSigs: [sigAAs],
	expr: zipE,
};
