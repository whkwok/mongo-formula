import { ArgsSig, ArrayExpr, AssignableExpr, Callback } from '../../types';
import { unionDep } from '../../util/dep';
import {
	arrayTyp,
	callbackTyp,
	getItemsTyp,
	toNonLiteralTyp,
	unknownTyp,
} from '../../util/typ';

export const map = (v0: any, v1: string, v2: any) => ({
	$map: { input: v0, as: v1, in: v2 },
});

export const mapE = (e0: AssignableExpr<ArrayExpr>, e1: Callback) => {
	return {
		typ: arrayTyp(toNonLiteralTyp(e1.typ.in)),
		val: map(e0.val, e1.val.argKeys[0], e1.val.in),
		dep: unionDep(e0.dep, e1.dep),
	};
};

const sigMap: ArgsSig = {
	typs: [
		arrayTyp(unknownTyp()),
		(...args) =>
			callbackTyp(
				[getItemsTyp((args[0] as AssignableExpr<ArrayExpr>).typ)],
				unknownTyp()
			),
	],
	maxLen: 2,
	minLen: 2,
};

export const MAP = {
	argsSigs: [sigMap],
	expr: mapE,
};
