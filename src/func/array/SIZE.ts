import { ArrayExpr, AssignableExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigA } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isArrayVal, isNumberVal } from '../../util/val';

export const size = (v: any) => ({ $size: v });

export const _size = (v: any) => (isArrayVal(v) ? v.length : size(v));

export const sizeE = (e: AssignableExpr<ArrayExpr>) => {
	const val = _size(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: unionDep(e.dep) };
};

export const SIZE = {
	argsSigs: [sigA],
	expr: sizeE,
};
