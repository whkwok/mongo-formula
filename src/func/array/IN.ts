import { ArrayExpr, AssignableExpr, Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUA } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const _in = (v0: any, v1: any) => ({ $in: [v0, v1] });

export const inE = (e0: Expr, e1: AssignableExpr<ArrayExpr>) => ({
	typ: booleanTyp(),
	val: _in(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const IN = {
	argsSigs: [sigUA],
	expr: inE,
};
