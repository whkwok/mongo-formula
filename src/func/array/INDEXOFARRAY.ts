import { ArrayExpr, AssignableExpr, Expr, NumberExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigAUNoNo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { numberTyp } from '../../util/typ';

export const indexOfArray = (v0: any, v1: any, v2?: any, v3?: any) => ({
	$indexOfArray: trimUndefined([v0, v1, v2, v3]),
});

export const indexOfArrayE = (
	e0: AssignableExpr<ArrayExpr>,
	e1: Expr,
	e2?: AssignableExpr<NumberExpr>,
	e3?: AssignableExpr<NumberExpr>
) => ({
	typ: numberTyp(),
	val: indexOfArray(e0.val, e1.val, e2?.val, e3?.val),
	dep: unionDep(e0.dep, e1.dep, e2?.dep, e3?.dep),
});

export const INDEXOFARRAY = {
	argsSigs: [sigAUNoNo],
	expr: indexOfArrayE,
};
