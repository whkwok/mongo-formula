import { ARRAYELEMAT } from './ARRAYELEMAT';
import { CONCATARRAYS } from './CONCATARRAYS';
import { FILTER } from './FILTER';
import { IN } from './IN';
import { INDEXOFARRAY } from './INDEXOFARRAY';
import { ISARRAY } from './ISARRAY';
import { MAP } from './MAP';
import { RANGE } from './RANGE';
import { REDUCE } from './REDUCE';
import { REVERSEARRAY } from './REVERSEARRAY';
import { SAFESLICE } from './SAFESLICE';
import { SIZE } from './SIZE';
import { SLICE } from './SLICE';
import { ZIP } from './ZIP';

export const arrayFuncDict = {
	ARRAYELEMAT,
	CONCATARRAYS,
	FILTER,
	IN,
	INDEXOFARRAY,
	ISARRAY,
	MAP,
	RANGE,
	REDUCE,
	REVERSEARRAY,
	SAFESLICE,
	SIZE,
	SLICE,
	ZIP,
};
