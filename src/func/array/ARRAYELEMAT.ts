import { ArrayExpr, AssignableExpr, NumberExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { isLiteralExpr } from '../../util/expr';
import { sigAN } from '../../util/sigs';
import { getItemsTyp, neverTyp } from '../../util/typ';

const arrayElemAt = (v0: any, v1: any) => ({ $arrayElemAt: [v0, v1] });

export const arrayElemAtE = (
	e0: AssignableExpr<ArrayExpr>,
	e1: AssignableExpr<NumberExpr>
) => {
	if (isLiteralExpr(e0) && isLiteralExpr(e1)) {
		const v0 = e0.val;
		const v1 = e1.val;
		const index = v1 >= 0 ? v1 : v0.length + v1;
		return {
			typ: e0.typ.items[index] ?? neverTyp(),
			val: v0[index],
			dep: e0.dep[index] ?? [],
		};
	}
	return {
		typ: getItemsTyp(e0.typ),
		val: arrayElemAt(e0.val, e1.val),
		dep: unionDep(e0.dep, e1.dep),
	};
};

export const ARRAYELEMAT = {
	argsSigs: [sigAN],
	expr: arrayElemAtE,
};
