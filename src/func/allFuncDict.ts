import { FuncDict } from '../types';
import { accumulatorFuncDict } from './accumulator/accumulatorFuncDict';
import { arithmeticFuncDict } from './arithmetic/arithmeticFuncDict';
import { arrayFuncDict } from './array/arrayFuncDict';
import { booleanFuncDict } from './boolean/booleanFuncDict';
import { comparisonFuncDict } from './comparison/comparisonFuncDict';
import { conditionalFuncDict } from './conditional/conditionalFuncDict';
import { dateFuncDict } from './date/dateFuncDict';
import { literalFuncDict } from './literal/literalFuncDict';
import { objectFuncDict } from './object/objectFuncDict';
import { overloadFuncDict } from './overload/overloadFuncDict';
import { setFuncDict } from './set/setFuncDict';
import { stringFuncDict } from './string/stringFuncDict';
import { typeFuncDict } from './type/typeFuncDict';

export const allFuncDict: FuncDict = {
	...accumulatorFuncDict,
	...arithmeticFuncDict,
	...arrayFuncDict,
	...booleanFuncDict,
	...comparisonFuncDict,
	...conditionalFuncDict,
	...dateFuncDict,
	...literalFuncDict,
	...objectFuncDict,
	...overloadFuncDict,
	...setFuncDict,
	...stringFuncDict,
	...typeFuncDict,
};
