import {
	AssignableExpr,
	Dictionary,
	Expr,
	LiteralObjectExpr,
	NonLiteralTyp,
	ObjectExpr,
	Typ,
} from '../../types';
import { unionDep } from '../../util/dep';
import { isAnyExpr, isObjectExpr } from '../../util/expr';
import { sigOOs } from '../../util/sigs';
import {
	anyTyp,
	objectTyp,
	toNonLiteralTyp,
	unionTyp,
	unknownTyp,
} from '../../util/typ';
import { isObjectVal, isOpVal } from '../../util/val';

export const mergeObjects = (...vs: any[]) => ({ $mergeObjects: vs });

// Combine (non-operator) object value and flatten $mergeObjects expression
const getMergeObjectsVals = (
	obj: Dictionary,
	vs: any[]
): [any[], Dictionary] => {
	const _vs: any[] = [];
	for (let i = 0, l = vs.length; i < l; ++i) {
		const v = vs[i];
		if (isObjectVal(v)) Object.assign(obj, v);
		else if (
			isOpVal(v, '$mergeObjects') &&
			v.$mergeObjects instanceof Array
		) {
			const [__vs, __obj] = getMergeObjectsVals(obj, v.$mergeObjects);
			obj = __obj;
			_vs.push(...__vs);
		} else {
			if (Object.keys(obj).length) {
				_vs.push(obj, v);
				obj = {};
			} else _vs.push(v);
		}
	}
	return [_vs, obj];
};

export const _mergeObjects = (...vs: any[]) => {
	const [_vs, obj] = getMergeObjectsVals({}, vs);
	if (Object.keys(obj).length) _vs.push(obj);
	if (_vs.length === 0) return {};
	if (_vs.length === 1) return _vs[0];
	return mergeObjects(..._vs);
};

export const mergeObjectsE = (
	...es: readonly AssignableExpr<ObjectExpr>[]
): Expr => {
	const val = _mergeObjects(...es.map((e) => e.val));
	if (isObjectVal(val)) {
		return {
			typ: objectTyp(
				Object.assign(
					{},
					...(es as LiteralObjectExpr[]).map((e) => e.typ.properties)
				),
				undefined,
				undefined,
				true
			),
			val,
			dep: Object.assign({}, ...es.map((e) => e.dep)),
		};
	}
	if (es.every(isObjectExpr)) {
		const _properties: Dictionary<NonLiteralTyp> = {};
		const _optionalSet = new Set<string>();
		const _additionalPropertiesList: Typ[] = [];
		es.forEach((e) => {
			const { properties, optional, additionalProperties } = e.typ;
			Object.keys(properties).forEach((k) => {
				_properties[k] = toNonLiteralTyp(properties[k]!);
				_optionalSet.delete(k); // remove new keys
			});
			if (optional) for (const k of optional) _optionalSet.add(k); // add new optional keys
			if (additionalProperties)
				_additionalPropertiesList.push(additionalProperties);
		});
		const _optional = [..._optionalSet];
		const _additionalProperties =
			_additionalPropertiesList.length === 0
				? undefined
				: unionTyp(..._additionalPropertiesList);
		return {
			typ: objectTyp(_properties, _optional, _additionalProperties),
			val,
			dep: unionDep(...es.map((e) => e.dep)),
		};
	}
	if (es.some(isAnyExpr))
		return { typ: anyTyp(), val, dep: unionDep(...es.map((e) => e.dep)) };
	return { typ: unknownTyp(), val, dep: unionDep(...es.map((e) => e.dep)) };
};

export const MERGEOBJECTS = {
	argsSigs: [sigOOs],
	expr: mergeObjectsE,
};
