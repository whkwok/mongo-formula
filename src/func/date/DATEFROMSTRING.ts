import { AssignableExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigSSoSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { dateTyp } from '../../util/typ';

export const dateFromString = (v0: any, v1?: any, v2?: any) => ({
	$dateFromString: trimUndefined({
		dateString: v0,
		onError: null,
		onNull: null,
		format: v1,
		timezone: v2,
	}),
});

export const dateFromStringE = (
	e0: AssignableExpr<StringExpr>,
	e1?: AssignableExpr<StringExpr>,
	e2?: AssignableExpr<StringExpr>
) => ({
	typ: dateTyp(),
	val: dateFromString(e0.val, e1?.val, e2?.val),
	dep: unionDep(e0.dep, e1?.dep, e2?.dep),
});

export const DATEFROMSTRING = {
	argsSigs: [sigSSoSo],
	expr: dateFromStringE,
};
