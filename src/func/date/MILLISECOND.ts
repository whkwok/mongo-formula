import { AssignableExpr, DateExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { numberTyp } from '../../util/typ';

export const millisecond = (v0: any, v1?: any) => ({
	$millisecond: trimUndefined({ date: v0, timezone: v1 }),
});

export const millisecondE = (
	e0: AssignableExpr<DateExpr>,
	e1?: AssignableExpr<StringExpr>
) => ({
	typ: numberTyp(),
	val: millisecond(e0.val, e1?.val),
	dep: unionDep(e0.dep, e1?.dep),
});

export const MILLISECOND = {
	argsSigs: [sigDSo],
	expr: millisecondE,
};
