import { AssignableExpr, DateExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { numberTyp } from '../../util/typ';

export const week = (v0: any, v1?: any) => ({
	$week: trimUndefined({ date: v0, timezone: v1 }),
});

export const weekE = (
	e0: AssignableExpr<DateExpr>,
	e1?: AssignableExpr<StringExpr>
) => ({
	typ: numberTyp(),
	val: week(e0.val, e1?.val),
	dep: unionDep(e0.dep, e1?.dep),
});

export const WEEK = {
	argsSigs: [sigDSo],
	expr: weekE,
};
