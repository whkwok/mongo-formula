import { DATEFROMISOPARTS } from './DATEFROMISOPARTS';
import { DATEFROMPARTS } from './DATEFROMPARTS';
import { DATEFROMSTRING } from './DATEFROMSTRING';
import { DATETOISOPARTS } from './DATETOISOPARTS';
import { DATETOPARTS } from './DATETOPARTS';
import { DATETOSTRING } from './DATETOSTRING';
import { DAYOFMONTH } from './DAYOFMONTH';
import { DAYOFWEEK } from './DAYOFWEEK';
import { DAYOFYEAR } from './DAYOFYEAR';
import { HOUR } from './HOUR';
import { ISODAYOFWEEK } from './ISODAYOFWEEK';
import { ISOWEEK } from './ISOWEEK';
import { ISOWEEKYEAR } from './ISOWEEKYEAR';
import { MILLISECOND } from './MILLISECOND';
import { MILLISECONDOFDAY } from './MILLISECONDOFDAY';
import { MINUTE } from './MINUTE';
import { MONTH } from './MONTH';
import { SECOND } from './SECOND';
import { WEEK } from './WEEK';
import { YEAR } from './YEAR';

export const dateFuncDict = {
	DATEFROMISOPARTS,
	DATEFROMPARTS,
	DATEFROMSTRING,
	DATETOISOPARTS,
	DATETOPARTS,
	DATETOSTRING,
	DAYOFMONTH,
	DAYOFWEEK,
	DAYOFYEAR,
	HOUR,
	ISODAYOFWEEK,
	ISOWEEK,
	ISOWEEKYEAR,
	MILLISECOND,
	MILLISECONDOFDAY,
	MINUTE,
	MONTH,
	SECOND,
	WEEK,
	YEAR,
};
