import { AssignableExpr, DateExpr, StringExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDSo } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { add } from '../arithmetic/ADD';
import { multiply } from '../arithmetic/MULTIPLY';
import { dateToParts } from './DATETOPARTS';

export const getMillisecondOfDay = (reservedSuffix: string) => (
	v0: any,
	v1: any
) => {
	const [partsVK, partsVV] = getReservedVarKeyVal('parts', reservedSuffix);
	const partsHourVV = partsVV + '.hour';
	const partsMinuteVV = partsVV + '.minute';
	const partsSecondVV = partsVV + '.second';
	const partsMillisecondVV = partsVV + '.millisecond';

	return _let(
		{ [partsVK]: dateToParts(v0, v1) },
		add(
			multiply(partsHourVV, 3600000),
			multiply(partsMinuteVV, 60000),
			multiply(partsSecondVV, 1000),
			partsMillisecondVV
		)
	);
};

export const getMillisecondOfDayE = (reservedSuffix: string) => {
	const millisecondOfDay = getMillisecondOfDay(reservedSuffix);
	return (e0: AssignableExpr<DateExpr>, e1?: AssignableExpr<StringExpr>) => ({
		typ: numberTyp(),
		val: millisecondOfDay(e0.val, e1?.val),
		dep: unionDep(e0.dep, e1?.dep),
	});
};

export const MILLISECONDOFDAY = ({ options: { reservedSuffix } }: YY) => {
	const millisecondOfDayE = getMillisecondOfDayE(reservedSuffix);
	return {
		argsSigs: [sigDSo],
		expr: millisecondOfDayE,
	};
};
