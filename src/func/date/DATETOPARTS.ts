import { AssignableExpr, DateExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { datePartsTypProperties, objectTyp } from '../../util/typ';

export const dateToParts = (v0: any, v1?: any) => ({
	$dateToParts: trimUndefined({
		date: v0,
		timezone: v1,
	}),
});

export const dateToPartsE = (
	e0: AssignableExpr<DateExpr>,
	e1?: AssignableExpr<StringExpr>
) => ({
	typ: objectTyp(datePartsTypProperties()),
	val: dateToParts(e0.val, e1?.val),
	dep: unionDep(e0.dep, e1?.dep),
});

export const DATETOPARTS = {
	argsSigs: [sigDSo],
	expr: dateToPartsE,
};
