import { ArgsSig, AssignableExpr, DateExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import {
	dateTyp,
	isoDatePartsTypProperties,
	objectTyp,
	stringTyp,
} from '../../util/typ';
import { dateFromParts } from './DATEFROMPARTS';

export const dateFromIsoParts = dateFromParts;

export const dateFromIsoPartsE = (
	e0: AssignableExpr<DateExpr>,
	e1?: AssignableExpr<StringExpr>
) => ({
	typ: dateTyp(),
	val: dateFromIsoParts(e0.val, e1?.val),
	dep: unionDep(e0.dep, e1?.dep),
});

const sigDateFromIsoParts: ArgsSig = {
	typs: [
		objectTyp(isoDatePartsTypProperties(), [
			'isoWeek',
			'isoDayOfWeek',
			'hour',
			'minute',
			'second',
			'millisecond',
		]),
		stringTyp(),
	],
	minLen: 1,
	maxLen: 2,
};

export const DATEFROMISOPARTS = {
	argsSigs: [sigDateFromIsoParts],
	expr: dateFromIsoPartsE,
};
