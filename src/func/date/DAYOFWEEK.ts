import { AssignableExpr, DateExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { numberTyp } from '../../util/typ';

export const dayOfWeek = (v0: any, v1?: any) => ({
	$dayOfWeek: trimUndefined({ date: v0, timezone: v1 }),
});

export const dayOfWeekE = (
	e0: AssignableExpr<DateExpr>,
	e1?: AssignableExpr<StringExpr>
) => ({
	typ: numberTyp(),
	val: dayOfWeek(e0.val, e1?.val),
	dep: unionDep(e0.dep, e1?.dep),
});

export const DAYOFWEEK = {
	argsSigs: [sigDSo],
	expr: dayOfWeekE,
};
