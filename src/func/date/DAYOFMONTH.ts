import { AssignableExpr, DateExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { numberTyp } from '../../util/typ';

export const dayOfMonth = (v0: any, v1?: any) => ({
	$dayOfMonth: trimUndefined({ date: v0, timezone: v1 }),
});

export const dayOfMonthE = (
	e0: AssignableExpr<DateExpr>,
	e1?: AssignableExpr<StringExpr>
) => ({
	typ: numberTyp(),
	val: dayOfMonth(e0.val, e1?.val),
	dep: unionDep(e0.dep, e1?.dep),
});

export const DAYOFMONTH = {
	argsSigs: [sigDSo],
	expr: dayOfMonthE,
};
