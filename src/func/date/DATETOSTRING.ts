import { AssignableExpr, DateExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDSoSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { stringTyp } from '../../util/typ';

export const dateToString = (v0: any, v1?: any, v2?: any) => ({
	$dateToString: trimUndefined({
		date: v0,
		onNull: null,
		format: v1,
		timezone: v2,
	}),
});

export const dateToStringE = (
	e0: AssignableExpr<DateExpr>,
	e1?: AssignableExpr<StringExpr>,
	e2?: AssignableExpr<StringExpr>
) => ({
	typ: stringTyp(),
	val: dateToString(e0.val, e1?.val, e2?.val),
	dep: unionDep(e0.dep, e1?.dep, e2?.dep),
});

export const DATETOSTRING = {
	expr: dateToStringE,
	argsSigs: [sigDSoSo],
};
