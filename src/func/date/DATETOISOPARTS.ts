import { AssignableExpr, DateExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDSo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { isoDatePartsTypProperties, objectTyp } from '../../util/typ';

export const dateToIsoParts = (v0: any, v1?: any) => ({
	$dateToParts: trimUndefined({
		date: v0,
		iso8601: true,
		timezone: v1,
	}),
});

export const dateToIsoPartsE = (
	e0: AssignableExpr<DateExpr>,
	e1?: AssignableExpr<StringExpr>
) => ({
	typ: objectTyp(isoDatePartsTypProperties()),
	val: dateToIsoParts(e0.val, e1?.val),
	dep: unionDep(e0.dep, e1?.dep),
});

export const DATETOISOPARTS = {
	argsSigs: [sigDSo],
	expr: dateToIsoPartsE,
};
