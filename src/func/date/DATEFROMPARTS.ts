import { ArgsSig, AssignableExpr, ObjectExpr, StringExpr } from '../../types';
import { unionDep } from '../../util/dep';
import {
	datePartsTypProperties,
	dateTyp,
	objectTyp,
	stringTyp,
} from '../../util/typ';
import { _mergeObjects } from '../object/MERGEOBJECTS';

export const dateFromParts = (v0: any, v1?: any) => {
	if (v1 === undefined) return { $dateFromParts: v0 };
	return { $dateFromParts: _mergeObjects([v0, { timezone: v1 }]) };
};

export const dateFromPartsE = (
	e0: AssignableExpr<ObjectExpr>,
	e1?: AssignableExpr<StringExpr>
) => ({
	typ: dateTyp(),
	val: dateFromParts(e0.val, e1?.val),
	dep: unionDep(e0.dep, e1?.dep),
});

const sigDateFromParts: ArgsSig = {
	typs: [
		objectTyp(datePartsTypProperties(), [
			'month',
			'day',
			'hour',
			'minute',
			'second',
			'millisecond',
		]),
		stringTyp(),
	],
	minLen: 1,
	maxLen: 2,
};

export const DATEFROMPARTS = {
	argsSigs: [sigDateFromParts],
	expr: dateFromPartsE,
};
