import { Expr } from '../../types';
import { sigU } from '../../util/sigs';
import { inferTyp, toNonLiteralTyp } from '../../util/typ';

export const literal = (v: any) => ({ $literal: v });

export const literalE = (e: Expr) => ({
	typ: toNonLiteralTyp(inferTyp(e.val)),
	val: literal(e.val),
	dep: [],
});

export const LITERAL = {
	argsSigs: [sigU],
	expr: literalE,
};
