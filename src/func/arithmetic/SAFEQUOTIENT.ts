import { AssignableExpr, NumberExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';
import { _getSafeDivide, getSafeDivide } from './SAFEDIVIDE';
import { _trunc, trunc } from './TRUNC';

// Return null when divisor is 0
export const getSafeQuotient = (reservedSuffix: string) => {
	const safeDivide = getSafeDivide(reservedSuffix);
	return (v0: any, v1: any) => trunc(safeDivide(v0, v1));
};

export const _getSafeQuotient = (reservedSuffix: string) => {
	const _safeDivide = _getSafeDivide(reservedSuffix);
	return (v0: any, v1: any) => _trunc(_safeDivide(v0, v1));
};

export const getSafeQuotientE = (reservedSuffix: string) => {
	const _safeQuotient = _getSafeQuotient(reservedSuffix);
	return (e0: AssignableExpr<NumberExpr>, e1: AssignableExpr<NumberExpr>) => {
		const val = _safeQuotient(e0.val, e1.val);
		if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
		return { typ: numberTyp(), val, dep: unionDep(e0.dep, e1.dep) };
	};
};

export const SAFEQUOTIENT = ({ options: { reservedSuffix } }: YY) => {
	const safeQuotientE = getSafeQuotientE(reservedSuffix);
	return {
		argsSigs: [sigNN],
		expr: safeQuotientE,
	};
};
