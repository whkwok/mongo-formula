import { AssignableExpr, NumberExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { gt } from '../comparison/GT';
import { cond } from '../conditional/COND';
import { abs } from './ABS';
import { divide } from './DIVIDE';
import { floor } from './FLOOR';
import { multiply } from './MULTIPLY';
import { sign } from './SIGN';

export const getRoundDownInt = (reservedSuffix: string) => (v0: any) => {
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	return _let({ [v0VK]: v0 }, multiply(sign(v0VV), floor(abs(v0VV))));
};

// MROUNDDOWN rounds number towards 0
export const getMroundDown = (reservedSuffix: string) => (v0: any, v1: any) => {
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	const [v1VK, v1VV] = getReservedVarKeyVal('v1', reservedSuffix);
	const roundDownInt = getRoundDownInt(reservedSuffix);
	return _let(
		{ [v0VK]: v0, [v1VK]: v1 },
		cond(
			gt(v1VV, 0),
			multiply(roundDownInt(divide(v0VV, v1VV)), v1VV),
			null
		)
	);
};

export const getMroundDownE = (reservedSuffix: string) => {
	const mroundDown = getMroundDown(reservedSuffix);
	return (
		e0: AssignableExpr<NumberExpr>,
		e1: AssignableExpr<NumberExpr>
	) => ({
		typ: numberTyp(),
		val: mroundDown(e0.val, e1.val),
		dep: unionDep(e0.dep, e1.dep),
	});
};

export const MROUNDDOWN = ({ options: { reservedSuffix } }: YY) => {
	const mroundDownE = getMroundDownE(reservedSuffix);
	return {
		argsSigs: [sigNN],
		expr: mroundDownE,
	};
};
