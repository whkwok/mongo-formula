import { AssignableExpr, NumberExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { gt } from '../comparison/GT';
import { cond } from '../conditional/COND';
import { abs } from './ABS';
import { ceil } from './CEIL';
import { divide } from './DIVIDE';
import { multiply } from './MULTIPLY';
import { sign } from './SIGN';

export const getRoundUpInt = (reservedSuffix: string) => (v0: any) => {
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	return _let({ [v0VK]: v0 }, multiply(sign(v0VV), ceil(abs(v0VV))));
};

// MROUNDUP rounds number away from 0
export const getMroundUp = (reservedSuffix: string) => (v0: any, v1: any) => {
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	const [v1VK, v1VV] = getReservedVarKeyVal('v1', reservedSuffix);
	const roundUpInt = getRoundUpInt(reservedSuffix);
	return _let(
		{ [v0VK]: v0, [v1VK]: v1 },
		cond(gt(v1VV, 0), multiply(roundUpInt(divide(v0VV, v1VV)), v1VV), null)
	);
};

export const getMroundUpE = (reservedSuffix: string) => {
	const mroundUp = getMroundUp(reservedSuffix);
	return (
		e0: AssignableExpr<NumberExpr>,
		e1: AssignableExpr<NumberExpr>
	) => ({
		typ: numberTyp(),
		val: mroundUp(e0.val, e1.val),
		dep: unionDep(e0.dep, e1.dep),
	});
};

export const MROUNDUP = ({ options: { reservedSuffix } }: YY) => {
	const mroundUpE = getMroundUpE(reservedSuffix);
	return {
		argsSigs: [sigNN],
		expr: mroundUpE,
	};
};
