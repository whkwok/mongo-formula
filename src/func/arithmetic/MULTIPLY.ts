import { AssignableExpr, NumberExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNNs } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal, isOpVal } from '../../util/val';

export const multiply = (...vs: any[]) => ({ $multiply: vs });

// Accumulate number value and flatten $multiply expression
const getMultiplyVals = (num: number, vs: any[]): 0 | [any[], number] => {
	const _vs: any[] = [];
	for (let i = 0, l = vs.length; i < l; ++i) {
		const v = vs[i];
		if (isNumberVal(v)) {
			if (v === 0) return 0;
			num *= v;
		} else if (isOpVal(v, '$multiply') && v.$multiply instanceof Array) {
			const vals = getMultiplyVals(num, v.$multiply);
			if (vals === 0) return 0;
			const [__vs, __num] = vals;
			num = __num;
			_vs.push(...__vs);
		} else {
			if (num !== 1) {
				_vs.push(num, v);
				num = 1;
			} else _vs.push(v);
		}
	}
	return [_vs, num];
};

export const _multiply = (...vs: any[]) => {
	const vals = getMultiplyVals(1, vs);
	if (vals === 0) return 0;
	const [_vs, _num] = vals;
	if (_num !== 1) _vs.push(_num);
	if (_vs.length === 1) return _vs[0];
	return multiply(..._vs);
};

export const multiplyE = (...es: AssignableExpr<NumberExpr>[]) => {
	const val = _multiply(...es.map((e) => e.val));
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: unionDep(...es.map((e) => e.dep)) };
};

export const MULTIPLY = {
	argsSigs: [sigNNs],
	expr: multiplyE,
};
