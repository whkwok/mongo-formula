import { AssignableExpr, DateExpr, NumberExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDD, sigDN, sigNN } from '../../util/sigs';
import { isDateTyp, isNumberTyp } from '../../util/typ';
import { dateTyp, numberTyp, unknownTyp } from '../../util/typ/typs';
import { isNumberVal } from '../../util/val';

export const subtract = (v0: any, v1: any) => ({ $subtract: [v0, v1] });

export const _subtract = (v0: any, v1: any) =>
	isNumberVal(v0) && isNumberVal(v1) ? v0 - v1 : subtract(v0, v1);

export const subtractE = (
	e0: AssignableExpr<DateExpr | NumberExpr>,
	e1: AssignableExpr<DateExpr | NumberExpr>
) => {
	const val = _subtract(e0.val, e1.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	const t0Date = isDateTyp(e0.typ);
	const t0Number = isNumberTyp(e0.typ);
	const t1Date = isDateTyp(e1.typ);
	const t1Number = isNumberTyp(e1.typ);
	return {
		typ:
			(t0Number && t1Number) || (t0Date && t1Date)
				? numberTyp()
				: t0Date && t1Number
				? dateTyp()
				: unknownTyp(),
		val,
		dep: unionDep(e0.dep, e1.dep),
	};
};

export const SUBTRACT = {
	argsSigs: [sigNN, sigDN, sigDD],
	expr: subtractE,
};
