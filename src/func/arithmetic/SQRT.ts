import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';

export const sqrt = (v: any) => ({ $sqrt: v });

export const _sqrt = (v: any) => (isNumberVal(v) ? Math.sqrt(v) : sqrt(v));

export const sqrtE = (e: AssignableExpr<NumberExpr>) => {
	const val = _sqrt(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const SQRT = {
	argsSigs: [sigN],
	expr: sqrtE,
};
