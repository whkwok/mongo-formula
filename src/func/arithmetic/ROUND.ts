import { AssignableExpr, NumberExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNNo } from '../../util/sigs';
import { trimUndefined } from '../../util/trimUndefined';
import { numberTyp } from '../../util/typ';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { convert } from '../type/CONVERT';
import { toDecimal } from '../type/TODECIMAL';
import { _type } from '../type/TYPE';
import { getMround, getRoundInt } from './MROUND';
import { _neg } from './NEG';
import { _pow } from './POW';

// Compatible round with mongodb < 4.2
// COMPATROUND behavior at 5 case may differ from $round
// rounds away from zero
export const getRound = (reservedSuffix: string, compatMode: boolean) => (
	v0: any,
	v1?: any
) => {
	if (!compatMode) return { $round: trimUndefined([v0, v1]) };
	const roundInt = getRoundInt(reservedSuffix, compatMode);
	if (v1 === undefined) return roundInt(v0);
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	const [decV0VK, decV0VV] = getReservedVarKeyVal('decV0', reservedSuffix);
	const [v0DTypeVK, v0DTypeVV] = getReservedVarKeyVal(
		'v0DType',
		reservedSuffix
	);
	const [mVK, mVV] = getReservedVarKeyVal('m', reservedSuffix);
	const mroundV = getMround(reservedSuffix, compatMode);
	return _let(
		{ [v0VK]: v0 },
		_let(
			{
				[v0DTypeVK]: _type(v0VV),
				[decV0VK]: toDecimal(v0VV),
				[mVK]: _pow(10, _neg(v1)),
			},
			convert(mroundV(decV0VV, mVV), v0DTypeVV)
		)
	);
};

export const getRoundE = (reservedSuffix: string, compatMode: boolean) => {
	const round = getRound(reservedSuffix, compatMode);
	return (
		e0: AssignableExpr<NumberExpr>,
		e1?: AssignableExpr<NumberExpr>
	) => ({
		typ: numberTyp(),
		val: round(e0.val, e1?.val),
		dep: unionDep(e0.dep, e1?.dep),
	});
};

export const ROUND = ({ options: { reservedSuffix, compatMode } }: YY) => {
	const roundE = getRoundE(reservedSuffix, compatMode);
	return {
		argsSigs: [sigNNo],
		expr: roundE,
	};
};
