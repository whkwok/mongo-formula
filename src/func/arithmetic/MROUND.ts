import { AssignableExpr, NumberExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { gt } from '../comparison/GT';
import { gte } from '../comparison/GTE';
import { cond } from '../conditional/COND';
import { abs } from './ABS';
import { add } from './ADD';
import { divide } from './DIVIDE';
import { multiply } from './MULTIPLY';
import { sign } from './SIGN';
import { subtract } from './SUBTRACT';
import { trunc } from './TRUNC';

// Compatible mround with mongodb < 4.2
// COMPATMROUND behavior at 5 case may differ from $round
// rounds away from zero
const toInt = (v: any) => ({ $toInt: v });

export const getRoundInt = (reservedSuffix: string, compatMode: boolean) => (
	v: any
) => {
	if (!compatMode) return { $round: v };
	const [vVK, vVV] = getReservedVarKeyVal('v', reservedSuffix);
	const [magnVK, magnVV] = getReservedVarKeyVal('magn', reservedSuffix);
	const [signVK, signVV] = getReservedVarKeyVal('sign', reservedSuffix);
	const [truncedVK, truncedVV] = getReservedVarKeyVal(
		'trunced',
		reservedSuffix
	);

	return _let(
		{ [vVK]: v },
		_let(
			{ [magnVK]: abs(vVV), [signVK]: sign(vVV) },
			_let(
				{ [truncedVK]: trunc(magnVV) },
				multiply(
					signVV,
					add(
						truncedVV,
						toInt(gte(subtract(magnVV, truncedVV), 1 / 2))
					)
				)
			)
		)
	);
};

export const getMround = (reservedSuffix: string, compatMode: boolean) => (
	v0: any,
	v1: any
) => {
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	const [v1VK, v1VV] = getReservedVarKeyVal('v1', reservedSuffix);
	const roundInt = getRoundInt(reservedSuffix, compatMode);
	return _let(
		{ [v0VK]: v0, [v1VK]: v1 },
		cond(gt(v1VV, 0), multiply(roundInt(divide(v0VV, v1VV)), v1VV), null)
	);
};

export const getMroundE = (reservedSuffix: string, compatMode: boolean) => {
	const compatMround = getMround(reservedSuffix, compatMode);
	return (
		e0: AssignableExpr<NumberExpr>,
		e1: AssignableExpr<NumberExpr>
	) => ({
		typ: numberTyp(),
		val: compatMround(e0.val, e1.val),
		dep: unionDep(e0.dep, e1.dep),
	});
};

export const MROUND = ({ options: { reservedSuffix, compatMode } }: YY) => {
	const mroundE = getMroundE(reservedSuffix, compatMode);
	return {
		argsSigs: [sigNN],
		expr: mroundE,
	};
};
