import { ABS } from './ABS';
import { ADD } from './ADD';
import { CEIL } from './CEIL';
import { DIVIDE } from './DIVIDE';
import { EXP } from './EXP';
import { FLOOR } from './FLOOR';
import { LN } from './LN';
import { LOG } from './LOG';
import { LOG10 } from './LOG10';
import { MOD } from './MOD';
import { MROUND } from './MROUND';
import { MROUNDDOWN } from './MROUNDDOWN';
import { MROUNDUP } from './MROUNDUP';
import { MULTIPLY } from './MULTIPLY';
import { NEG } from './NEG';
import { POW } from './POW';
import { QUOTIENT } from './QUOTIENT';
import { ROUND } from './ROUND';
import { ROUNDDOWN } from './ROUNDDOWN';
import { ROUNDUP } from './ROUNDUP';
import { SAFEDIVIDE } from './SAFEDIVIDE';
import { SAFEQUOTIENT } from './SAFEQUOTIENT';
import { SIGN } from './SIGN';
import { SQRT } from './SQRT';
import { SUBTRACT } from './SUBTRACT';
import { TRUNC } from './TRUNC';

export const arithmeticFuncDict = {
	ABS,
	ADD,
	CEIL,
	DIVIDE,
	EXP,
	FLOOR,
	LN,
	LOG,
	LOG10,
	MOD,
	MROUND,
	MROUNDDOWN,
	MROUNDUP,
	MULTIPLY,
	NEG,
	POW,
	QUOTIENT,
	ROUND,
	ROUNDDOWN,
	ROUNDUP,
	SAFEDIVIDE,
	SAFEQUOTIENT,
	SIGN,
	SQRT,
	SUBTRACT,
	TRUNC,
};
