import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';
import { multiply } from './MULTIPLY';

export const neg = (v: any) => multiply(-1, v);

export const _neg = (v: any) => (isNumberVal(v) ? -v : neg(v));

export const negE = (e: AssignableExpr<NumberExpr>) => {
	const val = _neg(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const NEG = {
	argsSigs: [sigN],
	expr: negE,
};
