import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';

export const trunc = (v: any) => ({ $trunc: v });

export const _trunc = (v: any) => (isNumberVal(v) ? Math.trunc(v) : trunc(v));

export const truncE = (e: AssignableExpr<NumberExpr>) => {
	const val = _trunc(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const TRUNC = {
	argsSigs: [sigN],
	expr: truncE,
};
