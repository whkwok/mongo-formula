import { AssignableExpr, NumberExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNNo } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { convert } from '../type/CONVERT';
import { toDecimal } from '../type/TODECIMAL';
import { _type } from '../type/TYPE';
import { getMroundDown, getRoundDownInt } from './MROUNDDOWN';
import { _neg } from './NEG';
import { _pow } from './POW';

// ROUNDDOWN rounds number towards 0
export const getRoundDown = (reservedSuffix: string) => (v0: any, v1?: any) => {
	if (v1 === undefined) {
		const roundDownInt = getRoundDownInt(reservedSuffix);
		return roundDownInt(v0);
	}
	const [v0VK, v0VV] = getReservedVarKeyVal('v0', reservedSuffix);
	const [decV0VK, decV0VV] = getReservedVarKeyVal('decV0', reservedSuffix);
	const [v0DTypeVK, v0DTypeVV] = getReservedVarKeyVal(
		'v0DType',
		reservedSuffix
	);
	const [mVK, mVV] = getReservedVarKeyVal('m', reservedSuffix);
	const mroundDown = getMroundDown(reservedSuffix);
	return _let(
		{ [v0VK]: v0 },
		_let(
			{
				[v0DTypeVK]: _type(v0VV),
				[decV0VK]: toDecimal(v0VV),
				[mVK]: _pow(10, _neg(v1)),
			},
			convert(mroundDown(decV0VV, mVV), v0DTypeVV)
		)
	);
};

export const getRoundDownE = (reservedSuffix: string) => {
	const roundDown = getRoundDown(reservedSuffix);
	return (
		e0: AssignableExpr<NumberExpr>,
		e1?: AssignableExpr<NumberExpr>
	) => ({
		typ: numberTyp(),
		val: roundDown(e0.val, e1?.val),
		dep: unionDep(e0.dep, e1?.dep),
	});
};

export const ROUNDDOWN = ({ options: { reservedSuffix } }: YY) => {
	const roundDownE = getRoundDownE(reservedSuffix);
	return {
		argsSigs: [sigNNo],
		expr: roundDownE,
	};
};
