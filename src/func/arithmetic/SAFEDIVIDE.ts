import { AssignableExpr, NumberExpr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';
import { _let, getReservedVarKeyVal } from '../../util/var';
import { eq } from '../comparison/EQ';
import { cond } from '../conditional/COND';
import { divide } from './DIVIDE';

// Return null when divisor is 0
export const getSafeDivide = (reservedSuffix: string) => (v0: any, v1: any) => {
	const [v1VK, v1VV] = getReservedVarKeyVal('v1', reservedSuffix);
	return _let({ [v1VK]: v1 }, cond(eq(v1VV, 0), null, divide(v0, v1VV)));
};

export const _getSafeDivide = (reservedSuffix: string) => {
	const safeDivide = getSafeDivide(reservedSuffix);
	return (v0: any, v1: any) => {
		if (isNumberVal(v0) && isNumberVal(v1)) {
			if (v1 === 0) return null;
			return v0 / v1;
		}
		return safeDivide(v0, v1);
	};
};

export const getSafeDivideE = (reservedSuffix: string) => {
	const _safeDivide = _getSafeDivide(reservedSuffix);
	return (e0: AssignableExpr<NumberExpr>, e1: AssignableExpr<NumberExpr>) => {
		const val = _safeDivide(e0.val, e1.val);
		if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
		return { typ: numberTyp(), val, dep: unionDep(e0.dep, e1.dep) };
	};
};

export const SAFEDIVIDE = ({ options: { reservedSuffix } }: YY) => {
	const safeDivideE = getSafeDivideE(reservedSuffix);
	return {
		argsSigs: [sigNN],
		expr: safeDivideE,
	};
};
