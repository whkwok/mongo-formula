import { AssignableExpr, NumberExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';

export const divide = (v0: any, v1: any) => ({ $divide: [v0, v1] });

export const _divide = (v0: any, v1: any) =>
	isNumberVal(v0) && isNumberVal(v1) ? v0 / v1 : divide(v0, v1);

export const divideE = (
	e0: AssignableExpr<NumberExpr>,
	e1: AssignableExpr<NumberExpr>
) => {
	const val = _divide(e0.val, e1.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: unionDep(e0.dep, e1.dep) };
};

export const DIVIDE = {
	argsSigs: [sigNN],
	expr: divideE,
};
