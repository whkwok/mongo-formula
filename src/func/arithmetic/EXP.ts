import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';

export const exp = (v: any) => ({ $exp: v });

export const _exp = (v: any) => (isNumberVal(v) ? Math.exp(v) : exp(v));

export const expE = (e: AssignableExpr<NumberExpr>) => {
	const val = _exp(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const EXP = {
	argsSigs: [sigN],
	expr: expE,
};
