import { AssignableExpr, DateExpr, NumberExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigDNs, sigNNs } from '../../util/sigs';
import { numberTyp, toNonLiteralTyp } from '../../util/typ';
import { isNumberVal, isOpVal } from '../../util/val';

export const add = (...vs: any[]) => ({ $add: vs });

// Combine number value and flatten $add expression
const getAddVals = (num: number, vs: any[]): [any[], number] => {
	const _vs: any[] = [];
	for (let i = 0, l = vs.length; i < l; ++i) {
		const v = vs[i];
		if (isNumberVal(v)) num += v;
		else if (isOpVal(v, '$add') && v.$add instanceof Array) {
			const [__vs, __num] = getAddVals(num, v.$add);
			num = __num;
			_vs.push(...__vs);
		} else {
			if (num) {
				_vs.push(num, v);
				num = 0;
			} else _vs.push(v);
		}
	}
	return [_vs, num];
};

export const _add = (...vs: any[]) => {
	const [_vs, _num] = getAddVals(0, vs);
	if (_num !== 0) _vs.push(_num);
	if (_vs.length === 1) return _vs[0];
	return add(..._vs);
};

export const addE = (...es: AssignableExpr<NumberExpr | DateExpr>[]) => {
	const val = _add(...es.map((e) => e.val));
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return {
		typ: toNonLiteralTyp(es[0].typ),
		val,
		dep: unionDep(...es.map((e) => e.dep)),
	};
};

export const ADD = {
	argsSigs: [sigNNs, sigDNs],
	expr: addE,
};
