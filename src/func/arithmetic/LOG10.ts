import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';

export const log10 = (v: any) => ({ $log10: v });

export const _log10 = (v: any) => (isNumberVal(v) ? Math.log10(v) : log10(v));

export const log10E = (e: AssignableExpr<NumberExpr>) => {
	const val = _log10(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const LOG10 = {
	argsSigs: [sigN],
	expr: log10E,
};
