import { AssignableExpr, NumberExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';
import { _divide, divide } from './DIVIDE';
import { _trunc, trunc } from './TRUNC';

export const quotient = (v0: any, v1: any) => trunc(divide(v0, v1));

export const _quotient = (v0: any, v1: any) => _trunc(_divide(v0, v1));

export const quotientE = (
	e0: AssignableExpr<NumberExpr>,
	e1: AssignableExpr<NumberExpr>
) => {
	const val = _quotient(e0.val, e1.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: unionDep(e0.dep, e1.dep) };
};

export const QUOTIENT = {
	argsSigs: [sigNN],
	expr: quotientE,
};
