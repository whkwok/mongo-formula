import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';
import { cmp } from '../comparison/CMP';

export const sign = (v: any) => cmp(v, 0);

export const _sign = (v: any) => (isNumberVal(v) ? Math.sign(v) : sign(v));

export const signE = (e: AssignableExpr<NumberExpr>) => {
	const val = _sign(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const SIGN = {
	argsSigs: [sigN],
	expr: signE,
};
