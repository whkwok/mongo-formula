import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';

export const floor = (v: any) => ({ $floor: v });

export const _floor = (v: any) => (isNumberVal(v) ? Math.floor(v) : floor(v));

export const floorE = (e: AssignableExpr<NumberExpr>) => {
	const val = _floor(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const FLOOR = {
	argsSigs: [sigN],
	expr: floorE,
};
