import { AssignableExpr, NumberExpr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigNN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';

export const log = (v0: any, v1: any) => ({ $log: [v0, v1] });

export const logE = (
	e0: AssignableExpr<NumberExpr>,
	e1: AssignableExpr<NumberExpr>
) => ({
	typ: numberTyp(),
	val: log(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const LOG = {
	argsSigs: [sigNN],
	expr: logE,
};
