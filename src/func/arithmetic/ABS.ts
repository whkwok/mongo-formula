import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';

export const abs = (v: any) => ({ $abs: v });

export const _abs = (v: any) => (isNumberVal(v) ? Math.abs(v) : abs(v));

export const absE = (e: AssignableExpr<NumberExpr>) => {
	const val = _abs(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const ABS = {
	argsSigs: [sigN],
	expr: absE,
};
