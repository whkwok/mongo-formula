import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';

export const ln = (v: any) => ({ $ln: v });

export const _ln = (v: any) => (isNumberVal(v) ? Math.log(v) : ln(v));

export const lnE = (e: AssignableExpr<NumberExpr>) => {
	const val = _ln(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const LN = {
	argsSigs: [sigN],
	expr: lnE,
};
