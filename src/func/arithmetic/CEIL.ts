import { AssignableExpr, NumberExpr } from '../../types';
import { sigN } from '../../util/sigs';
import { numberTyp } from '../../util/typ';
import { isNumberVal } from '../../util/val';

export const ceil = (v: any) => ({ $ceil: v });

export const _ceil = (v: any) => (isNumberVal(v) ? Math.ceil(v) : ceil(v));

export const ceilE = (e: AssignableExpr<NumberExpr>) => {
	const val = _ceil(e.val);
	if (isNumberVal(val)) return { typ: numberTyp(true), val, dep: [] };
	return { typ: numberTyp(), val, dep: e.dep };
};

export const CEIL = {
	argsSigs: [sigN],
	expr: ceilE,
};
