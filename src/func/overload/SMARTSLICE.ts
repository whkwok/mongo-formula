import { OverloadErr } from '../../errors';
import {
	ArrayExpr,
	AssignableExpr,
	Loc,
	NumberExpr,
	StringExpr,
	YY,
} from '../../types';
import { isArrayExpr, isStringExpr } from '../../util/expr';
import { combineLoc } from '../../util/loc';
import { sigANNo, sigSNNo } from '../../util/sigs';
import { getSafeSliceE } from '../array/SAFESLICE';
import { getSafeSubStrCPE } from '../string/SAFESUBSTRCP';

export const getSmartSliceE = (reservedSuffix: string) => {
	const safeSliceE = getSafeSliceE(reservedSuffix);
	const safeSubStrCPE = getSafeSubStrCPE(reservedSuffix);
	return (
		e0: AssignableExpr<StringExpr | ArrayExpr> & { loc?: Loc },
		e1: AssignableExpr<NumberExpr> & { loc?: Loc },
		e2?: AssignableExpr<NumberExpr> & { loc?: Loc }
	) => {
		if (isArrayExpr(e0)) return safeSliceE(e0, e1, e2);
		if (isStringExpr(e0)) return safeSubStrCPE(e0, e1, e2);
		throw new OverloadErr({
			context: 'SMARTSLICE',
			receivedTyps: e2 ? [e0.typ, e1.typ, e2.typ] : [e0.typ, e1.typ],
			loc: combineLoc(e0.loc, e1.loc, e2?.loc),
		});
	};
};

export const SMARTSLICE = ({ options: { reservedSuffix } }: YY) => {
	const smartSliceE = getSmartSliceE(reservedSuffix);
	return {
		argsSigs: [sigSNNo, sigANNo],
		expr: smartSliceE,
	};
};
