import { OverloadErr } from '../../errors';
import { AnyExpr, Loc } from '../../types';
import { isDateExpr, isNumberExpr, isStringExpr } from '../../util/expr';
import { combineLoc } from '../../util/loc';
import { sigDN, sigNN, sigSS } from '../../util/sigs';
import { addE } from '../arithmetic/ADD';
import { concatE } from '../string/CONCAT';

export const smartAddE = (
	e0: AnyExpr & { loc?: Loc },
	e1: AnyExpr & { loc?: Loc }
) => {
	if (isNumberExpr(e0) || isDateExpr(e0) || isNumberExpr(e1))
		return addE(e0, e1);
	if (isStringExpr(e0) || isStringExpr(e1)) return concatE(e0, e1);
	throw new OverloadErr({
		context: 'SMARTADD',
		receivedTyps: [e0.typ, e1.typ],
		loc: combineLoc(e0.loc, e1.loc),
	});
};

export const SMARTADD = {
	argsSigs: [sigNN, sigDN, sigSS],
	expr: smartAddE,
};
