import { OverloadErr } from '../../errors';
import {
	ArrayExpr,
	AssignableExpr,
	Loc,
	NumberExpr,
	StringExpr,
} from '../../types';
import { isArrayExpr, isNumberExpr, isStringExpr } from '../../util/expr';
import { sigA, sigN, sigS } from '../../util/sigs';
import { absE } from '../arithmetic/ABS';
import { sizeE } from '../array/SIZE';
import { strLenCPE } from '../string/STRLENCP';

export const smartLenE = (
	e: AssignableExpr<NumberExpr | StringExpr | ArrayExpr> & { loc?: Loc }
) => {
	if (isNumberExpr(e)) return absE(e);
	if (isStringExpr(e)) return strLenCPE(e);
	if (isArrayExpr(e)) return sizeE(e);
	throw new OverloadErr({
		context: 'SMARTLEN',
		receivedTyps: [e.typ],
		loc: e.loc,
	});
};

export const SMARTLEN = {
	argsSigs: [sigN, sigS, sigA],
	expr: smartLenE,
};
