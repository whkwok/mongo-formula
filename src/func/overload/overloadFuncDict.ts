import { SMARTADD } from './SMARTADD';
import { SMARTINDEX } from './SMARTINDEX';
import { SMARTLEN } from './SMARTLEN';
import { SMARTSLICE } from './SMARTSLICE';

export const overloadFuncDict = {
	SMARTADD,
	SMARTINDEX,
	SMARTLEN,
	SMARTSLICE,
};
