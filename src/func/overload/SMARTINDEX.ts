import { OverloadErr } from '../../errors';
import {
	ArrayExpr,
	AssignableExpr,
	Loc,
	NumberExpr,
	StringExpr,
	YY,
} from '../../types';
import { isArrayExpr, isStringExpr } from '../../util/expr';
import { combineLoc } from '../../util/loc';
import { sigAN, sigSN } from '../../util/sigs';
import { arrayElemAtE } from '../array/ARRAYELEMAT';
import { getStrElemAtCPE } from '../string/STRELEMATCP';

export const getSmartIndexE = (reservedSuffix: string) => {
	const strElemAtCPE = getStrElemAtCPE(reservedSuffix);
	return (
		e0: AssignableExpr<StringExpr | ArrayExpr> & { loc?: Loc },
		e1: AssignableExpr<NumberExpr> & { loc?: Loc }
	) => {
		if (isStringExpr(e0)) return strElemAtCPE(e0, e1);
		if (isArrayExpr(e0)) return arrayElemAtE(e0, e1);
		throw new OverloadErr({
			context: 'SMARTINDEX',
			receivedTyps: [e0.typ, e1.typ],
			loc: combineLoc(e0.loc, e1.loc),
		});
	};
};

export const SMARTINDEX = ({ options: { reservedSuffix } }: YY) => {
	const smartIndexE = getSmartIndexE(reservedSuffix);
	return {
		argsSigs: [sigSN, sigAN],
		expr: smartIndexE,
	};
};
