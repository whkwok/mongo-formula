import { ArrayExpr, AssignableExpr, Expr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { findScope } from '../../util/scope';
import { sigA, sigU } from '../../util/sigs';
import { accumulatorTyp, getItemsTyp, toNonLiteralTyp } from '../../util/typ';

export const max = (v: any) => ({ $max: v });

export const maxE = (e: AssignableExpr<ArrayExpr>) => ({
	typ: getItemsTyp(e.typ),
	val: max(e.val),
	dep: unionDep(e.dep),
});

export const maxGE = (e: Expr) => ({
	typ: accumulatorTyp(toNonLiteralTyp(e.typ)),
	val: max(e.val),
	dep: unionDep(e.dep),
});

export const MAX = ({ scopeStack }: YY) => {
	try {
		const scope = findScope(scopeStack, 'STAGE');
		if (scope.stageKey === 'GROUP')
			return {
				argsSigs: [sigU],
				expr: maxGE,
			};
	} catch {}
	return {
		argsSigs: [sigA],
		expr: maxE,
	};
};
