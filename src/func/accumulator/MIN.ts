import { ArrayExpr, AssignableExpr, Expr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { findScope } from '../../util/scope';
import { sigA, sigU } from '../../util/sigs';
import { accumulatorTyp, getItemsTyp, toNonLiteralTyp } from '../../util/typ';

export const min = (v: any) => ({ $min: v });

export const minE = (e: AssignableExpr<ArrayExpr>) => ({
	typ: getItemsTyp(e.typ),
	val: min(e.val),
	dep: unionDep(e.dep),
});

export const minGE = (e: Expr) => ({
	typ: accumulatorTyp(toNonLiteralTyp(e.typ)),
	val: min(e.val),
	dep: unionDep(e.dep),
});

export const MIN = ({ scopeStack }: YY) => {
	try {
		const scope = findScope(scopeStack, 'STAGE');
		if (scope.stageKey === 'GROUP')
			return {
				argsSigs: [sigU],
				expr: minGE,
			};
	} catch {}
	return {
		argsSigs: [sigA],
		expr: minE,
	};
};
