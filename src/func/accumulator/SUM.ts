import { Expr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { findScope } from '../../util/scope';
import { sigA, sigU } from '../../util/sigs';
import { accumulatorTyp, numberTyp } from '../../util/typ';

export const sum = (v: any) => ({ $sum: v });

export const sumE = (e: Expr) => ({
	typ: numberTyp(),
	val: sum(e.val),
	dep: unionDep(e.dep),
});

export const sumGE = (e: Expr) => ({
	typ: accumulatorTyp(numberTyp()),
	val: sum(e.val),
	dep: unionDep(e.dep),
});

export const SUM = ({ scopeStack }: YY) => {
	try {
		const scope = findScope(scopeStack, 'STAGE');
		if (scope.stageKey === 'GROUP')
			return {
				argsSigs: [sigU],
				expr: sumGE,
			};
	} catch {}
	return {
		argsSigs: [sigA],
		expr: sumE,
	};
};
