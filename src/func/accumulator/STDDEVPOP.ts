import { Expr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { findScope } from '../../util/scope';
import { sigA, sigU } from '../../util/sigs';
import { accumulatorTyp, numberTyp } from '../../util/typ';

export const stdDevPop = (v: any) => ({ $stdDevPop: v });

export const stdDevPopE = (e: Expr) => ({
	typ: numberTyp(),
	val: stdDevPop(e.val),
	dep: unionDep(e.dep),
});

export const stdDevPopGE = (e: Expr) => ({
	typ: accumulatorTyp(numberTyp()),
	val: stdDevPop(e.val),
	dep: unionDep(e.dep),
});

export const STDDEVPOP = ({ scopeStack }: YY) => {
	try {
		const scope = findScope(scopeStack, 'STAGE');
		if (scope.stageKey === 'GROUP')
			return {
				argsSigs: [sigU],
				expr: stdDevPopGE,
			};
	} catch {}
	return {
		argsSigs: [sigA],
		expr: stdDevPopE,
	};
};
