import { ArrayExpr, AssignableExpr, Expr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { findScope } from '../../util/scope';
import { sigA, sigU } from '../../util/sigs';
import { accumulatorTyp, getItemsTyp, toNonLiteralTyp } from '../../util/typ';

export const last = (v: any) => ({ $last: v });

export const lastE = (e: AssignableExpr<ArrayExpr>) => ({
	typ: getItemsTyp(e.typ),
	val: last(e.val),
	dep: unionDep(e.dep),
});

export const lastGE = (e: Expr) => ({
	typ: accumulatorTyp(toNonLiteralTyp(e.typ)),
	val: last(e.val),
	dep: unionDep(e.dep),
});

export const LAST = ({ scopeStack }: YY) => {
	try {
		const scope = findScope(scopeStack, 'STAGE');
		if (scope.stageKey === 'GROUP')
			return {
				argsSigs: [sigU],
				expr: lastGE,
			};
	} catch {}
	return {
		argsSigs: [sigA],
		expr: lastE,
	};
};
