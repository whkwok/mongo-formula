import { Expr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { findScope } from '../../util/scope';
import { sigA, sigU } from '../../util/sigs';
import { accumulatorTyp, numberTyp } from '../../util/typ';

export const avg = (v: any) => ({ $avg: v });

export const avgE = (e: Expr) => ({
	typ: numberTyp(),
	val: avg(e.val),
	dep: unionDep(e.dep),
});

export const avgGE = (e: Expr) => ({
	typ: accumulatorTyp(numberTyp()),
	val: avg(e.val),
	dep: unionDep(e.dep),
});

export const AVG = ({ scopeStack }: YY) => {
	try {
		const scope = findScope(scopeStack, 'STAGE');
		if (scope.stageKey === 'GROUP')
			return {
				argsSigs: [sigU],
				expr: avgGE,
			};
	} catch {}
	return {
		argsSigs: [sigA],
		expr: avgE,
	};
};
