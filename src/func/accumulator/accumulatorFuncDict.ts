import { AVG } from './AVG';
import { FIRST } from './FIRST';
import { LAST } from './LAST';
import { MAX } from './MAX';
import { MIN } from './MIN';
import { STDDEVPOP } from './STDDEVPOP';
import { STDDEVSAMP } from './STDDEVSAMP';
import { SUM } from './SUM';

export const accumulatorFuncDict = {
	AVG,
	FIRST,
	LAST,
	MAX,
	MIN,
	STDDEVPOP,
	STDDEVSAMP,
	SUM,
};
