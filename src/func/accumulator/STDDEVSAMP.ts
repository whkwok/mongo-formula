import { Expr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { findScope } from '../../util/scope';
import { sigA, sigU } from '../../util/sigs';
import { accumulatorTyp, numberTyp } from '../../util/typ';

export const stdDevSamp = (v: any) => ({ $stdDevSamp: v });

export const stdDevSampE = (e: Expr) => ({
	typ: numberTyp(),
	val: stdDevSamp(e.val),
	dep: unionDep(e.dep),
});

export const stdDevSampGE = (e: Expr) => ({
	typ: accumulatorTyp(numberTyp()),
	val: stdDevSamp(e.val),
	dep: unionDep(e.dep),
});

export const STDDEVSAMP = ({ scopeStack }: YY) => {
	try {
		const scope = findScope(scopeStack, 'STAGE');
		if (scope.stageKey === 'GROUP')
			return {
				argsSigs: [sigU],
				expr: stdDevSampGE,
			};
	} catch {}
	return {
		argsSigs: [sigA],
		expr: stdDevSampE,
	};
};
