import { ArrayExpr, AssignableExpr, Expr, YY } from '../../types';
import { unionDep } from '../../util/dep';
import { findScope } from '../../util/scope';
import { sigA, sigU } from '../../util/sigs';
import { accumulatorTyp, getItemsTyp, toNonLiteralTyp } from '../../util/typ';

export const first = (v: any) => ({ $first: v });

export const firstE = (e: AssignableExpr<ArrayExpr>) => ({
	typ: getItemsTyp(e.typ),
	val: first(e.val),
	dep: unionDep(e.dep),
});

export const firstGE = (e: Expr) => ({
	typ: accumulatorTyp(toNonLiteralTyp(e.typ)),
	val: first(e.val),
	dep: unionDep(e.dep),
});

export const FIRST = ({ scopeStack }: YY) => {
	try {
		const scope = findScope(scopeStack, 'STAGE');
		if (scope.stageKey === 'GROUP')
			return {
				expr: firstGE,
				argsSigs: [sigU],
			};
	} catch {}
	return {
		expr: firstE,
		argsSigs: [sigA],
	};
};
