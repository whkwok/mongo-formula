import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUU } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const gt = (v0: any, v1: any) => ({ $gt: [v0, v1] });

export const gtE = (e0: Expr, e1: Expr): Expr => ({
	typ: booleanTyp(),
	val: gt(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const GT = {
	argsSigs: [sigUU],
	expr: gtE,
};
