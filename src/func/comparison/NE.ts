import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUU } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const ne = (v0: any, v1: any) => ({ $ne: [v0, v1] });

export const neE = (e0: Expr, e1: Expr): Expr => ({
	typ: booleanTyp(),
	val: ne(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const NE = {
	argsSigs: [sigUU],
	expr: neE,
};
