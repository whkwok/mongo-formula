import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUU } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const gte = (v0: any, v1: any) => ({ $gte: [v0, v1] });

export const gteE = (e0: Expr, e1: Expr): Expr => ({
	typ: booleanTyp(),
	val: gte(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const GTE = {
	argsSigs: [sigUU],
	expr: gteE,
};
