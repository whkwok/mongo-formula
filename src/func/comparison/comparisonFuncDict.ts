import { CMP } from './CMP';
import { EQ } from './EQ';
import { GT } from './GT';
import { GTE } from './GTE';
import { LT } from './LT';
import { LTE } from './LTE';
import { NE } from './NE';

export const comparisonFuncDict = { CMP, EQ, GT, GTE, LT, LTE, NE };
