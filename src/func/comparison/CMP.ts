import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUU } from '../../util/sigs';
import { numberTyp } from '../../util/typ';

export const cmp = (v0: any, v1: any) => ({ $cmp: [v0, v1] });

export const cmpE = (e0: Expr, e1: Expr): Expr => ({
	typ: numberTyp(),
	val: cmp(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const CMP = {
	argsSigs: [sigUU],
	expr: cmpE,
};
