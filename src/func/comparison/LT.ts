import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUU } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const lt = (v0: any, v1: any) => ({ $lt: [v0, v1] });

export const ltE = (e0: Expr, e1: Expr): Expr => ({
	typ: booleanTyp(),
	val: lt(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const LT = {
	argsSigs: [sigUU],
	expr: ltE,
};
