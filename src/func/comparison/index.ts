export { CMP } from './CMP';
export { EQ } from './EQ';
export { GT } from './GT';
export { GTE } from './GTE';
export { LT } from './LT';
export { LTE } from './LTE';
export { NE } from './NE';
export { comparisonFuncDict } from './comparisonFuncDict';
