import { Expr } from '../../types';
import { unionDep } from '../../util/dep';
import { sigUU } from '../../util/sigs';
import { booleanTyp } from '../../util/typ';

export const lte = (v0: any, v1: any) => ({ $lte: [v0, v1] });

export const lteE = (e0: Expr, e1: Expr): Expr => ({
	typ: booleanTyp(),
	val: lte(e0.val, e1.val),
	dep: unionDep(e0.dep, e1.dep),
});

export const LTE = {
	argsSigs: [sigUU],
	expr: lteE,
};
