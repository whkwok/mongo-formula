#!/usr/bin/env node

import {
	Interface,
	clearScreenDown,
	createInterface,
	cursorTo,
} from 'readline';
import { inspect } from 'util';

import createDebug from 'debug';

import { BaseErr } from './errors';
import { allFuncDict } from './func';
import { MongoFormulaParser } from './MongoFormulaParser';
import { allStageDict } from './stage';
import { Schema } from './types';
import { isSchema } from './util/typ';

const debug = createDebug('mongo-formula:cli');
const mongonFormulaParser = new MongoFormulaParser(
	{},
	allFuncDict,
	allStageDict
);
const schemaMap = new WeakMap<Interface, Schema>();

const rl = createInterface({
	input: process.stdin,
	output: process.stdout,
	prompt: '> ',
});

rl.prompt();
rl.on('line', function (line) {
	try {
		const setSchemaCommand = line.match(/^setSchema\((.*)\)/);
		if (setSchemaCommand) {
			const result = mongonFormulaParser.parse(setSchemaCommand[1]);
			const s = result.val;
			if (!isSchema(s)) throw new Error('Invalid schema.');
			schemaMap.set(rl, s);
			console.log('setSchema:');
			console.log(inspect(schemaMap.get(rl), false, null, true));
		} else if (line == 'clear') {
			cursorTo(process.stdout, 0, 0);
			clearScreenDown(process.stdout);
		} else if (line == 'exit') {
			rl.close();
		} else {
			const { info, ...restParsed } = mongonFormulaParser.parse(
				line,
				schemaMap.get(rl)
			);
			console.log(inspect(restParsed, false, null, true));
			debug(info);
		}
	} catch (err) {
		console.log(err.message);
		if (err instanceof BaseErr) {
			const { details, info } = err;
			debug(details);
			debug(info);
		}
	}
	rl.prompt();
}).on('close', function () {
	process.exit(0);
});
