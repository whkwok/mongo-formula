import { Dep } from '../../types';
import { isLiteralArrayDep, isLiteralObjectDep } from './isDep';

export function unionDep(...deps: (Dep | undefined)[]): string[] {
	const seenDepSet = new Set<string>();
	const unionedDep: string[] = [];
	for (let i = 0, l = deps.length; i < l; ++i) {
		let dep = deps[i];
		if (dep === undefined) continue;
		if (isLiteralArrayDep(dep)) dep = unionDep(...dep);
		else if (isLiteralObjectDep(dep)) dep = unionDep(...Object.values(dep));
		for (const d of dep) {
			if (seenDepSet.has(d)) continue;
			unionedDep.push(d);
			seenDepSet.add(d);
		}
	}
	return unionedDep;
}
