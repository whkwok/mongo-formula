import { Dep, LiteralArrayDep, LiteralObjectDep } from '../../types';

const isString = (_d: unknown) => typeof _d === 'string';
const isNotString = (_d: unknown) => typeof _d !== 'string';

export function isDep(d: any): d is Dep {
	if (d instanceof Array) return d.every(isString) || d.every(isDep);
	if (typeof d === 'object') return Object.values(d).every(isDep);
	return false;
}

export function isReducedDep(d: Dep): d is string[] {
	return d instanceof Array && (d as unknown[]).every(isString);
}

export function isLiteralArrayDep(d: Dep): d is LiteralArrayDep {
	return d instanceof Array && (d as unknown[]).every(isNotString);
}

export function isLiteralObjectDep(d: Dep): d is LiteralObjectDep {
	return !(d instanceof Array);
}
