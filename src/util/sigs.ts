import { ArgsSig } from '../types';
import {
	arrayTyp,
	booleanTyp,
	dateTyp,
	numberTyp,
	objectTyp,
	stringTyp,
	unknownTyp,
} from './typ';

// U: unknown, B: boolean, D: date, N: number, S: string, A: array, O: object

export const sigN: ArgsSig = {
	typs: [numberTyp()],
	maxLen: 1,
	minLen: 1,
};
export const sigNN: ArgsSig = {
	typs: [numberTyp(), numberTyp()],
	maxLen: 2,
	minLen: 2,
};
export const sigNNo: ArgsSig = {
	typs: [numberTyp(), numberTyp()],
	maxLen: 2,
	minLen: 1,
};
export const sigNNNo: ArgsSig = {
	typs: [numberTyp(), numberTyp(), numberTyp()],
	maxLen: 3,
	minLen: 2,
};
export const sigNNs: ArgsSig = {
	typs: [numberTyp()],
	spreadTyp: numberTyp(),
	minLen: 1,
};
export const sigDN: ArgsSig = {
	typs: [dateTyp(), numberTyp()],
	maxLen: 2,
	minLen: 2,
};
export const sigDNs: ArgsSig = {
	typs: [dateTyp()],
	spreadTyp: numberTyp(),
	minLen: 1,
};
export const sigDD: ArgsSig = {
	typs: [dateTyp(), dateTyp()],
	maxLen: 2,
	minLen: 2,
};
export const sigA: ArgsSig = {
	typs: [arrayTyp(unknownTyp())],
	maxLen: 1,
	minLen: 1,
};
export const sigAA: ArgsSig = {
	typs: [arrayTyp(unknownTyp()), arrayTyp(unknownTyp())],
	maxLen: 2,
	minLen: 2,
};
export const sigAAs: ArgsSig = {
	typs: [arrayTyp(unknownTyp()), arrayTyp(unknownTyp())],
	spreadTyp: arrayTyp(unknownTyp()),
	minLen: 1,
};
export const sigAUNoNo: ArgsSig = {
	typs: [arrayTyp(unknownTyp()), unknownTyp(), numberTyp(), numberTyp()],
	maxLen: 4,
	minLen: 2,
};
export const sigAN: ArgsSig = {
	typs: [arrayTyp(unknownTyp()), numberTyp()],
	maxLen: 2,
	minLen: 2,
};
export const sigANNo: ArgsSig = {
	typs: [arrayTyp(unknownTyp()), numberTyp(), numberTyp()],
	maxLen: 3,
	minLen: 2,
};
export const sigU: ArgsSig = {
	typs: [unknownTyp()],
	maxLen: 1,
	minLen: 1,
};
export const sigUU: ArgsSig = {
	typs: [unknownTyp(), unknownTyp()],
	minLen: 2,
	maxLen: 2,
};
export const sigUA: ArgsSig = {
	typs: [unknownTyp(), arrayTyp(unknownTyp())],
	maxLen: 2,
	minLen: 2,
};
export const sigUS: ArgsSig = {
	typs: [unknownTyp(), stringTyp()],
	maxLen: 2,
	minLen: 2,
};
export const sigUUU: ArgsSig = {
	typs: [unknownTyp(), unknownTyp(), unknownTyp()],
	maxLen: 3,
	minLen: 3,
};
export const sigUUs: ArgsSig = {
	typs: [unknownTyp()],
	spreadTyp: unknownTyp(),
	minLen: 1,
};
export const sigS: ArgsSig = {
	typs: [stringTyp()],
	maxLen: 1,
	minLen: 1,
};
export const sigSSo: ArgsSig = {
	typs: [stringTyp(), stringTyp()],
	maxLen: 2,
	minLen: 1,
};
export const sigSSoSo: ArgsSig = {
	typs: [stringTyp(), stringTyp(), stringTyp()],
	maxLen: 3,
	minLen: 1,
};
export const sigSS: ArgsSig = {
	typs: [stringTyp(), stringTyp()],
	maxLen: 2,
	minLen: 2,
};
export const sigSSs: ArgsSig = {
	typs: [stringTyp()],
	spreadTyp: stringTyp(),
	minLen: 1,
};
export const sigSSNoNo: ArgsSig = {
	typs: [stringTyp(), stringTyp(), numberTyp(), numberTyp()],
	maxLen: 4,
	minLen: 2,
};
export const sigSN: ArgsSig = {
	typs: [stringTyp(), numberTyp()],
	maxLen: 2,
	minLen: 2,
};
export const sigSNNo: ArgsSig = {
	typs: [stringTyp(), numberTyp(), numberTyp()],
	maxLen: 3,
	minLen: 2,
};
export const sigSNN: ArgsSig = {
	typs: [stringTyp(), numberTyp(), numberTyp()],
	maxLen: 3,
	minLen: 3,
};
export const sigDSo: ArgsSig = {
	typs: [dateTyp(), stringTyp()],
	maxLen: 2,
	minLen: 1,
};
export const sigDSoSo: ArgsSig = {
	typs: [dateTyp(), stringTyp(), stringTyp()],
	maxLen: 3,
	minLen: 1,
};
export const sigO: ArgsSig = {
	typs: [objectTyp({})],
	maxLen: 1,
	minLen: 1,
};
export const sigOOs: ArgsSig = {
	typs: [objectTyp({})],
	spreadTyp: objectTyp({}),
	minLen: 1,
};
export const sigB: ArgsSig = {
	typs: [booleanTyp()],
	maxLen: 1,
	minLen: 1,
};
export const sigNL: ArgsSig = {
	typs: [numberTyp(true)],
	maxLen: 1,
	minLen: 1,
};
export const sigOL: ArgsSig = {
	typs: [objectTyp({}, undefined, undefined, true)],
	maxLen: 1,
	minLen: 1,
};

export const sigSL: ArgsSig = {
	typs: [stringTyp(true)],
	maxLen: 1,
	minLen: 1,
};

export const sigSLSLs: ArgsSig = {
	typs: [stringTyp(true)],
	spreadTyp: stringTyp(true),
	minLen: 1,
};
