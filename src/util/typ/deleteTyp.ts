import { Mutable, ObjectTyp } from '../../types';
import { isObjectTyp } from './isTyp';

export function deletePropertyTyp(typ: ObjectTyp, key: string): void {
	const dotIndex = key.indexOf('.');
	if (dotIndex !== -1) {
		const subKey = key.slice(0, dotIndex);
		const restKey = key.slice(dotIndex + 1);
		const subTyp = typ.properties[subKey];
		if (subTyp && isObjectTyp(subTyp)) deletePropertyTyp(subTyp, restKey);
	} else {
		delete typ.properties[key];
		const optional = typ.optional;
		if (optional) {
			const optionalKeyIndex = optional.indexOf(key);
			if (optionalKeyIndex !== -1) optional.splice(optionalKeyIndex, 1);
			if (optional.length === 0)
				delete (typ as Mutable<ObjectTyp>).optional;
		}
	}
}
