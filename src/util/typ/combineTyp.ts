import { Dictionary, NonLiteralTyp, Typ } from '../../types';
import { toNonLiteralTyp } from './convertTyp';
import {
	isAnyTyp,
	isArrayTyp,
	isNeverTyp,
	isObjectTyp,
	isUnknownTyp,
} from './isTyp';
import { anyTyp, arrayTyp, neverTyp, objectTyp, unknownTyp } from './typs';

// CombinedTyp will alway lose literalness
export function unionTyp(...ts: Typ[]): NonLiteralTyp {
	const _ts = ts.filter((t) => !isNeverTyp(t));
	if (_ts.length === 0) return neverTyp();
	if (_ts.some(isAnyTyp)) return anyTyp();
	if (_ts.every(isArrayTyp))
		return arrayTyp(unionTyp(..._ts.flatMap((t) => t.items)));
	if (_ts.every(isObjectTyp)) {
		const tsProps = _ts.map((t) => t.properties);
		const tsOptionalSet = _ts.map((t) => new Set(t.optional));
		const tsAdditionalProps = _ts
			.map((t) => t.additionalProperties)
			.filter((t): t is Typ => t !== undefined);
		// Property keys are the intersection of all typs
		const [t0Props, ...restTsProps] = tsProps;
		const propKeysSet = new Set(Object.keys(t0Props));
		restTsProps.forEach((p) => {
			const keysSet = new Set(Object.keys(p));
			for (const key of propKeysSet) {
				if (!keysSet.has(key)) propKeysSet.delete(key);
			}
		});
		const props: Dictionary<NonLiteralTyp> = {};
		const optional: string[] = [];
		for (const key of propKeysSet) {
			const tsPropTyp: Typ[] = [];
			let propOptional: boolean = false;
			tsProps.forEach((p, i) => {
				const propTyp = p[key]!; // it must be defined
				tsPropTyp.push(propTyp);
				propOptional ||= tsOptionalSet[i].has(key); // prop is optional only if it is optional in some typs containing it
			});
			props[key] = unionTyp(...tsPropTyp);
			if (propOptional) optional.push(key);
		}
		const additionalProps =
			tsAdditionalProps.length === 0
				? undefined
				: unionTyp(...tsAdditionalProps);
		return objectTyp(props, optional, additionalProps);
	}
	const t0 = _ts[0];
	if (_ts.every((t) => t.type === t0.type)) return toNonLiteralTyp(t0);
	return unknownTyp();
}

export function intersectTyp(...ts: Typ[]): NonLiteralTyp {
	const _ts = ts.filter((t) => !isUnknownTyp(t));
	if (_ts.length === 0) return unknownTyp();
	if (_ts.some(isAnyTyp)) return anyTyp();
	if (_ts.every(isArrayTyp))
		return arrayTyp(intersectTyp(..._ts.flatMap((t) => t.items)));
	if (_ts.every(isObjectTyp)) {
		const tsProps = _ts.map((t) => t.properties);
		const tsOptionalSet = _ts.map((t) => new Set(t.optional));
		const tsAdditionalProps = _ts
			.map((t) => t.additionalProperties)
			.filter((t): t is Typ => t !== undefined);
		// Property keys are the union of all typs
		const propKeysSet = new Set([
			...tsProps.flatMap((p) => Object.keys(p)),
		]);
		const props: Dictionary<NonLiteralTyp> = {};
		const optional: string[] = [];
		for (const key of propKeysSet) {
			const tsPropTyp: Typ[] = [];
			let propOptional: boolean = true;
			tsProps.forEach((p, i) => {
				const propTyp = p[key];
				if (propTyp) {
					tsPropTyp.push(propTyp);
					propOptional &&= tsOptionalSet[i].has(key); // prop is optional only if it is optional in all typs containing it
				}
			});
			props[key] = intersectTyp(...tsPropTyp);
			if (propOptional) optional.push(key);
		}
		const additionalProps =
			tsAdditionalProps.length === 0
				? undefined
				: intersectTyp(...tsAdditionalProps);
		return objectTyp(props, optional, additionalProps);
	}
	const t0 = _ts[0];
	if (_ts.every((t) => t.type === t0.type)) return toNonLiteralTyp(t0);
	return neverTyp();
}
