import { Dictionary, Typ } from '../../types';
import {
	arrayTyp,
	booleanTyp,
	dateTyp,
	neverTyp,
	numberTyp,
	objectTyp,
	stringTyp,
	unknownTyp,
} from './typs';

export const inferTyp = (v: unknown): Typ => {
	if (v === null) return neverTyp();
	if (typeof v === 'string') return stringTyp(true);
	if (typeof v === 'number') return numberTyp(true);
	if (typeof v === 'boolean') return booleanTyp(true);
	if (v instanceof Date) return dateTyp(true); // dateTyp only appears when using template.
	if (v instanceof Array) return arrayTyp(v.map(inferTyp), true);
	if (typeof v === 'object') {
		// Note that expr.val is object if formula contains any operators
		const properties: Dictionary<Typ> = {};
		Object.keys(v as Dictionary).forEach((key) => {
			properties[key] = inferTyp((v as Dictionary)[key]);
		});
		return objectTyp(properties, undefined, undefined, true);
	}
	return unknownTyp();
};
