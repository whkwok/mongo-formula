import {
	AccumulatorTyp,
	AnyTyp,
	BooleanTyp,
	CallbackTyp,
	DateTyp,
	Dictionary,
	LiteralArrayTyp,
	LiteralBooleanTyp,
	LiteralDateTyp,
	LiteralNumberTyp,
	LiteralObjectTyp,
	LiteralStringTyp,
	NeverTyp,
	NonLiteralArrayTyp,
	NonLiteralBooleanTyp,
	NonLiteralDateTyp,
	NonLiteralNumberTyp,
	NonLiteralObjectTyp,
	NonLiteralStringTyp,
	NonLiteralTyp,
	NumberTyp,
	PipelineTyp,
	StringTyp,
	Typ,
	UnknownTyp,
} from '../../types';

const _anyTyp: AnyTyp = { type: 'any' };
export function anyTyp(): AnyTyp {
	return _anyTyp;
}

const _neverTyp: NeverTyp = { type: 'never' };
export function neverTyp(): NeverTyp {
	return _neverTyp;
}

const _unknownTyp: UnknownTyp = { type: 'unknown' };
export function unknownTyp(): UnknownTyp {
	return _unknownTyp;
}

const _booleanTyp: NonLiteralBooleanTyp = { type: 'boolean' };
const _booleanLTyp: LiteralBooleanTyp = { type: 'boolean', literal: true };
export function booleanTyp(literal?: false): NonLiteralBooleanTyp;
export function booleanTyp(literal: true): LiteralBooleanTyp;
export function booleanTyp(literal?: boolean): BooleanTyp;
export function booleanTyp(literal: boolean = false): BooleanTyp {
	return literal ? _booleanLTyp : _booleanTyp;
}

const _dateTyp: NonLiteralDateTyp = { type: 'date' };
const _dateLTyp: LiteralDateTyp = { type: 'date', literal: true };
export function dateTyp(literal?: false): NonLiteralDateTyp;
export function dateTyp(literal: true): LiteralDateTyp;
export function dateTyp(literal?: boolean): DateTyp;
export function dateTyp(literal: boolean = false): DateTyp {
	return literal ? _dateLTyp : _dateTyp;
}

const _numberTyp: NonLiteralNumberTyp = { type: 'number' };
const _numberLTyp: LiteralNumberTyp = { type: 'number', literal: true };
export function numberTyp(literal?: false): NonLiteralNumberTyp;
export function numberTyp(literal: true): LiteralNumberTyp;
export function numberTyp(literal?: boolean): NumberTyp;
export function numberTyp(literal: boolean = false): NumberTyp {
	return literal ? _numberLTyp : _numberTyp;
}

const _stringTyp: NonLiteralStringTyp = { type: 'string' };
const _stringLTyp: LiteralStringTyp = { type: 'string', literal: true };
export function stringTyp(literal?: false): NonLiteralStringTyp;
export function stringTyp(literal: true): LiteralStringTyp;
export function stringTyp(literal?: boolean): StringTyp;
export function stringTyp(literal: boolean = false): StringTyp {
	return literal ? _stringLTyp : _stringTyp;
}

export function arrayTyp(
	items: NonLiteralTyp,
	literal?: false
): NonLiteralArrayTyp;
export function arrayTyp(items: Typ[], literal: true): LiteralArrayTyp;
export function arrayTyp(
	items: NonLiteralTyp | Typ[],
	literal: boolean = false
) {
	return literal
		? { type: 'array', items, literal: true }
		: { type: 'array', items };
}

export function objectTyp<P extends Dictionary<NonLiteralTyp>>(
	properties: P,
	optional?: { [K in keyof P]: K }[keyof P][],
	additionalProperties?: NonLiteralTyp,
	literal?: false
): NonLiteralObjectTyp;
export function objectTyp<P extends Dictionary<Typ>>(
	properties: P,
	optional: { [K in keyof P]: K }[keyof P][] | undefined,
	additionalProperties: Typ | undefined,
	literal: true
): LiteralObjectTyp;
export function objectTyp<P extends Dictionary<Typ>>(
	properties: P,
	optional: { [K in keyof P]: K }[keyof P][] = [],
	additionalProperties?: Typ | undefined,
	literal: boolean = false
) {
	const typ: Dictionary = { type: 'object', properties };
	if (optional.length) typ.optional = optional;
	if (additionalProperties) typ.additionalProperties = additionalProperties;
	if (literal === true) typ.literal = literal;
	return typ;
}

export function accumulatorTyp(_in: NonLiteralTyp): AccumulatorTyp {
	return { type: 'accumulator', in: _in };
}

export function callbackTyp(args: Typ[], _in: Typ): CallbackTyp {
	return { type: 'callback', args, in: _in };
}

const _pipelineTyp: PipelineTyp = { type: 'pipeline' };
export function pipelineTyp(): PipelineTyp {
	return _pipelineTyp;
}

const _datePartsTypProperties = {
	year: numberTyp(),
	month: numberTyp(),
	day: numberTyp(),
	hour: numberTyp(),
	minute: numberTyp(),
	second: numberTyp(),
	millisecond: numberTyp(),
};
export const datePartsTypProperties = () => _datePartsTypProperties;

const _isoDatePartsTypProperties = {
	isoWeekYear: numberTyp(),
	isoWeek: numberTyp(),
	isoDayOfWeek: numberTyp(),
	hour: numberTyp(),
	minute: numberTyp(),
	second: numberTyp(),
	millisecond: numberTyp(),
};
export const isoDatePartsTypProperties = () => _isoDatePartsTypProperties;
