import {
	ArrayTyp,
	AssignableTyp,
	NonLiteralTyp,
	ObjectTyp,
	Typ,
} from '../../types';
import { unionTyp } from './combineTyp';
import { anyTyp, unknownTyp } from './typs';

export function getItemsTyp(typ: AssignableTyp<ArrayTyp>): NonLiteralTyp {
	switch (typ.type) {
		case 'any':
			return anyTyp();
		case 'never':
			return unknownTyp();
		case 'array': {
			const _items = typ.items;
			return _items instanceof Array ? unionTyp(..._items) : _items;
		}
		default:
			return unknownTyp();
	}
}

export function getPropertyTyp(
	typ: AssignableTyp<ObjectTyp>,
	key: string
): Typ | undefined {
	switch (typ.type) {
		case 'any':
			return anyTyp();
		case 'never':
			return;
		case 'object': {
			const propTyp = typ.properties[key] ?? typ.additionalProperties;
			if (!propTyp) return;
			return propTyp;
		}
		default:
			return;
	}
}
