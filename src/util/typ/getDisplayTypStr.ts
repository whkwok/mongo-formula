import { Typ } from '../../types';
import {
	isAccumulatorTyp,
	isArrayTyp,
	isCallbackTyp,
	isObjectTyp,
} from './isTyp';

export function getDisplayTypStr(t: Typ): string {
	if (isAccumulatorTyp(t)) {
		const { in: _in } = t;
		const inStr = getDisplayTypStr(_in);
		return `accumulator(${inStr})`;
	}
	if (isCallbackTyp(t)) {
		const { args, in: _in } = t;
		const argsStr = args.map(getDisplayTypStr).join(', ');
		const inStr = getDisplayTypStr(_in);
		return `(${argsStr}) => ${inStr}`;
	}
	if (isArrayTyp(t))
		return t.items instanceof Array
			? `[${t.items.map(getDisplayTypStr).join(', ')}]`
			: `${getDisplayTypStr(t.items)}[]`;
	const literalStr = t.literal === true ? 'L' : '';
	if (isObjectTyp(t)) {
		const { properties, additionalProperties, optional } = t;
		const optionalSet = new Set(optional);
		const entryStrs: string[] = [];
		Object.keys(properties).forEach((k) => {
			entryStrs.push(
				`${k}${optionalSet.has(k) ? '?' : ''}: ${getDisplayTypStr(
					properties[k]!
				)}`
			);
		});
		if (additionalProperties)
			entryStrs.push(`+ ${getDisplayTypStr(additionalProperties)}`);
		const entryStr = entryStrs.join(', ');
		if (entryStr.length === 0) return '{}' + literalStr;
		return `{ ${entryStr} }` + literalStr;
	}
	return t.type + literalStr;
}
