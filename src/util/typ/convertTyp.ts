import {
	AccumulatorTyp,
	ArrayTyp,
	CallbackTyp,
	Dictionary,
	NonLiteralTyp,
	ObjectTyp,
	Typ,
} from '../../types';
import { unionTyp } from './combineTyp';
import {
	accumulatorTyp,
	anyTyp,
	arrayTyp,
	booleanTyp,
	callbackTyp,
	dateTyp,
	neverTyp,
	numberTyp,
	objectTyp,
	pipelineTyp,
	stringTyp,
	unknownTyp,
} from './typs';

export function toNonLiteralTyp<T extends Typ>(t: T): T & NonLiteralTyp {
	switch (t.type) {
		case 'any':
			return anyTyp();
		case 'boolean':
			return booleanTyp();
		case 'date':
			return dateTyp();
		case 'never':
			return neverTyp();
		case 'number':
			return numberTyp();
		case 'string':
			return stringTyp();
		case 'unknown':
			return unknownTyp();
		case 'array': {
			const _items = (t as ArrayTyp).items;
			return arrayTyp(
				_items instanceof Array ? unionTyp(..._items) : _items
			);
		}
		case 'object': {
			const {
				properties,
				optional,
				additionalProperties,
			} = t as ObjectTyp;
			const _properties: Dictionary<NonLiteralTyp> = {};
			let _additionalProperties: NonLiteralTyp | undefined;
			Object.keys(properties).forEach((key) => {
				_properties[key] = toNonLiteralTyp(properties[key]!);
			});
			if (additionalProperties)
				_additionalProperties = toNonLiteralTyp(additionalProperties);
			return objectTyp(_properties, optional, _additionalProperties);
		}
		case 'accumulator': {
			const { in: _in } = t as AccumulatorTyp;
			return accumulatorTyp(_in);
		}
		case 'callback': {
			const { args, in: _in } = t as CallbackTyp;
			return callbackTyp(args, _in);
		}
		case 'pipeline':
			return pipelineTyp();
		default:
			return unknownTyp();
	}
}
