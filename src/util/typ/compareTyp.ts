import { Typ } from '../../types';
import { unionTyp } from './combineTyp';
import {
	isAccumulatorTyp,
	isAnyTyp,
	isArrayTyp,
	isCallbackTyp,
	isNeverTyp,
	isObjectTyp,
	isUnknownTyp,
} from './isTyp';

export function isAssignableTyp(t0: Typ, t1: Typ): boolean {
	if (t0.literal !== true && t1.literal === true) return false; // Non-literal typ is not assignable to its corresponding literal typ.
	if (isAnyTyp(t0) || isAnyTyp(t1) || isNeverTyp(t0) || isUnknownTyp(t1))
		return true; // All typs is assignable to unknownTyp, neverTyp is assignable to all typs
	if (isAccumulatorTyp(t0) && isAccumulatorTyp(t1))
		return isAssignableTyp(t0.in, t1.in);
	if (isCallbackTyp(t0) && isCallbackTyp(t1)) {
		// if t0 is assignable to t1, then
		// t1 argTyp is assignable to t0 argTyp (contravariance)
		// t0 inTyp is assignable to t1 inTyp (covariance)
		const t0Args = t0.args;
		const t1Args = t1.args;
		return (
			t0Args.length >= t1Args.length &&
			t1Args.every((t, i) => isAssignableTyp(t, t0Args[i])) &&
			isAssignableTyp(t0.in, t1.in)
		);
	}
	if (isArrayTyp(t0) && isArrayTyp(t1)) {
		const t0Items = t0.items;
		const t1Items = t1.items;
		if (t1Items instanceof Array)
			return (
				t0Items instanceof Array &&
				t0Items.length <= t1Items.length &&
				t0Items.every((t, i) => isAssignableTyp(t, t1Items[i]))
			);
		if (t0Items instanceof Array)
			return isAssignableTyp(unionTyp(...t0Items), t1Items);
		return isAssignableTyp(t0Items, t1Items);
	}
	if (isObjectTyp(t0) && isObjectTyp(t1)) {
		// if t0 is assignable to t1, then
		// t0 propTyp should be assignable to t1 propTyp.
		// If t0 propTyp is undefined, t1 propTyp should be optional.
		// If t0 propTyp is optional, t1 propTyp must not be optional.
		const t0Props = t0.properties;
		const t1Props = t1.properties;
		const t0OptionalSet = new Set(t0.optional);
		const t1OptionalSet = new Set(t1.optional);
		const t0AdditionalProps = t0.additionalProperties;
		const t1AdditionalProps = t1.additionalProperties;
		const propKeys = [
			...new Set([...Object.keys(t0Props), ...Object.keys(t1Props)]),
		];
		return (
			propKeys.every((key) => {
				const t0PropTyp = t0Props[key];
				const t1PropTyp = t1Props[key];
				const t1PropOptional = t1OptionalSet.has(key);
				if (!t1PropTyp) {
					if (t1AdditionalProps)
						return isAssignableTyp(t0PropTyp!, t1AdditionalProps); // t0PropTyp must be defined when t1PropTyp is undefined
					return true;
				}
				if (!t0PropTyp) {
					if (t1PropOptional) {
						if (t0AdditionalProps)
							return isAssignableTyp(
								t0AdditionalProps,
								t1PropTyp
							);
						return true;
					}
					return false;
				}
				const t0PropOptional = t0OptionalSet.has(key);
				if (t0PropOptional && !t1PropOptional) return false;
				return isAssignableTyp(t0PropTyp, t1PropTyp);
			}) &&
			(!t1AdditionalProps ||
				!t0AdditionalProps ||
				isAssignableTyp(t0AdditionalProps, t1AdditionalProps))
		);
	}
	return t0.type === t1.type;
}
