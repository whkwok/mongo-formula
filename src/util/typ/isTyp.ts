import {
	AccumulatorTyp,
	AnyTyp,
	ArrayTyp,
	BooleanTyp,
	CallbackTyp,
	DateTyp,
	LiteralTyp,
	NeverTyp,
	NonLiteralTyp,
	NumberTyp,
	ObjectTyp,
	PipelineTyp,
	StringTyp,
	Typ,
	UnknownTyp,
} from '../../types';
import { isDictionary } from '../isDictionary';

export function isTyp(t: unknown): t is Typ {
	if (!isDictionary(t)) return false;
	const literal = t.literal;
	if (literal !== undefined && literal !== true) return false;
	const typeStr = t.type;
	if (typeStr === 'array') {
		const items = t.items;
		return (
			items &&
			(items instanceof Array && literal
				? items.every(isTyp)
				: isTyp(items))
		);
	}
	if (typeStr === 'object') {
		const properties = t.properties;
		const optional = t.optional;
		const additionalProperties = t.additionalProperties;
		return (
			isDictionary(properties) &&
			Object.values(properties).every(isTyp) &&
			(!optional ||
				(optional instanceof Array &&
					optional.every((k) => typeof k === 'string'))) &&
			(!additionalProperties || isTyp(additionalProperties))
		);
	}
	if (typeStr === 'accumulator') return isTyp(t.in);
	if (typeStr === 'callback')
		return t.args instanceof Array && t.args.every(isTyp) && isTyp(t.in);
	return (
		(typeStr === 'any' && !literal) ||
		(typeStr === 'never' && !literal) ||
		(typeStr === 'unknown' && !literal) ||
		typeStr === 'boolean' ||
		typeStr === 'date' ||
		typeStr === 'number' ||
		typeStr === 'string' ||
		typeStr === 'pipeline'
	);
}

export function isAnyTyp(t: Typ): t is AnyTyp {
	return t.type === 'any';
}

export function isBooleanTyp(t: Typ): t is BooleanTyp {
	return t.type === 'boolean';
}

export function isDateTyp(t: Typ): t is DateTyp {
	return t.type === 'date';
}

export function isNumberTyp(t: Typ): t is NumberTyp {
	return t.type === 'number';
}

export function isNeverTyp(t: Typ): t is NeverTyp {
	return t.type === 'never';
}

export function isStringTyp(t: Typ): t is StringTyp {
	return t.type === 'string';
}

export function isUnknownTyp(t: Typ): t is UnknownTyp {
	return t.type === 'unknown';
}

export function isArrayTyp(t: Typ): t is ArrayTyp {
	return t.type === 'array';
}

export function isObjectTyp(t: Typ): t is ObjectTyp {
	return t.type === 'object';
}

export function isAccumulatorTyp(t: Typ): t is AccumulatorTyp {
	return t.type === 'accumulator';
}

export function isCallbackTyp(t: Typ): t is CallbackTyp {
	return t.type === 'callback';
}

export function isPipelineTyp(t: Typ): t is PipelineTyp {
	return t.type === 'pipeline';
}

export function isLiteralTyp<T extends Typ>(t: T): t is T & LiteralTyp {
	return t.literal == true;
}

export function isNonLiteralTyp<T extends Typ>(t: T): t is T & NonLiteralTyp {
	return t.literal !== true;
}
