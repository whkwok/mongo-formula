import { ObjectTyp, Typ } from '../../types';
import { isObjectTyp } from './isTyp';
import { objectTyp } from './typs';

export function updatePropertyTyp(
	typ: ObjectTyp,
	key: string,
	propertyTyp: Typ | ((originalPropertyTyp?: Typ) => Typ | undefined)
): void {
	const dotIndex = key.indexOf('.');
	if (dotIndex !== -1) {
		const subKey = key.slice(0, dotIndex);
		const restKey = key.slice(dotIndex + 1);
		let subTyp = typ.properties[subKey];
		if (!subTyp) {
			subTyp = objectTyp({});
			typ.properties[subKey] = subTyp;
		}
		if (isObjectTyp(subTyp))
			updatePropertyTyp(subTyp, restKey, propertyTyp);
	} else {
		const newPropTyp =
			typeof propertyTyp === 'function'
				? propertyTyp(typ.properties[key])
				: propertyTyp;
		if (newPropTyp) typ.properties[key] = newPropTyp;
	}
}
