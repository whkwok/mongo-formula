import { Schema } from '../../types';
import { isDictionary } from '../isDictionary';
import { isLiteralTyp, isTyp } from './isTyp';

export function isSchema(s: unknown): s is Schema {
	if (!isDictionary(s)) return false;
	return Object.keys(s).every((key) => {
		const t = s[key];
		return isTyp(t) && !isLiteralTyp(t);
	});
}
