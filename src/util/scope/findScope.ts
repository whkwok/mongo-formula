import { ScopeErr } from '../../errors';
import { Scope } from '../../types';

export function findScope<N extends Scope['name']>(
	scopeStack: Scope[],
	scopeName: N,
	depth?: number
): Scope & { readonly name: N } {
	const scopeStackLen = scopeStack.length;
	depth ??= scopeStackLen;
	for (let i = 1; i <= depth; ++i) {
		const scope = scopeStack[scopeStackLen - i];
		if (scope.name === scopeName)
			return scope as Scope & { readonly name: N };
	}
	throw new ScopeErr({
		expectedScopeName: scopeName,
		reason: 'Scope is not found.',
	});
}
