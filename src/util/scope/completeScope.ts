import {
	ArrayScope,
	Callback,
	CallbackScope,
	Expr,
	FuncScope,
	LetScope,
	ObjectScope,
	Pipeline,
	StageScope,
	VarDict,
	YY,
} from '../../types';
import { checkArgsLen } from '../arg';
import { unionDep } from '../dep';
import { callFunc } from '../func';
import { toNonLiteralTyp } from '../typ';
import { _let } from '../var';

export function completeArrayScope(
	scope: ArrayScope,
	concatArraysFuncKey: string,
	yy: YY
): Expr {
	const { literalExpr, combiningExprs } = scope;
	if (combiningExprs.length) {
		const args = literalExpr.typ.items
			? [...combiningExprs, literalExpr]
			: [...combiningExprs];
		return callFunc(concatArraysFuncKey, args, yy);
	}
	return literalExpr;
}

export function completeObjectScope(
	scope: ObjectScope,
	mergeObjectsFuncKey: string,
	yy: YY
): Expr {
	const { literalExpr, combiningExprs } = scope;
	if (combiningExprs.length) {
		const args = literalExpr.typ.properties
			? [...combiningExprs, literalExpr]
			: [...combiningExprs];
		return callFunc(mergeObjectsFuncKey, args, yy);
	}
	return literalExpr;
}

export function completeLetScope(scope: LetScope, inExpr: Expr): Expr {
	const { varDict, vars } = scope;
	return {
		typ: toNonLiteralTyp(inExpr.typ),
		val: _let(vars, inExpr.val),
		dep: unionDep(
			...Object.values(varDict).map((_var) => _var!.dep),
			inExpr.dep
		),
	};
}

export function completeFuncScope(
	scope: FuncScope,
	callOnFuncEnd: boolean = true
): Expr {
	const { func, funcKey, args, argsSigs } = scope;
	const { expr, onFuncEnd } = func;
	checkArgsLen(funcKey, args, argsSigs);
	if (callOnFuncEnd) onFuncEnd?.(scope);
	return expr(...args);
}

export function completeStageScope(
	scope: StageScope,
	rootVarDict: VarDict,
	callOnStageEnd: boolean = true
): [Pipeline, VarDict] {
	const { stageKey, stage, args, argsSigs } = scope;
	const { pipeline, varDict = (vD) => vD, onStageEnd } = stage;
	checkArgsLen(stageKey, args, argsSigs);
	if (callOnStageEnd) onStageEnd?.(scope);
	return [pipeline(...args), varDict(rootVarDict, ...args)];
}

export function completeCallbackScope(
	scope: CallbackScope,
	inExpr: Expr
): Callback {
	const { argTyps, argKeys } = scope;
	return {
		typ: { type: 'callback', args: argTyps, in: inExpr.typ },
		val: { argKeys, in: inExpr.val },
		dep: inExpr.dep,
	};
}
