import {
	ArrayScope,
	CallbackScope,
	Dep,
	FuncScope,
	LetScope,
	Loc,
	ObjectScope,
	RootScope,
	Schema,
	StageScope,
	TemplateScope,
	TemplateVarSpec,
	Typ,
	VarDict,
	YY,
} from '../../types';
import { findFunc } from '../func';
import { findStage } from '../stage';
import { arrayTyp, objectTyp } from '../typ';

export function createRootScope(schema: Schema): RootScope {
	const varDict: VarDict = {};
	Object.keys(schema).forEach((k) => {
		varDict[k] = { typ: schema[k]!, val: '$' + k, dep: [k] };
	});
	return { name: 'ROOT', varDict };
}

export function createArrayScope(): ArrayScope {
	return {
		name: 'ARRAY',
		literalExpr: {
			typ: arrayTyp([], true),
			val: [],
			dep: [],
		},
		combiningExprs: [],
	};
}

export function createObjectScope(): ObjectScope {
	return {
		name: 'OBJECT',
		literalExpr: {
			typ: objectTyp({}, undefined, undefined, true),
			val: {},
			dep: {},
		},
		combiningExprs: [],
	};
}

export function createLetScope(): LetScope {
	return {
		name: 'LET',
		varDict: {},
		vars: {},
		assignCompleted: false,
	};
}

export function createFuncScope(
	funcKey: string,
	funcKeyLoc: Loc | undefined,
	yy: YY
): FuncScope {
	funcKey = funcKey.toUpperCase(); // All func keys are in upper case.
	const func = findFunc(funcKey, funcKeyLoc, yy);
	return {
		name: 'FUNC',
		funcKey,
		func,
		args: [],
		argsSigs: func.argsSigs,
	};
}

export function createCallbackScope(
	funcKey: string,
	argTyps: Typ[]
): CallbackScope {
	return {
		name: 'CALLBACK',
		funcKey,
		varDict: {},
		argTyps,
		argKeys: [],
	};
}

export function createStageScope(
	stageKey: string,
	stageKeyLoc: Loc | undefined,
	yy: YY
): StageScope {
	stageKey = stageKey.toUpperCase(); // All stage keys are in upper case.
	const stage = findStage(stageKey, stageKeyLoc, yy);
	return {
		name: 'STAGE',
		stageKey,
		stage,
		args: [],
		argsSigs: stage.argsSigs,
	};
}

export function createTemplateScope(specs: TemplateVarSpec[]): TemplateScope {
	const varDict: VarDict = {};
	specs.forEach((spec, i) => {
		let key: string, typ: Typ, dep: Dep;
		if ('typ' in spec) {
			typ = spec.typ;
			key = spec.key ?? `template_var${i}`;
			dep = spec.dep ?? [];
		} else {
			typ = spec;
			key = `template_var${i}`;
			dep = [];
		}
		varDict[key] = { typ, val: {}, dep };
	});
	return { name: 'TEMPLATE', varDict };
}
