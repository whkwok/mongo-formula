import { Dictionary } from '../../types';
import { isDictionary } from '../isDictionary';

export function isBooleanVal(v: unknown): v is boolean {
	return typeof v === 'boolean';
}

export function isNumberVal(v: unknown): v is number {
	return typeof v === 'number';
}

export function isNullVal(v: unknown): v is null {
	return v === null;
}

export function isDateVal(v: unknown): v is Date {
	return v instanceof Date;
}

export function isStringVal(
	v: unknown,
	allowVarVal: boolean = false
): v is string {
	return typeof v === 'string' && (allowVarVal || v.indexOf('$') !== 0);
}

export function isArrayVal(v: unknown): v is unknown[] {
	return v instanceof Array;
}

export function isObjectVal(v: unknown): v is Dictionary {
	return (
		isDictionary(v) &&
		!(v instanceof Array) &&
		Object.keys(v).every((k) => k.indexOf('$') !== 0)
	);
}

export function isOpVal<K extends string>(
	v: unknown,
	opKey: K
): v is Record<K, unknown> {
	return (
		typeof v === 'object' && Object.prototype.hasOwnProperty.call(v, opKey)
	);
}

export function isVarVal(v: any): v is string {
	return typeof v === 'string' && v.indexOf('$') === 0;
}
