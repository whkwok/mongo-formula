import { Dictionary } from '../../types';

export function _let(vars: Dictionary, _in: any) {
	return { $let: { vars, in: _in } };
}
