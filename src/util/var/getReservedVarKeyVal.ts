export function getReservedVarKeyVal(
	stem: string,
	suffix: string,
	prefix: string = '$$'
): [key: string, val: string] {
	const key = `${stem}${suffix}`;
	const val = prefix + key;
	return [key, val];
}
