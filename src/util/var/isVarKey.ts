const varKeyRegex = /^[a-z]\w*$/;

export function isVarKey(varKey: string, reservedSuffix: string): boolean {
	return varKeyRegex.test(varKey) && !varKey.endsWith(reservedSuffix);
}
