import { Dictionary } from '../types';

export function isDictionary(v: unknown): v is Dictionary<any> {
	return typeof v === 'object' && v !== null;
}
