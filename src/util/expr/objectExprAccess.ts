import {
	AssignableExpr,
	Dictionary,
	Expr,
	LiteralObjectExpr,
	ObjectExpr,
} from '../../types';
import { getPropertyTyp, toNonLiteralTyp } from '../typ';
import { isOpVal, isVarVal } from '../val';
import { getReservedVarKeyVal } from '../var';
import { isLiteralExpr } from './isExpr';

export function literalObjectExprAccess(
	expr: LiteralObjectExpr,
	key: string
): Expr | undefined {
	const typ = expr.typ.properties[key] ?? expr.typ.additionalProperties;
	if (!typ) return;
	return {
		typ,
		val: expr.val[key],
		dep: expr.dep[key] ?? [],
	};
}

export function objectExprAccess(
	expr: AssignableExpr<ObjectExpr>,
	key: string,
	reservedSuffix: string
): Expr | undefined {
	const typ = getPropertyTyp(expr.typ, key);
	if (!typ) return;
	if (isLiteralExpr(expr))
		return { typ, val: expr.val[key], dep: expr.dep[key] ?? [] };
	const [oVK, oVV] = getReservedVarKeyVal('o', reservedSuffix);
	if (isVarVal(expr.val))
		return { typ, val: `${expr.val}.${key}`, dep: expr.dep };
	if (isOpVal(expr.val, '$let')) {
		const { vars: _vars, in: _in } = expr.val.$let as Dictionary;
		if (
			_vars[oVK] !== undefined &&
			typeof _in === 'string' &&
			_in.indexOf(oVV) === 0
		)
			return {
				typ: toNonLiteralTyp(typ),
				val: { $let: { vars: _vars, in: `${_in}.${key}` } },
				dep: expr.dep,
			};
	}
	return {
		typ: toNonLiteralTyp(typ),
		val: { $let: { vars: { [oVK]: expr.val }, in: `${oVV}.${key}` } },
		dep: expr.dep,
	};
}
