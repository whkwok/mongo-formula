import {
	AnyExpr,
	ArrayExpr,
	BooleanExpr,
	DateExpr,
	Expr,
	ExtendedExpr,
	LiteralExpr,
	NeverExpr,
	NumberExpr,
	ObjectExpr,
	StringExpr,
	UnknownExpr,
} from '../../types';
import { isDep } from '../dep';
import {
	isAnyTyp,
	isArrayTyp,
	isBooleanTyp,
	isDateTyp,
	isLiteralTyp,
	isNeverTyp,
	isNumberTyp,
	isObjectTyp,
	isStringTyp,
	isTyp,
	isUnknownTyp,
} from '../typ';

export function isExpr(e: any): e is Expr {
	if (typeof e !== 'object') return false;
	return (
		isTyp(e.typ) &&
		isDep(e.dep) &&
		Object.prototype.hasOwnProperty.call(e, 'val')
	);
}

export function isBooleanExpr(e: ExtendedExpr): e is BooleanExpr {
	return isBooleanTyp(e.typ);
}

export function isAnyExpr(e: ExtendedExpr): e is AnyExpr {
	return isAnyTyp(e.typ);
}

export function isNeverExpr(e: ExtendedExpr): e is NeverExpr {
	return isNeverTyp(e.typ);
}

export function isUnknownExpr(e: ExtendedExpr): e is UnknownExpr {
	return isUnknownTyp(e.typ);
}

export function isDateExpr(e: ExtendedExpr): e is DateExpr {
	return isDateTyp(e.typ);
}

export function isNumberExpr(e: ExtendedExpr): e is NumberExpr {
	return isNumberTyp(e.typ);
}

export function isStringExpr(e: ExtendedExpr): e is StringExpr {
	return isStringTyp(e.typ);
}

export function isArrayExpr(e: ExtendedExpr): e is ArrayExpr {
	return isArrayTyp(e.typ);
}

export function isObjectExpr(e: ExtendedExpr): e is ObjectExpr {
	return isObjectTyp(e.typ);
}

export function isLiteralExpr<E extends ExtendedExpr>(
	e: E
): e is E & LiteralExpr {
	return isLiteralTyp(e.typ);
}
