import { Expr, ExtendedExprLoc, YY } from '../../types';
import { checkArgsLen, matchArgsSigs } from '../arg';
import { findFunc } from './getAndCheckFunc';

export function callFunc(
	funcKey: string,
	args: ExtendedExprLoc[],
	yy: YY
): Expr {
	const func = findFunc(funcKey, undefined, yy);
	const { expr } = func;
	let argsSigs = func.argsSigs;
	for (let i = 1, l = args.length; i <= l; ++i) {
		const _args = args.slice(0, i);
		argsSigs = matchArgsSigs(funcKey, _args, argsSigs);
	}
	checkArgsLen(funcKey, args, argsSigs);
	return expr(...args);
}
