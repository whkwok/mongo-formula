import { KeyErr } from '../../errors';
import { Func, Loc, YY } from '../../types';

export function findFunc(
	funcKey: string,
	funcKeyLoc: Loc | undefined,
	yy: YY
): Func {
	const func = yy.funcDict[funcKey];
	if (!func)
		throw new KeyErr({
			key: funcKey,
			keyType: 'func',
			loc: funcKeyLoc,
			reason: 'Function is not found.',
		});
	return typeof func === 'function' ? func(yy) : func;
}
