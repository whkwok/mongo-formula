import { KeyErr } from '../../errors';
import { Loc, Stage, YY } from '../../types';

export function findStage(
	stageKey: string,
	stageKeyLoc: Loc | undefined,
	yy: YY
): Stage {
	const stage = yy.stageDict[stageKey];
	if (!stage)
		throw new KeyErr({
			key: stageKey,
			keyType: 'stage',
			loc: stageKeyLoc,
			reason: 'Stage is not found.',
		});
	return typeof stage === 'function' ? stage(yy) : stage;
}
