import { ExtendedExprLoc, Pipeline, VarDict, YY } from '../../types';
import { checkArgsLen, matchArgsSigs } from '../arg';
import { findStage } from './getAndCheckStage';

export function callStage(
	stageKey: string,
	rootVarDict: VarDict,
	args: ExtendedExprLoc[],
	yy: YY
): [Pipeline, VarDict] {
	const stage = findStage(stageKey, undefined, yy);
	const { pipeline, varDict = (vD) => vD } = stage;
	let argsSigs = stage.argsSigs;
	for (let i = 1, l = args.length; i <= l; ++i) {
		const _args = args.slice(0, i);
		argsSigs = matchArgsSigs(stageKey, _args, argsSigs);
	}
	checkArgsLen(stageKey, args, argsSigs);
	return [pipeline(...args), varDict(rootVarDict, ...args)];
}
