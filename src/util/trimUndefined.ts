import { Dictionary } from '../types';

export function trimUndefined<T extends any[] | Dictionary>(v: T): T {
	if (v instanceof Array) {
		const undefinedIndex = v.indexOf(undefined);
		if (undefinedIndex !== -1)
			v.splice(undefinedIndex, v.length - undefinedIndex);
	} else if (typeof v === 'object') {
		Object.keys(v).forEach((k) => {
			if ((v as Dictionary)[k] === undefined) delete (v as Dictionary)[k];
		});
	}
	return v;
}
