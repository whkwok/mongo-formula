// Deepclone object enumerable properties and array
export function deepClone(v: any): any {
	if (v instanceof Array) return v.map((_v) => deepClone(_v));
	if (typeof v === 'object' && v) {
		const _v: any = {};
		Object.keys(v).forEach((k) => {
			_v[k] = deepClone(v[k]);
		});
		return _v;
	}
	return v;
}
