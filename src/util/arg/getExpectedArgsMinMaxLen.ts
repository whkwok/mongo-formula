import { ArgsSig } from '../../types';

export function getExpectedArgsMinMaxLen(
	sigs: ArgsSig[]
): [min: number, max: number] {
	let _minLen = Infinity;
	let _maxLen = 0;
	for (let i = 0, l = sigs.length; i < l; ++i) {
		const { minLen = 0, maxLen = Infinity } = sigs[i];
		if (_minLen > minLen) _minLen = minLen;
		if (_maxLen < maxLen) _maxLen = maxLen;
	}
	return [_minLen, _maxLen];
}
