import { ArgsLenErr } from '../../errors';
import { ArgsSig, ExtendedExprLoc } from '../../types';
import { combineLoc } from '../loc';
import { getExpectedArgsMinMaxLen } from './getExpectedArgsMinMaxLen';

export function checkArgsLen(
	context: string,
	args: ExtendedExprLoc[],
	argsSigs: ArgsSig[]
): void {
	const argsLen = args.length;
	const [expectedMinLen, expectedMaxLen] = getExpectedArgsMinMaxLen(argsSigs);
	if (argsLen < expectedMinLen || argsLen > expectedMaxLen)
		throw new ArgsLenErr({
			context,
			receivedLen: argsLen,
			expectedMinLen,
			expectedMaxLen,
			loc: combineLoc(...args.map((a) => a.loc)),
		});
}
