import { ArgsSig, ExtendedExpr, Typ } from '../../types';
import { neverTyp } from '../typ';

export function getExpectedArgTyp(
	args: ExtendedExpr[],
	argsSig: ArgsSig,
	index?: number
): Typ {
	index ??= args.length - 1;
	const { typs, spreadTyp = neverTyp() } = argsSig;
	const typ = typs[index] ?? spreadTyp;
	return typeof typ === 'function' ? typ(...args) : typ;
}
