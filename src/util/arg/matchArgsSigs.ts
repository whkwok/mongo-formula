import { ExprTypErr } from '../../errors';
import { ArgsSig, ExtendedExprLoc, Typ } from '../../types';
import { isAssignableTyp } from '../typ';
import { getExpectedArgTyp } from './getExpectedArgTyp';

export function matchArgsSigs(
	context: string,
	args: ExtendedExprLoc[],
	argsSigs: ArgsSig[]
): ArgsSig[] {
	const index = args.length - 1;
	const arg = args[index];
	const argTyp = arg?.typ;
	const matchingArgsSigs: ArgsSig[] = [];
	const expectedArgTyps: Typ[] = [];
	for (let i = 0, l = argsSigs.length; i < l; ++i) {
		const argsSig = argsSigs[i];
		const expectedArgTyp = getExpectedArgTyp(args, argsSig);
		expectedArgTyps.push(expectedArgTyp);
		if (argTyp && isAssignableTyp(argTyp, expectedArgTyp))
			matchingArgsSigs.push(argsSig);
	}
	if (matchingArgsSigs.length === 0)
		throw new ExprTypErr({
			context,
			index,
			receivedTyp: argTyp,
			expectedTyps: expectedArgTyps,
			loc: arg.loc,
		});
	return matchingArgsSigs;
}
