import { Loc } from '../../types';

// Assume locs is sorted in ascending order.
export function combineLoc(...locs: (Loc | undefined)[]): Loc | undefined {
	const _locs = locs.filter((loc): loc is Loc => loc !== undefined);
	if (_locs.length === 0) return undefined;
	const firstLoc = _locs[0];
	const lastLoc = _locs[_locs.length - 1];
	return [firstLoc[0], lastLoc[1]];
}
