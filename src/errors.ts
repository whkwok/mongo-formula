import { Loc, ParseInfo, TemplateVarSpec, Typ } from './types';
import { getDisplayTypStr } from './util/typ';

export interface LexErrDetails {
	readonly pos: number;
	readonly char: string;
}
export interface ParseErrDetails {
	readonly receivedToken: string;
	readonly expectedTokens: string[];
}
export interface KeyErrDetails {
	readonly key: string;
	readonly keyType: 'var' | 'object' | 'func' | 'stage';
	readonly loc?: Loc;
	readonly reason?: string;
}
export interface ScopeErrDetails {
	readonly expectedScopeName?: string;
	readonly reason?: string;
}
export interface ExprTypErrDetails {
	readonly context?: string;
	readonly index?: number;
	readonly receivedTyp: Typ;
	readonly expectedTyps?: Typ[];
	readonly loc?: Loc;
	readonly reason?: string;
}
export interface ExprValErrDetails {
	readonly context?: string;
	readonly index?: number;
	readonly receivedVal: any;
	readonly loc?: Loc;
	readonly reason?: string;
}
export interface ArgsLenErrDetails {
	readonly context?: string;
	readonly receivedLen: number;
	readonly expectedMinLen: number;
	readonly expectedMaxLen: number;
	readonly loc?: Loc;
	readonly reason?: string;
}
export interface OverloadErrDetails {
	readonly context?: string;
	readonly receivedTyps: Typ[];
	readonly loc?: Loc;
	readonly reason?: string;
}
export interface TemplateErrDetails {
	readonly specs: TemplateVarSpec[];
	readonly formulaParts: TemplateStringsArray;
	readonly reason?: string;
}

export class BaseErr<T> extends Error {
	details: T;
	info?: ParseInfo;

	constructor(message: string, details: T) {
		super();
		this.name = this.constructor.name;
		this.message = message;
		this.details = details;
	}
}

function getUnexpectedErrMessage(details: any) {
	if (typeof details === 'object' && details.message) return details.message;
	return 'Unexpected error occurs.';
}
export class UnexpectedErr extends BaseErr<any> {
	constructor(details: any) {
		const message = getUnexpectedErrMessage(details);
		super(message, details);
	}
}

function getLexErrMessage(details: LexErrDetails): string {
	const { pos, char } = details;
	return `No recognized token for "${char}" at position ${pos}.`;
}
export class LexErr extends BaseErr<LexErrDetails> {
	constructor(details: LexErrDetails) {
		const message = getLexErrMessage(details);
		super(message, details);
	}
}

function getParseErrMessage(details: ParseErrDetails): string {
	const { receivedToken, expectedTokens } = details;
	const expectedTokensStr = expectedTokens
		.map((token) => `"${token}"`)
		.join(', ');
	return `Unexpected token "${receivedToken}". Expected ${expectedTokensStr}.`;
}
export class ParseErr extends BaseErr<ParseErrDetails> {
	constructor(details: ParseErrDetails) {
		const message = getParseErrMessage(details);
		super(message, details);
	}
}

function getKeyErrMessage(details: KeyErrDetails): string {
	const { key, keyType, reason } = details;
	let message = `Invalid ${keyType} key "${key}".`;
	if (reason) message += ` ${reason}`;
	return message;
}
export class KeyErr extends BaseErr<KeyErrDetails> {
	constructor(details: KeyErrDetails) {
		const message = getKeyErrMessage(details);
		super(message, details);
	}
}

function getScopeErrMessage(details: ScopeErrDetails): string {
	const { reason } = details;
	let message = `Invalid scope.`;
	if (reason) message += ` ${reason}`;
	return message;
}
export class ScopeErr extends BaseErr<ScopeErrDetails> {
	constructor(details: ScopeErrDetails) {
		const message = getScopeErrMessage(details);
		super(message, details);
	}
}

function getExprTypErrMessage(details: ExprTypErrDetails): string {
	const { context, index, receivedTyp, expectedTyps, reason } = details;
	let message = `Invalid expr${index ?? ''} typ.`;
	const receivedTypStr = `"${getDisplayTypStr(receivedTyp)}"`;
	message += ` Received ${receivedTypStr}.`;
	if (expectedTyps) {
		const expectedTypsStr = expectedTyps
			.map((typ) => `"${getDisplayTypStr(typ)}"`)
			.join(' or ');
		message += ` Expected ${expectedTypsStr}.`;
	}
	if (context) message = `${context}: ` + message;
	if (reason) message += ` ${reason}`;
	return message;
}
export class ExprTypErr extends BaseErr<ExprTypErrDetails> {
	constructor(details: ExprTypErrDetails) {
		const message = getExprTypErrMessage(details);
		super(message, details);
	}
}

function getExprValErrMessage(details: ExprValErrDetails): string {
	const { context, index, reason } = details;
	let message = `Invalid expr${index ?? ''} val.`;
	if (context) message = `${context}: ` + message;
	if (reason) message += ` ${reason}`;
	return message;
}
export class ExprValErr extends BaseErr<ExprValErrDetails> {
	constructor(details: ExprValErrDetails) {
		const message = getExprValErrMessage(details);
		super(message, details);
	}
}

function getArgsLenErrMessage(details: ArgsLenErrDetails): string {
	const {
		context,
		receivedLen,
		expectedMinLen,
		expectedMaxLen,
		reason,
	} = details;
	const expectedLenStr =
		expectedMinLen === expectedMaxLen
			? expectedMinLen
			: `${expectedMinLen} to ${expectedMaxLen}`;
	let message = `Invalid len of args. Received ${receivedLen}. Expected ${expectedLenStr}.`;
	if (context) message = `${context}: ` + message;
	if (reason) message += ` ${reason}`;
	return message;
}
export class ArgsLenErr extends BaseErr<ArgsLenErrDetails> {
	constructor(details: ArgsLenErrDetails) {
		const message = getArgsLenErrMessage(details);
		super(message, details);
	}
}

function getOverloadErrMessage(details: OverloadErrDetails): string {
	const { context, receivedTyps, reason } = details;
	const receivedTypsStr =
		'"(' + receivedTyps.map(getDisplayTypStr).join(', ') + ')"';
	let message = `Cannot determine which function to use. Received ${receivedTypsStr}.`;
	if (context) message = `${context}: ` + message;
	if (reason) message += ` ${reason}`;
	return message;
}
export class OverloadErr extends BaseErr<OverloadErrDetails> {
	constructor(details: OverloadErrDetails) {
		const message = getOverloadErrMessage(details);
		super(message, details);
	}
}

function getTemplateErrMessage(details: TemplateErrDetails): string {
	const { reason } = details;
	let message = 'Invalid template.';
	if (reason) message += ` ${reason}`;
	return message;
}
export class TemplateErr extends BaseErr<TemplateErrDetails> {
	constructor(details: TemplateErrDetails) {
		const message = getTemplateErrMessage(details);
		super(message, details);
	}
}
