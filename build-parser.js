/* 
	Workaround: Jison doesn't support ESM
	1. Build parser with js moduleType option in jison first
	2. Append CJS export statement in the end.
	3. Use babel-plugin-transform-commonjs to transform the jison output to ESM (Refer to babel.esm.config.js)

	Why not commonjs options?
	Output generated with commonjs modulteType option requires 'fs' and 'path' modules to be executed directly as main.
	This makes output incompatible with browser.
*/
const fs = require('fs');
const jison = require('jison');
const parserGrammar = fs.readFileSync('src/parser/Parser.jison', 'utf8');
const generator = new jison.Generator(parserGrammar);
const parserCode = generator.generate({ moduleType: 'js' });
fs.writeFileSync(
	'src/parser/Parser.js',
	parserCode + '\nexports.Parser = parser.Parser'
);
